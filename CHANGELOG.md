# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

Next release will be a major upgrade (v2.0.0), and DMTMService renamed to GeneralService.

### Added

- Add DMTM Library
- [CHANGELOG.md](CHANGELOG.md)
- [DEVELOPMENT.md](DEVELOPMENT.md) for plugin development guide
- Add excel helper
- Add `GetValue<T>` for getting PluginSettings value
- Add more Sample plugin project
- Add GenerateNewBusiness File Plugin
- Add Decryptor Plugin
- Add MailService & set MailHelper as obsolete
- Add MailService.Smtp plugin

### Changed

- Rename DMTMService to GeneralService
- Update .net version
- Update System.ValueTuple package
- Set GetValue to obsolete
- Use AppContext.BaseDirectory when parsing Plugin
- Modify Plugin Structure



## [1.3.1](https://gitlab.com/iteam-amfs/generalservice_amfs/-/tags/1.3.1) - 2021-01-06
### Added
- [README.md](README.md)
- README for AccountingService

  


## [1.3.0](https://gitlab.com/iteam-amfs/generalservice_amfs/-/tags/1.2.1) - 2020-12-01

### Added

- [AccountingReport] Set executionTimeout to 10 minutes

### Changed

- Include Bin/*.dll.refresh file on .gitignore



## [1.2.1](https://gitlab.com/iteam-amfs/generalservice_amfs/-/tags/1.2.1) - 2020-11-20

### Fixed

- Fix error notification when journal is empty (usually on weekend)



## [1.1.0](https://gitlab.com/iteam-amfs/dmtm-amfs/-/tags/v2.0.0-rc.1) -  2020-11-03

### Added

- [AccountingReport] Add GeneratedReportTypes as new PluginSettings value, only generate flat file for some report types based on settings

### Changed

- Use semantic versioning for tagging

### Fixed

- [Core] Failure when sending email using MailHelper



## [v1.0.0.3](https://gitlab.com/iteam-amfs/generalservice_amfs/-/tags/v1.0.0.3) - 2020-10-22

### Added

- [Core] NLog package for logging
- [Core] Helper/DmtmServiceSettings for reading app.config configuration
- [Core] Helper/LogHelper for logging using NLogger
- [Core] Helper/MailHelper for sending email
- [Core] Helper/SqlHelper
- [AccountingReport] Add Mail Notification
- [AccountingReport] Enable/Disable updating sequence number via PluginSettings

### Changed

- [AccountingReport] Replace hardcoded transdate with real transdate from Journal

  

## [v1.0.0.2](https://gitlab.com/iteam-amfs/generalservice_amfs/-/tags/v1.0.0.2) - 2020-08-14

### Fixed

- [AccountingReport] Fix comments pattern/format



## [v1.0.0.1](https://gitlab.com/iteam-amfs/generalservice_amfs/-/tags/v1.0.0.1) - 2020-08-10

### Fixed

- [AccountingReport] Fix sequence on comments is static. It should be auto increment



## [v1.0.0.0](https://gitlab.com/iteam-amfs/generalservice_amfs/-/tags/v1.0.0.0) - 2020-07-07

Initial version