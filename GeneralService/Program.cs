﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneralService.Core;
using GeneralService.Core.Helper;

namespace GeneralService
{
    internal static class Program
    {
        private static string _serviceId = "";
        private static Dictionary<string, string> _args;

        private static void Main(string[] args)
        {
            try
            {
                EntlibHelper.InitializeData();

                HandleArgs(args);

                var service = new Service(_serviceId, _args);
                LogHelper.LogInfo($"Service {_serviceId} started");
                service.Run();
                LogHelper.LogInfo($"Service {_serviceId} finished");
            }
            catch (Exception ex)
            {
                LogHelper.LogError($"{ex.Message} {ex.StackTrace} {(ex.InnerException != null ? ex.InnerException.Message + ex.InnerException.StackTrace : "")}", ex);
            }
        }

        private static void HandleArgs(string[] args)
        {
            if (args.Any())
            {
                switch (args[0])
                {
                    case "/help":
                        ShowHelp(); break;
                    case "/plugins":
                        ShowAllPlugins(); break;
                    default:
                        _serviceId = args[0]; break;
                }

                if (args.Length <= 1) return;

                _args = new Dictionary<string, string>();
                for (var i = 1; i < args.Length; i++)
                {
                    if (args[i].Contains("=")) //new method, use = symbol for separator
                    {
                        var keyvalues = args[i].Split('=');
                        var key = keyvalues[0];
                        var value = keyvalues[1];

                        _args.Add(key, value);
                    }
                    else
                    {
                        var keyvalues = args[i].Split(':');
                        var key = keyvalues[0];
                        var value = keyvalues[1];

                        _args.Add(key, value);
                    }
                }
            }
            else
            {
                ShowHelp();
            }
        }

        private static void ShowHelp()
        {
            Console.WriteLine("run GeneralService.exe {serviceid} {parameterkey1:parametervalue1} {parameterkey2:parametervalue2} ..");
            Console.WriteLine("run GeneralService.exe GeneralService.Sample for sample");
            Console.WriteLine("run GeneralService.exe /plugins to view all installed plugins");
            Console.ReadKey();
            Environment.Exit(0);
        }

        private static void ShowAllPlugins()
        {
            var service = new Service("");
            var plugins = service.GetAllPlugins();

            if (plugins is null || plugins.Count == 0)
            {
                Console.WriteLine("No plugins installed.");
            }
            else
            {
                Console.WriteLine($"Plugins count {plugins.Count}");
                foreach (var plugin in plugins)
                {
                    var result = plugin.Key + ": " + plugin.Value;
                    Console.WriteLine(result);
                }
            }
            Console.ReadKey();
            Environment.Exit(0);
        }
    }

}
