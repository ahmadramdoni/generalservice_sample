﻿using Microsoft.Practices.EnterpriseLibrary.Data;

namespace GeneralService
{
    public class EntlibHelper
    {
        public static void InitializeData()
        {
            DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory());
        }
    }
}
