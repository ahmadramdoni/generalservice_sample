# Development

GeneralService.exe is a console application with some core functionality.  There is no built in plugin/service in GeneralService. 

Every service need to be developed as a Net Framework class library and must implement IPlugin interface from GeneralService.Plugin.

To execute the plugin, run GeneralService.exe followed by plugin id via command prompt or powershell.

For example : 

`GeneralService.exe SamplePlugin`



## Prerequisite

1. Visual Studio 2019
2. .NET Framework >= 4.6.1



## Plugin Development

### Case study

User need new Report with below criteria :

1. All Policy with status Inforce
2. Data saved as excel file with PolicyNo, HolderName, EffectiveDate, Premium, BirthDate as column.
3. User need the report to be sent every morning at 5.00 am.



### Development

We can create plugin in new solution or in the same solution as GeneralService. In this example, we will create plugin in GeneralService.sln.

**Create New Class Library Project**

1. Open GeneralService.sln with Visual Studio 2019
2. Right click on Services folder, Choose Add > New Project.
3. Choose C# Language, Windows as platform, and Library for Project types, and then choose Class Library (.NET Framework), Click Next.
4. Fill project name, use descriptive name so your team can understand what the plugin do without even need to read the code. We will use `GeneralService.PolicyInforceReport` in this example.
5. On Location textbox, browse and select folder Services
6. On Framework, choose .NET Framework 4.6.1
7. Click Create
8. Rename Class1.cs to Service.cs

**Add Plugin Metadata**

Every plugin has a metadata defined in a json file.

1. Righ click on the Plugin Project, choose Add > New Item.

2. Search for JSON file, type `GeneralService.PolicyInforceReport.json` as file name, click Add.

3. Copy text below for the json content

   ```json
   {
       "ServiceId": "GeneralService.PolicyInforceReport",
       "Name": "Report Policy Inforce",
       "Description": "Generate Inforce policies as an excel file",
       "Author": "sdf",
       "ExecuteFileName": "GeneralService.PolicyInforceReport.dll",
       "PluginSettings": [
       ] 
   }
   
   ```

   Description

   | Name            | Description                                                  |
   | --------------- | ------------------------------------------------------------ |
   | ServiceId       | plugin id, must be unique. This will be used for running the plugin. For example `GeneralService.exe GeneralService.PolicyInforceReport` |
   | Name            | Plugin name                                                  |
   | Description     | Summary of what this plugin can do                           |
   | Author          |                                                              |
   | ExecuteFileName | Assembly name, default is same as project name               |
   | PluginSettings  | Custom setting for plugin.                                   |

4. On Solution Explorer, right click on the json file, click properties.

5. Set Copy to Output Directiory value to Copy if newer



**Add Reference to GeneralService.Plugin**

1. On your plugin project, right click References , click Add reference...
2. On the left pane, choose Projects, and check GeneralService.Plugin
3. Click Ok



**Implementing IPlugin**

1. Open Service.cs
2. Implement IPlugin interface from GeneralService.Plugin

```c#
using GeneralService.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralService.PolicyInforceReport
{
    public class Service : IPlugin
    {
        public void Run(PluginMetadata metadata, Dictionary<string, string> args = null)
        {
            
        }
    }
}
```

3. Create new method for getting the data, we can use SqlHelper and return the result as a DataSet

```c#
using GeneralService.Plugin;
using System.Data;
using System.Collections.Generic;
using GeneralService.Core.Helper;

namespace GeneralService.PolicyInforceReport
{
    public class Service : IPlugin
    {
        public void Run(PluginMetadata metadata, Dictionary<string, string> args = null)
        {
            //get data  
            var policies = GetPolicyInforce();
        }

        private DataSet GetPolicyInforce()
        {
            var policies = SqlHelper.ExecuteAsDataSet("select top 100 p.PolicyNo, c.FirstName as HolderName, p.EffectiveDate, p.TotalPremium as Premium, c.BirthDate from Policy p inner join Customer c on c.CustomerId = p.CustomerNo", CommandType.Text);

            return policies;
        }
    }
}
```

4. To export the result to Excel, we can use DataSet extensions. 

```c#
using GeneralService.Plugin;
using System.Data;
using System.Collections.Generic;
using GeneralService.Core.Helper;
using System.IO;
using System;

namespace GeneralService.PolicyInforceReport
{
    public class Service : IPlugin
    {
        public void Run(PluginMetadata metadata, Dictionary<string, string> args = null)
        {
            //get data  
            var policies = GetPolicyInforce();

            //export to excel
            var path = ExportToExcel(policies);
        }

        private DataSet GetPolicyInforce()
        {
            var policies = SqlHelper.ExecuteAsDataSet("select top 100 p.PolicyNo, c.FirstName as HolderName, p.EffectiveDate, p.TotalPremium as Premium, c.BirthDate from Policy p inner join Customer c on c.CustomerId = p.CustomerNo", CommandType.Text);

            return policies;
        }

        private string ExportToExcel(DataSet policies)
        {
            //save to My Documents > DMTM > PolicyInforceReport directory
            var pathToSave = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "DMTM", "PolicyInforceReport");
            var fileName = $"PolicyInforce_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
            var finalPath = Path.Combine(pathToSave, fileName);

            //use DataSet extensions to export (GeneralService.Core.Helper > ExportHelper.cs)
            policies.ExportToExcel(finalPath);

            return finalPath;
        }
    }
}

```

5. Build the project and copy `GeneralService.PolicyInforceReport.dll` and `GeneralService.PolicyInforceReport.json` to Executable Services directory (in this example we can copy to GeneralService\bin\Debug\Services\)
6. To run this service, go to GeneralService\bin\Debug\Services directory from command prompt or powershell, and execute command `GeneralService.exe GeneralService.PolicyInforceReport`. New excel file will be saved on Documents\DMTM\PolicyInforceReport directory.
7. To send the file as an Email attachment , use MailHelper.SendMail. The final code will be. Don't forget to build & copy plugin dll & json file every time your code changed.

```c#
using GeneralService.Plugin;
using System.Data;
using System.Collections.Generic;
using GeneralService.Core.Helper;
using System.IO;
using System;

namespace GeneralService.PolicyInforceReport
{
    public class Service : IPlugin
    {
        public void Run(PluginMetadata metadata, Dictionary<string, string> args = null)
        {
            //get data  
            var policies = GetPolicyInforce();

            //export to excel
            var path = ExportToExcel(policies);

            //send email
            var mailDestination = "youremail@ciptadrasoft.com";
            MailHelper.SendMail(MailHelper.MailType.SMTP, mailDestination, "[Sample] Policy Inforce Report", "Hi, Please find attached Policy Inforce Report", new List<string> { path });
        }

        private DataSet GetPolicyInforce()
        {
            var policies = SqlHelper.ExecuteAsDataSet("select top 100 p.PolicyNo, c.FirstName as HolderName, p.EffectiveDate, p.TotalPremium as Premium, c.BirthDate from Policy p inner join Customer c on c.CustomerId = p.CustomerNo", CommandType.Text);

            return policies;
        }

        private string ExportToExcel(DataSet policies)
        {
            //save to My Documents > DMTM > PolicyInforceReport directory
            var pathToSave = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "DMTM", "PolicyInforceReport");
            var fileName = $"PolicyInforce_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
            var finalPath = Path.Combine(pathToSave, fileName);

            //use DataSet extensions to export (GeneralService.Core.Helper > ExportHelper.cs)
            policies.ExportToExcel(finalPath);

            return finalPath;
        }
    }
}

```

8. Run the service again, it should generate excel file and send the email.



### Add Logging

GeneralService use NLogger by default.

To use logging, use LogHelper.LogInfo or LogHelper.LogError.

below are final code with logging.

```c#
using GeneralService.Plugin;
using System.Data;
using System.Collections.Generic;
using GeneralService.Core.Helper;
using System.IO;
using System;

namespace GeneralService.PolicyInforceReport
{
    public class Service : IPlugin
    {
        private static PluginMetadata _pluginMetadata;
        public void Run(PluginMetadata metadata, Dictionary<string, string> args = null)
        {
            _pluginMetadata = metadata;

            try
            {
                //get data  
                LogHelper.LogInfo("Getting data..", _pluginMetadata);
                var policies = GetPolicyInforce();

                //export to excel
                LogHelper.LogInfo("Export data to excel..", _pluginMetadata);
                var path = ExportToExcel(policies);

                //send email
                var mailDestination = "septian.dwi@ciptadrasoft.com";
                LogHelper.LogInfo($"Sending email to {mailDestination}..", _pluginMetadata);
                MailHelper.SendMail(MailHelper.MailType.SMTP, mailDestination, "[Sample] Policy Inforce Report", "Hi, Please find attached Policy Inforce Report", new List<string> { path });
                LogHelper.LogInfo("Mail Sent", _pluginMetadata);
            }
            catch (Exception ex)
            {
                LogHelper.LogError($"{ex.Message}, {ex.StackTrace}", ex, _pluginMetadata);
            }
        }

        private DataSet GetPolicyInforce()
        {
            var policies = SqlHelper.ExecuteAsDataSet("select top 100 p.PolicyNo, c.FirstName as HolderName, p.EffectiveDate, p.TotalPremium as Premium, c.BirthDate from Policy p inner join Customer c on c.CustomerId = p.CustomerNo", CommandType.Text);

            return policies;
        }

        private string ExportToExcel(DataSet policies)
        {
            //save to My Documents > DMTM > PolicyInforceReport directory
            var pathToSave = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "DMTM", "PolicyInforceReport");
            var fileName = $"PolicyInforce_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
            var finalPath = Path.Combine(pathToSave, fileName);

            LogHelper.LogInfo($"Exporting file as {finalPath}..", _pluginMetadata);

            //use DataSet extensions to export (GeneralService.Core.Helper > ExportHelper.cs)
            policies.ExportToExcel(finalPath);

            LogHelper.LogInfo($"File {finalPath} saved.", _pluginMetadata);

            return finalPath;
        }
    }
}
```

Try to run the plugin again, you can check the log on logs folder.

```
2021-04-19 15:17:05.0227|INFO|General|Service GeneralService.PolicyInforceReport started
2021-04-19 15:17:05.0807|INFO|GeneralService.PolicyInforceReport|Getting data..
2021-04-19 15:17:05.2476|INFO|GeneralService.PolicyInforceReport|Export data to excel..
2021-04-19 15:17:05.2476|INFO|GeneralService.PolicyInforceReport|Exporting file as D:\Libraries\Documents\DMTM\PolicyInforceReport\PolicyInforce_20210419031705.xlsx..
2021-04-19 15:17:05.9136|INFO|GeneralService.PolicyInforceReport|File D:\Libraries\Documents\DMTM\PolicyInforceReport\PolicyInforce_20210419031705.xlsx saved.
2021-04-19 15:17:05.9136|INFO|GeneralService.PolicyInforceReport|Sending email to septian.dwi@ciptadrasoft.com..
2021-04-19 15:17:11.5732|INFO|GeneralService.PolicyInforceReport|Mail Sent
2021-04-19 15:17:11.5732|INFO|General|Service GeneralService.PolicyInforceReport finished
```



### Using PluginSettings

On above example, some value are hard-coded. To make the service easily configurable, you can use PluginSettings on plugin json files.

In this example, We will add maildestination and enablemailnotification as PluginSettings value.

1. Modify GeneralService.PolicyInforceReport.json

```json
{
    "ServiceId": "GeneralService.PolicyInforceReport",
    "Name": "Report Policy Inforce",
    "Description": "Generate Inforce policies as an excel file",
    "Author": "sdf",
    "ExecuteFileName": "GeneralService.PolicyInforceReport.dll",
    "PluginSettings": [
      {
        "Key": "EnableMailNotification",
        "Value": "true"
      },
      {
        "Key": "MailDestination",
        "Value": "youremail@ciptadrasoft.com"
      }
    ]
}

```

2. To read settings value, use PluginMetada.PluginSettings.GetValue method

```c#
var enableMailNotification = _pluginMetadata.PluginSettings.GetValue<bool>("EnableMailNotification");
var mailDestination = _pluginMetadata.PluginSettings.GetValue<string>("MailDestination");
LogHelper.LogInfo($"MailNotification is {(enableMailNotification ? "enabled" : "disabled")}", _pluginMetadata);

if (enableMailNotification)
{
	LogHelper.LogInfo($"Sending email to {mailDestination}..", _pluginMetadata);
	MailHelper.SendMail(MailHelper.MailType.SMTP, mailDestination, "[Sample] Policy Inforce Report", "Hi, Please find attached Policy Inforce Report", new List<string> { path });
	LogHelper.LogInfo("Mail Sent", _pluginMetadata);
}
```

3. Build and run to see it in action.



### Deployment

To setup the scheduler, follow Installation instruction in [README](README.md)