# GeneralService

GeneralService.exe is a console application to execute some task like generating files, sending email, update database, etc.  
GeneralService is a plugin based application, all services located in GeneralService/Services directory.

## Installation
Scheduled service can be setup using default Windows Task Sceduler. Task Scheduler need to execute GeneralService.exe {service key}  
Below is sample how to configure Accounting Report Service in Task Scheduler.  

1. Make sure all GeneralService files exists in server directory
2. For AccountingReport, copy GeneralService.AccountingReport.dll and GeneralService.AccountingReport.json to GeneralService/Services folder
3. Open Task Scheduler
4. Click Task Scheduler Library on the left pane
5. Right Click on the middle pane, click Create New Task..
6. Tab General :  
Name : DMTM Accounting Service  
Description : Service to generate flat file  
Security options : Run whether user is logged on or not. Uncheck Do not store password.
7. Tab Triggers :  
Create trigger Daily at 05.00 am
8. Tab Actions :  
Create new Action, select Start a program  
On Program/script box, select GeneralService.exe directory. Ex D:\Apps\GeneralService\GeneralService.exe  
On Add arguments, type the services key, key is defined in ServiceId on services json file, ex GeneralService.AccountingReport  
On Start in, select GeneralService directory. Ex D:\Apps\GeneralService\
9. Tab Settings  
Check Allow task to be run on demand  
Other options is depend on your need.
10. Click OK, input password if needed.



## Configuration
You can configure the service by editing GeneralService.exe.config  
This config only contains general value, like connectionstring and email configuration.  
Services configuration is a json files located next to the plugin dll file.  
Ex :  
Flie structure of service Accounting Report

- GeneralService.AccountingReport.dll
- GeneralService.AccountingReport.json

### Section connectionString

| Name | Description | Default/Sample Value |
| ---- | ----------- | ------- |
| ConnectionString | Contains connection string to main DMTM database | Data Source=localhost;Initial Catalog=DMTM;UID=sa;Password=password;Persist Security Info=false; |
| ConnectionStringBE | Contains connection string to Billing database  | Data Source=localhost;Initial Catalog=db_billing;UID=sa;Password=password;Persist Security Info=false; |

### Section appSettings
| Key | Description | Default/Sample Value |
| --- | ----------- | ------------- |
| MailServer  | Host server for SMTP/Mail service. | smtp.gmail.com  |
| MailPort  | SMTP Port service | 587 |
| MailSender | Account used for sending mail notification | notification@ciptadrasoft.com |
| MailPassword | SMTP mail password | 1234 |
| MailDisplayName | Display Name for Email Sender | DMTM System |
| EnableSsl | Use SSL for SMTP Server. True/False | True |
| IsBodyHtml | Use HTML for mail body. True/False | True |
| CommandTimeout | Timeout value for executing query | 2400 |
| DbMailProfileName | SQL Server DB Mail Profile. Services can also use DBMail for sending email,  instead of via SMTP  | Default |



## Service/Plugin Development
[DEVELOPMENT.md](DEVELOPMENT.md)