﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using GeneralService.Core.Helper;
using GeneralService.Plugin;

namespace GeneralService.Core.Plugin
{
    public static class PluginsLoader<T>
    {
        public static List<PluginPair<T>> Plugins(IEnumerable<PluginMetadata> metadatas)
        {
            var plugins = new List<PluginPair<T>>();

            foreach (var metadata in metadatas)
            {
                Assembly assembly;
                try
                {
                    //var serviceName = PluginConfig.PluginPath + @"\" + metadata.ExecuteFileName;
                    var name = AssemblyName.GetAssemblyName(metadata.PluginFullPath);
                    assembly = Assembly.Load(name);
                }
                catch (Exception e)
                {
                    LogHelper.LogInfo($"Couldn't load assembly for {metadata.Name}" + e.Message + e.StackTrace);
                    throw new Exception($"Couldn't load assembly for {metadata.Name}" + e.Message + e.StackTrace, e.InnerException);
                }
                var types = assembly.GetTypes();
                Type type;
                try
                {
                    type = types.First(o => o.IsClass && !o.IsAbstract && o.GetInterfaces().Contains(typeof(T)));
                }
                catch (Exception e)
                {
                    LogHelper.LogInfo($"Can't find class implement {typeof(T)} for <{metadata.Name}>" + e.Message);
                    throw new Exception($"Can't find class implement {typeof(T)} for <{metadata.Name}>" + e.Message, e.InnerException);
                }
                T plugin;
                try
                {
                    plugin = (T)Activator.CreateInstance(type);
                }
                catch (Exception e)
                {
                    LogHelper.LogInfo($"Can't create instance for <{metadata.Name}>" + e.Message);
                    throw new Exception($"Can't create instance for <{metadata.Name}>" + e.Message, e.InnerException);
                }

                var pair = new PluginPair<T>
                {
                    Plugin = plugin,
                    PluginMetadata = metadata
                };

                plugins.Add(pair);
            }

            return plugins;
        }
    }
}
