﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using GeneralService.Core.Helper;
using GeneralService.Plugin;
using Newtonsoft.Json;

namespace GeneralService.Core.Plugin
{
    internal abstract class PluginConfig
    {
        private static readonly List<PluginMetadata> PluginMetadatas = new List<PluginMetadata>();
        public static string PluginPath;

        public static IEnumerable<PluginMetadata> Parse(string path = "Services")
        {
            PluginPath = Path.Combine(AppContext.BaseDirectory, path);
            PluginMetadatas.Clear();
            ParsePluginConfigs(PluginPath);
            return PluginMetadatas;
        }

        private static void ParsePluginConfigs(string pluginDirectories)
        {
            if (!Directory.Exists(pluginDirectories))
            {
                Directory.CreateDirectory(pluginDirectories);
            }

            //LogHelper.LogInfo($"Parse plugin from {pluginDirectories}");

            var fileList = Directory.GetFileSystemEntries(pluginDirectories, "PluginMetadata.json", SearchOption.AllDirectories);
            
            //LogHelper.LogInfo($"Found {fileList.Count()} PluginMetadata.json");

            foreach (var file in fileList)
            {
                var metadata = GetPluginMetadata(file);
                metadata.PluginFullPath = Path.Combine(Path.GetDirectoryName(file), metadata.ExecuteFileName);
                PluginMetadatas.Add(metadata);
            }
        }

        private static PluginMetadata GetPluginMetadata(string file)
        {
            try
            {
                var metadata = JsonConvert.DeserializeObject<PluginMetadata>(File.ReadAllText(file));

                //PluginMetadatas.Add(metadata);
                return metadata;
            }
            catch (Exception e)
            {
                throw new Exception($"|PluginConfig.GetPluginMetadata|invalid json for config <{file}>", e);
            }
        }
    }
}
