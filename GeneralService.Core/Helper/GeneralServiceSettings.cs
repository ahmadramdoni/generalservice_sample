﻿using System.Configuration;

namespace GeneralService.Core.Helper
{
    public class GeneralServiceSettings
    {
        public static string MailServer => ConfigurationManager.AppSettings["SMTPServer"];
        public static string DbMailProfileName => ConfigurationManager.AppSettings["DbMailProfileName"];

        public static int MailPort
        {
            get
            {
                var confVal = 587;
                var strVal = ConfigurationManager.AppSettings["SMTPPort"];
                if (!string.IsNullOrWhiteSpace(strVal))
                    int.TryParse(strVal, out confVal);

                return confVal;
            }

        }

        public static string MailSender => ConfigurationManager.AppSettings["EmailFrom"];

        public static string MailPassword => ConfigurationManager.AppSettings["MailPassword"];

        public static string MailDisplayName
        {
            get
            {
                var val = ConfigurationManager.AppSettings["MailDisplayName"];
                if (string.IsNullOrWhiteSpace(val))
                    val = "DMTM System";

                return val;
            }

        }

        public static bool EnableSsl
        {
            get
            {
                var confVal = false;
                var strVal = ConfigurationManager.AppSettings["EnableSsl"];
                if (!string.IsNullOrWhiteSpace(strVal))
                    bool.TryParse(strVal, out confVal);
                return confVal;
            }
        }

        public static bool IsBodyHtml
        {
            get
            {
                var confVal = false;
                var strVal = ConfigurationManager.AppSettings["IsBodyHtml"];
                if (!string.IsNullOrWhiteSpace(strVal))
                    bool.TryParse(strVal, out confVal);
                return confVal;
            }
        }

        public static int CommandTimeout
        {
            get
            {
                var confVal = 2400;
                var strVal = ConfigurationManager.AppSettings["CommandTimeout"];
                if (!string.IsNullOrWhiteSpace(strVal))
                    int.TryParse(strVal, out confVal);

                return confVal;
            }

        }

        public static string MailAttachmentShareServer => ConfigurationManager.AppSettings["MailAttachmentShareServer"];
        public static string MailAttachmentSharePath => ConfigurationManager.AppSettings["MailAttachmentSharePath"];
        public static string MailAttachmentNetworkUserNameDomain => ConfigurationManager.AppSettings["MailAttachmentNetworkUserNameDomain"];
        public static string MailAttachmentPassword => ConfigurationManager.AppSettings["MailAttachmentPassword"];
        public static string MailAttachmentFriendlyPath => ConfigurationManager.AppSettings["MailAttachmentFriendlyPath"];
        public static string DefaultMailService => ConfigurationManager.AppSettings["DefaultMailService"];
    }
}
