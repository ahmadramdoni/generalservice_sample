﻿using GeneralService.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneralService.Core.Helper
{
    public static class LogHelper
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public static void LogInfo(string message, PluginMetadata pluginMetadata = null)
        {
            Logger.Info($"{(pluginMetadata != null ? pluginMetadata.ServiceId : "General")}|{message}");
        }

        public static void LogError(string message, Exception exception = null, PluginMetadata pluginMetadata = null)
        {
            if (exception != null)
            {
                Logger.Error(exception, $"{(pluginMetadata != null ? pluginMetadata.ServiceId : "General")}|{message}");
            }
            else
            {
                Logger.Error($"{(pluginMetadata != null ? pluginMetadata.ServiceId : "General")}|{message}");
            }
        }
    }
}
