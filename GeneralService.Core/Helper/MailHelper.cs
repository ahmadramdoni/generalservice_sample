﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace GeneralService.Core.Helper
{
    public static class MailHelper
    {
        [Obsolete("MailHelper.SendMail is obsolote, use MailService instead")]
        public static void SendMail(MailType mailType, string destinationMails, string subject, string body, List<string> attachments = null)
        {
            switch (mailType)
            {
                case MailType.SMTP:
                    SendMail(destinationMails, subject, body, attachments);
                    break;
                case MailType.DBMAIL:
                    SendMailViaDbMail(destinationMails, subject, body, attachments);
                    break;
                default:
                    SendMail(destinationMails, subject, body, attachments);
                    break;
            }
        }

        [Obsolete("MailHelper.SendMail is obsolote, use MailService instead")]
        public static void SendMail(string destinationMails, string subject, string body,
            List<string> attachments = null)
        {
            if (string.IsNullOrEmpty(destinationMails))
                return;
            try
            {
                using (var smtp = new SmtpClient())
                {
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials =
                        new NetworkCredential(GeneralServiceSettings.MailSender, GeneralServiceSettings.MailPassword);
                    smtp.Port = GeneralServiceSettings.MailPort;
                    smtp.EnableSsl = GeneralServiceSettings.EnableSsl;
                    smtp.Host = GeneralServiceSettings.MailServer;

                    var destinations = destinationMails.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                    //using (var mail = new MailMessage(GeneralServiceSettings.MailSender, destinations[0]))
                    using (var mail = new MailMessage())
                    {
                        mail.From = new MailAddress(GeneralServiceSettings.MailSender, GeneralServiceSettings.MailDisplayName);

                        foreach (var address in destinations)
                        {
                            mail.To.Add(address);
                        }

                        mail.Subject = subject;
                        mail.IsBodyHtml = GeneralServiceSettings.IsBodyHtml;
                        mail.Body = body;

                        if (attachments != null)
                        {
                            foreach (var att in attachments)
                            {
                                var attachment = new Attachment(att);
                                mail.Attachments.Add(attachment);
                            }
                        }

                        smtp.Send(mail);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace);
                throw new Exception(ex.Message + ex.StackTrace + ex.InnerException.Message);
            }
        }

        private static void SendMailViaDbMail(string destinationMails, string subject, string body, List<string> attachments = null)
        {
            var mailProfile = GetDbMailProfileName();

            const string query = "EXEC msdb.dbo.sp_send_dbmail @profile_name = @ProfileName" +
                                 ", @recipients = @MailDestinations" +
                                 ", @subject = @MailSubject" +
                                 ", @body = @tableHTML" +
                                 ", @body_format = 'HTML';";
            //+", @file_attachments= @attachment ;";

            //var attachment = "";
            if (attachments != null && attachments.Count > 0)
            {
                var movedAttachment = MoveAttachmentsToSharedFolder(attachments);
                var attachmentNameList = movedAttachment.Select(x => Path.GetFileName(x));
                //attachment = string.Join(";", attachments);
                body = body.Replace("</body>", $"You can get your attachment on {GeneralServiceSettings.MailAttachmentFriendlyPath}, file(s) : {string.Join(", ", attachmentNameList)} </body>");
            }


            var parameters = new List<DbParam>
            {
                new DbParam { Name = "ProfileName", SqlDbType = SqlDbType.VarChar, Size = 20, Value = mailProfile },
                new DbParam { Name = "MailDestinations", SqlDbType = SqlDbType.VarChar, Size = 300, Value = destinationMails },
                new DbParam { Name = "MailSubject", SqlDbType = SqlDbType.VarChar, Size = 100, Value = subject },
                new DbParam { Name = "tableHTML", SqlDbType = SqlDbType.VarChar, Size = 8000, Value = body },
                //new DbParam { Name = "attachment", SqlDbType = SqlDbType.VarChar, Size = 8000, Value = attachment }
            };

            SqlHelper.ExecuteNonQuery(query, CommandType.Text, parameters);
        }

        private static string GetDbMailProfileName()
        {
            return GeneralServiceSettings.DbMailProfileName;
        }

        public enum MailType
        {
            SMTP,
            DBMAIL
        }

        private static List<string> MoveAttachmentsToSharedFolder(List<string> attachments)
        {
            var movedAttachmens = new List<string>();
            var destinationDirectory = Path.Combine(GeneralServiceSettings.MailAttachmentSharePath, DateTime.Now.ToString("yyyyMMdd"));

            var networkCredential = new NetworkCredential(GeneralServiceSettings.MailAttachmentNetworkUserNameDomain, GeneralServiceSettings.MailAttachmentPassword);
            var credentialCache = new CredentialCache();
            credentialCache.Add(new Uri(GeneralServiceSettings.MailAttachmentShareServer), "Basic", networkCredential);

            foreach (var attachment in attachments)
            {
                if (string.IsNullOrEmpty(attachment)) continue;

                var file = new FileInfo(attachment);
                var destinationPath = destinationDirectory;// Path.Combine(destinationDirectory, file.Directory.Name);

                if (!Directory.Exists(destinationPath))
                    Directory.CreateDirectory(destinationPath);

                var destinationFullPath = Path.Combine(destinationPath, file.Name);
                file.CopyTo(destinationFullPath, true);

                movedAttachmens.Add(destinationFullPath);
            }

            return movedAttachmens;
        }
    }
}
