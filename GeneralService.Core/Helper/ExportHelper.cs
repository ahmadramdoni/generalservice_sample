﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using OfficeOpenXml;
using System.IO.Compression;
using CsvHelper;
using CsvHelper.Configuration;

namespace GeneralService.Core.Helper
{
    public static class ExportHelper
    {

        public static string ExportToText(this DataSet dataSet, string filePath, bool isUseHeader = true, bool resultAsOneFile = true, string columns = "",
        string delimeter = "\t")
        {
            try
            {
                if (dataSet == null)
                    throw new Exception("DataSet can't be null");

                var files = new List<string>();

                for (var a = 0; a < dataSet.Tables.Count; a++)
                {
                    var table = dataSet.Tables[a];
                    var rowCount = table.Rows.Count;

                    var contentBuilder = new StringBuilder();

                    if (string.IsNullOrEmpty(columns))
                    {
                        var columnNames = new List<string>();

                        for (var h = 0; h < table.Columns.Count; h++)
                            columnNames.Add(table.Columns[h].ColumnName);

                        #region header
                        if (isUseHeader)
                        {
                            for (var h = 0; h < columnNames.Count; h++)
                            {
                                if (h > 0)
                                    contentBuilder.Append(delimeter);
                                contentBuilder.Append(columnNames[h]);
                            }

                            contentBuilder.AppendLine();
                            //writer.Write("\r\n");
                        }
                        #endregion

                        #region content
                        for (var i = 0; i < rowCount; i++)
                        {
                            var rowData = table.Rows[i];
                            if (i > 0)
                                contentBuilder.AppendLine();

                            for (var h = 0; h < columnNames.Count; h++)
                            {
                                if (h > 0)
                                    contentBuilder.Append(delimeter);
                                contentBuilder.Append(rowData[columnNames[h]]);
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        var includedColumns = columns.Split(",".ToCharArray()).ToList();
                        if (isUseHeader)
                        {
                            #region header
                            for (var h = 0; h < includedColumns.Count; h++)
                            {
                                if (h > 0)
                                    contentBuilder.Append(delimeter);

                                contentBuilder.Append(includedColumns[h].Trim());
                            }

                            contentBuilder.AppendLine();
                            #endregion
                        }

                        #region content
                        for (var i = 0; i < rowCount; i++)
                        {
                            var rowData = table.Rows[i];
                            if (i > 0)
                                contentBuilder.AppendLine();

                            for (var h = 0; h < includedColumns.Count; h++)
                            {
                                if (h > 0)
                                    contentBuilder.Append(delimeter);
                                contentBuilder.Append(rowData[includedColumns[h].Trim()]);
                            }

                        }
                        #endregion
                    }

                    if (a == 0)
                    {
                        using (var writer = new StreamWriter(filePath))
                        {
                            writer.Write(contentBuilder.ToString());
                        }
                        files.Add(filePath);
                    }
                    else
                    {
                        if (resultAsOneFile)
                        {
                            File.AppendAllText(filePath, Environment.NewLine + contentBuilder);
                        }
                        else
                        {
                            var newFile = Path.Combine(Path.GetDirectoryName(filePath), string.Format("{0}-{1}{2}", Path.GetFileNameWithoutExtension(filePath), a, Path.GetExtension(filePath)));

                            using (var writer = new StreamWriter(newFile))
                            {
                                writer.Write(contentBuilder.ToString());
                            }

                            files.Add(newFile);
                        }
                    }
                }

                if (files.Count > 1)
                    filePath = CompressFiles(filePath, files);

                if (!File.Exists(filePath))
                    throw new Exception("Error ExportToText, can't file processed file(s)");

                return filePath;
            }
            catch (Exception e)
            {
                throw new Exception("Error ExportToText " + e.Message, e.InnerException);
            }
        }

        private static string CompressFiles(string filePath, List<string> files)
        {
            var zipName = Path.Combine(Path.GetDirectoryName(filePath), Path.GetFileNameWithoutExtension(filePath) + ".zip");

            using (var newFile = ZipFile.Open(zipName, ZipArchiveMode.Create))
            {
                foreach (var file in files)
                {
                    newFile.CreateEntryFromFile(file, Path.GetFileName(file));
                    File.Delete(file);
                }
            }
            return zipName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataSet"></param>
        /// <param name="filePath"></param>
        /// <param name="isUseHeader"></param>
        /// <param name="resultAsOneFile"></param>
        /// <param name="columnFormat"></param>
        /// <returns></returns>
        public static string ExportToExcel(this DataSet dataSet, string filePath, bool isUseHeader = true, bool resultAsOneFile = true, string columnFormat = "")
        {
            if (dataSet == null)
                throw new Exception("DataSet can't be null");

            var files = new List<string>();

            var file = new FileInfo(filePath);

            if (file.Directory != null && !file.Directory.Exists)
            {
                Directory.CreateDirectory(Path.Combine(file.Directory.FullName));
            }

            if (file.Exists)
                file.Delete();
            if (resultAsOneFile)
            {
                using (var pck = new ExcelPackage(file))
                {
                    for (var a = 0; a < dataSet.Tables.Count; a++)
                    {
                        //var table = dataSet.Tables[a];
                        var table = ConvertDataTableToNullable(dataSet.Tables[a]);
                        var ws = pck.Workbook.Worksheets.Add($"Reports-{a + 1}");
                        ws.Cells["A1"].LoadFromDataTable(table, isUseHeader);

                        if (string.IsNullOrEmpty(columnFormat))
                        {
                            for (var index = 0; index < table.Columns.Count; index++)
                            {
                                var col = table.Columns[index];
                                if (col.DataType == typeof(DateTime))
                                {
                                    ws.Column(index + 1).Style.Numberformat.Format = "dd/MM/yyyy";
                                }
                            }
                        }
                        else //not implemented yet
                        {
                            //columnname1:format1, columnname2:format2
                            var columnList = columnFormat.Split(",".ToCharArray()).ToList();
                            //foreach (var columns in columnList)
                            //{
                            //    var nameFormat = columns.Trim().Split(":".ToCharArray()).ToList();
                            //    var name = nameFormat[0];
                            //    var format = nameFormat[1];

                            //    if (!string.IsNullOrEmpty(format))
                            //        ws.Column(ws.GetColumnByName(name)).Style.Numberformat.Format = format;
                            //}
                        }

                        ws.Cells[ws.Dimension.Address].AutoFitColumns();
                    }
                    pck.Save();
                }
            }
            else
            {
                for (var a = 0; a < dataSet.Tables.Count; a++)
                {
                    var table = ConvertDataTableToNullable(dataSet.Tables[a]);
                    //var table = dataSet.Tables[a];

                    if (a == 0)
                    {

                        using (var pck = new ExcelPackage(file))
                        {
                            var ws = pck.Workbook.Worksheets.Add("Reports");
                            ws.Cells["A1"].LoadFromDataTable(table, isUseHeader);

                            if (string.IsNullOrEmpty(columnFormat))
                            {
                                for (var index = 0; index < table.Columns.Count; index++)
                                {
                                    var col = table.Columns[index];
                                    if (col.DataType == typeof(DateTime))
                                    {
                                        ws.Column(index + 1).Style.Numberformat.Format = "dd/MM/yyyy";
                                    }
                                }
                            }
                            else
                            {
                                //columnname1:format1, columnname2:format2
                                var columnList = columnFormat.Split(",".ToCharArray()).ToList();
                                foreach (var columns in columnList)
                                {
                                    var nameFormat = columns.Trim().Split(":".ToCharArray()).ToList();
                                    //var name = nameFormat[0];
                                    //var format = nameFormat[1];

                                    //if (!string.IsNullOrEmpty(format))
                                    //    ws.Column(ws.GetColumnByName(name)).Style.Numberformat.Format = format;
                                }
                            }

                            ws.Cells[ws.Dimension.Address].AutoFitColumns();

                            pck.Save();

                        }

                        files.Add(filePath);
                    }
                    else
                    {
                        var newFile = Path.Combine(Path.GetDirectoryName(filePath),
                            $"{Path.GetFileNameWithoutExtension(filePath)}-{a}{Path.GetExtension(filePath)}");

                        using (var pck = new ExcelPackage(new FileInfo(newFile)))
                        {
                            var ws = pck.Workbook.Worksheets.Add("Reports");
                            ws.Cells["A1"].LoadFromDataTable(table, isUseHeader);

                            if (string.IsNullOrEmpty(columnFormat))
                            {
                                for (var index = 0; index < table.Columns.Count; index++)
                                {
                                    var col = table.Columns[index];
                                    if (col.DataType == typeof(DateTime))
                                    {
                                        ws.Column(index + 1).Style.Numberformat.Format = "dd/MM/yyyy";
                                    }
                                }
                            }
                            else //not implemented yet
                            {
                                //columnname1:format1, columnname2:format2
                                var columnList = columnFormat.Split(",".ToCharArray()).ToList();
                                //foreach (var columns in columnList)
                                //{
                                //    var nameFormat = columns.Trim().Split(":".ToCharArray()).ToList();
                                //    var name = nameFormat[0];
                                //    var format = nameFormat[1];

                                //    if (!string.IsNullOrEmpty(format))
                                //        ws.Column(ws.GetColumnByName(name)).Style.Numberformat.Format = format;
                                //}
                            }

                            ws.Cells[ws.Dimension.Address].AutoFitColumns();
                            pck.Save();
                        }

                        files.Add(newFile);
                    }
                }
            }

            if (files.Count > 1)
                filePath = CompressFiles(filePath, files);

            if (!File.Exists(filePath))
                throw new Exception("Error ExportToExcel, can't find processed file(s)");

            return filePath;
        }

        public static void ExportToCsv(this DataTable table, string filePath)
        {
            try
            {
                if (table == null || table.Columns.Count == 0)
                    throw new Exception("Null or empty input table!\n");

                var rowCount = table.Rows.Count;

                using (File.Create(filePath))
                {
                }

                using (var writer = new StreamWriter(filePath))
                {
                    var config = new CsvConfiguration(null)
                    {
                        Delimiter = ",",
                        HasHeaderRecord = true
                    };

                    var cw = new CsvWriter(writer, config);

                    var columnNames = new List<string>();

                    for (var h = 0; h < table.Columns.Count; h++)
                    {
                        columnNames.Add(table.Columns[h].ColumnName);
                    }

                    #region header

                    foreach (var columnName in columnNames)
                    {
                        cw.WriteField(columnName);
                    }

                    cw.NextRecord();

                    #endregion

                    #region content

                    for (var i = 0; i < rowCount; i++)
                    {
                        var rowData = table.Rows[i];

                        foreach (var c in columnNames)
                        {
                            cw.WriteField(rowData[c]);
                        }

                        cw.NextRecord();
                    }

                    #endregion
                }

            }
            catch (Exception ex)
            {
                throw new Exception("ExportToCsv: \n" + ex.Message + ex.StackTrace);
            }
        }

        /// <summary>
        /// DataTable hasil dari executedataset (sepertinya) tidak support nullable, mengakibatkan failed saat format date saat export excel
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        private static DataTable ConvertDataTableToNullable(DataTable dataTable)
        {
            var data = new DataTable();

            var dateColumns = new List<int>();

            for (var i = 0; i < dataTable.Columns.Count; i++)
            {
                var col = dataTable.Columns[i];
                var column = new DataColumn(col.ColumnName, col.DataType);
                column.AllowDBNull = true;
                data.Columns.Add(column);

                if (col.DataType == typeof(DateTime))
                    dateColumns.Add(i);
            }

            for (var i = 0; i < dataTable.Rows.Count; i++)
            {
                var row = data.NewRow();

                for (var x = 0; x < dataTable.Columns.Count; x++)
                {
                    var value = dataTable.Rows[i][x];

                    if (dateColumns.Contains(x) && (value == null || value == DBNull.Value))
                        row[x] = DBNull.Value; //DateTime.Parse("1900-01-01 00:00:00");
                    else
                        row[x] = value;

                }

                data.Rows.Add(row);
            }

            return data;
        }
    }
}
