﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace GeneralService.Core.Helper
{
    public class DbParam
    {
        public string Name { get; set; }
        public SqlDbType SqlDbType { get; set; }
        public int? Size { get; set; }
        public object Value { get; set; }
    }

    public static class SqlHelper
    {
        private static readonly string Connection = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;

        public static DataSet ExecuteAsDataSet(string query, CommandType commandType, List<DbParam> dbParams = null)
        {
            var dataset = new DataSet();

            using (var connection = new SqlConnection(Connection))
            {
                connection.Open();

                using (var command = new SqlCommand(query, connection))
                {
                    command.CommandType = commandType;
                    command.CommandTimeout = GeneralServiceSettings.CommandTimeout;
                    if (dbParams != null && dbParams.Any())
                    {
                        foreach (var parameter in dbParams)
                        {
                            var name = parameter.Name.Substring(0, 1).Equals("@")
                                ? parameter.Name
                                : "@" + parameter.Name;

                            if (parameter.Size.HasValue)
                                command.Parameters.Add(name, parameter.SqlDbType, parameter.Size.Value).Value =
                                    parameter.Value ?? DBNull.Value;
                            else
                                command.Parameters.Add(name, parameter.SqlDbType).Value = parameter.Value ?? DBNull.Value;
                        }
                    }

                    var adapter = new SqlDataAdapter { SelectCommand = command };

                    adapter.Fill(dataset);
                }
            }

            return dataset;
        }

        public static int ExecuteNonQuery(string query, CommandType commandType, List<DbParam> dbParams = null)
        {
            using (var connection = new SqlConnection(Connection))
            {
                connection.Open();

                using (var command = new SqlCommand(query, connection))
                {
                    command.CommandType = commandType;
                    command.CommandTimeout = GeneralServiceSettings.CommandTimeout;
                    if (dbParams != null && dbParams.Any())
                    {
                        foreach (var parameter in dbParams)
                        {
                            var name = parameter.Name.Substring(0, 1).Equals("@")
                                ? parameter.Name
                                : "@" + parameter.Name;

                            if (parameter.Size.HasValue)
                                command.Parameters.Add(name, parameter.SqlDbType, parameter.Size.Value).Value =
                                    parameter.Value ?? DBNull.Value;
                            else
                                command.Parameters.Add(name, parameter.SqlDbType).Value = parameter.Value ?? DBNull.Value;
                        }
                    }

                    return command.ExecuteNonQuery();
                }
            }
        }

        public static object ExecuteScalar(string query, CommandType commandType, List<DbParam> dbParams = null)
        {
            using (var connection = new SqlConnection(Connection))
            {
                connection.Open();

                using (var command = new SqlCommand(query, connection))
                {
                    command.CommandType = commandType;
                    command.CommandTimeout = GeneralServiceSettings.CommandTimeout;
                    if (dbParams != null && dbParams.Any())
                    {
                        foreach (var parameter in dbParams)
                        {
                            var name = parameter.Name.Substring(0, 1).Equals("@")
                                ? parameter.Name
                                : "@" + parameter.Name;

                            if (parameter.Size.HasValue)
                                command.Parameters.Add(name, parameter.SqlDbType, parameter.Size.Value).Value =
                                    parameter.Value ?? DBNull.Value;
                            else
                                command.Parameters.Add(name, parameter.SqlDbType).Value = parameter.Value ?? DBNull.Value;
                        }
                    }

                    return command.ExecuteScalar();
                }
            }
        }
    }
}
