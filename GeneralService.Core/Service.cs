﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneralService.Core.Plugin;
using GeneralService.Plugin;

namespace GeneralService.Core
{
    public class Service
    {
        private static string _serviceId;
        private static Dictionary<string, string> _args;
        private static List<PluginPair<IPlugin>> _plugins;

        public Service(string serviceId, Dictionary<string, string> args = null)
        {
            _serviceId = serviceId;
            _args = args;
            var metadatas = PluginConfig.Parse("Services");
            _plugins = PluginsLoader<IPlugin>.Plugins(metadatas);
        }

        public void Run()
        {
            var plugin = _plugins.FirstOrDefault(p => p.PluginMetadata.ServiceId.Equals(_serviceId));
            if (plugin != null)
            {
                plugin.Plugin.Run(plugin.PluginMetadata, _args);
            }
            else
            {
                throw new Exception($"Can't find any service with id {_serviceId} {Environment.NewLine}");
            }
        }

        public Dictionary<string, string> GetAllPlugins()
        {
            var pluginLists = _plugins.ToList();
            var plugins = new Dictionary<string, string>();

            foreach (var plugin in pluginLists)
            {
                plugins.Add(plugin.PluginMetadata.ServiceId, plugin.PluginMetadata.Description);
            }

            return plugins;
        }
    }
}
