﻿using GeneralService.Core.Helper;
using GeneralService.Core.Plugin;
using GeneralService.Plugin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GeneralService.Core
{
    public class MailService
    {
        private static List<PluginPair<IEmailPlugin>> _plugins;
        private static string _serviceId;

        public MailService(string mailServiceId = "")
        {
            var metadatas = PluginConfig.Parse("MailServices");
            _plugins = PluginsLoader<IEmailPlugin>.Plugins(metadatas);

            if (string.IsNullOrWhiteSpace(mailServiceId))
            {
                _serviceId = GeneralServiceSettings.DefaultMailService;
            }
        }

        public void Send(string recipients, string subject, string body, List<string> attachments = null, string mailDisplayName = "")
        {
            LogHelper.LogInfo($"Load plugin with id {_serviceId}");

            var plugin = _plugins.FirstOrDefault(p => p.PluginMetadata.ServiceId.Equals(_serviceId));
            
            if (plugin != null)
            {
                var mailArgs = new MailArgs
                {
                    Recipients = recipients,
                    Body = body,
                    Subject = subject,
                    Attachments = attachments,
                    MailDisplayName = mailDisplayName
                };

                LogHelper.LogInfo($"Sending email. Args : {JsonConvert.SerializeObject(mailArgs)}", plugin.PluginMetadata);

                plugin.Plugin.Send(plugin.PluginMetadata, mailArgs);
            }
            else
            {
                throw new Exception($"Can't find any service with id {_serviceId} {Environment.NewLine}");
            }
        }
    }
}