﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GeneralService.Plugin
{
    public interface IEmailPlugin
    {
        void Send(PluginMetadata metadata, MailArgs mailArgs);
    }

    public class MailArgs
    {
        /// <summary>
        /// List of recipients, separated by ;
        /// </summary>
        public string Recipients { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        /// <summary>
        /// List of attachment path
        /// </summary>
        public List<string> Attachments { get; set; }
        /// <summary>
        /// Display Name, optional
        /// </summary>
        public string MailDisplayName { get; set; }
    }
}
