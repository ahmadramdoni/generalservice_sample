﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GeneralService.Plugin
{
    public class PluginMetadata
    {
        public string ServiceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public string ExecuteFileName { get; set; }
        public List<PluginSetting> PluginSettings { get; set; }

        public string PluginFullPath { get; set; }
    }

    public class PluginSetting
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public static class PluginSettingExtension
    {
        [Obsolete("GetValue is obsolote, use GetValue<T> instead")]
        public static string GetValue(this List<PluginSetting> pluginSettings, string key)
        {
            return GetSetting(pluginSettings, key).Value;
        }

        public static T GetValue<T>(this List<PluginSetting> pluginSettings, string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                throw new Exception("Key can't be empty");
            }

            var setting = GetSetting(pluginSettings, key);

            return (T)Convert.ChangeType(setting.Value, typeof(T));
        }

        public static PluginSetting GetSetting(this List<PluginSetting> pluginSettings, string key)
        {
            var pluginSetting = pluginSettings.SingleOrDefault(x => x.Key != null && x.Key == key);

            if (pluginSetting == null)
            {
                throw new KeyNotFoundException($"Can't find settings with key {key}");
            }

            return pluginSetting;
        }
    }
}
