﻿using System.Collections.Generic;

namespace GeneralService.Plugin
{
    public interface IPlugin
    {
        /// <summary>
        /// interface for services
        /// </summary>
        /// <param name="metadata">metadatas : for custom/individual configuration value</param>
        /// <param name="args">parameters : parametername:parametervalue</param>
        void Run(PluginMetadata metadata, Dictionary<string, string> args = null);
    }
}
