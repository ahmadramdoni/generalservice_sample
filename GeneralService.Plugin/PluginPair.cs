﻿namespace GeneralService.Plugin
{
    public class PluginPair<T>
    {
        public T Plugin { get; set; }
        public PluginMetadata PluginMetadata { get; set; }
    }
}
