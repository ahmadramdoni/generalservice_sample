﻿using GeneralService.Plugin;
using System.Collections.Generic;
using GeneralService.Core;
using GeneralService.Core.Helper;

namespace GeneralService.SendMailSample
{
    public class Service : IPlugin
    {
        private static PluginMetadata _metadata;
        public void Run(PluginMetadata metadata, Dictionary<string, string> args = null)
        {
            _metadata = metadata;

            Send(metadata.PluginSettings.GetValue<string>("MailDestination"));
        }

        private void Send(string mailDestination)
        {
            LogHelper.LogInfo("Preparing to Send Email", _metadata);

            MailService mailService = new MailService();

            LogHelper.LogInfo("Sending email", _metadata);
            mailService.Send(mailDestination, "Sample Mail", "Sample Mail Body", null, "General Service");
            LogHelper.LogInfo("Done", _metadata);
        }
    }
}
