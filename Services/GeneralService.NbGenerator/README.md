# NewBusiness Generator
This is a plugin to generate fake new business file



## Usage

### Default

```powershell
GeneralService.exe generatenb 
```

This will generate 5 policy records with MSKV product (should be configurable on json > PluginSettings).

By default, generated file is saved in Desktop.

### Custom

Below command will generate file on D:\ directory, with 20 records total, and product MSKV, MFC, MHLA

``` powershell
GeneralService.exe generatenb Path=D:\ Total=20 Products=MSKV,MFC,MHLA
```

