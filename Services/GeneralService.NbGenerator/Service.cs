﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Axa.Insurance.Helper;
using Axa.Insurance.Model;
using Axa.Insurance.Service;
using Bogus;
using GeneralService.Core.Helper;
using GeneralService.NbGenerator.Model;
using GeneralService.Plugin;

namespace GeneralService.NbGenerator
{
    public class Service : IPlugin
    {
        private static PluginMetadata _pluginMetadata;
        private static Dictionary<string, string> _args;
        private int _totalRecords = 5;
        private string _products = "MSKV";
        //private string _paymentMethods = "DBB,DBC"; use ProductPaymentMethod
        private string _cardTypes = "VS";
        private string _destinationPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop); //default
        private bool _useToken = true;
        private string _paymentModes = "M";
        private string[] _gender = new[] { "F", "M" };

        private readonly IPlanService _planService = ContextHelper.GetObject<IPlanService>();
        private readonly IProductService _productService = ContextHelper.GetObject<IProductService>();
        private readonly IPaymentModeService _paymentModeService = ContextHelper.GetObject<IPaymentModeService>();
        
        public void Run(PluginMetadata metadata, Dictionary<string, string> args = null)
        {
            _args = args;
            _pluginMetadata = metadata;

            BindArgs();

            GenerateFile();
        }

        private void BindArgs()
        {
            if (_args != null)
            {
                _args.TryGetValue("Total", out string total);

                if (!string.IsNullOrWhiteSpace(total))
                {
                    _totalRecords = int.Parse(total);
                }

                if (_args.ContainsKey("Path"))
                {
                    _args.TryGetValue("Path", out string destinationPath);
                    if (!string.IsNullOrWhiteSpace(destinationPath))
                    {
                        _destinationPath = destinationPath;
                    }
                }

                if (_args.ContainsKey("Products"))
                {
                    _args.TryGetValue("Products", out _products);
                }

                if (_args.ContainsKey("CardTypes"))
                {
                    _args.TryGetValue("CardTypes", out _cardTypes);
                }

                if (_args.ContainsKey("UseToken"))
                {
                    _args.TryGetValue("UseToken", out string useToken);
                    
                    if (!string.IsNullOrWhiteSpace(useToken))
                    {
                        _useToken = bool.Parse(useToken);
                    }
                }

                if (_args.ContainsKey("PaymentModes"))
                {
                    _args.TryGetValue("PaymentModes", out _paymentModes);
                } 
            }

        }

        private void GenerateFile()
        {
            var fileName = $"NewBusiness_{DateTime.Now:yyyyMMddhhmmss}.txt";
            var filePath = Path.Combine(_destinationPath, fileName);

            StringBuilder contents = new StringBuilder();

            var policies = GeneratePolicy();

            contents.AppendLine("[Policy]");
            contents.Append(GeneratePolicyContent(policies));

            contents.AppendLine("[Insured]");
            contents.Append(GenerateInsuredContent(policies));

            contents.AppendLine("[Beneficiary]");
            contents.Append(GenerateBeneficiaryContent(policies));

            File.WriteAllText(filePath, contents.ToString());
        }

        private List<TextPolicy> GeneratePolicy()
        {
            var policies = new List<TextPolicy>();
            var now = DateTime.Now;

            var products = _products.Split(',');
            //var paymentMethods = _paymentMethods.Split(',');
            var cardTypes = _cardTypes.Split(',');
            var paymentModes = _paymentModes.Split(',');
            
            for (int i = 0; i < _totalRecords; i++)
            {
                try
                {
                    var faker = new Faker("id_ID");
                    var productId = faker.PickRandom(products);
                    var product = _productService.GetProduct(productId);
                    var paymentMethods = product.PaymentMethods.Select(x => x.PaymentMethodId).ToArray();
                    var policy = new TextPolicy
                    {
                        payer_sex = faker.PickRandom(_gender),
                        policy_id = $"{now:yyyyMMddhhmmss}-{i}",
                        policy_ref = $"REF{DateTime.Now:yyyyMMddhhmmss}{i}",
                        prospect_id = $"REF{DateTime.Now:yyyyMMddhhmmss}{i}",
                        product_id = productId,
                        campaign_id = "NbGenerator",
                        campaign_TBSS = "NbGenerator",
                        input = now,
                        effdt = now,
                        payer_cifno = "",
                        payer_dob = faker.Date.Between(now.AddYears(-product.MIMaxAge), now.AddYears(-product.MIMinAge)),
                        addrtype = "HA",
                        addr1 = faker.Address.SecondaryAddress(),
                        addr2 = faker.Address.StreetName(),
                        addr3 = faker.Address.StreetAddress(),
                        addr4 = faker.Address.State(),
                        city = faker.Address.City(),
                        post = faker.Address.ZipCode(),
                        phone = faker.Phone.PhoneNumber(),
                        pay_type = faker.PickRandom(paymentMethods),
                        card_type = faker.PickRandom(cardTypes),
                        bank = "1"
                    };

                    if (policy.pay_type == "DBC")
                    {
                        if (_useToken)
                        {
                            policy.acctnum = faker.Finance.Account(23);
                        }
                        else
                        {
                            policy.acctnum = faker.Finance.CreditCardNumber(Bogus.DataSets.CardType.Visa);
                            policy.ccexpdate = now.AddYears(2).ToString("MM/yy");
                            policy.paccnumber = policy.acctnum;
                            policy.paccname = policy.payer_fname;
                        }
                    }
                    else
                    {
                        policy.acctnum = faker.Finance.Account();
                        policy.paccnumber = policy.acctnum;
                        policy.paccname = policy.payer_fname;
                    }

                    policy.payer_title = policy.payer_title == "M" ? "MR" : "MRS";
                    policy.payer_fname = faker.Name.FirstName(policy.payer_sex == "M" ? Bogus.DataSets.Name.Gender.Male : Bogus.DataSets.Name.Gender.Female);
                    policy.payer_lname = faker.Name.LastName(policy.payer_sex == "M" ? Bogus.DataSets.Name.Gender.Male : Bogus.DataSets.Name.Gender.Female);

                    policy.email = faker.Internet.Email(policy.payer_fname, policy.payer_lname);
                    policy.paystatus = "0";
                    policy.bill_freq = faker.PickRandom(paymentModes);
                    policy.export = "N";
                    policy.callDate2 = now;
                    policy.paycurency = "IDR";
                    policy.payer_idtype = "KTP";
                    policy.payer_personalid = faker.Person.Random.String2(16, "0123456789");
                    policy.payer_mobilephone = faker.Person.Phone;
                    policy.status = "1";
                    policy.payer_birthplace = faker.Person.Address.City;
                    policy.payer_income = "0";
                    policy.operid = faker.Internet.UserName().ToUpper();
                    policy.sellerid = faker.Internet.UserName().ToUpper();
                    policy.spv_id = faker.Internet.UserName().ToUpper();
                    policy.atm_id = faker.Internet.UserName().ToUpper();
                    policy.tsm_id = faker.Internet.UserName().ToUpper();
                    policy.calldate = policy.callDate2;
                    policy.policy_delivery = "REQUEST HARD COPY";
                    policy.notification_delivery = "REQUEST HARD COPY";
                    policy.is_allow_datasharing = "0";
                    policy.is_allow_productoffering = "0";
                    policy.pay_type2 = policy.pay_type;
                    policy.acctnum2 = faker.Finance.Account();


                    var plans = _planService.GetAllPlans(policy.product_id).Select(p => p.PlanId);

                    policy.benefit_level = faker.PickRandom(plans); //get random plan

                    var insureds = GenerateInsureds(policy);

                    policy.premium = insureds.FirstOrDefault().premium; //currently only generate and get 1 insured
                    policy.nbi = 0;

                    policy.insureds = insureds;

                    policies.Add(policy);
                }
                catch (Exception ex)
                {
                    LogHelper.LogError($"Failed generate policy {i}, {ex.Message} {ex.StackTrace}", ex, _pluginMetadata);
                }
            }

            return policies;
        }

        private List<TextInsured> GenerateInsureds(TextPolicy policy)
        {
            if (policy is null)
            {
                throw new ArgumentNullException(nameof(policy));
            }

            var insureds = new List<TextInsured>();
            var product = _productService.GetProduct(policy.product_id);
            var discounts = product.Discounts;
            var factors = product.RatingFactors;
            float multiplier = product.PaymentModes.SingleOrDefault(p => p.PaymentModeId == policy.bill_freq).Multiplier;
            var ageDate = DateTime.Now;
            var plan = _planService.GetPlan(policy.product_id, policy.benefit_level);

            //for now, only generate 1 insured. can be improved later

            var insured = new TextInsured
            {
                policy_id = policy.policy_id,
                holder_id = "1",
                holder_type = "MI",
                holder_title = policy.payer_title,
                holder_fname = policy.payer_fname,
                holder_lname = policy.payer_lname,
                holder_sex = policy.payer_sex,
                holder_dob = policy.payer_dob,
                holder_ssn = policy.payer_personalid,
                relation = "MI",
                benefit_level = policy.benefit_level,
                holder_idtype = policy.payer_idtype,
                holder_issmoker = 0,
                holder_height = 0,
                holder_weight = 0,
                product_id = policy.product_id
            };

            if (plan != null)
            {
                int age = int.MinValue;
                string sexId = "M";
                string typeId = "MI";
                string factorId1 = null;
                string factorId2 = null;

                foreach (RatingFactor factor in factors)
                {
                    switch (factor.FactorId)
                    {
                        case "SEX":
                            sexId = insured.holder_sex;
                            break;
                        case "AGE":
                            if (insured.holder_dob.HasValue)
                                age = DateHelper.GetAge(insured.holder_dob, ageDate, product.AgeLogicId);
                            else
                                age = 0;
                            break;
                        case "INS":
                            typeId = insured.holder_type;
                            break;
                        default:
                            if (factorId1 == null)
                            {
                                factorId1 = factor.FactorId;
                            }
                            else if (factorId2 == null)
                            {
                                factorId2 = factor.FactorId;
                            }
                            break;
                    }
                }
                LogHelper.LogInfo($"{policy.payer_fname}, Product {policy.product_id}, age {age}, sex {sexId}, type {insured.holder_type}, birthdate {insured.holder_dob:yyyy-MM-dd}, plan {policy.benefit_level}", _pluginMetadata);
                List<Premium> premiums = plan.Premiums;

                Premium insuredPremium = null;
                int maxAge = 0;
                foreach (Premium premium in premiums)
                {
                    if (typeId == premium.InsuredTypeId)
                        if (sexId == premium.SexId)
                        {
                            maxAge = premium.AgeEnd;
                            if (age > int.MinValue && (premium.AgeBegin <= age && premium.AgeEnd >= age))
                            {
                                if (factorId1 == null || (factorId1 != null && (insured.rating_factor1 == premium.RatingItemId1)))
                                {
                                    if (factorId2 == null || (factorId2 != null && (insured.rating_factor2 == premium.RatingItemId2)))
                                    {
                                        insuredPremium = premium;
                                        break;
                                    }
                                }
                            }
                        }
                }

                #region insuredpremium > max ageend, 20160222
                if (insuredPremium == null)
                {
                    age = maxAge;
                    foreach (Premium premium in premiums)
                    {
                        if (typeId == premium.InsuredTypeId)
                            if (sexId == premium.SexId)
                                if (age > int.MinValue && (premium.AgeBegin <= age && premium.AgeEnd >= age))
                                {
                                    if (factorId1 == null || (factorId1 != null && (insured.rating_factor1 == premium.RatingItemId1)))
                                    {
                                        if (factorId2 == null || (factorId2 != null && (insured.rating_factor2 == premium.RatingItemId2)))
                                        {
                                            insuredPremium = premium;
                                            break;
                                        }
                                    }
                                }
                    }
                }
                #endregion

                if (insuredPremium != null)
                {
                    decimal rounded = 0;

                    insured.premium = insuredPremium.Amount;

                    rounded = Math.Ceiling((insured.premium * (decimal)multiplier));
                    //insured.Premium = insured.BasicPremium * multiplier;
                    insured.premium = rounded;
                }
                else
                {
                    throw new NotValidException("Could not find premium in premium table, based on insured's combination of plan, age, sex, insured type, rating factor 1, 2, etc");
                }
            }

            var beneficiaries = new List<TextBeneficiary>();

            beneficiaries.Add(GenerateBeneficiary(insured));

            insured.Beneficiaries = beneficiaries;

            insureds.Add(insured);

            return insureds;
        }

        private TextBeneficiary GenerateBeneficiary(TextInsured insured)
        {
            var faker = new Faker("id_ID");
            var gender = faker.Person.Gender;

            var beneficiary = new TextBeneficiary
            {
                policy_id = insured.policy_id,
                holder_id = insured.holder_id,
                bnf_id = "1",
                bnf_fname = faker.Person.FirstName,
                bnf_lname = faker.Person.LastName,
                bnf_sex = gender == Bogus.DataSets.Name.Gender.Male ? "M" : "F",
                bnf_ssn = faker.Person.Random.String2(16, "0123456789"),
                bnf_percent = "100",
                bnf_coverage = "ALL",
                bnf_relation = "DP",
                bnf_dob = faker.Person.DateOfBirth
            };

            return beneficiary;
        }

        private string GeneratePolicyContent(List<TextPolicy> policies)
        {
            StringBuilder content = new StringBuilder();

            var header = "policy_id,policy_ref,prospect_id,product_id,campaign_id,campaign_TBSS,input,effdt,payer_cifno,payer_title,payer_fname,payer_lname,payer_sex,payer_dob,addrtype,addr1,addr2,addr3,addr4,city,post,province,phone,faxphone,email,pay_type,card_type,bank,branch,acctnum,ccexpdate,paystatus,ORDER_ID,bill_freq,question1,question2,question3,question4,question5,benefit_level,premium,nbi,export,exportdate,canceldate,callDate2,paynotes,payauthcode,paytransdate,payorderno,payccnum,paycvv,payexpdate,paycurency,paycardtype,payer_idtype,payer_personalid,payer_mobilephone,payer_officephone,deliverydate,seperate_policy,status,payer_occupationid,payer_birthplace,payer_religionid,payer_income,payer_position,payer_company,operid,sellerid,spv_id,atm_id,tsm_id,pcifnumber,pcardtype,prefnumber,paccnumber,paccname,pcardnumber,record_id,calldate,phone2,payer_mobilephone2,payer_officephone2,policy_delivery,notification_delivery,is_allow_datasharing,is_allow_productoffering,customer_segment,district,subdistrict,village,pay_type2,acctnum2";

            var headerSplitted = header.Split(',');

            foreach (var item in headerSplitted)
            {
                content.Append(item);
                content.Append("\t");
            }

            content.AppendLine();

            foreach (var policy in policies)
            {
                foreach (var item in headerSplitted)
                {
                    var value = GetPropValue(policy, item);
                    var type = GetPropType(policy, item);

                    if (type == typeof(DateTime?) && value != null)
                    {
                        var dateValue = DateTime.Parse(value.ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                        content.Append(dateValue);
                    }
                    else
                    {
                        content.Append(value);
                    }
                    content.Append("\t");
                }
                content.AppendLine();
            }

            return content.ToString();
        }

        private string GenerateInsuredContent(List<TextPolicy> policies)
        {
            StringBuilder content = new StringBuilder();

            var insureds = new List<TextInsured>();

            foreach (var policy in policies)
            {
                foreach (var insured in policy.insureds)
                {
                    insureds.Add(insured);
                }
            }

            var header = "policy_id,holder_id,holder_type,holder_title,holder_fname,holder_lname,holder_sex,holder_dob,holder_ssn,relation,benefit_level,premium,holder_race,holder_idtype,holder_issmoker,holder_nationality,holder_maritalstatus,holder_occupation,holder_jobtype,holder_position,holder_height,holder_weight,uwstatus,uwlastupdate,uwapprovedate,uwprintdate,product_id,rating_factor1,rating_factor2,holder_birthplace";
            var headerSplitted = header.Split(',');

            foreach (var item in headerSplitted)
            {
                content.Append(item);
                content.Append("\t");
            }

            content.AppendLine();

            foreach (var insured in insureds)
            {
                foreach (var item in headerSplitted)
                {
                    var value = GetPropValue(insured, item);
                    var type = GetPropType(insured, item);

                    if (type == typeof(DateTime?) && value != null)
                    {
                        var dateValue = DateTime.Parse(value.ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                        content.Append(dateValue);
                    }
                    else
                    {
                        content.Append(value);
                    }
                    content.Append("\t");
                }
                content.AppendLine();
            }

            return content.ToString();
        }

        private string GenerateBeneficiaryContent(List<TextPolicy> policies)
        {
            StringBuilder content = new StringBuilder();

            var beneficiaries = new List<TextBeneficiary>();

            foreach (var policy in policies)
            {
                foreach (var insured in policy.insureds)
                {
                    foreach (var benef in insured.Beneficiaries)
                    {
                        beneficiaries.Add(benef);
                    }
                }
            }

            var header = "policy_id,holder_id,bnf_id,bnf_fname,bnf_lname,bnf_sex,bnf_ssn,bnf_bene_ind,bnf_client_type,bnf_percent,bnf_coverage,bnf_relation,bnf_dob";
            var headerSplitted = header.Split(',');

            foreach (var item in headerSplitted)
            {
                content.Append(item);
                content.Append("\t");
            }

            content.AppendLine();

            foreach (var beneficiary in beneficiaries)
            {
                foreach (var item in headerSplitted)
                {
                    var value = GetPropValue(beneficiary, item);
                    var type = GetPropType(beneficiary, item);

                    if (type == typeof(DateTime?) && value != null)
                    {
                        var dateValue = DateTime.Parse(value.ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                        content.Append(dateValue);
                    }
                    else
                    {
                        content.Append(value);
                    }
                    content.Append("\t");
                }
                content.AppendLine();
            }

            return content.ToString();
        }

        public static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        public static Type GetPropType(object src, string propName)
        {
            System.Reflection.PropertyInfo p = src.GetType().GetProperty(propName);
            Type t = p.PropertyType;
            return t;
        }
    }
}
