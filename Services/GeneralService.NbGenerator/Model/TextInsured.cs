﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralService.NbGenerator.Model
{
    public class TextInsured
    {
        public string policy_id { get; set; }
        public string holder_id { get; set; }
        public string holder_type { get; set; }
        public string holder_title { get; set; }
        public string holder_fname { get; set; }
        public string holder_lname { get; set; }
        public string holder_sex { get; set; }
        public DateTime? holder_dob { get; set; }
        public string holder_ssn { get; set; }
        public string relation { get; set; }
        public string benefit_level { get; set; }
        public decimal premium { get; set; }
        public string holder_race { get; set; }
        public string holder_idtype { get; set; }
        public int holder_issmoker { get; set; }
        public string holder_nationality { get; set; }
        public string holder_maritalstatus { get; set; }
        public string holder_occupation { get; set; }
        public string holder_jobtype { get; set; }
        public string holder_position { get; set; }
        public int holder_height { get; set; }
        public int holder_weight { get; set; }
        public string uwstatus { get; set; }
        public DateTime? uwlastupdate { get; set; }
        public DateTime? uwapprovedate { get; set; }
        public DateTime? uwprintdate { get; set; }
        public string product_id { get; set; }
        public string rating_factor1 { get; set; }
        public string rating_factor2 { get; set; }
        public string holder_birthplace { get; set; }

        public List<TextBeneficiary> Beneficiaries { get; set; }
    }
}
