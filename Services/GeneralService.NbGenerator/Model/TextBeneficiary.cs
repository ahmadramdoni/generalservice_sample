﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralService.NbGenerator.Model
{
    public class TextBeneficiary
    {
        public string policy_id { get; set; }
        public string holder_id { get; set; }
        public string bnf_id { get; set; }
        public string bnf_fname { get; set; }
        public string bnf_lname { get; set; }
        public string bnf_sex { get; set; }
        public string bnf_ssn { get; set; }
        public string bnf_bene_ind { get; set; }
        public string bnf_client_type { get; set; }
        public string bnf_percent { get; set; }
        public string bnf_coverage { get; set; }
        public string bnf_relation { get; set; }
        public DateTime? bnf_dob { get; set; }

    }
}
