﻿using System;
using System.Collections.Generic;

namespace GeneralService.NbGenerator.Model
{
    public class TextPolicy
    {
        public string policy_id { get; set; }
        public string policy_ref { get; set; }
        public string prospect_id { get; set; }
        public string product_id { get; set; }
        public string campaign_id { get; set; }
        public string campaign_TBSS { get; set; }
        public DateTime? input { get; set; }
        public DateTime? effdt { get; set; }
        public string payer_cifno { get; set; }
        public string payer_title { get; set; }
        public string payer_fname { get; set; }
        public string payer_lname { get; set; }
        public string payer_sex { get; set; }
        public DateTime? payer_dob { get; set; }
        public string addrtype { get; set; }
        public string addr1 { get; set; }
        public string addr2 { get; set; }
        public string addr3 { get; set; }
        public string addr4 { get; set; }
        public string city { get; set; }
        public string post { get; set; }
        public string province { get; set; }
        public string phone { get; set; }
        public string faxphone { get; set; }
        public string email { get; set; }
        public string pay_type { get; set; }
        public string card_type { get; set; }
        public string bank { get; set; }
        public string branch { get; set; }
        public string acctnum { get; set; }
        public string ccexpdate { get; set; }
        public string paystatus { get; set; }
        public string ORDER_ID { get; set; }
        public string bill_freq { get; set; }
        public string question1 { get; set; }
        public string question2 { get; set; }
        public string question3 { get; set; }
        public string question4 { get; set; }
        public string question5 { get; set; }
        public string benefit_level { get; set; }
        public decimal premium { get; set; }
        public float nbi { get; set; }
        public string export { get; set; }
        public string exportdate { get; set; }
        public string canceldate { get; set; }
        public DateTime? callDate2 { get; set; }
        public string paynotes { get; set; }
        public string payauthcode { get; set; }
        public string paytransdate { get; set; }
        public string payorderno { get; set; }
        public string payccnum { get; set; }
        public string paycvv { get; set; }
        public string payexpdate { get; set; }
        public string paycurency { get; set; }
        public string paycardtype { get; set; }
        public string payer_idtype { get; set; }
        public string payer_personalid { get; set; }
        public string payer_mobilephone { get; set; }
        public string payer_officephone { get; set; }
        public string deliverydate { get; set; }
        public string seperate_policy { get; set; }
        public string status { get; set; }
        public string payer_occupationid { get; set; }
        public string payer_birthplace { get; set; }
        public string payer_religionid { get; set; }
        public string payer_income { get; set; }
        public string payer_position { get; set; }
        public string payer_company { get; set; }
        public string operid { get; set; }
        public string sellerid { get; set; }
        public string spv_id { get; set; }
        public string atm_id { get; set; }
        public string tsm_id { get; set; }
        public string pcifnumber { get; set; }
        public string pcardtype { get; set; }
        public string prefnumber { get; set; }
        public string paccnumber { get; set; }
        public string paccname { get; set; }
        public string pcardnumber { get; set; }
        public string record_id { get; set; }
        public DateTime? calldate { get; set; }
        public string phone2 { get; set; }
        public string payer_mobilephone2 { get; set; }
        public string payer_officephone2 { get; set; }

        public string policy_delivery { get; set; }
        public string notification_delivery { get; set; }
        public string is_allow_datasharing { get; set; }
        public string is_allow_productoffering { get; set; }
        public string customer_segment { get; set; }
        public string district { get; set; }
        public string subdistrict { get; set; }
        public string village { get; set; }
        public string pay_type2 { get; set; }
        public string acctnum2 { get; set; }

        public List<TextInsured> insureds { get; set; }
    }
}
