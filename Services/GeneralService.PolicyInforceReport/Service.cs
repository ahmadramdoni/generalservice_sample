﻿using GeneralService.Plugin;
using System.Data;
using System.Collections.Generic;
using GeneralService.Core.Helper;
using System.IO;
using System;

namespace GeneralService.PolicyInforceReport
{
    public class Service : IPlugin
    {
        private static PluginMetadata _pluginMetadata;
        public void Run(PluginMetadata metadata, Dictionary<string, string> args = null)
        {
            _pluginMetadata = metadata;

            try
            {
                //get data  
                LogHelper.LogInfo("Getting data..", _pluginMetadata);
                var policies = GetPolicyInforce();

                //export to excel
                LogHelper.LogInfo("Export data to excel..", _pluginMetadata);
                var path = ExportToExcel(policies);

                //send email
                var enableMailNotification = _pluginMetadata.PluginSettings.GetValue<bool>("EnableMailNotification");
                var mailDestination = _pluginMetadata.PluginSettings.GetValue<string>("MailDestination");

                LogHelper.LogInfo($"MailNotification is {(enableMailNotification ? "enabled" : "disabled")}", _pluginMetadata);

                if (enableMailNotification)
                {
                    LogHelper.LogInfo($"Sending email to {mailDestination}..", _pluginMetadata);
                    MailHelper.SendMail(MailHelper.MailType.SMTP, mailDestination, "[Sample] Policy Inforce Report", "Hi, Please find attached Policy Inforce Report", new List<string> { path });
                    LogHelper.LogInfo("Mail Sent", _pluginMetadata);
                }

            }
            catch (Exception ex)
            {
                LogHelper.LogError($"{ex.Message}, {ex.StackTrace}", ex, _pluginMetadata);
            }
        }

        private DataSet GetPolicyInforce()
        {
            var policies = SqlHelper.ExecuteAsDataSet("select top 100 p.PolicyNo, c.FirstName as HolderName, p.EffectiveDate, p.TotalPremium as Premium, c.BirthDate from Policy p inner join Customer c on c.CustomerId = p.CustomerNo", CommandType.Text);

            return policies;
        }

        private string ExportToExcel(DataSet policies)
        {
            //save to My Documents > DMTM > PolicyInforceReport directory
            var pathToSave = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "DMTM", "PolicyInforceReport");
            var fileName = $"PolicyInforce_{DateTime.Now:yyyyMMddhhmmss}.xlsx";
            var finalPath = Path.Combine(pathToSave, fileName);

            LogHelper.LogInfo($"Exporting file as {finalPath}..", _pluginMetadata);

            //use DataSet extensions to export (GeneralService.Core.Helper > ExportHelper.cs)
            policies.ExportToExcel(finalPath);

            LogHelper.LogInfo($"File {finalPath} saved.", _pluginMetadata);

            return finalPath;
        }
    }
}
