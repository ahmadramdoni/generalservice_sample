# Accounting Report
This is a service to Generate Flat file for Accounting  

## Installation
Copy DMTMService.AccountingReport.dll and DMTMService.AccountingReport.json to DMTMService/Services directory.  
For scheduler setup, follow instructions on DMTMService README file.

## Configuration
You can configure this service by editing DMTMService.AccountingReport.json

### General Service Configuration
| Key | Description | Default/Sample Value |
| ---- | ----------- | ------- |
| ServiceId | Id/key used for execute this service from command line/task scheduler | DMTMService.AccountingReport |
| Name | Service Name  | Accounting Report |
| Description | Service Description | Service to generate flat file. Run everyday at 5.00 am  |
| Author | Developer of this service  | John Doe |
| ExecuteFileName | The dll file for this service | DMTMService.AccountingReport.dll |

### Accounting Service Configuration
Specific service configuration is array of key valued object in PluginSettings section
| Key | Description | Default/Sample Value |
| ---- | ----------- | ------- |
| FilePath | File path for generated flat file, if empty then its located in My Documents\DMTM\Accounting |  |
| DestinationPath* | Flat file will be copied to this directory if needed. * is sequence (1, 2, 3, ...) | D:\Shared\FlatFile\ |
| DestinationDomain* | Domain, If target path is on shared network  | S |
| DestinationUser* | User, If target path is on shared network | Admin |
| DestinationPassword* | Password, If target path is on shared network | pass |
| EnableMailNotif | Enable mail notification for success/fail notification | true |
| isCopyToSharedFolder | Enable copy flat file to shared folder. Related to *Destination** key | true |
| EnableSubFolderResult | If true, flat file will be generated in sub folder named ddmmyyyy, based on generate date | true |
| DestinationMailAddress | Mail address that received notification | helpdesk@ciptadrasoft.com |
| EnableGenerateJournalDetail | If true, journal detail (xlsx) file will be generated | false |
| MailType | SMTP/DBMail | SMTP |
| EnableUpdateSequenceNumber | If true, every time this service executed, sequence number will be updated. Sequence number used in flat file. Set to false if you need to generated Production data without updating sequence (Testing only) | true |
| GeneratedReportTypes | Comma delimeted for wich report will be generated (DCR,RPR,CLR,CR,OSR). Needed when some report is still on UAT stage. | DCR,CLR,RPR |