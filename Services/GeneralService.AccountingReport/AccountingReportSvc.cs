﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using GeneralService.Plugin;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using GeneralService.Core.Helper;
using System.Linq;

namespace GeneralService.AccountingReport
{
    public class AccountingReportSvc : IPlugin
    {
        private static DataSourceHelper _dataSourceHelper;
        private static DirectoryHelper _directoryHelper;
        private static string _patternHeader;
        private static string _patternLine;
        private static string _patternControl;
        private static IFormatProvider _formatInfo;
        private static List<PluginSetting> _pluginSettings;
        private static PluginMetadata _pluginMetadata;
        private static int _totalHeader;
        private static int _totalLine;
        private static int _sequenceNum;
        private MailHelper.MailType mailType;
        private bool isGenerateSuccess = true;
        private bool isEnableMailNotif = false;
        private bool isJournalExists = false;

        private static List<DateTime> _transDateGenerated;

        public void Run(PluginMetadata metadata, Dictionary<string, string> args = null)
        {
            try
            {
                _dataSourceHelper = new DataSourceHelper(args);
                _pluginSettings = metadata.PluginSettings;
                _directoryHelper = new DirectoryHelper(_pluginSettings);
                _pluginMetadata = metadata;
                mailType = EmailType(metadata);
                isEnableMailNotif = bool.Parse(_pluginSettings.GetValue("EnableMailNotif"));
                _transDateGenerated = new List<DateTime>();

                SetTemplatePattern();
                SetSequenceNumber();

                GenerateFiles();

                UpdateSequenceNumber();
            }
            catch (Exception ex)
            {
                isGenerateSuccess = false;
                LogHelper.LogError($"Error executing {metadata.ServiceId}, {ex.Message} || {ex.StackTrace}", ex, _pluginMetadata);

                if (isEnableMailNotif)
                {
                    LogHelper.LogInfo("Sending Mail Notification", _pluginMetadata);
                    var mailBody = GetMailBody().Replace("{ErrorMessage}", ex.Message);
                    MailHelper.SendMail(mailType, _pluginSettings.GetValue("DestinationMailAddress"), $"DMTM Accounting Notification - {(isGenerateSuccess ? "Success" : "Failed")}", mailBody, null);
                }
            }
        }

        private void GenerateFiles()
        {
            //get journal data
            var datas = _dataSourceHelper.GetJournal(_pluginSettings.GetValue("GeneratedReportTypes"));

            if (datas != null && datas.Rows != null && datas.Rows.Count > 0)
            {
                isJournalExists = true;

                LogHelper.LogInfo("Generating Flat File", _pluginMetadata);
                GenerateFlatFile(datas);

                LogHelper.LogInfo("Generating Control File", _pluginMetadata);
                GenerateControlFile();

                LogHelper.LogInfo("Generating Summary File", _pluginMetadata);
                var summaryFile = GenerateSummaryFile();

                GenerateJournalDetail();

                if (isEnableMailNotif)
                {
                    LogHelper.LogInfo("Sending Mail Notification", _pluginMetadata);
                    SendMail(summaryFile);
                }
            }
            else
            {
                LogHelper.LogInfo("No record(s) found", _pluginMetadata);
                if (isEnableMailNotif)
                {
                    LogHelper.LogInfo("Sending Mail Notification", _pluginMetadata);
                    SendMail();
                }
            }
        }

        private void GenerateFlatFile(DataTable datas)
        {
            var fileName = $"AGIDJNL4.dat";

            _directoryHelper.SaveFile(fileName, GetComments());

            SaveContent(fileName, datas);

            _directoryHelper.SaveFile(fileName, "\u001a");
        }

        private void GenerateControlFile()
        {
            var fileNameControl = $"AGIDCTL4.dat";
            var contentControl = GetControlFileContent();

            _directoryHelper.SaveFile(fileNameControl, contentControl);
        }

        private FileInfo GenerateSummaryFile()
        {
            var datas = _dataSourceHelper.GetJournalSummary();
            var folderPath = _directoryHelper.GetFolderPath();
            var fileName = $"JournalSummary_{DateTime.Now:yyyy-MM-dd}.xlsx";

            if (string.IsNullOrWhiteSpace(folderPath))
                folderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "DMTM",
                    "Accounting");

            var file = new FileInfo(Path.Combine(folderPath, fileName));
            var table = ConvertDataTableToNullable(datas);

            if (file.Exists)
                file.Delete();

            using (var pck = new ExcelPackage(file))
            {
                var ws = pck.Workbook.Worksheets.Add("Summary");
                ws.Cells["A1"].LoadFromDataTable(table, true, TableStyles.Light2);
                ws.Cells[ws.Dimension.Address].AutoFitColumns();
                pck.Save();
            }

            _directoryHelper.CopyFile(file.FullName);

            return file;
        }

        private void SendMail(FileInfo file = null)
        {
            var attachments = new List<string>();

            if (file != null)
            {
                attachments.Add(file.FullName);
            }
            
            MailHelper.SendMail(mailType, _pluginSettings.GetValue("DestinationMailAddress"), $"DMTM Accounting Notification - {(isGenerateSuccess ? "Success" : "Failed")}", GetMailBody(), attachments);
        }

        private void SetTemplatePattern()
        {
            LogHelper.LogInfo("Getting template pattern", _pluginMetadata);
            //get the template
            var template = _dataSourceHelper.GetTemplate();
            _patternHeader = template.Rows[0]["HEADER"].ToString();
            _patternLine = template.Rows[0]["BODY"].ToString();
            _patternControl = template.Rows[0]["FOOTER"].ToString();

            //custom format for write formatted template
            _formatInfo = new CustomFormatInfo();
        }

        private void SetSequenceNumber()
        {
            _sequenceNum = _dataSourceHelper.GetLastSequence();
            LogHelper.LogInfo($"Getting Flat File Sequence Number : {_sequenceNum}", _pluginMetadata);
        }

        private void UpdateSequenceNumber()
        {
            var enableUpdateSequenceNumber = bool.Parse(_pluginSettings.GetValue("EnableUpdateSequenceNumber"));

            if (enableUpdateSequenceNumber)
            {
                _sequenceNum++;
                _dataSourceHelper.UpdateSequence(_sequenceNum);
            }
        }

        private static string GetComments()
        {
            var commentPattern = "#ID {date:p,10, } BID{sequence:p,6, }";

            var values = new Dictionary<string, string>
            {
                { "date", DateTime.Now.ToString("dd/MM/yyyy") },
                { "sequence", _sequenceNum.ToString() }
            };

            var content = new StringBuilder();

            //content.Append($"#ID {DateTime.Now:dd/MM/yyyy} BID   999");
            content.Append(commentPattern.Inject(values, _formatInfo));
            content.AppendLine();

            return content.ToString();
        }

        private static void SaveContent(string fileName, DataTable datas)
        {
            for (var i = 0; i < datas.Rows.Count; i++)
            {
                var content = new StringBuilder();
                _totalHeader++;
                //save result to dictionary, <columnname, columnvalue>
                var values = new Dictionary<string, string>();
                for (var c = 0; c < datas.Columns.Count; c++)
                {
                    values.Add(datas.Columns[c].ColumnName, datas.Rows[i][c].ToString());
                }

                //format the result using template 
                var header = _patternHeader.Inject(values, _formatInfo);
                Console.WriteLine(header);

                content.Append(header);
                content.AppendLine();

                //loop through journaldetail for every journalid
                var journalIds = values["JOURNALIDs"].Split(',');
                var lines = GetContentDetail(journalIds);

                content.Append(lines);
                
                var controls = GetControl(values);
                content.Append(controls);

                _directoryHelper.SaveFile(fileName, content.ToString());

                var transDate = DateTime.ParseExact(datas.Rows[i]["JournalDate"].ToString(), "MMddyyyy", null);

                if (!_transDateGenerated.Equals(transDate))
                {
                    _transDateGenerated.Add(transDate);
                }
            }

            //return content.ToString();
        }

        private static string GetContentDetail(IEnumerable<string> journalIds)
        {
            var content = new StringBuilder();

            var fields = new List<string>();
            var values = new List<object>();

            var isPatternMapped = false;

            foreach (var j in journalIds)
            {
                var dataDetails = _dataSourceHelper.GetJournalDetail(j);

                if (!isPatternMapped)
                {
                    for (var i = 0; i < dataDetails.Columns.Count; i++)
                    {
                        var field = dataDetails.Columns[i].ColumnName;
                        var token = "{" + field + ":";

                        if (!_patternLine.Contains(token)) continue;

                        _patternLine = _patternLine.Replace(token, "{" + fields.Count + ":");
                        fields.Add(field);
                    }

                    isPatternMapped = true;
                }

                for (var dt = 0; dt < dataDetails.Rows.Count; dt++)
                {
                    _totalLine++;
                    values.Clear();

                    for (var vd = 0; vd < dataDetails.Columns.Count; vd++)
                    {
                        values.Add(dataDetails.Rows[dt][vd].ToString());
                    }
                    
                    content.AppendLine(string.Format(_formatInfo, _patternLine, values.ToArray()));
                }
            }

            return content.ToString();
        }

        private static string GetControl(IDictionary values)
        {
            var content = new StringBuilder();
            var control = _patternControl.Inject(values, _formatInfo);
            content.Append(control);
            content.AppendLine();
            return content.ToString();
        }

        private static string GetControlFileContent()
        {
            var content = new StringBuilder();

            content.Append(GetComments());
            content.AppendLine("H" + _totalHeader);
            content.AppendLine("L" + _totalLine);
            content.AppendLine("C" + _totalHeader);
            content.Append("\u001a");

            return content.ToString();
        }

        /// <summary>
        /// DataTable hasil dari executedataset (sepertinya) tidak support nullable, mengakibatkan failed saat format date saat export excel
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        private static DataTable ConvertDataTableToNullable(DataTable dataTable)
        {
            var data = new DataTable();

            var dateColumns = new List<int>();

            for (var i = 0; i < dataTable.Columns.Count; i++)
            {
                var col = dataTable.Columns[i];
                var column = new DataColumn(col.ColumnName, col.DataType);
                column.AllowDBNull = true;
                data.Columns.Add(column);

                if (col.DataType == typeof(DateTime))
                    dateColumns.Add(i);
            }

            for (var i = 0; i < dataTable.Rows.Count; i++)
            {
                var row = data.NewRow();

                for (var x = 0; x < dataTable.Columns.Count; x++)
                {
                    var value = dataTable.Rows[i][x];

                    if (dateColumns.Contains(x) && (value == null || value == DBNull.Value))
                        row[x] = DBNull.Value; //DateTime.Parse("1900-01-01 00:00:00");
                    else
                        row[x] = value;

                }

                data.Rows.Add(row);
            }

            return data;
        }

        private void GenerateJournalDetail()
        {
            bool.TryParse(_pluginSettings.GetValue("EnableGenerateJournalDetail"), out bool enableJournalDetail);

            if (enableJournalDetail)
            {
                LogHelper.LogInfo("Generating Report Detail", _pluginMetadata);
                //clr
                var datas = _dataSourceHelper.GetCLRDetail();
                var folderPath = _directoryHelper.GetFolderPath();
                var fileName = $"ReportCLR {DateTime.Now:yyyy-MM-dd}.xlsx";

                if (string.IsNullOrWhiteSpace(folderPath))
                    folderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "DMTM",
                        "Accounting");

                var file = new FileInfo(Path.Combine(folderPath, fileName));
                var table = ConvertDataTableToNullable(datas);

                if (file.Exists)
                    file.Delete();

                using (var pck = new ExcelPackage(file))
                {
                    var ws = pck.Workbook.Worksheets.Add("Claim");
                    ws.Cells["A1"].LoadFromDataTable(table, true, TableStyles.Light2);
                    ws.Cells[ws.Dimension.Address].AutoFitColumns();
                    pck.Save();
                }

                _directoryHelper.CopyFile(file.FullName);
            }
        }

        private MailHelper.MailType EmailType(PluginMetadata metadata)
        {
            var mType = MailHelper.MailType.SMTP;

            var config = metadata.PluginSettings.FirstOrDefault(x => x.Key.Equals("MailType"));
            if (config == null) return mType;

            if (config.Value.ToUpper().Equals("SMTP"))
                mType = MailHelper.MailType.SMTP;
            else if (config.Value.ToUpper().Equals("DBMAIL"))
                mType = MailHelper.MailType.DBMAIL;

            return mType;
        }

        private string GetMailBody()
        {
            string body;
            if (isGenerateSuccess)
            {
                if (isJournalExists)
                {
                    var startPeriod = _transDateGenerated.Min();
                    var endPeriod = _transDateGenerated.Max();

                    var periode = startPeriod.ToString("dd/MM/yyyy");

                    if (startPeriod != endPeriod)
                    {
                        periode = $"{startPeriod:dd/MM/yyyy} - {endPeriod:dd/MM/yyyy}";
                    }

                    body = @"<html>
	                        <body>
		                        <p>Dear User,</p>
		                        <p>Flat file for periode {periode} has been generated</p>
		                        <p>Thank You. </p>
	                        </body>
                        </html>";

                    body = body.Replace("{periode}", periode);
                }
                else
                {
                    body = @"<html>
	                        <body>
		                        <p>Dear User,</p>
		                        <p>There is no data in this periode</p>
		                        <p>Thank You. </p>
	                        </body>
                        </html>";
                }
            }
            else
            {
                body =
                    @"<html>
	                    <body>
		                    <p>Dear User,</p>
		                    <p>There is a problem when generating Flat File</p>
		                    <p style='color: #d00000;'>{ErrorMessage}</p>
		                    <p>Thank You. </p>
	                    </body>
                    </html>
                    ";
            }

            return body;
        }
    }
}
