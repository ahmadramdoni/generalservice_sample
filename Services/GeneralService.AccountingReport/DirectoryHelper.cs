﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using GeneralService.Plugin;

namespace GeneralService.AccountingReport
{
    public class DirectoryHelper
    {
        private static List<PluginSetting> _settings;
        private static string _folderPath;
        public DirectoryHelper(List<PluginSetting> settings)
        {
            _settings = settings;
            _folderPath = _settings.GetValue("FilePath");
            var enableSubFolderResult = _settings.GetValue("EnableSubFolderResult");
            bool.TryParse(enableSubFolderResult, out bool enableSubFolderResultValue);

            if (string.IsNullOrWhiteSpace(_folderPath))
            {
                _folderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "DMTM", "Accounting", enableSubFolderResultValue ? DateTime.Now.ToString("ddMMyyyy") : "");
            }
            //Add else due to AMFS Migration and change directory pointing 2022-04-27 - NSE
            else
            {
                _folderPath = Path.Combine(_folderPath, enableSubFolderResultValue ? DateTime.Now.ToString("ddMMyyyy") : "");
            }

            if (!Directory.Exists(_folderPath))
                Directory.CreateDirectory(_folderPath);
        }

        public string GetFolderPath()
        {
            return _folderPath;
        }
        
        public void SaveFile(string fileName, string content)
        {
            var filePath = Path.Combine(_folderPath, fileName);

            File.AppendAllText(filePath, content);

            //if (isAppend)
            //    File.AppendAllText(filePath, content);
            //else
            //    File.WriteAllText(filePath, content);

            CopyFile(filePath);
        }

        public void CopyFile(string filePath)
        {
            //copy to sharing folder
            var isCopyToSharedFolder = _settings.GetValue("isCopyToSharedFolder");

            bool.TryParse(isCopyToSharedFolder, out bool isCopyToSharedFolderValue);

            if (!isCopyToSharedFolderValue)
            {
                return;
            }

            //get total path from json file
            var totalPath = _settings.Count(s => s != null && s.Key.ToLower().Contains("destinationpath"));

            var file = new FileInfo(filePath);

            //loop through path list
            for (var i = 1; i <= totalPath; i++)
            {
                var destinationDir = _settings.GetValue($"DestinationPath{i}");
                var destinationDomain = _settings.GetValue($"DestinationDomain{i}");
                var destinationUserName = _settings.GetValue($"DestinationUser{i}");
                var destinationPassword = _settings.GetValue($"DestinationPassword{i}");

                if (string.IsNullOrWhiteSpace(destinationDir)) continue;

                
                if (string.IsNullOrWhiteSpace(destinationUserName) && string.IsNullOrWhiteSpace(destinationPassword) &&
                    string.IsNullOrWhiteSpace(destinationDomain))
                {
                    if (!Directory.Exists(destinationDir))
                        Directory.CreateDirectory(destinationDir);

                    var destinationPath = Path.Combine(destinationDir, file.Name);
                    
                    file.CopyTo(destinationPath, true);
                }
                else
                {
                    destinationDir = "\\" + destinationDir;

                    if (!Directory.Exists(destinationDir))
                        Directory.CreateDirectory(destinationDir);

                    var destinationPath = Path.Combine(destinationDir, file.Name);

                    using (new Impersonator(destinationUserName, destinationDomain, destinationPassword))
                    {
                        file.CopyTo(destinationPath, true);
                    }
                }

                #region alternative solution

                //alternative solution
                //if (!string.IsNullOrWhiteSpace(destinationUserName) &&
                //    !string.IsNullOrWhiteSpace(destinationPassword))
                //{
                //    var ip = "";
                //    var match = Regex.Match(destinationPath, @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}");
                //    if (match.Success)
                //    {
                //        ip = match.Value;
                //    }

                //    var networkCredential = new NetworkCredential(destinationUserName, destinationPassword);
                //    var credentialCache =
                //        new CredentialCache {{new Uri("//" + ip), "Basic", networkCredential}};

                //}

                //file.CopyTo(destinationPath, true);

                #endregion
            }

        }
    }
}
