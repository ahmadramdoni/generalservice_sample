﻿using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace GeneralService.AccountingReport
{
    public class DataSourceHelper
    {
        private static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
        private static Dictionary<string, string> _args;

        public DataSourceHelper(Dictionary<string, string> args = null)
        {
            _args = args;
        }

        public DataTable GetTemplate()
        {
            var dataTable = new DataTable();
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                const string query = "select * from template where templateid = 'JFLAT01'";
                using (var command = new SqlCommand(query, sqlConnection))
                {
                    var da = new SqlDataAdapter(command);
                    da.Fill(dataTable);
                    sqlConnection.Close();
                    da.Dispose();
                }
            }

            return dataTable;
        }

        public DataTable GetJournal(string reportTypes = "")
        {
            var result = new DataSet();

            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();

                using (var command = new SqlCommand("sp_Rpt_GetJournal", sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    if (_args != null)
                    {
                        foreach (var arg in _args)
                        {
                            command.Parameters.AddWithValue($"@{arg.Key}", arg.Value);
                        }
                    }
                    else
                    {
                        //task running automatically on the next day of postdate
                        command.Parameters.AddWithValue("@TransDate", DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd"));
                    }

                    //generate only selected report based on config
                    command.Parameters.AddWithValue("@ReportType", reportTypes);

                    var da = new SqlDataAdapter(command);
                    da.SelectCommand.CommandTimeout = 600; //set timeout to 10 minutes
                    da.Fill(result);
                    sqlConnection.Close();
                    da.Dispose();
                }
            }

            return result.Tables[0];
        }

        public DataTable GetJournalDetail(string journalId)
        {
            var result = new DataSet();
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();
                using (var command = new SqlCommand("sp_Rpt_GetJournalDetail", sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@JournalId", journalId);

                    var da = new SqlDataAdapter(command);
                    da.SelectCommand.CommandTimeout = 600;
                    da.Fill(result);
                    sqlConnection.Close();
                    da.Dispose();
                }
            }

            return result.Tables[0];
        }

        public DataTable GetJournalSummary()
        {
            var result = new DataSet();
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();

                using (var command = new SqlCommand("sp_Rpt_GetJournalSummary", sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    if (_args != null)
                    {
                        foreach (var arg in _args)
                        {
                            command.Parameters.AddWithValue($"@{arg.Key}", arg.Value);
                        }
                    }
                    else
                    {
                        //command.Parameters.AddWithValue("@TransDate", DateTime.Now.ToString("yyyy-MM-dd"));
                        //task running automatically on the next day of postdate
                        command.Parameters.AddWithValue("@TransDate", DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd"));
                    }

                    var da = new SqlDataAdapter(command);
                    da.SelectCommand.CommandTimeout = 600;
                    da.Fill(result);
                    sqlConnection.Close();
                    da.Dispose();
                }
            }

            return result.Tables[0];
        }

        public DataTable GetCLRDetail()
        {
            var result = new DataSet();
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();

                using (var command = new SqlCommand("sp_RptCLR", sqlConnection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    if (_args != null)
                    {
                        foreach (var arg in _args)
                        {
                            command.Parameters.AddWithValue($"@{arg.Key}", arg.Value);
                        }
                    }
                    else
                    {
                        //command.Parameters.AddWithValue("@TransDate", DateTime.Now.ToString("yyyy-MM-dd"));
                        //task running automatically on the next day of postdate
                        command.Parameters.AddWithValue("@TransDate", DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd"));
                    }

                    var da = new SqlDataAdapter(command);
                    da.SelectCommand.CommandTimeout = 600;
                    da.Fill(result);
                    sqlConnection.Close();
                    da.Dispose();
                }
            }

            return result.Tables[0];
        }

        public int GetLastSequence()
        {
            int sequence = 0;

            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();

                var query = "select NUMBER from sequence where CLASSNAME = 'FlatfileSeq'";

                using (var command = new SqlCommand(query, sqlConnection))
                {
                    command.CommandType = CommandType.Text;

                    int.TryParse(command.ExecuteScalar().ToString(), out sequence);
                }
            }

            return sequence;
        }

        public void UpdateSequence(int newSequnce)
        {
            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();

                var query = $"update SEQUENCE set NUMBER = {newSequnce} where CLASSNAME = 'FlatfileSeq'";

                using (var command = new SqlCommand(query, sqlConnection))
                {
                    command.CommandType = CommandType.Text;

                    command.ExecuteNonQuery();
}
            }
        }
    }
}
