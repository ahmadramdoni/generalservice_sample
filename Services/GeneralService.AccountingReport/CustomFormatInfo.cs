﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneralService.AccountingReport
{
    public class CustomFormatInfo : IFormatProvider, ICustomFormatter
    {
        #region ICustomFormatter Members

        private static object FormatTn(object value, string format)
        {
            var args = format.Split(',');

            var result = ((string)value).Replace(",", "").Replace(".", "");

            return result;
        }

        private static object FormatP(object value, string format)
        {
            var args = format.Split(',');
            var length = int.Parse(args[1]);

            var result = value.ToString();

            result = length > 0 ? result.PadLeft(length, args[2][0]) : result.PadRight(length * -1, args[2][0]);

            length = Math.Abs(length);

            if (result.Length > length) result = result.Substring(0, length);

            return result;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if (arg == null)
            {
                throw new ArgumentNullException("Argument cannot be null");
            }

            string[] formats;

            if (format != null)
            {
                formats = format.Split(':');
            }
            else
            {
                return arg.ToString();
            }

            //string pattern = "{AccountNo:p,19,x}{CustName:p,-25}DR{Refference:p,-40}0000{BillingDate}{Amount,-15:n2}STANDING INSTR PAYMENT";
            //string pattern = "{AccountNo:p,19,0}{CustName:p,-25, }DR{PolicyId:p,-40, }0000{Tommorow:ddMMyy}{Amount:#.00:tn:p,17,0}STANDING INSTR PAYMENT";

            foreach (var f in formats)
            {
                if (f.StartsWith("tn"))
                {
                    arg = FormatTn(arg, f);
                }

                arg = f.StartsWith("p") ? FormatP(arg, f) : string.Format("{0:" + f + "}", arg);
            }

            return arg.ToString();
        }

        #endregion

        #region IFormatProvider Members

        public object GetFormat(Type formatType)
        {
            return typeof(ICustomFormatter) == formatType ? this : null;
        }

        #endregion
    }
}
