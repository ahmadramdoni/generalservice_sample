﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using GeneralService.Core.Helper;
using GeneralService.Plugin;

namespace GeneralService.Decryptor
{
    public class Service : IPlugin
    {
        private const string CSPNAME = "Microsoft Base Cryptographic Provider v1.0";
        private static PluginMetadata _pluginMetadata;
        private static Dictionary<string, string> _args;
        private string _encryptKey;
        private string _filePath;
        
        public void Run(PluginMetadata metadata, Dictionary<string, string> args = null)
        {
            _pluginMetadata = metadata;
            _args = args;

            try
            {
                Validate();
                BindArgs();
                Decrypt();
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex.Message, ex, _pluginMetadata);
            }
        }

        private void Validate()
        {
            var args = "";
            foreach (var item in _args)
            {
                args += $"{item.Key} {item.Value}, ";
            }

            LogHelper.LogInfo(args, _pluginMetadata);

            if (_args is null)
            {
                throw new Exception("Empty args");
            }
            else
            {
                if (!_args.ContainsKey("FilePath"))
                {
                    throw new Exception("Missing args FilePath");
                }
                else
                {
                    if (!File.Exists(_args["FilePath"]))
                    {
                        throw new Exception("File not found");
                    }
                }
            }
        }

        private void BindArgs()
        {
            _encryptKey = _pluginMetadata.PluginSettings.GetValue<string>("EncryptKey");

            if (_args != null)
            {
                if (_args.ContainsKey("EncryptKey"))
                {
                    _args.TryGetValue("EncryptKey", out _encryptKey);
                }
                if (_args.ContainsKey("FilePath"))
                {
                    _args.TryGetValue("FilePath", out _filePath);
                }
            }
        }

        private void Decrypt()
        {
            LogHelper.LogInfo($"Trying to decrypt with key {_encryptKey} ...");

            byte[] arData = File.ReadAllBytes(_args["FilePath"]);

            CspParameters cspParams = new CspParameters(1, CSPNAME);
            PasswordDeriveBytes deriveBytes = new PasswordDeriveBytes(_encryptKey, null, "SHA -1", 1, cspParams);
            byte[] rgbIV = new byte[8];
            byte[] key = deriveBytes.CryptDeriveKey("RC2", "SHA1", 0, rgbIV);

            RC2CryptoServiceProvider provider = new RC2CryptoServiceProvider();
            // 
            provider.Key = key;
            provider.IV = rgbIV;
            // 
            ICryptoTransform transform = provider.CreateDecryptor();
            // 
            byte[] decyptedBlob = transform.TransformFinalBlock(arData, 0, arData.Length - 1);

            var result = Encoding.Unicode.GetString(decyptedBlob).Remove(Encoding.Unicode.GetString(decyptedBlob).Length - 1);

            var path = Path.GetDirectoryName(_filePath);
            var fileName = $"{Path.GetFileNameWithoutExtension(_filePath)}_decrypted.txt";

            var decryptedFilePath = Path.Combine(path, fileName);

            File.WriteAllText(decryptedFilePath, result);

            LogHelper.LogInfo($"Decrypt success, Decrypted File Path : {decryptedFilePath}", _pluginMetadata);
        }
    }
}
