# Decryptor
This is a plugin to decrypt new business file



## Configuration

You can configure this plugin by editing GeneralService.Decryptor.json

| Key | Description | Default/Sample Value |
| ---- | ----------- | ------- |
| EncryptKey | Key used for decrypting | axalife |



## Usage

Run below command line arguments, modify path arguments to the file that need to be decrypted.

```powershell
GeneralService.exe decrypt FilePath=D:\EncryptedFilePath.txt
```

The file will be decrypted with default key that setup on GeneralService.Decryptor.json.  

You can override the key by passing EncryptKey argument

``` powershell
GeneralService.exe decrypt FilePath=D:\EncryptedFilePath.txt EncryptKey=axalife
```