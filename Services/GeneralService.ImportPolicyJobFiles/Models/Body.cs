﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralService.ImportPolicyJobFiles.Models
{
    public class Body
    {
        public string transactionId { get; set; }
        public string operation { get; set; }
        public string appID { get; set; }
        public string entity { get; set; }
        public string service { get; set; }
        public Customer Customer { get; set; }
        public string status { get; set; }
        public ExceptionResp exception { get; set; } 
    }
}
