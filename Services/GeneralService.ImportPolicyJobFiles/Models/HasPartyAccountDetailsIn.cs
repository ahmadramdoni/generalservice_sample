﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralService.ImportPolicyJobFiles.Models
{
    public class HasPartyAccountDetailsIn
    {
        public string payorFirstNM { get; set; }
        public string accountBankCD { get; set; } 
        public string accountNO { get; set; } 
    }
}
