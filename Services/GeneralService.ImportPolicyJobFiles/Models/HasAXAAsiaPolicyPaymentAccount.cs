﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralService.ImportPolicyJobFiles.Models
{
    public class HasAXAAsiaPolicyPaymentAccount
    { 
        public List<HasAXAAsiaPartyAccount> hasAXAAsiaPartyAccount { get; set; }

    }
}
