﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralService.ImportPolicyJobFiles.Models
{
    public class HasPolicyAccount
    {
        public string paidToDT { get; set; }
        public string validFromDttm { get; set; }
        public string termProcessDt { get; set; }
        public string policyRK { get; set; }
        public string policyStatusCD { get; set; }
        public string agencyBinderNo { get; set; }
        public string mailingResponsibiltyCd { get; set; }
        public string paymentMethodCD { get; set; }
        public string paymentModeCD { get; set; }
        public string paymentStatusCd { get; set; }
        public string issueBranchCD { get; set; }
        public string otherInsuredInd { get; set; }
        public string premiumAMT { get; set; } 
        public List<HasAXAAsiaPolicyPaymentAccount> hasAXAAsiaPolicyPaymentAccount { get; set; }
        public IsAssociatedWithIssuer isAssociatedWithIssuer { get; set; }
        public List<HasDetailsOfRisksIn> hasDetailsOfRisksIn { get; set; }
        public List<IsAssociatedWithLifeInsuranceProduct> isAssociatedWithLifeInsuranceProduct { get; set; }
        public string coveredSalaryAmt { get; set; }
        public string expSalaryGrowthRt { get; set; }
        public string applicationRk { get; set; }
        public string policyReserveAmt { get; set; }
        public string premiumIndexRt { get; set; }
        public string maturityDt { get; set; }
        public string bonusPaidUpToDt { get; set; }
        public string centralTaxNo { get; set; }
        public string validToDttm { get; set; }
        public string mailingResponsibilityCd { get; set; } 
        public string policyTypeCd { get; set; }
        public string nextReEntryDt { get; set; }
        public string policyEffectiveDTTM { get; set; }
        public string policyNO { get; set; } 
        public string ePolicy { get; set; }
        public List<HasDocumentDetailsIn> hasDocumentDetailsIn { get; set; }
        
    }
}
