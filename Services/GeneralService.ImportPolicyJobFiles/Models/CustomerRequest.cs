﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralService.ImportPolicyJobFiles.Models
{
    public class CustomerRequest
    {
        public List<HaveCommunicatedMessage> haveCommunicatedMessage { get; set; }
    }
}
