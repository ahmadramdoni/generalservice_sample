﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralService.ImportPolicyJobFiles.Models
{
    public class HasDetailsOfRisksIn
    {
        public string occupationClassCd { get; set; }
        public string memberPlanNo { get; set; }
        public string existingInsuranceInd { get; set; }
        public string insuredRK { get; set; }
        public string coveredSalaryAmt { get; set; }
        public string expSalaryGrowthRt { get; set; }
        public List<HasPersonalDetailsIn> hasPersonalDetailsIn { get; set; }
        public string rejectionInd { get; set; }
        public string relationshipToOwnerCD { get; set; }
        public string insuredID { get; set; }

    }
}
