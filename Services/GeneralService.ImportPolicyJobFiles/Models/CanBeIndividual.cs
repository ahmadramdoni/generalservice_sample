﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralService.ImportPolicyJobFiles.Models
{
    public class CanBeIndividual
    {
        public string centralTaxNo { get; set; }
        public List<HasAddressesIn> hasAddressesIn { get; set; }
        public List<HasPartyAccountDetailsIn> hasPartyAccountDetailsIns { get; set; }
        public string birthDT { get; set; }
        public string birtPlace { get; set; }
        public string genderCD { get; set; }
        public string firstNM { get; set; }
        public string socialSecurityNO { get; set; }
        public string occupation { get; set; }
        public string documentID { get; set; }
        public HasPartyAccountDetailsIn hasPartyAccountDetailsIn { get; set; }
        public string nationalityCD { get; set; }
        public string idInd { get; set; }
        public string incomeLVL { get; set; }
        public string stdOccupationCD { get; set; }
        public string distributionCD { get; set; }
        public string religion { get; set; }
        public string maritalStatusCD { get; set; }
    }
}
