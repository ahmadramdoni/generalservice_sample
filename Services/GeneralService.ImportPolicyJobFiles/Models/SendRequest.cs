﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralService.ImportPolicyJobFiles.Models
{
    public class SendRequest
    {
        public Header Header { get; set; }
        public Body Body { get; set; }
    }
}
