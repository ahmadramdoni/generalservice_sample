﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralService.ImportPolicyJobFiles.Models
{
    public class HasAXAAsiaPartyAccount
    {
        public string accountNO { get; set; }
        public string partyAccountNM { get; set; }
        public string cardExpirationDT { get; set; }
        public string creditCardNO { get; set; }
        public string cardType { get; set; }
        public string bankName { get; set; }
        public string cardTokenNumber { get; set; }

    }
}
