﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralService.ImportPolicyJobFiles.Models
{
    public class IsAssociatedWithIssuer
    {
        public string issuerName { get; set; }
        public string issuerAppID { get; set; }
        public string issuedNO { get; set; }

    }
}
