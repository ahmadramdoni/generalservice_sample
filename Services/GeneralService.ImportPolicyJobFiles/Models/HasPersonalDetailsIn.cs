﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralService.ImportPolicyJobFiles.Models
{
    public class HasPersonalDetailsIn
    {
        public string centralTaxNo { get; set; }
        public string birthPlace { get; set; }
        public string birthDT { get; set; }
        public string genderCD { get; set; }
        public string socialSecurityNO { get; set; }
        public string documentTypeCD { get; set; }
        public string firstNM { get; set; }
        public List<HasAddressesIn> hasAddressesIn { get; set; }
        public string firstName { get; set; }
        public string idInd { get; set; }
        public string incomeLVL { get; set; }
        public string stdOccupationCD { get; set; }
    }
}
