﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralService.ImportPolicyJobFiles.Models
{
    public class HasAddressesIn
    {
        public string addressTypeCD { get; set; }
        public string mobilePhoneNO { get; set; }
        public string telephoneNO { get; set; }
        public string officeTelNo { get; set; }
        public string addressLine1 { get; set; } 
        public string addressLine2 { get; set; }
        public string addressLine3 { get; set; }
        public string addressLine4 { get; set; }
        public string countryCD { get; set; }
        public string emailAddress { get; set; }
        public string cityNM { get; set; }
        public string postalCD { get; set; }
        public string provinceNm { get; set; }
        public string officeAddressLine1 { get; set; }
        public string officeAddressLine2 { get; set; }
        public string officeAddressLine3 { get; set; }
        public string officeAddressLine4 { get; set; }
    }
}
