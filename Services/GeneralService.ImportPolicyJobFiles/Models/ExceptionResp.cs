﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralService.ImportPolicyJobFiles.Models
{
    public class ExceptionResp
    {
        public string message { get; set; }
        public string backEndError { get; set; }
        public string code { get; set; } 
    }
}
