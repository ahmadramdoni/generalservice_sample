﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralService.ImportPolicyJobFiles.Models
{
    public class Customer
    {
        public string sourceCInd { get; set; }
        public CanBeIndividual canBeIndividual { get; set; }
        public List<HasPolicyAccount> hasPolicyAccount { get; set; }

    }
}
