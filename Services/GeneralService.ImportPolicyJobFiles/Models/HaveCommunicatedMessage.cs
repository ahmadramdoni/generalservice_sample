﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralService.ImportPolicyJobFiles.Models
{
    public class HaveCommunicatedMessage
    {
        public string sender { get; set; }
        public string target { get; set; }
        public string message { get; set; }
        public string deliveryStatusDesc { get; set; }
        public string messageID { get; set; }
        public string deliveryStatusCD { get; set; }
        public string bizContext { get; set; }
        public string batchName { get; set; }
        public string triggeredBy { get; set; }
        public string channelID { get; set; }
        public string replyTo { get; set; }
        public string additionalRk { get; set; }
        public string deliveryDT { get; set; }
        public string chargeAmt { get; set; }
    }
}
