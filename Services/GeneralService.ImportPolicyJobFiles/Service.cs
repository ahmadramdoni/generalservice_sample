﻿using GeneralService.Plugin;
using System.Collections.Generic;
using GeneralService.Core.Helper;
using System;
using System.IO;
using Axa.Insurance.Service;
using Axa.Insurance.Model;
using Axa.Insurance.Helper;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Configuration;
using System.Data.SqlTypes;
using System.Globalization;
using GeneralService.ImportPolicyJobFiles.Helper;
using OfficeOpenXml;
using System.Linq;
using GeneralService.ImportPolicyJobFiles.Models;
using System.Threading.Tasks;
using Axa.Insurance.Persistence;

namespace GeneralService.ImportPolicyJobFiles
{
    public class Service : IPlugin
    {
        private IBatchService _batchService = ContextHelper.GetObject<IBatchService>();
        private IBatchPersistence _batchPersistence = ContextHelper.GetObject<IBatchPersistence>();
        private IBatchDetailPersistence _batchDetailPersistence = ContextHelper.GetObject<IBatchDetailPersistence>();
        private IJobFilesService _jobFilesService = ContextHelper.GetObject<IJobFilesService>();
        private IJobFileDetailService _jobFileDetailsService = ContextHelper.GetObject<IJobFileDetailService>();
        private IPolicyService _policyService = ContextHelper.GetObject<IPolicyService>();
        private IPolicyHistoryService _historyService = ContextHelper.GetObject<IPolicyHistoryService>();
        private ICustomerService _customerService = ContextHelper.GetObject<ICustomerService>();
        private IAddressService _addressService = ContextHelper.GetObject<IAddressService>();
        private IOccupationService _occupationService = ContextHelper.GetObject<IOccupationService>();
        private IProductService _productServices = ContextHelper.GetObject<IProductService>();
        private ISequencePersistence _sequencePersistence = ContextHelper.GetObject<ISequencePersistence>();

        const string LineSep = "\r\n";
        const string FieldSep = "\t";
        private int total = 0;
        private string _productId = string.Empty;
        private string _statusId = string.Empty;
        private string _jobfileId = string.Empty;
        private string content = string.Empty;
        private string messageError = string.Empty;
        private string successFolder = "";
        private string failedFolder = "";
        private string targetDirectory = "";
        private string uploadBy = "";
        private string _batchId = "";
        private int recordLimit = 0;
        private int recordFiles = 0;
        private int startRecord = 0;
        private Database _database = DatabaseFactory.CreateDatabase();
        private static PluginMetadata _pluginMetadata;
        private APIHelper _apiHelper;
        private Product _product;
        FileHelper fileservice = new FileHelper();
        ContextHelper helper = new ContextHelper();
        private static Dictionary<string, string> _args;
        private string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        public void Run(PluginMetadata metadata, Dictionary<string, string> args = null)
        {
            try
            {
                _args = args;
                _pluginMetadata = metadata;
                BindArgs();
                _apiHelper = new APIHelper(_pluginMetadata);

                uploadBy = _pluginMetadata.PluginSettings.GetValue<string>("UploadBy");
                successFolder = _pluginMetadata.PluginSettings.GetValue<string>("SuccessFolder");
                failedFolder = _pluginMetadata.PluginSettings.GetValue<string>("FailedFolder");
                targetDirectory = _pluginMetadata.PluginSettings.GetValue<string>("DestinationFolder");
                recordLimit = _pluginMetadata.PluginSettings.GetValue<int>("MaxRecordProcessedPerBatch") > 0 ? _pluginMetadata.PluginSettings.GetValue<int>("MaxRecordProcessedPerBatch") : 10000;
                recordFiles = _pluginMetadata.PluginSettings.GetValue<int>("MaxRecordFilesSelected") > 0 ? _pluginMetadata.PluginSettings.GetValue<int>("MaxRecordFilesSelected") : 1;
                startRecord = _pluginMetadata.PluginSettings.GetValue<int>("StartRecord") >= 0 ? _pluginMetadata.PluginSettings.GetValue<int>("StartRecord") : 1;

                bool autoGenerate = _pluginMetadata.PluginSettings.GetValue<bool>("AutoGenerateJobFiles");
                if (autoGenerate)
                {
                    GenerateJobFiles();
                }

                LogHelper.LogInfo($"ProductId : " + _productId + ", Status : " + _statusId + ", JobFileId : " + _jobfileId, _pluginMetadata);
                ProcessDirectory();
            }
            catch (Exception ex)
            {
                LogHelper.LogError($"Error executing {metadata.ServiceId}, {ex.Message} || {ex.StackTrace}", ex, _pluginMetadata);
            }
        }
        private void BindArgs()
        {
            if (_args != null)
            {
                if (_args.ContainsKey("JobFileId"))
                {
                    _args.TryGetValue("JobFileId", out _jobfileId);
                    _jobfileId = _jobfileId.Trim();
                }
                if (_args.ContainsKey("ProductId"))
                {
                    _args.TryGetValue("ProductId", out _productId);
                    _productId = _productId.Trim();
                }
                if (_args.ContainsKey("Status"))
                {
                    _args.TryGetValue("Status", out _statusId);
                    _statusId = _statusId.Trim();
                }
            }

        }

        protected void ProcessDirectory()
        {
            FileHelper fileservice = new FileHelper();

            List<Criteria> criterias = new List<Criteria>();
            List<JobFiles> jobFiles = new List<JobFiles>();
            //criterias.Add(new Criteria("Query", " p.Status IN ('Processing','New')", Comparison.None));
            try
            {
                if (!string.IsNullOrEmpty(_productId) && !string.IsNullOrEmpty(_statusId))
                {
                    criterias.Add(new Criteria("p.ProductId", _productId, Comparison.Equal));
                    criterias.Add(new Criteria("p.Status", _statusId, Comparison.Equal));

                }
                else if (!string.IsNullOrEmpty(_jobfileId) && !string.IsNullOrEmpty(_statusId))
                {
                    criterias.Add(new Criteria("p.JobFileId", _jobfileId, Comparison.Equal));
                    criterias.Add(new Criteria("p.Status", _statusId, Comparison.Equal));
                }
                else if (!string.IsNullOrEmpty(_productId))
                {
                    criterias.Add(new Criteria("p.JobFileId", _jobfileId, Comparison.Equal));
                    criterias.Add(new Criteria("p.Status", "New", Comparison.Equal));
                }
                else
                {
                    throw new Exception($"Error executing ProductId or Status or JobFileId is empty");
                }

                jobFiles = _jobFilesService.FindJobFiles(criterias, startRecord, recordFiles, "p.CreateDate asc");

                LogHelper.LogInfo($"job files found = " + jobFiles.Count + " records", _pluginMetadata);

                if (jobFiles != null && jobFiles.Count > 0)
                {
                    foreach (var jobFile in jobFiles)
                    {
                        LogHelper.LogInfo($"processing job file with id = " + jobFile.JobFileId, _pluginMetadata);

                        string filename = jobFile.FileName;
                        string fullPath = Path.Combine(targetDirectory, filename);

                        //if (Path.GetExtension(filename) == ".txt" && File.Exists(fullPath))
                        if (File.Exists(fullPath))
                        {
                            LogHelper.LogInfo($"Process : " + jobFile.JobFileId + " FileName : " + filename + " Start", _pluginMetadata);

                            try
                            {
                                switch (jobFile.ProductId.ToLower())
                                {
                                    case "manpro":
                                        ProcessManpro(fullPath, jobFile);
                                        break;
                                    case "trmidr":
                                    case "tabins_usd":
                                        //ProcessMTR(fullPath, jobFile);
                                        ProcessMTRRevamp(fullPath, jobFile);
                                        break;
                                    default:
                                        if (jobFile.ProductId.ToLower().Contains("clp"))
                                        {
                                            ProcessCLP(fullPath, jobFile);
                                        }
                                        break;
                                }
                            }
                            catch (System.Exception ex)
                            {
                                fileservice.MoveFile(fullPath, failedFolder);
                                LogHelper.LogError($"Error executing {_pluginMetadata.ServiceId}, {ex.Message} || {ex.StackTrace}", ex, _pluginMetadata);
                            }
                            LogHelper.LogInfo($"Process : " + jobFile.JobFileId + " FileName : " + filename + " Finish", _pluginMetadata);
                        }
                        else if (!string.IsNullOrEmpty(jobFile.Status) && jobFile.Status.ToLower() == "done")
                        {
                            try
                            {
                                Database database = DatabaseFactory.CreateDatabase();
                                DbCommand cmd = database.GetStoredProcCommand("sp_UPLOAD_GENERALSERVICETODMTM");

                                cmd.CommandTimeout = 0;
                                database.ExecuteNonQuery(cmd);
                            }
                            catch (Exception ex)
                            {
                                LogHelper.LogError($"Invalid ExecuteNonQuery : " + ex.Message + " " + ex.StackTrace + " " + ex.Source, ex, _pluginMetadata);
                            }
                            /*
                            SqlClass ObjSql = new SqlClass();

                            using (SqlConnection conn = ObjSql.OpenSqlConn())
                            {
                                try
                                {
                                    using (var command = new SqlCommand($"EXEC sp_UPLOAD_GENERALSERVICETODMTM", conn))
                                    {
                                        command.CommandType = CommandType.Text;

                                        command.ExecuteNonQuery();
                                    }
                                    ObjSql.CloseSqlConn(conn);
                                }
                                catch (Exception ex)
                                {
                                    ObjSql.CloseSqlConn(conn);
                                    //throw new Exception("Invalid ExecuteNonQuery : " + ex.Message);
                                    LogHelper.LogError($"Invalid ExecuteNonQuery : " + ex.Message + " " + ex.StackTrace + " " + ex.Source, ex, _pluginMetadata);
                                }
                            }
                            */

                        }
                        else
                        {
                            throw new Exception($"format file is not .txt or not exist");
                        }
                    }
                }
                else
                {
                    throw new Exception($"jobFiles is Empty");
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex.Message + ex.StackTrace, ex, _pluginMetadata);
            }

        }
        private void GenerateJobFiles()
        {
            LogHelper.LogInfo("GenerateJobFiles", _pluginMetadata);
            var files = Directory.GetFiles(targetDirectory);

            foreach (var file in files)
            {
                FileInfo fileInfo = new FileInfo(file);
                LogHelper.LogInfo($"Filename {fileInfo.Name}", _pluginMetadata);

                var batch = CreateBatch(file);

                var jobFiles = new JobFiles
                {
                    JobFileId = NextJobFileId(),
                    CreateDate = DateTime.Now,
                    UserId = uploadBy, // "AutoImport";
                    FileName = fileInfo.Name,
                    FileSize = fileInfo.Length,
                    FileCreationDate = fileInfo.CreationTime.ToString("yyyy-MM-dd hh:mm:ss"),
                    Status = "New",
                    JobType = "UPL"
                };
                _jobFilesService.AddJobFiles(jobFiles);
            }
        }
        private Batch CreateBatch(string filePath)
        {
            List<BatchDetail> batches = new List<BatchDetail>();

            string batchDesc = string.Format($"Src: Manpro, File: {Path.GetFileName(filePath)} Succeed Upload, file will be processed in the background");

            string typeId = "FNM";

            BatchDetail detail = new BatchDetail
            {
                Description = "Upload File",
                Remark = "File Manpro",
                TypeId = typeId
            };

            batches.Add(detail);

            Batch batch = new Batch
            {
                CreateBy = "AutoImport",
                ModifyBy = "AutoImport",
                Description = batchDesc,
                TypeId = "FNM",
                Details = batches
            };
            return _batchService.AddBatch(batch);
        }
        public string NextJobFileId()
        {
            return SqlHelper.ExecuteScalar("select dbo.NextJobFileId()", System.Data.CommandType.Text).ToString();
        }
        protected void ProcessManpro(string filePath, JobFiles jobFile)
        {

            Dictionary<string, Policy> policies = new Dictionary<string, Policy>();
            Dictionary<string, BatchDetail> batches = new Dictionary<string, BatchDetail>();

            content = File.ReadAllText(filePath);

            Batch batch = new Batch();
            //if (!string.IsNullOrEmpty(jobFile.Remark))
            //{
            //    _batchId = jobFile.Remark;
            //}
            BatchDetail detail = new BatchDetail();
            try
            {
                if (!string.IsNullOrEmpty(jobFile.Status) && jobFile.Status.ToLower() == "new")
                {
                    jobFile.Status = "Processing";
                    ProcessTables(content, jobFile);
                    jobFile.TotalRow = total;
                    jobFile.Remark = string.Empty;
                    _jobFilesService.UpdateJobFiles(jobFile);
                }
                else if (!string.IsNullOrEmpty(jobFile.Status) && jobFile.Status.ToLower() == "processing")
                {
                    if (!jobFile.DateStartProcess.HasValue)
                    {
                        jobFile.DateStartProcess = DateTime.Now;
                    }
                    policies = ProcessPolicies(batches, jobFile);
                    if (policies.Count > 0)
                    {
                        List<Policy> pols = new List<Policy>(policies.Values);
                        int succeed = ImportPolicies(pols, batches);
                    }
                    DataSet ds_unprocessed = new DataSet();
                    DataSet ds_succeed = new DataSet();
                    DataSet ds_failed = new DataSet();
                    string batchDesc = string.Empty;

                    ds_unprocessed = ExecuteSqlQuery(jobFile, 0, false);

                    if (ds_unprocessed.Tables[0].Rows.Count == 0)
                    {
                        ds_succeed = ExecuteSqlQuery(jobFile, 1, false);

                        ds_failed = ExecuteSqlQuery(jobFile, 2, false);

                        batchDesc = string.Format("File: {0}, Total Data : {1}, Success: {2}, Fail: {3}, unprocessed: {4}", Path.GetFileName(filePath), jobFile.TotalRow, ds_succeed.Tables[0].Rows.Count, ds_failed.Tables[0].Rows.Count, ds_unprocessed.Tables[0].Rows.Count);

                        jobFile.SuksesRow = ds_succeed.Tables[0].Rows.Count;
                        jobFile.GagalRow = ds_failed.Tables[0].Rows.Count;

                        //v2
                        if (jobFile.SuksesRow + jobFile.GagalRow == jobFile.TotalRow && jobFile.GagalRow != jobFile.TotalRow)
                        {
                            jobFile.Status = "Done";
                            jobFile.DateEndProcess = DateTime.Now;
                            fileservice.MoveFile(filePath, successFolder);
                        }
                        else if (jobFile.GagalRow == jobFile.TotalRow)
                        {
                            jobFile.Status = "Failed";
                            jobFile.DateEndProcess = DateTime.Now;
                            fileservice.MoveFile(filePath, failedFolder);
                        }
                    }
                    else
                    {
                        batchDesc = string.Format("File: {0}, Total Data : {1}, unprocessed: {2}", Path.GetFileName(filePath), jobFile.TotalRow, ds_unprocessed.Tables[0].Rows.Count);
                    }

                    LogHelper.LogInfo($" Batch Description : " + batchDesc, _pluginMetadata);

                    if (string.IsNullOrEmpty(jobFile.Remark))
                    {
                        LogHelper.LogInfo($" Batch Creating.....", _pluginMetadata);

                        batch = SaveBatch(batches, batchDesc);
                    }
                    else
                    {
                        LogHelper.LogInfo($" Batch Updating.....", _pluginMetadata);

                        batch = UpdateBatch(batches, batchDesc, jobFile, ds_unprocessed.Tables[0].Rows.Count);
                    }

                    LogHelper.LogInfo($"BatchId : " + batch.BatchId, _pluginMetadata);

                    if (string.IsNullOrEmpty(jobFile.Remark))
                    {
                        jobFile.Remark = batch.BatchId;
                    }

                    _jobFilesService.UpdateJobFiles(jobFile);

                    //v1
                    //DataSet ds = new DataSet();
                    //ds = ExecuteSqlQuery(jobFile, 0, true);
                    //if (ds.Tables[0].Rows.Count == 0)
                    //{
                    //    EndProcessJobFiles(jobFile, filePath);

                    //    //ProcFailedData(jobFile);
                    //}
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogError($"Remark Error: " + ex.Message + " " + ex.StackTrace + " " + ex.Source, ex, _pluginMetadata);
            }
        }
        protected void ProcessCLP(string filePath, JobFiles jobFile)
        {

            Dictionary<string, Policy> policies = new Dictionary<string, Policy>();
            Dictionary<string, BatchDetail> batches = new Dictionary<string, BatchDetail>();
            FileInfo fileInfo = new FileInfo(filePath);
            ExcelPackage package = new ExcelPackage(fileInfo);
            ExcelWorksheet sheet = package.Workbook.Worksheets.FirstOrDefault();

            Batch batch = new Batch();
            //if (!string.IsNullOrEmpty(jobFile.Remark))
            //{
            //    _batchId = jobFile.Remark;
            //}
            BatchDetail detail = new BatchDetail();
            try
            {
                int rowCount = sheet.Dimension.Rows;
                int columns = sheet.Dimension.Columns;
                string typeId = "H";
                string line = "";
                for (int col = 1; col <= columns; col++)
                {
                    if (col == columns)
                    {
                        line += sheet.Cells[1, col].Value.ToString();
                    }
                    else
                    {
                        line += sheet.Cells[1, col].Value.ToString() + FieldSep;
                    }
                }

                detail.Description = " Header";
                detail.Remark = line;
                detail.TypeId = typeId;
                batches.Add(typeId, detail);

                if (!string.IsNullOrEmpty(jobFile.Status) && jobFile.Status.ToLower() == "new")
                {
                    jobFile.Status = "Processing";
                    ProcessTablesCLP(sheet, jobFile, batches, filePath);
                    jobFile.Remark = string.Empty;
                    jobFile.TotalRow = total;
                    _jobFilesService.UpdateJobFiles(jobFile);

                }
                else if (!string.IsNullOrEmpty(jobFile.Status) && jobFile.Status.ToLower() == "processing")
                {
                    if (!jobFile.DateStartProcess.HasValue)
                    {
                        jobFile.DateStartProcess = DateTime.Now;
                    }
                    List<JobFileDetail> fileDetails = GetJobFileDetails(sheet, jobFile, (int)EnumHelper.StatusJobfileDetail.New, batches);
                    if (fileDetails.Count > 0)
                    {
                        List<Policy> pols = new List<Policy>(policies.Values);
                        int succeed = JobFileDetailAPI(jobFile, fileDetails, batches);
                    }
                    DataSet ds_unprocessed = new DataSet();
                    DataSet ds_succeed = new DataSet();
                    DataSet ds_failed = new DataSet();
                    string batchDesc = string.Empty;

                    ds_unprocessed = ExecuteSqlQuery(jobFile, 0, false);

                    if (ds_unprocessed.Tables[0].Rows.Count == 0)
                    {
                        ds_succeed = ExecuteSqlQuery(jobFile, 1, false);

                        ds_failed = ExecuteSqlQuery(jobFile, 2, false);

                        batchDesc = string.Format("File: {0}, Total Data : {1}, Success: {2}, Fail: {3}, unprocessed: {4}", Path.GetFileName(filePath), jobFile.TotalRow, ds_succeed.Tables[0].Rows.Count, ds_failed.Tables[0].Rows.Count, ds_unprocessed.Tables[0].Rows.Count);

                        jobFile.SuksesRow = ds_succeed.Tables[0].Rows.Count;
                        jobFile.GagalRow = ds_failed.Tables[0].Rows.Count;

                        //v2
                        if (jobFile.SuksesRow + jobFile.GagalRow == jobFile.TotalRow && jobFile.GagalRow != jobFile.TotalRow)
                        {
                            jobFile.Status = "Done";
                            jobFile.DateEndProcess = DateTime.Now;
                            fileservice.MoveFile(filePath, successFolder);
                        }
                        else if (jobFile.GagalRow == jobFile.TotalRow)
                        {
                            jobFile.Status = "Failed";
                            jobFile.DateEndProcess = DateTime.Now;
                            fileservice.MoveFile(filePath, failedFolder);
                        }
                    }
                    else
                    {
                        batchDesc = string.Format("File: {0}, Total Data : {1}, unprocessed: {2}", Path.GetFileName(filePath), jobFile.TotalRow, ds_unprocessed.Tables[0].Rows.Count);
                    }

                    LogHelper.LogInfo($" Batch Description : " + batchDesc, _pluginMetadata);

                    if (string.IsNullOrEmpty(jobFile.Remark))
                    {
                        LogHelper.LogInfo($" Batch Creating.....", _pluginMetadata);

                        batch = SaveBatch(batches, batchDesc);
                    }
                    else
                    {
                        LogHelper.LogInfo($" Batch Updating.....", _pluginMetadata);

                        batch = UpdateBatch(batches, batchDesc, jobFile, ds_unprocessed.Tables[0].Rows.Count);
                    }

                    LogHelper.LogInfo($"BatchId : " + batch.BatchId, _pluginMetadata);

                    if (string.IsNullOrEmpty(jobFile.Remark))
                    {
                        jobFile.Remark = batch.BatchId;
                    }

                    _jobFilesService.UpdateJobFiles(jobFile);

                    //v1
                    //DataSet ds = new DataSet();
                    //ds = ExecuteSqlQuery(jobFile, 0, true);
                    //if (ds.Tables[0].Rows.Count == 0)
                    //{
                    //    EndProcessJobFiles(jobFile, filePath);

                    //    //ProcFailedData(jobFile);
                    //}
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogError($"Remark Error: " + ex.Message + " " + ex.StackTrace + " " + ex.Source, ex, _pluginMetadata);
            }
        }
        protected void ProcessMTR(string filePath, JobFiles jobFile)
        {

            Dictionary<string, Policy> policies = new Dictionary<string, Policy>();
            Dictionary<string, BatchDetail> batches = new Dictionary<string, BatchDetail>();

            content = File.ReadAllText(filePath);

            Batch batch = new Batch();
            //if (!string.IsNullOrEmpty(jobFile.Remark))
            //{
            //    _batchId = jobFile.Remark;
            //}
            BatchDetail detail = new BatchDetail();
            try
            {
                if (!string.IsNullOrEmpty(jobFile.Status) && jobFile.Status.ToLower() == "new")
                {
                    jobFile.Remark = string.Empty;
                    ProcessTablesMTR(content, jobFile, filePath);
                    jobFile.Status = "Processing";
                    jobFile.TotalRow = total;
                    _jobFilesService.UpdateJobFiles(jobFile);
                }
                else if (!string.IsNullOrEmpty(jobFile.Status) && jobFile.Status.ToLower() == "processing")
                {
                    if (!jobFile.DateStartProcess.HasValue)
                    {
                        jobFile.DateStartProcess = DateTime.Now;
                    }

                    policies = ProcessPolicies(batches, jobFile);
                    if (policies.Count > 0)
                    {
                        List<Policy> pols = new List<Policy>(policies.Values);
                        int succeed = ImportPolicies(pols, batches);
                    }

                    DataSet ds_unprocessed = new DataSet();
                    DataSet ds_succeed = new DataSet();
                    DataSet ds_failed = new DataSet();
                    string batchDesc = string.Empty;

                    ds_unprocessed = ExecuteSqlQuery(jobFile, 0, false);

                    if (ds_unprocessed.Tables[0].Rows.Count == 0)
                    {
                        ds_succeed = ExecuteSqlQuery(jobFile, 1, false);

                        ds_failed = ExecuteSqlQuery(jobFile, 2, false);

                        batchDesc = string.Format("File: {0}, Total Data : {1}, Success: {2}, Fail: {3}, unprocessed: {4}", Path.GetFileName(filePath), jobFile.TotalRow, ds_succeed.Tables[0].Rows.Count, ds_failed.Tables[0].Rows.Count, ds_unprocessed.Tables[0].Rows.Count);

                        jobFile.SuksesRow = ds_succeed.Tables[0].Rows.Count;
                        jobFile.GagalRow = ds_failed.Tables[0].Rows.Count;

                        //v2
                        if (jobFile.SuksesRow + jobFile.GagalRow == jobFile.TotalRow && jobFile.GagalRow != jobFile.TotalRow)
                        {
                            jobFile.Status = "Done";
                            jobFile.DateEndProcess = DateTime.Now;
                            fileservice.MoveFile(filePath, successFolder);
                        }
                        else if (jobFile.GagalRow == jobFile.TotalRow)
                        {
                            jobFile.Status = "Failed";
                            jobFile.DateEndProcess = DateTime.Now;
                            fileservice.MoveFile(filePath, failedFolder);
                        }
                    }
                    else
                    {
                        batchDesc = string.Format("File: {0}, Total Data : {1}, unprocessed: {2}", Path.GetFileName(filePath), jobFile.TotalRow, ds_unprocessed.Tables[0].Rows.Count);
                    }

                    LogHelper.LogInfo($" Batch Description : " + batchDesc, _pluginMetadata);

                    if (string.IsNullOrEmpty(jobFile.Remark))
                    {
                        LogHelper.LogInfo($" Batch Creating.....", _pluginMetadata);

                        batch = SaveBatch(batches, batchDesc);
                    }
                    else
                    {
                        LogHelper.LogInfo($" Batch Updating.....", _pluginMetadata);

                        batch = UpdateBatch(batches, batchDesc, jobFile, ds_unprocessed.Tables[0].Rows.Count);
                    }

                    LogHelper.LogInfo($"BatchId : " + batch.BatchId, _pluginMetadata);

                    if (string.IsNullOrEmpty(jobFile.Remark))
                    {
                        jobFile.Remark = batch.BatchId;
                    }

                    _jobFilesService.UpdateJobFiles(jobFile);

                    //v1
                    //DataSet ds = new DataSet();
                    //ds = ExecuteSqlQuery(jobFile, 0, true);
                    //if (ds.Tables[0].Rows.Count == 0)
                    //{
                    //    EndProcessJobFiles(jobFile, filePath);

                    //    //ProcFailedData(jobFile);
                    //}
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogError($"Remark Error: " + ex.Message + " " + ex.StackTrace + " " + ex.Source, ex, _pluginMetadata);
            }
        }

        protected void ProcessMTRRevamp(string filePath, JobFiles jobFile)
        {

            Dictionary<string, Policy> policies = new Dictionary<string, Policy>();
            Dictionary<string, BatchDetail> batches = new Dictionary<string, BatchDetail>();

            Batch batch = new Batch();
            BatchDetail detail = new BatchDetail();
            try
            {
                if (!string.IsNullOrEmpty(jobFile.Status) && jobFile.Status.ToLower() == "new")
                {
                    jobFile.Remark = string.Empty;
                    ProcessTablesMTRRevamp(batches, jobFile, filePath);
                    jobFile.Status = "Processing";
                    jobFile.TotalRow = total;
                    jobFile.DateStartProcess = DateTime.Now;

                    DataSet ds_unprocessed = new DataSet();
                    DataSet ds_succeed = new DataSet();
                    DataSet ds_failed = new DataSet();
                    string batchDesc = string.Empty;

                    ds_unprocessed = ExecuteSqlQuery(jobFile, 0, false);

                    if (ds_unprocessed.Tables[0].Rows.Count == 0)
                    {
                        ds_succeed = ExecuteSqlQuery(jobFile, 1, false);

                        ds_failed = ExecuteSqlQuery(jobFile, 2, false);

                        batchDesc = string.Format("File: {0}, Total Data : {1}, Success: {2}, Fail: {3}, unprocessed: {4}", Path.GetFileName(filePath), jobFile.TotalRow, ds_succeed.Tables[0].Rows.Count, ds_failed.Tables[0].Rows.Count, ds_unprocessed.Tables[0].Rows.Count);

                        jobFile.SuksesRow = ds_succeed.Tables[0].Rows.Count;
                        jobFile.GagalRow = ds_failed.Tables[0].Rows.Count;

                        //v2
                        if (jobFile.SuksesRow + jobFile.GagalRow == jobFile.TotalRow && jobFile.GagalRow != jobFile.TotalRow)
                        {
                            jobFile.Status = "Done";
                            jobFile.DateEndProcess = DateTime.Now;
                            fileservice.MoveFile(filePath, successFolder);
                        }
                        else if (jobFile.GagalRow == jobFile.TotalRow)
                        {
                            jobFile.Status = "Failed";
                            jobFile.DateEndProcess = DateTime.Now;
                            fileservice.MoveFile(filePath, failedFolder);
                        }
                    }
                    else
                    {
                        batchDesc = string.Format("File: {0}, Total Data : {1}, unprocessed: {2}", Path.GetFileName(filePath), jobFile.TotalRow, ds_unprocessed.Tables[0].Rows.Count);
                    }

                    LogHelper.LogInfo($" Batch Description : " + batchDesc, _pluginMetadata);

                    if (string.IsNullOrEmpty(jobFile.Remark))
                    {
                        LogHelper.LogInfo($" Batch Creating.....", _pluginMetadata);

                        batch = SaveBatch(batches, batchDesc);
                    }

                    //else
                    //{
                    //    LogHelper.LogInfo($" Batch Updating.....", _pluginMetadata);

                    //    batch = UpdateBatch(batches, batchDesc, jobFile, ds_unprocessed.Tables[0].Rows.Count);
                    //}

                    LogHelper.LogInfo($"BatchId : " + batch.BatchId, _pluginMetadata);

                    if (string.IsNullOrEmpty(jobFile.Remark))
                    {
                        jobFile.Remark = batch.BatchId;
                    }

                    _jobFilesService.UpdateJobFiles(jobFile);

                }
            }
            catch (Exception ex)
            {
                jobFile.Status = "Failed";
                jobFile.Remark = "Remark Error: " + ex.Message + " " + ex.StackTrace + " " + ex.Source;
                jobFile.DateEndProcess = DateTime.Now;
                _jobFilesService.UpdateJobFiles(jobFile);
                LogHelper.LogError($"Remark Error: " + ex.Message + " " + ex.StackTrace + " " + ex.Source, ex, _pluginMetadata);
            }
        }
        protected void ProcessTables(string content, JobFiles jobFile)
        {
            List<JobFileDetail> jobDetails = new List<JobFileDetail>();
            string[] rows = content.Split(new string[] { LineSep }, StringSplitOptions.None);
            string no = "";
            string msg = "";
            string dataEffDate = "";
            string dataCifNo = "";
            string dataNama = "";
            string dataGender = "";
            string dataUsia = "";
            string dataPremi = "";
            string dataOutstanding = "";
            string productId = string.Empty;
            string policyId = string.Empty;

            total = 0;
            for (int row = 1; row < rows.Length; row++)
            {

                //LogHelper.LogInfo($"Process at row : " + row, _pluginMetadata); // row - header

                string[] colls = rows[row].Split(new string[] { FieldSep }, StringSplitOptions.None);
                try
                {
                    total += 1;
                    if (colls.Length == 1)
                    {
                        total -= 1;
                        continue;
                    }
                    else if ((colls.Length > 1 && colls.Length < 8))
                    {
                        throw new Exception("wrong tab delimiter template data at row = " + row);
                    }

                    #region bind-fileTxt
                    if (!string.IsNullOrEmpty(colls[0].Trim()))
                    {
                        no = colls[0].Trim();
                    }
                    if (!string.IsNullOrEmpty(colls[1].Trim()))
                    {
                        dataEffDate = colls[1].Trim();
                    }
                    if (!string.IsNullOrEmpty(colls[2].Trim()))
                    {
                        dataCifNo = colls[2].Trim();
                    }
                    if (!string.IsNullOrEmpty(colls[3].Trim()))
                    {
                        dataNama = colls[3].Trim();
                    }
                    if (!string.IsNullOrEmpty(colls[4].Trim()))
                    {
                        dataGender = colls[4].Trim();
                    }
                    if (!string.IsNullOrEmpty(colls[5].Trim()))
                    {
                        dataUsia = colls[5].Trim();
                    }
                    if (!string.IsNullOrEmpty(colls[6].Trim()))
                    {
                        dataPremi = colls[6].Trim();
                    }
                    else
                    {
                        dataPremi = "0";
                    }
                    if (!string.IsNullOrEmpty(colls[7].Trim()))
                    {
                        dataOutstanding = colls[7].Trim();
                    }
                    else
                    {
                        dataOutstanding = "0";
                    }
                    #endregion
                    string effDate = dataEffDate.Substring(1, 6);   //fromat 1yymmdd  
                    int year = int.Parse(DateTime.Now.Year.ToString().Substring(0, 2) + effDate.Substring(0, 2));
                    int month = int.Parse(effDate.Substring(2, 2));
                    int day = int.Parse(effDate.Substring(4, 2));

                    #region add data
                    JobFileDetail _jobDetail = new JobFileDetail();
                    _jobDetail.JobFileId = jobFile.JobFileId;
                    _jobDetail.No = no;
                    //_jobDetail.EffectiveDate = new DateTime(year, month, day);
                    _jobDetail.EffectiveDate = GetDate(effDate, "yymmdd");
                    _jobDetail.Cifno = dataCifNo;
                    _jobDetail.Name = dataNama;
                    _jobDetail.SexId = GetMaritalStatusId(dataGender); ;
                    _jobDetail.Age = int.Parse(dataUsia, CultureInfo.InvariantCulture);
                    _jobDetail.Premium = decimal.Parse(dataPremi, CultureInfo.InvariantCulture);
                    _jobDetail.Outstanding = decimal.Parse(dataOutstanding, CultureInfo.InvariantCulture);
                    _jobDetail.ProductId = jobFile.ProductId.ToUpper();
                    _jobDetail.PaymentModeId = "P";
                    _jobDetail.PaymentMethodId = "DBC";
                    _jobDetail.CreateDate = jobFile.CreateDate;
                    _jobDetail.CreateBy = jobFile.UserId;
                    _jobDetail.DetailId = jobFile.JobFileId + "-" + no;
                    _jobDetail.Reference = _jobDetail.DetailId;
                    _jobDetail.IdentityTypeId = "OTH";
                    _jobDetail.IdentityNo = "-";
                    _jobDetail.BankId = "1";
                    _jobDetail.StatusId = "IN";
                    _jobDetail.Currency = "IDR";
                    _jobDetail.PaymentDate = jobFile.CreateDate.Value.AddMonths(1);
                    jobDetails.Add(_jobDetail);

                    //DataRow dr = dtTemp.NewRow();
                    //dr[0] = jobFile.JobFileId;
                    //dr[1] = no;
                    //dr[2] = new DateTime(year, month, day);
                    //dr[3] = dataCifNo;
                    //dr[4] = dataNama;
                    //dr[5] = GetMaritalStatusId(dataGender);
                    //dr[6] = dataUsia;
                    //dr[7] = decimal.Parse(dataPremi, CultureInfo.InvariantCulture);
                    //dr[8] = decimal.Parse(dataOutstanding, CultureInfo.InvariantCulture);

                    ////dr[8] = dataPremi;
                    ////dr[9] = dataOutstanding;

                    //dr[9] = 0;
                    //dr[10] = "MANPRO";
                    //dr[11] = "P";
                    //dr[12] = "DBC";
                    //dr[13] = "IDR";
                    //dr[14] = jobFile.CreateDate;
                    //dr[15] = jobFile.UserId;
                    //dr[16] = jobFile.JobFileId + "-" + no;
                    //dtTemp.Rows.Add(dr);
                    #endregion

                }
                catch (Exception ex)
                {
                    LogHelper.LogError($"error Process Bind : " + ex.Message + " " + ex.StackTrace + " " + ex.Source, ex, _pluginMetadata);
                    throw new Exception($"error Process Bind : " + ex.Message + " " + ex.StackTrace + " " + ex.Source);
                }
            }

            DataTable dtTemp = MapperJobFileDetail(jobDetails);

            #region Connection

            LogHelper.LogInfo($"Insert Bulk - Start", _pluginMetadata);

            using (DbConnection conn = _database.CreateConnection())
            {
                //msg += "2";
                conn.Open();
                //msg += "3";
                using (SqlBulkCopy copy = new SqlBulkCopy((SqlConnection)conn, SqlBulkCopyOptions.Default, null))
                {
                    try
                    {
                        copy.BatchSize = 500;
                        copy.NotifyAfter = 500;
                        copy.DestinationTableName = "JOBFILEDETAIL";
                        copy.BulkCopyTimeout = 0;
                        copy.WriteToServer(dtTemp);
                        //msg += "4";

                        LogHelper.LogInfo($"Insert Bulk - Success", _pluginMetadata);
                    }
                    catch (Exception ex)
                    {
                        msg += ex.Message + Environment.NewLine + Environment.NewLine + ex.StackTrace;

                        LogHelper.LogInfo($"Insert Bulk - Failed \n " + msg, _pluginMetadata);
                        throw new Exception($"Insert Bulk - Failed \n " + msg);
                    }

                }
                conn.Close();
            }

            LogHelper.LogInfo($"Insert Bulk - Finish", _pluginMetadata);
            #endregion 

        }

        protected void ProcessTablesMTR(string content, JobFiles jobFile, string filePath)
        {
            List<JobFileDetail> jobDetails = new List<JobFileDetail>();
            //string[] rows = content.Split(new string[] { LineSep }, StringSplitOptions.None);
            string[] rows = content.Split(new string[] { "\n" }, StringSplitOptions.None);
            string msg = "";

            total = 0;
            for (int row = 0; row < rows.Length; row++)
            {
                string PISBR = string.Empty; //Cabang
                string PISACT = string.Empty; //Nomor polis
                string PISCUR = string.Empty; //Mata Uang/Valuta
                string PISSTS = string.Empty; //Status 1.Active 2.Close 4.Active 7.Claim 9.Freeze 
                string PISPRD = string.Empty; //Product
                string PISCIF = string.Empty; //CIF
                string PISNAM = string.Empty; //Nama Peserta
                string PISSEX = string.Empty; //Gender
                string PISCFC = string.Empty; //ID Card
                string PISCFS = string.Empty; //ID #
                string PISAFT = string.Empty; //Manfaat Bulanan (dalam plain text)
                string PISAMT = string.Empty; //Premi (dalam plain text)
                string PISCOD = string.Empty; //Kode Claim
                string PISDB6 = string.Empty; //Tgl Lahir (ddmmyy)
                string PISOP6 = string.Empty; //Tgl buka Polis (ddmmyy)
                string PISMT6 = string.Empty; //Jatuh Tempo/Maturity Date (ddmmyy)
                string PISLP6 = string.Empty; //Tgl terakhir Bayar(ddmmyy)
                string PISDOB = string.Empty; //Tgl tLahir (julian date)
                string PISOPD = string.Empty; //Tgl buka Polis(julian date)
                string PISMTD = string.Empty; //Jatuh Tempo/Maturity Date (julian date)
                string PISLPD = string.Empty; // Tgl terakhir Bayar (julian date)
                /*
                    Julian Date
                    2021044 -> hari ke 044 di tahun 2021 
                    Kode Claim -> 
                 */
                string data = rows[row];
                if ((data.Length > 10 && data.Length < 210) || data.Length > 212)
                {
                    total += 1;
                    PISBR = data.Substring(0, 5);
                    PISACT = data.Substring(5, 19);
                    PISCUR = data.Substring(24, 4);
                    PISSTS = data.Substring(28, 1);
                    PISPRD = data.Substring(29, 10);
                    PISCIF = data.Substring(39, 19);
                    PISNAM = data.Substring(58, 20);
                    PISSEX = data.Substring(78, 1);
                    PISCFC = data.Substring(79, 4);
                    //PISCFS = data.Substring(83, 40);
                }
                else if (data.Length < 10)
                {
                    continue;
                }
                else
                {
                    total += 1;
                    PISBR = data.Substring(0, 5);
                    PISACT = data.Substring(5, 19);
                    PISCUR = data.Substring(24, 4);
                    PISSTS = data.Substring(28, 1);
                    PISPRD = data.Substring(29, 10);
                    PISCIF = data.Substring(39, 19);
                    PISNAM = data.Substring(58, 20);
                    PISSEX = data.Substring(78, 1);
                    PISCFC = data.Substring(79, 4);
                    PISCFS = data.Substring(83, 40);
                    PISAFT = data.Substring(123, 17);
                    PISAMT = data.Substring(140, 17);
                    PISCOD = data.Substring(157, 1);
                    PISDB6 = data.Substring(158, 6);
                    PISOP6 = data.Substring(164, 6);
                    PISMT6 = data.Substring(170, 6);
                    PISLP6 = data.Substring(176, 6);
                    PISDOB = data.Substring(182, 7);
                    PISOPD = data.Substring(189, 7);
                    PISMTD = data.Substring(196, 7);
                    PISLPD = data.Substring(203, 7);
                }
                DateTime? effDate = null;
                DateTime? birthDate = null;
                DateTime? matureDate = null;
                DateTime? payDate = null;
                if (!string.IsNullOrEmpty(PISOP6) && !string.IsNullOrEmpty(PISOPD))
                { effDate = GetDate(PISOP6.Substring(0, 4) + PISOPD.Substring(0, 4), "ddmmyyyy"); }
                if (!string.IsNullOrEmpty(PISDB6) && !string.IsNullOrEmpty(PISDOB))
                { birthDate = GetDate(PISDB6.Substring(0, 4) + PISDOB.Substring(0, 4), "ddmmyyyy"); }
                if (!string.IsNullOrEmpty(PISMT6) && !string.IsNullOrEmpty(PISMTD))
                { matureDate = GetDate(PISMT6.Substring(0, 4) + PISMTD.Substring(0, 4), "ddmmyyyy"); }
                if (!string.IsNullOrEmpty(PISLP6) && !string.IsNullOrEmpty(PISLPD))
                { payDate = GetDate(PISLP6.Substring(0, 4) + PISLPD.Substring(0, 4), "ddmmyyyy"); }

                #region add data
                JobFileDetail _jobDetail = new JobFileDetail();
                _jobDetail.JobFileId = jobFile.JobFileId;
                _jobDetail.No = (row + 1).ToString();

                _jobDetail.EffectiveDate = effDate;
                _jobDetail.BirthDate = birthDate;
                _jobDetail.MatureDate = matureDate;
                _jobDetail.PaymentDate = payDate;

                _jobDetail.Cifno = PISCIF.Trim();
                _jobDetail.Name = PISNAM.Trim();
                _jobDetail.SexId = PISSEX.Trim();
                _jobDetail.Premium = GetValue(PISAMT) == -1 ? -1 : GetValue(PISAMT) * (decimal)0.01;
                _jobDetail.PaymentModeId = "M";
                _jobDetail.PaymentMethodId = "DBB";
                _jobDetail.IdentityTypeId = PISCFC.Trim();
                _jobDetail.IdentityNo = PISCFS.Trim();

                _jobDetail.Currency = PISCUR.Trim();
                _jobDetail.ProductId = PISPRD.Trim();

                if (!string.IsNullOrEmpty(_jobDetail.ProductId))
                {
                    if (_jobDetail.ProductId.ToLower().Contains("tab"))
                    {
                        _jobDetail.ProductId = "TABINS_USD";
                    }
                    else
                    {
                        _jobDetail.ProductId = "TRMIDR";
                    }
                }

                _jobDetail.KodeCabang = PISBR.Trim();
                _jobDetail.Reference = PISACT.Trim();
                _jobDetail.CreateDate = jobFile.CreateDate;
                _jobDetail.CreateBy = jobFile.UserId;
                _jobDetail.DetailId = jobFile.JobFileId + "-" + _jobDetail.No;
                _jobDetail.ClaimCode = PISCOD.Trim();
                _jobDetail.CreditValue = GetValue(PISAFT) == -1 ? -1 : GetValue(PISAFT) * (decimal)0.01;
                _jobDetail.StatusId = PISSTS.Trim();
                _jobDetail.BankId = "1";
                _jobDetail.ProductReference = PISPRD.Trim();

                jobDetails.Add(_jobDetail);

                #endregion

            }

            DataTable dtTemp = MapperJobFileDetail(jobDetails);

            #region Connection

            LogHelper.LogInfo($"Insert Bulk - Start", _pluginMetadata);

            using (DbConnection conn = _database.CreateConnection())
            {
                //msg += "2";
                conn.Open();
                //msg += "3";
                using (SqlBulkCopy copy = new SqlBulkCopy((SqlConnection)conn, SqlBulkCopyOptions.Default, null))
                {
                    try
                    {
                        copy.BatchSize = 500;
                        copy.NotifyAfter = 500;
                        copy.DestinationTableName = "JOBFILEDETAIL";
                        copy.BulkCopyTimeout = 0;
                        copy.WriteToServer(dtTemp);
                        //msg += "4";

                        LogHelper.LogInfo($"Insert Bulk - Success", _pluginMetadata);
                    }
                    catch (Exception ex)
                    {
                        msg += ex.Message + Environment.NewLine + Environment.NewLine + ex.StackTrace;

                        LogHelper.LogInfo($"Insert Bulk - Failed \n " + msg, _pluginMetadata);
                        throw new Exception($"Insert Bulk - Failed \n " + msg);
                    }

                }
                conn.Close();
            }

            LogHelper.LogInfo($"Insert Bulk - Finish", _pluginMetadata);
            #endregion

        }

        protected void ProcessTablesMTRRevamp(Dictionary<string, BatchDetail> batches, JobFiles jobFile, string filePath)
        {
            List<JobFileDetail> jobDetails = new List<JobFileDetail>();
            DataSet ds_uploadMTR = new DataSet();
            total = 0;
            string msg = "";
            int row = 0;
            ds_uploadMTR = ExecuteSqlQuery(jobFile);
            //Product _prodIDR = _productServices.GetProduct("TRMIDR");
            //Product _prodUSD = _productServices.GetProduct("TABINS_USD");


            if (ds_uploadMTR.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds_uploadMTR.Tables[0].Rows)
                {
                    row++;
                    total += 1;
                    //for (int row = 0; row < ds_uploadMTR.Tables[0].Rows.Count; row++)
                    //{
                    UploadMTR dataMTR = new UploadMTR();
                    BatchDetail detail = new BatchDetail();
                    /*
                    string PISBR = string.Empty; //Cabang
                    string PISACT = string.Empty; //Nomor polis
                    string PISCUR = string.Empty; //Mata Uang/Valuta
                    string PISSTS = string.Empty; //Status 1.Active 2.Close 4.Active 7.Claim 9.Freeze 
                    string PISPRD = string.Empty; //Product
                    string PISCIF = string.Empty; //CIF
                    string PISNAM = string.Empty; //Nama Peserta
                    string PISSEX = string.Empty; //Gender
                    string PISCFC = string.Empty; //ID Card
                    string PISCFS = string.Empty; //ID #
                    string PISAFT = string.Empty; //Manfaat Bulanan (dalam plain text)
                    string PISAMT = string.Empty; //Premi (dalam plain text)
                    string PISCOD = string.Empty; //Kode Claim
                    string PISDB6 = string.Empty; //Tgl Lahir (ddmmyy)
                    string PISOP6 = string.Empty; //Tgl buka Polis (ddmmyy)
                    string PISMT6 = string.Empty; //Jatuh Tempo/Maturity Date (ddmmyy)
                    string PISLP6 = string.Empty; //Tgl terakhir Bayar(ddmmyy)
                    string PISDOB = string.Empty; //Tgl tLahir (julian date)
                    string PISOPD = string.Empty; //Tgl buka Polis(julian date)
                    string PISMTD = string.Empty; //Jatuh Tempo/Maturity Date (julian date)
                    string PISLPD = string.Empty; // Tgl terakhir Bayar (julian date)
                    */
                    /*
                        Julian Date
                        2021044 -> hari ke 044 di tahun 2021 
                        Kode Claim -> 
                     */

                    dataMTR.ID = dr["ID"] == DBNull.Value ? "" : dr["ID"].ToString();
                    dataMTR.PISBR = dr["PISBR#"] == DBNull.Value ? "" : dr["PISBR#"].ToString();
                    dataMTR.PISACT = dr["PISACT"] == DBNull.Value ? "" : dr["PISACT"].ToString();
                    dataMTR.PISCUR = dr["PISCUR"] == DBNull.Value ? "" : dr["PISCUR"].ToString();
                    dataMTR.PISSTS = dr["PISSTS"] == DBNull.Value ? "" : dr["PISSTS"].ToString();
                    dataMTR.PISPRD = dr["PISPRD"] == DBNull.Value ? "" : dr["PISPRD"].ToString();
                    dataMTR.PISCIF = dr["PISCIF"] == DBNull.Value ? "" : dr["PISCIF"].ToString();
                    dataMTR.PISNAM = dr["PISNAM"] == DBNull.Value ? "" : dr["PISNAM"].ToString();
                    dataMTR.PISSEX = dr["PISSEX"] == DBNull.Value ? "" : dr["PISSEX"].ToString();
                    dataMTR.PISCFC = dr["PISCFC"] == DBNull.Value ? "" : dr["PISCFC"].ToString();
                    dataMTR.PISCFS = dr["PISCFS"] == DBNull.Value ? "" : dr["PISCFS"].ToString();
                    dataMTR.PISAFT = dr["PISAFT"] == DBNull.Value ? "0" : dr["PISAFT"].ToString();
                    dataMTR.PISAMT = dr["PISAMT"] == DBNull.Value ? "0" : dr["PISAMT"].ToString();
                    dataMTR.PISCOD = dr["PISCOD"] == DBNull.Value ? "" : dr["PISCOD"].ToString();
                    dataMTR.PISDB6 = dr["PISDB6"] == DBNull.Value ? "" : dr["PISDB6"].ToString();
                    dataMTR.PISOP6 = dr["PISOP6"] == DBNull.Value ? "" : dr["PISOP6"].ToString();
                    dataMTR.PISMT6 = dr["PISMT6"] == DBNull.Value ? "" : dr["PISMT6"].ToString();
                    dataMTR.PISLP6 = dr["PISLP6"] == DBNull.Value ? "" : dr["PISLP6"].ToString();
                    dataMTR.PISDOB = dr["PISDOB"] == DBNull.Value ? "" : dr["PISDOB"].ToString();
                    dataMTR.PISOPD = dr["PISOPD"] == DBNull.Value ? "" : dr["PISOPD"].ToString();
                    dataMTR.PISMTD = dr["PISMTD"] == DBNull.Value ? "" : dr["PISMTD"].ToString();
                    dataMTR.PISLPD = dr["PISLPD"] == DBNull.Value ? "" : dr["PISLPD"].ToString();
                    dataMTR.STATUSPOLICY = dr["STATUSPOLICY"] == DBNull.Value ? "" : dr["STATUSPOLICY"].ToString();
                    dataMTR.INVALIDREASON = dr["INVALIDREASON"] == DBNull.Value ? "" : dr["INVALIDREASON"].ToString();

                    DateTime? effDate = null;
                    DateTime? birthDate = null;
                    DateTime? matureDate = null;
                    DateTime? payDate = null;
                    if (!string.IsNullOrEmpty(dataMTR.PISOP6) && !string.IsNullOrEmpty(dataMTR.PISOPD) && dataMTR.PISOP6.Length >= 4 && dataMTR.PISOPD.Length >= 4)
                    { effDate = GetDate(dataMTR.PISOP6.Substring(0, 4) + dataMTR.PISOPD.Substring(0, 4), "ddmmyyyy"); }
                    if (!string.IsNullOrEmpty(dataMTR.PISDB6) && !string.IsNullOrEmpty(dataMTR.PISDOB) && dataMTR.PISDB6.Length >= 4 && dataMTR.PISDOB.Length >= 4)
                    { birthDate = GetDate(dataMTR.PISDB6.Substring(0, 4) + dataMTR.PISDOB.Substring(0, 4), "ddmmyyyy"); }
                    if (!string.IsNullOrEmpty(dataMTR.PISMT6) && !string.IsNullOrEmpty(dataMTR.PISMTD) && dataMTR.PISMT6.Length >= 4 && dataMTR.PISMTD.Length >= 4)
                    { matureDate = GetDate(dataMTR.PISMT6.Substring(0, 4) + dataMTR.PISMTD.Substring(0, 4), "ddmmyyyy"); }
                    if (!string.IsNullOrEmpty(dataMTR.PISLP6) && !string.IsNullOrEmpty(dataMTR.PISLPD) && dataMTR.PISLP6.Length >= 4 && dataMTR.PISLPD.Length >= 4)
                    { payDate = GetDate(dataMTR.PISLP6.Substring(0, 4) + dataMTR.PISLPD.Substring(0, 4), "ddmmyyyy"); }

                    #region add data
                    JobFileDetail _jobDetail = new JobFileDetail();
                    _jobDetail.JobFileId = jobFile.JobFileId;
                    _jobDetail.No = row.ToString();

                    _jobDetail.EffectiveDate = effDate;
                    _jobDetail.BirthDate = birthDate;
                    _jobDetail.MatureDate = matureDate;
                    _jobDetail.PaymentDate = payDate;

                    _jobDetail.Cifno = dataMTR.PISCIF;
                    _jobDetail.Name = dataMTR.PISNAM;
                    _jobDetail.SexId = dataMTR.PISSEX;
                    _jobDetail.Premium = GetValue(dataMTR.PISAMT) == -1 ? -1 : GetValue(dataMTR.PISAMT) * (decimal)0.01;
                    _jobDetail.PaymentModeId = "M";
                    _jobDetail.PaymentMethodId = "DBB";
                    _jobDetail.IdentityTypeId = dataMTR.PISCFC;
                    _jobDetail.IdentityNo = dataMTR.PISCFS;

                    _jobDetail.Currency = dataMTR.PISCUR;

                    Product _prod = new Product();
                    if (!string.IsNullOrEmpty(_jobDetail.Currency))
                    {
                        if (_jobDetail.Currency.Contains("IDR"))
                        {
                            _jobDetail.ProductId = "TRMIDR";
                            //_prod = _prodIDR;
                        }
                        else if (_jobDetail.Currency.Contains("USD"))
                        {
                            _jobDetail.ProductId = "TABINS_USD";
                            // _prod = _prodUSD;
                        }
                    }

                    _jobDetail.KodeCabang = dataMTR.PISBR;
                    _jobDetail.Reference = dataMTR.PISACT;
                    _jobDetail.CreateDate = jobFile.CreateDate;
                    _jobDetail.CreateBy = jobFile.UserId;
                    _jobDetail.DetailId = dataMTR.ID; //jobFile.JobFileId + "-" + _jobDetail.No;
                    _jobDetail.ClaimCode = dataMTR.PISCOD;
                    _jobDetail.CreditValue = GetValue(dataMTR.PISAFT) == -1 ? -1 : GetValue(dataMTR.PISAFT) * (decimal)0.01;
                    _jobDetail.StatusId = dataMTR.PISSTS;
                    _jobDetail.BankId = "1";
                    _jobDetail.ProductReference = dataMTR.PISPRD;
                    _jobDetail.StatusProcess = 1;
                    _jobDetail.StatusDMTM = 0;

                    if (dataMTR.STATUSPOLICY == "F")
                    {
                        _jobDetail.StatusProcess = 2;
                        _jobDetail.StatusDMTM = 2;

                        if (dataMTR.INVALIDREASON.Contains("AGE") || dataMTR.INVALIDREASON.Contains("COVERAGE"))
                        {
                            detail.Description = dataMTR.INVALIDREASON;
                            detail.Remark = dataMTR.PISBR + "" + dataMTR.PISACT + "" + dataMTR.PISCUR + "" +
                            dataMTR.PISSTS + "" + dataMTR.PISPRD + "" + dataMTR.PISCIF + "" + dataMTR.PISNAM + "" + dataMTR.PISSEX + "" + dataMTR.PISCFC + "" + dataMTR.PISCFS
                            + "" + dataMTR.PISAFT + "" + dataMTR.PISAFT + "" + dataMTR.PISAMT + "" + dataMTR.PISCOD + "" + dataMTR.PISDB6 + "" + dataMTR.PISOP6 + "" + dataMTR.PISMT6
                             + "" + dataMTR.PISLP6 + "" + dataMTR.PISDOB + "" + dataMTR.PISOPD + "" + dataMTR.PISMTD + "" + dataMTR.PISLPD;

                        }
                        else
                        {
                            detail.Description = string.Format("INVALID DATA");
                            detail.Remark = dataMTR.INVALIDREASON;
                        }

                        detail.OrderId = dataMTR.ID;
                        detail.Remark2 = _jobDetail.Reference;
                        detail.CreateDate = DateTime.Now;
                        detail.ModifyDate = DateTime.Now;
                        detail.ModifyBy = "System";
                        detail.CreateBy = "System";
                        detail.DetailId = Convert.ToInt32(dataMTR.ID.Substring(16));

                        batches.Add(dataMTR.ID, detail);
                    }
                    else
                    {
                        if (_jobDetail.StatusId != "2")
                        {
                            _jobDetail.PaymentId = Guid.NewGuid().ToString().ToUpper();
                        }

                        #region Policy Reserved
                        /*
                        string sqlCust = $"SELECT CUSTOMERID FROM CUSTOMER WHERE CIFNO = '{_jobDetail.Cifno}'";

                        DataSet dsCust = new DataSet();
                        DataSet dsPol = new DataSet();
                        dsCust = helper.ExecuteSqlQuery(sqlCust);
                        if (dsCust.Tables[0].Rows.Count > 0)
                        {
                            _jobDetail.CustomerId = dsCust.Tables[0].Rows[0]["CUSTOMERID"].ToString();
                            string sqlPolicy = $"SELECT TOP 1 POLICYNO, CUSTOMERNO, PRODUCTID, CURRENCYID, PAYMENTMODEID, PAYMENTMETHODID, CREDITVALUE FROM POLICY WHERE PRODUCTID = '{_jobDetail.ProductId}' and STATUSID IN('IN','LA') and REFFERENCENO = '{_jobDetail.Reference}' and CUSTOMERNO='{_jobDetail.CustomerId}'";
                            dsPol = helper.ExecuteSqlQuery(sqlPolicy);

                            if (dsPol.Tables[0].Rows.Count > 0)
                            {
                                _jobDetail.IsNB = 0;
                                _jobDetail.PolicyNo = dsPol.Tables[0].Rows[0]["POLICYNO"].ToString();
                            }
                            else
                            {
                                _jobDetail.IsNB = 1;
                                //_jobDetail.PolicyNo = NextPolicyId(_prod);
                            }
                        }
                        else
                        {
                            _jobDetail.IsNB = 1;
                            //_jobDetail.PolicyNo = NextPolicyId(_prod);
                        }
                        */
                        #endregion
                    }

                    jobDetails.Add(_jobDetail);
                    #endregion

                }

            }


            DataTable dtTemp = MapperJobFileDetail(jobDetails);

            #region Connection

            LogHelper.LogInfo($"Insert Bulk - Start", _pluginMetadata);

            using (DbConnection conn = _database.CreateConnection())
            {
                //msg += "2";
                conn.Open();
                //msg += "3";
                using (SqlBulkCopy copy = new SqlBulkCopy((SqlConnection)conn, SqlBulkCopyOptions.Default, null))
                {
                    try
                    {
                        copy.BatchSize = 1000000;
                        //copy.NotifyAfter = 500;
                        copy.DestinationTableName = "JOBFILEDETAIL";
                        copy.BulkCopyTimeout = 0;
                        copy.WriteToServer(dtTemp);
                        //msg += "4";

                        LogHelper.LogInfo($"Insert Bulk - Success", _pluginMetadata);
                    }
                    catch (Exception ex)
                    {
                        msg += ex.Message + Environment.NewLine + Environment.NewLine + ex.StackTrace;

                        LogHelper.LogInfo($"Insert Bulk - Failed \n " + msg, _pluginMetadata);
                        throw new Exception($"Insert Bulk - Failed \n " + msg);
                    }

                }
                conn.Close();
            }

            LogHelper.LogInfo($"Insert Bulk - Finish", _pluginMetadata);
            #endregion

        }
        string NextPolicyId(Product p)
        {
            long number = _sequencePersistence.Get("Policy", p.ProductId);

            string policyId = p.PolicyPrefix + "-" + number.ToString().PadLeft(6, '0') + GetVerificationCode(number.ToString().PadLeft(6, '0'));

            return policyId;
        }
        public string GetVerificationCode(string number)
        {
            long code = 0;

            code = 98 - ((long.Parse(number) * 100) % 97) % 97;

            //alter by firyan - 20090221 - begin
            return code.ToString().Substring(0, 1);
            //alter by firyan - 20090221 - end
        }
        protected decimal GetValue(string value)
        {
            decimal val = 0;
            try
            {
                val = Convert.ToInt64(value);
            }
            catch (Exception ex)
            {
                val = -1;
                //messageError += "Row " + total + " value is empty/wrong, ";
            }
            return val;
        }
        protected void ProcessTablesCLP(ExcelWorksheet sheet, JobFiles jobFile, Dictionary<string, BatchDetail> batches, string filePath)
        {
            List<JobFileDetail> jobDetails = new List<JobFileDetail>();
            string msg = "";
            total = 0;

            //_product = _productServices.GetProduct(jobFile.ProductId);

            int rowCount = sheet.Dimension.Rows;
            int columns = sheet.Dimension.Columns;
            for (int row = 2; row <= rowCount; row++)
            {
                BatchDetail detail = new BatchDetail();
                JobFileDetail _jobDetail = new JobFileDetail();
                if (sheet.Cells[row, 1] == null || string.IsNullOrEmpty((string)sheet.GetValue(row, 1))
                    //||
                    //sheet.Cells[row, 3] == null || string.IsNullOrEmpty(sheet.Cells[row, 3].Value.ToString()) ||
                    //sheet.Cells[row, 4] == null || string.IsNullOrEmpty(sheet.Cells[row, 4].Value.ToString()) ||
                    //sheet.Cells[row, 5] == null || string.IsNullOrEmpty(sheet.Cells[row, 5].Value.ToString()) ||
                    //sheet.Cells[row, 6] == null || string.IsNullOrEmpty(sheet.Cells[row, 6].Value.ToString()) ||
                    //sheet.Cells[row, 7] == null || string.IsNullOrEmpty(sheet.Cells[row, 7].Value.ToString()) ||
                    //sheet.Cells[row, 8] == null || string.IsNullOrEmpty(sheet.Cells[row, 8].Value.ToString()) ||
                    //sheet.Cells[row, 9] == null || string.IsNullOrEmpty(sheet.Cells[row, 9].Value.ToString()) ||
                    //sheet.Cells[row, 10] == null || string.IsNullOrEmpty(sheet.Cells[row, 10].Value.ToString()) ||
                    //sheet.Cells[row, 11] == null || string.IsNullOrEmpty(sheet.Cells[row, 11].Value.ToString()) ||
                    //sheet.Cells[row, 12] == null || string.IsNullOrEmpty(sheet.Cells[row, 12].Value.ToString())
                    )
                {
                    continue;
                }

                try
                {
                    total += 1;
                    _jobDetail.Name = sheet.Cells[row, 1] != null ? sheet.Cells[row, 1].Value.ToString().Trim() : "";
                    _jobDetail.BirthPlace = sheet.Cells[row, 2] != null ? sheet.Cells[row, 2].Value.ToString().Trim() : "";
                    _jobDetail.BirthDate = Convert.ToDateTime(sheet.Cells[row, 3].Value.ToString().Trim());
                    _jobDetail.IdentityNo = sheet.Cells[row, 4] != null ? sheet.Cells[row, 4].Value.ToString().Trim() : "";
                    _jobDetail.SexId = GetMaritalStatusId(sheet.Cells[row, 5].Value.ToString().Trim());
                    _jobDetail.Line1 = sheet.Cells[row, 6] != null ? sheet.Cells[row, 6].Value.ToString() : "-";
                    _jobDetail.OccupationId = sheet.Cells[row, 7] != null ? sheet.Cells[row, 7].Value.ToString().Trim() : "";
                    _jobDetail.Reference = sheet.Cells[row, 8] != null ? sheet.Cells[row, 8].Value.ToString().Trim() : "";

                    _jobDetail.CreditValue = Convert.ToDecimal(sheet.Cells[row, 9].Value.ToString().Trim());
                    _jobDetail.EffectiveDate = Convert.ToDateTime(sheet.Cells[row, 10].Value.ToString().Trim());
                    _jobDetail.MatureDate = Convert.ToDateTime(sheet.Cells[row, 11].Value.ToString().Trim());
                    _jobDetail.Tenor = Convert.ToInt32(sheet.Cells[row, 12].Value.ToString().Trim());

                    _jobDetail.KodeCabang = sheet.Cells[row, 13] != null ? sheet.Cells[row, 13].Value.ToString().Trim() : "";
                    _jobDetail.NamaCabang = sheet.Cells[row, 14] != null ? sheet.Cells[row, 14].Value.ToString().Trim() : "";

                    _jobDetail.ProductId = jobFile.ProductId.ToUpper();
                    _jobDetail.JobFileId = jobFile.JobFileId;
                    _jobDetail.CreateDate = jobFile.CreateDate;
                    _jobDetail.CreateBy = jobFile.UserId;
                    _jobDetail.DetailId = jobFile.JobFileId + "-" + (row - 1);
                    _jobDetail.IdentityTypeId = "KTP";
                    _jobDetail.PaymentMethodId = "DBB";
                    _jobDetail.PaymentModeId = "P";
                    _jobDetail.StatusId = "IN";
                    _jobDetail.Currency = "IDR";

                    jobDetails.Add(_jobDetail);

                    string field = "";
                    for (int col = 1; col <= columns; col++)
                    {
                        if (col == columns)
                        {
                            field += sheet.Cells[row, col].Value.ToString();
                        }
                        else
                        {
                            field += sheet.Cells[row, col].Value.ToString() + FieldSep;
                        }
                    }
                    detail.TypeId = "POL";
                    detail.Remark = field;

                }
                catch (Exception ex)
                {
                    detail.Description = string.Format("Err: {0}", ex.Message);
                    LogHelper.LogError($"error Process Bind : " + ex.Message + " " + ex.StackTrace + " " + ex.Source, ex, _pluginMetadata);
                    //throw new Exception($"error Process Bind : " + ex.Message + " " + ex.StackTrace + " " + ex.Source); 

                    jobFile.Remark = "Err. Data:" + total + ", Message: " + ex.Message + ", Trace: " + ex.StackTrace + ", Source: " + ex.Source;
                    jobFile.Status = "Failed";
                    jobFile.DateEndProcess = DateTime.Now;
                    _jobFilesService.UpdateJobFiles(jobFile);
                    fileservice.MoveFile(filePath, failedFolder);
                }

                batches.Add(_jobDetail.DetailId, detail);
            }

            DataTable dtTemp = MapperJobFileDetail(jobDetails);

            #region Connection

            LogHelper.LogInfo($"Insert Bulk - Start", _pluginMetadata);

            using (DbConnection conn = _database.CreateConnection())
            {
                //msg += "2";
                conn.Open();
                //msg += "3";
                using (SqlBulkCopy copy = new SqlBulkCopy((SqlConnection)conn, SqlBulkCopyOptions.Default, null))
                {
                    try
                    {
                        copy.BatchSize = 500;
                        copy.NotifyAfter = 500;
                        copy.DestinationTableName = "JOBFILEDETAIL";
                        copy.BulkCopyTimeout = 0;
                        copy.WriteToServer(dtTemp);
                        //msg += "4";

                        LogHelper.LogInfo($"Insert Bulk - Success", _pluginMetadata);
                    }
                    catch (Exception ex)
                    {
                        msg += ex.Message + Environment.NewLine + Environment.NewLine + ex.StackTrace;

                        LogHelper.LogInfo($"Insert Bulk - Failed \n " + msg, _pluginMetadata);
                        throw new Exception($"Insert Bulk - Failed \n " + msg);
                    }

                }
                conn.Close();
            }

            LogHelper.LogInfo($"Insert Bulk - Finish", _pluginMetadata);
            #endregion 

        }
        protected DataTable MapperJobFileDetail(List<JobFileDetail> details)
        {
            DataTable dtTemp = new DataTable(); // PolicyHistory
            DataColumn col = new DataColumn();

            col = new DataColumn("JOBFILEID", typeof(string)); dtTemp.Columns.Add(col); //0
            col = new DataColumn("NO", typeof(string)); dtTemp.Columns.Add(col); //1
            col = new DataColumn("EFFECTIVEDATE", typeof(DateTime)); dtTemp.Columns.Add(col); //2
            col = new DataColumn("CIFNO", typeof(string)); dtTemp.Columns.Add(col); //3
            col = new DataColumn("NAME", typeof(string)); dtTemp.Columns.Add(col); //4
            col = new DataColumn("SEXID", typeof(string)); dtTemp.Columns.Add(col); //5
            col = new DataColumn("AGE", typeof(int)); dtTemp.Columns.Add(col); //6
            col = new DataColumn("PREMIUM", typeof(float)); dtTemp.Columns.Add(col); //7
            col = new DataColumn("OUTSTANDING", typeof(float)); dtTemp.Columns.Add(col); //8
            col = new DataColumn("STATUSPROCESS", typeof(int)); dtTemp.Columns.Add(col); //9
            col = new DataColumn("PRODUCTID", typeof(string)); dtTemp.Columns.Add(col); //10
            col = new DataColumn("PAYMENTMODEID", typeof(string)); dtTemp.Columns.Add(col); //11
            col = new DataColumn("PAYMENTMETHODID", typeof(string)); dtTemp.Columns.Add(col); //12
            col = new DataColumn("CURRENCY", typeof(string)); dtTemp.Columns.Add(col); //13
            col = new DataColumn("CREATEDATE", typeof(DateTime)); dtTemp.Columns.Add(col); //14
            col = new DataColumn("CREATEBY", typeof(string)); dtTemp.Columns.Add(col); //15
            col = new DataColumn("DETAILID", typeof(string)); dtTemp.Columns.Add(col); //16
            col = new DataColumn("MATUREDATE", typeof(DateTime)); dtTemp.Columns.Add(col); //17
            col = new DataColumn("TENOR", typeof(int)); dtTemp.Columns.Add(col); //18
            col = new DataColumn("CREDITVALUE", typeof(float)); dtTemp.Columns.Add(col); //19
            col = new DataColumn("BANKID", typeof(string)); dtTemp.Columns.Add(col); //20
            col = new DataColumn("BANKBRANCHID", typeof(string)); dtTemp.Columns.Add(col); //21
            col = new DataColumn("IDENTITYNO", typeof(string)); dtTemp.Columns.Add(col); //22
            col = new DataColumn("IDENTITYTYPEID", typeof(string)); dtTemp.Columns.Add(col); //23
            col = new DataColumn("OCCUPATIONID", typeof(string)); dtTemp.Columns.Add(col); //24
            col = new DataColumn("LINE1", typeof(string)); dtTemp.Columns.Add(col); //25
            col = new DataColumn("REFERENCE", typeof(string)); dtTemp.Columns.Add(col); //26  
            col = new DataColumn("BIRTHPLACE", typeof(string)); dtTemp.Columns.Add(col); //27  
            col = new DataColumn("BIRTHDATE", typeof(DateTime)); dtTemp.Columns.Add(col); //28 
            col = new DataColumn("KODECABANG", typeof(string)); dtTemp.Columns.Add(col); //29  
            col = new DataColumn("NAMACABANG", typeof(string)); dtTemp.Columns.Add(col); //30  
            col = new DataColumn("PAYMENTDATE", typeof(DateTime)); dtTemp.Columns.Add(col); //30  
            col = new DataColumn("BENEFITVALUE", typeof(float)); dtTemp.Columns.Add(col); //31  
            col = new DataColumn("CLAIMCODE", typeof(string)); dtTemp.Columns.Add(col); //32  
            col = new DataColumn("STATUSID", typeof(string)); dtTemp.Columns.Add(col); //33 
            col = new DataColumn("PRODUCTREFERENCE", typeof(string)); dtTemp.Columns.Add(col); //34
            col = new DataColumn("STATUSDMTM", typeof(int)); dtTemp.Columns.Add(col); //35
            col = new DataColumn("PAYMENTID", typeof(string)); dtTemp.Columns.Add(col); //36

            if (details.Count > 0)
            {
                foreach (JobFileDetail dt in details)
                {
                    DataRow dr = dtTemp.NewRow();
                    dr["JOBFILEID"] = dt.JobFileId;
                    dr["NO"] = dt.No;
                    dr["EFFECTIVEDATE"] = dt.EffectiveDate ?? (object)DBNull.Value;
                    dr["CIFNO"] = dt.Cifno;
                    dr["NAME"] = dt.Name;
                    dr["SEXID"] = dt.SexId;
                    dr["AGE"] = dt.Age;
                    dr["PREMIUM"] = dt.Premium;
                    dr["OUTSTANDING"] = dt.Outstanding;
                    dr["STATUSPROCESS"] = dt.StatusProcess;
                    dr["PRODUCTID"] = dt.ProductId;
                    dr["PAYMENTMODEID"] = dt.PaymentModeId;
                    dr["PAYMENTMETHODID"] = dt.PaymentMethodId;
                    dr["CURRENCY"] = dt.Currency;
                    dr["CREATEDATE"] = dt.CreateDate;
                    dr["CREATEBY"] = dt.CreateBy;
                    dr["DETAILID"] = dt.DetailId;
                    dr["MATUREDATE"] = dt.MatureDate ?? (object)DBNull.Value;
                    dr["TENOR"] = dt.Tenor;
                    dr["CREDITVALUE"] = dt.CreditValue;
                    dr["BANKID"] = dt.BankId;
                    dr["BANKBRANCHID"] = dt.BankBranchId;
                    dr["IDENTITYNO"] = dt.IdentityNo;
                    dr["IDENTITYTYPEID"] = dt.IdentityTypeId;
                    dr["OCCUPATIONID"] = dt.OccupationId;
                    dr["LINE1"] = dt.Line1;
                    dr["REFERENCE"] = dt.Reference;
                    dr["BIRTHPLACE"] = dt.BirthPlace;
                    dr["BIRTHDATE"] = dt.BirthDate ?? (object)DBNull.Value;
                    dr["KODECABANG"] = dt.KodeCabang;
                    dr["NAMACABANG"] = dt.NamaCabang;
                    dr["PAYMENTDATE"] = dt.PaymentDate ?? (object)DBNull.Value;
                    dr["BENEFITVALUE"] = dt.BenefitValue;
                    dr["CLAIMCODE"] = dt.ClaimCode;
                    dr["STATUSID"] = dt.StatusId;
                    dr["PRODUCTREFERENCE"] = dt.ProductReference;
                    dr["STATUSDMTM"] = dt.StatusDMTM;
                    dr["PAYMENTID"] = dt.PaymentId;
                    dtTemp.Rows.Add(dr);
                }
            }

            return dtTemp;
        }

        protected DataTable MapperBatchDetail(List<BatchDetail> details)
        {
            DataTable dtTemp = new DataTable(); // PolicyHistory
            DataColumn col = new DataColumn();

            col = new DataColumn("BATCHID", typeof(string)); dtTemp.Columns.Add(col); //0
            col = new DataColumn("DETAILID", typeof(string)); dtTemp.Columns.Add(col); //1
            col = new DataColumn("TYPEID", typeof(string)); dtTemp.Columns.Add(col); //2
            col = new DataColumn("DESCRIPTION", typeof(string)); dtTemp.Columns.Add(col); //3
            col = new DataColumn("REMARK", typeof(string)); dtTemp.Columns.Add(col); //4 
            col = new DataColumn("CREATEDATE", typeof(DateTime)); dtTemp.Columns.Add(col); //5
            col = new DataColumn("CREATEBY", typeof(string)); dtTemp.Columns.Add(col); //6
            col = new DataColumn("MODIFYDATE", typeof(DateTime)); dtTemp.Columns.Add(col); //7
            col = new DataColumn("MODIFYBY", typeof(string)); dtTemp.Columns.Add(col); //8
            col = new DataColumn("ORDERID", typeof(string)); dtTemp.Columns.Add(col); //9   
            col = new DataColumn("STATUS", typeof(string)); dtTemp.Columns.Add(col); //10
            col = new DataColumn("REMARK2", typeof(string)); dtTemp.Columns.Add(col); //11 

            if (details.Count > 0)
            {
                foreach (BatchDetail dt in details)
                {
                    DataRow dr = dtTemp.NewRow();
                    dr["BATCHID"] = dt.BatchId;
                    dr["DETAILID"] = dt.DetailId;
                    dr["TYPEID"] = dt.TypeId;
                    dr["DESCRIPTION"] = dt.Description;
                    dr["REMARK"] = dt.Remark;
                    dr["CREATEDATE"] = dt.CreateDate ?? (object)DBNull.Value;
                    dr["CREATEBY"] = dt.CreateBy;
                    dr["MODIFYDATE"] = dt.ModifyDate ?? (object)DBNull.Value;
                    dr["MODIFYBY"] = dt.ModifyBy;
                    dr["ORDERID"] = dt.OrderId;
                    dr["STATUS"] = dt.Status;
                    dr["REMARK2"] = dt.Remark2;
                    dtTemp.Rows.Add(dr);
                }
            }

            return dtTemp;
        }
        protected Dictionary<string, Policy> ProcessPolicies(Dictionary<string, BatchDetail> batches, JobFiles jobFile)
        {
            Dictionary<string, Policy> policies = new Dictionary<string, Policy>();
            string productId = string.Empty;
            string policyId = string.Empty;
            DataSet ds = new DataSet();
            //ds = ExecuteSqlQuery(jobFile, 0, true); //default not process
            //int row = ds.Tables[0].Rows.Count;
            //int column = ds.Tables[0].Columns.Count;

            List<JobFileDetail> fileDetails = GetJobFileDetails(null, jobFile, (int)EnumHelper.StatusJobfileDetail.New, batches);

            if (fileDetails.Count > 0)
            {
                total = 0;
                //for (int i = 0; i < row; i++)
                foreach (JobFileDetail jobDetail in fileDetails)
                {
                    //{
                    total += 1;
                    //string detailId = ds.Tables[0].Rows[i][16].ToString();
                    //string JobFileId = ds.Tables[0].Rows[i][1].ToString();
                    policyId = jobDetail.DetailId;
                    BatchDetail detail = new BatchDetail();
                    try
                    {
                        Policy policy = new Policy();
                        Insured insured = new Insured();

                        #region process Bind-JobDetail
                        /*
                        string no = ds.Tables[0].Rows[i][1].ToString();
                        string effectiveDate = ds.Tables[0].Rows[i][2].ToString();
                        string cifNo = ds.Tables[0].Rows[i][3].ToString();
                        string insuredName = ds.Tables[0].Rows[i][4].ToString();
                        string sexId = ds.Tables[0].Rows[i][5].ToString();
                        string ageInsured = ds.Tables[0].Rows[i][6].ToString();
                        string premium = ds.Tables[0].Rows[i][7].ToString();
                        string outstanding = ds.Tables[0].Rows[i][8].ToString();
                        string statusProcess = ds.Tables[0].Rows[i][9].ToString();
                        productId = ds.Tables[0].Rows[i][10].ToString();
                        string payModeId = ds.Tables[0].Rows[i][11].ToString();
                        string payMethodId = ds.Tables[0].Rows[i][12].ToString();
                        string currencyId = ds.Tables[0].Rows[i][13].ToString();
                        string createDate = ds.Tables[0].Rows[i][14].ToString();
                        string createBy = ds.Tables[0].Rows[i][15].ToString();
                        */
                        #endregion

                        #region bind-data Policy

                        detail.Remark = jobDetail.Reference + " " + jobDetail.Cifno + " " + jobDetail.Name;

                        policy = _policyService.CreatePolicy(policy, jobDetail.ProductId);
                        policy.PolicyId = policyId;
                        policy.CurrencyId = jobDetail.Currency;
                        policy.CreateBy = jobDetail.CreateBy;
                        policy.CreateDate = jobDetail.CreateDate;
                        policy.SubmitDate = jobDetail.CreateDate;
                        policy.PaymentMethodId = jobDetail.PaymentMethodId;
                        policy.PaymentModeId = jobDetail.PaymentModeId;
                        policy.StatusId = "IN";
                        policy.AgentId = "BM";
                        policy.ChannelId = "AL";
                        policy.PaidToDate = jobDetail.PaymentDate;
                        if (jobDetail.ProductId == "MANPRO")
                        {
                            policy.ChannelId = "TM";
                        }
                        policy.EffectiveDate = jobDetail.EffectiveDate;
                        policy.Premium = jobDetail.Premium;
                        policy.Discount = 0;
                        policy.RefferenceNo = jobDetail.Reference;
                        policy.Outstanding = jobDetail.Outstanding;
                        policy.CreditValue = jobDetail.CreditValue;
                        policy.StatusReference = jobDetail.StatusId;
                        #endregion

                        #region bind-data Customer
                        Axa.Insurance.Model.Customer customer = new Axa.Insurance.Model.Customer();
                        customer.CIFNo = jobDetail.Cifno;
                        customer.TitleId = GetTitleId(jobDetail.SexId);
                        customer.FirstName = jobDetail.Name;
                        customer.SexId = jobDetail.SexId;
                        customer.BirthDate = jobDetail.BirthDate;
                        customer.ReligionId = "000";
                        customer.BirthPlace = null;
                        customer.OccupationId = "OTH";
                        if (jobDetail.IdentityTypeId == "NPWP")
                        {
                            customer.NPWP = jobDetail.IdentityNo;
                        }
                        else
                        {
                            customer.IdentityTypeId = jobDetail.IdentityTypeId;
                            customer.IdentityNo = jobDetail.IdentityNo;
                        }
                        customer.BankId = jobDetail.BankId;
                        customer.AccountNo = null;
                        customer.AccountName = null;
                        customer.CardTypeId = "OTH";
                        customer.DefaultAddressTypeId = "HA";
                        customer.Email = "-";
                        customer.ModifyBy = "System";
                        customer.CreateDate = jobDetail.CreateDate;
                        customer.CreateBy = jobDetail.CreateBy;

                        if (jobDetail.ProductId == "MANPRO")
                        {
                            DateTime dob = DateTime.Now;
                            customer.BirthDate = new DateTime(dob.Year - jobDetail.Age, dob.Month, dob.Day);
                            customer.FirstName = "Bank Mandiri";
                            customer.AccountNo = "991-00-0000-753-5";
                            customer.AccountName = "GNC";
                            customer.CardName = "GNC";
                        }

                        if (policy.Product.IsPaymentMethodClaim)
                        {
                            customer.FirstName = "BANK MANDIRI";
                            customer.AccountNo = "";
                            customer.AccountName = "";
                            customer.CardName = "";
                        }
                        policy.Customer = customer;

                        #region bind-data Customer Address
                        Address address = customer.Address;
                        address.TypeId = "HA";
                        address.Line1 = "-";
                        address.Line2 = "-";
                        address.Line3 = "-";
                        address.Line4 = "-";
                        address.City = "-";
                        address.ZipCode = "-";
                        #endregion

                        #endregion

                        #region bind-data Insured
                        policy.Insureds = new List<Insured>();
                        insured.InsuredId = total;
                        insured.TypeId = "MI";
                        insured.FirstName = jobDetail.Name;
                        insured.BirthPlace = customer.BirthPlace;
                        insured.BirthDate = customer.BirthDate;
                        insured.ProductId = policy.ProductId;
                        insured.PlanId = "A";
                        if (jobDetail.ProductId == "MANPRO")
                        { insured.PlanId = "JCBP"; }
                        insured.TitleId = customer.TitleId;
                        insured.SexId = customer.SexId;
                        insured.EthnicityId = "001";
                        insured.CountryId = "ID";
                        insured.RelationshipId = "MI";
                        insured.OccupationId = customer.OccupationId;
                        insured.IdentityTypeId = customer.IdentityTypeId;
                        insured.IdentityNo = customer.IdentityNo;
                        insured.CoverageUntil = jobDetail.MatureDate;

                        policy.Insureds.Add(insured);
                        #endregion

                        LogHelper.LogInfo($" Add Policies : " + total, _pluginMetadata);
                        LogHelper.LogInfo($" Add Policies DetailId. " + policyId, _pluginMetadata);

                        policies.Add(policyId, policy);
                    }
                    catch (Exception ex)
                    {
                        LogHelper.LogError($"error Process Bind : " + ex.Message + " " + ex.StackTrace + " " + ex.Source, ex, _pluginMetadata);

                        detail.ModifyBy = "System";
                        detail.Description = string.Format("Err: {0}", ex.Message);
                    }

                    batches.Add(policyId, detail);
                }
            }

            return policies;

        }
        protected int ImportPolicies(List<Policy> policies, Dictionary<string, BatchDetail> batches)
        {
            int succeed = 0;
            foreach (Policy policy in policies)
            {
                string policyId = null;
                BatchDetail detail = null;
                try
                {
                    policyId = policy.PolicyId;
                    detail = batches[policyId];
                    ValidatePolicy(policy);

                    #region ImportPolicy
                    LogHelper.LogInfo($"Proses ImportPolicy - Start : " + succeed, _pluginMetadata);
                    LogHelper.LogInfo($"Proses ImportPolicy - productid : " + policy.ProductId, _pluginMetadata);

                    Policy _policy = _policyService.ImportPolicyLikeManpro(policy);

                    LogHelper.LogInfo($"Proses ImportPolicy - End : " + succeed, _pluginMetadata);
                    #endregion

                    succeed += 1;

                    LogHelper.LogInfo($"Update Detail : " + policyId + " = 1", _pluginMetadata);
                    UpdateJobDetail(policyId, 1);
                    LogHelper.LogInfo($"succeed : " + succeed, _pluginMetadata);
                }
                catch (Exception ex)
                {
                    LogHelper.LogInfo($"Update Detail : " + policyId + " = 2", _pluginMetadata);
                    UpdateJobDetail(policyId, (int)EnumHelper.StatusJobfileDetail.Failed);

                    LogHelper.LogError($"error import : " + ex.Message + " " + ex.StackTrace + " " + ex.Source, ex, _pluginMetadata);
                    if (detail != null)
                    {
                        detail.ModifyBy = "System";
                        if (policy.Product.IsPaymentMethodClaim)
                        {
                            if (ex.Message.Contains("AGE") || ex.Message.Contains("COVERAGE"))
                            {
                                detail.Description = string.Format(ex.Message);
                            }
                            else
                            {
                                detail.Description = string.Format("INVALID DATA");
                                detail.Remark = string.Format(ex.Message);
                            }
                            detail.OrderId = policyId;
                            detail.Remark2 = policy.RefferenceNo;
                        }
                        else
                        {
                            detail.Description = string.Format("Err: {0}", ex.Message);
                        }
                        detail.CreateDate = DateTime.Now;
                        detail.CreateBy = "System";
                        detail.DetailId = Convert.ToInt32(policyId.Substring(17));

                        if (detail.Description.ToLower().Contains("hibernate")) //add by zaky 2019-01-23
                        {
                            detail.Status = null;
                            detail.OrderId = null;
                        }

                        foreach (string key in batches.Keys)
                        {
                            if (key.Length > policyId.Length && key.StartsWith(policyId) && string.IsNullOrEmpty(batches[key].Description)) batches[key].Description = "Err:";
                        }
                    }
                }
            }
            return succeed;
        }
        protected Batch SaveBatch(Dictionary<string, BatchDetail> batches, string description)
        {
            List<BatchDetail> details = new List<BatchDetail>(batches.Values);
            try
            {
                Batch batch = new Batch();
                //Batch batch = _batchService.GetBatch(batchId);

                batch.ModifyBy = "System";
                batch.Description = description;
                batch.TypeId = "IP";
                batch.Details = null;

                batch = _batchService.AddBatch(batch);

                for (int i = details.Count - 1; i >= 0; i--)
                {
                    if (string.IsNullOrEmpty(details[i].Description) || (string.IsNullOrEmpty(details[i].Description) && string.IsNullOrEmpty(details[i].Remark2)))
                    {
                        details.RemoveAt(i);
                    }
                    details[i].BatchId = batch.BatchId;
                }

                LogHelper.LogInfo($"Insert Bulk BatchDetail - Start :" + batch.BatchId, _pluginMetadata);

                if (details != null)
                {
                    DataTable dtTemp = MapperBatchDetail(details);

                    #region Connection

                    LogHelper.LogInfo($"Insert Bulk BatchDetail - Start", _pluginMetadata);

                    using (DbConnection conn = _database.CreateConnection())
                    {
                        //msg += "2";
                        conn.Open();
                        //msg += "3";
                        using (SqlBulkCopy copy = new SqlBulkCopy((SqlConnection)conn, SqlBulkCopyOptions.Default, null))
                        {
                            try
                            {
                                copy.BatchSize = 1000000;
                                //copy.NotifyAfter = 500;
                                copy.DestinationTableName = "BATCHDETAIL";
                                copy.BulkCopyTimeout = 0;
                                copy.WriteToServer(dtTemp);
                                //msg += "4";

                                LogHelper.LogInfo($"Insert Bulk BatchDetail - Success", _pluginMetadata);
                            }
                            catch (Exception ex)
                            {
                                //msg += ex.Message + Environment.NewLine + Environment.NewLine + ex.StackTrace;

                                LogHelper.LogInfo($"Insert Bulk BatchDetail- Failed", _pluginMetadata);
                                throw new Exception($"Insert Bulk BatchDetail - Failed" + ex.Message + "\n" + ex.Source + "\n" + ex.StackTrace);
                            }

                        }
                        conn.Close();
                    }

                    LogHelper.LogInfo($"Insert Bulk BatchDetail - Finish", _pluginMetadata);
                    #endregion 
                }
                return batch;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "\n" + ex.Source + "\n" + ex.StackTrace);
            }
        }
        protected Batch UpdateBatch(Dictionary<string, BatchDetail> batches, string description, JobFiles jobFiles, int unprocessed)
        {
            List<BatchDetail> details = new List<BatchDetail>(batches.Values);
            try
            {
                for (int i = details.Count - 1; i >= 0; i--)
                {
                    if (string.IsNullOrEmpty(details[i].Description) || string.IsNullOrEmpty(details[i].Remark2))
                    {
                        details.RemoveAt(i);
                    }
                    details[i].BatchId = jobFiles.Remark;
                }

                Batch batch = new Batch();

                if (unprocessed == 0)
                {
                    batch = _batchService.GetBatch(jobFiles.Remark);

                    batch.Description = description;
                    batch.ModifyDate = DateTime.Now;
                    _batchPersistence.Update(batch);
                }

                if (details != null)
                {
                    //int detailId = 0;
                    //List<Criteria> criteriaDetail = new List<Criteria>();
                    //criteriaDetail.Add(new Criteria("p.BatchId", jobFiles.Remark, Comparison.Equal));
                    //detailId = _batchDetailPersistence.Find(criteriaDetail);
                    //detailId = _batchDetailPersistence.Count(criteriaDetail);

                    //foreach (BatchDetail detail in details)
                    //{
                    //    //detailId += 1;
                    //    detail.BatchId = jobFiles.Remark;
                    //    //detail.DetailId = detailId;

                    //    _batchDetailPersistence.Add(detail);
                    //}

                    DataTable dtTemp = MapperBatchDetail(details);

                    #region Connection

                    LogHelper.LogInfo($"Insert Bulk BatchDetail - Start", _pluginMetadata);

                    using (DbConnection conn = _database.CreateConnection())
                    {
                        //msg += "2";
                        conn.Open();
                        //msg += "3";
                        using (SqlBulkCopy copy = new SqlBulkCopy((SqlConnection)conn, SqlBulkCopyOptions.Default, null))
                        {
                            try
                            {
                                copy.BatchSize = 500;
                                copy.NotifyAfter = 500;
                                copy.DestinationTableName = "BATCHDETAIL";
                                copy.BulkCopyTimeout = 0;
                                copy.WriteToServer(dtTemp);
                                //msg += "4";

                                LogHelper.LogInfo($"Insert Bulk BatchDetail - Success", _pluginMetadata);
                            }
                            catch (Exception ex)
                            {
                                //msg += ex.Message + Environment.NewLine + Environment.NewLine + ex.StackTrace;

                                LogHelper.LogInfo($"Insert Bulk BatchDetail- Failed", _pluginMetadata);
                                throw new Exception($"Insert Bulk BatchDetail - Failed");
                            }

                        }
                        conn.Close();
                    }

                    LogHelper.LogInfo($"Insert Bulk BatchDetail - Finish", _pluginMetadata);
                    #endregion 
                }
                return batch;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        protected string Implode(string[] values, string sep)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < values.Length; i++)
            {
                if (i < values.Length - 1)
                {
                    sb.AppendFormat("{0}{1}", values[i], sep);
                }
                else
                {
                    sb.AppendFormat("{0}", values[i]);
                }
            }

            return sb.ToString();
        }
        protected string GetMaritalStatusId(string maritalId)
        {
            switch (maritalId)
            {
                case "L":
                case "M":
                    return "M";
                case "P":
                case "F":
                    return "F";
            }

            return null;
        }
        protected DateTime? GetDate(string value, string format)
        {
            int day = 0;
            int month = 0;
            int year = 0;
            DateTime? date = null;
            try
            {
                switch (format.ToLower().Trim())
                {
                    case "ddmmyyyy":
                        day = int.Parse(value.Substring(0, 2));
                        month = int.Parse(value.Substring(2, 2));
                        year = int.Parse(value.Substring(4, 4));
                        break;
                    case "yymmdd":
                        year = int.Parse(DateTime.Now.Year.ToString().Substring(0, 2) + value.Substring(0, 2));
                        month = int.Parse(value.Substring(2, 2));
                        day = int.Parse(value.Substring(4, 2));
                        break;
                }
                if (year == 0000 || month == 00 || day == 00)
                {
                    date = null;
                }
                else
                {
                    date = new DateTime(year, month, day);
                }
            }
            catch (Exception ex)
            {

            }

            return date;
        }
        protected string GetOccupationId(string desc)
        {
            string occupationId = string.Empty;
            List<Occupation> occupations = new List<Occupation>();
            List<Criteria> criterias = new List<Criteria>();
            try
            {
                criterias.Add(new Criteria("s.Name", desc, Comparison.Like));
                occupations = _occupationService.FindOccupations(criterias, 0, int.MaxValue, null);
                if (occupations.Count > 0)
                {
                    occupationId = occupations[0].OccupationId;
                }
                else
                {
                    throw new Exception("Occupation is Not Found");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return occupationId;
        }
        protected string GetTitleId(string maritalId)
        {
            switch (maritalId)
            {
                case "M":
                    return "MR";
                case "F":
                    return "MRS";
            }

            return null;
        }
        protected void ValidatePolicy(Policy policy)
        {
            StringBuilder error = new StringBuilder();
            if (policy.Product.IsPaymentMethodClaim)
            {
                if (string.IsNullOrEmpty(policy.CurrencyId))
                {
                    error.AppendFormat("INVALID CHAR ON FIELD PISCUR WITH VALUE : NULL or EMPTY");
                }
                else if (string.IsNullOrEmpty(policy.StatusReference))
                {
                    error.AppendFormat("INVALID CHAR ON FIELD PISSTS WITH VALUE : NULL or EMPTY");
                }
                else if (string.IsNullOrEmpty(policy.ProductId))
                {
                    error.AppendFormat("INVALID CHAR ON FIELD PISPRD WITH VALUE : NULL or EMPTY");
                }
                else if (string.IsNullOrEmpty(policy.Customer.CIFNo))
                {
                    error.AppendFormat("INVALID CHAR ON FIELD PISCIF WITH VALUE : NULL or EMPTY");
                }
                else if (!string.IsNullOrEmpty(policy.Customer.CIFNo) && !policy.Customer.CIFNo.All(char.IsDigit))
                {
                    error.AppendFormat("INVALID CHAR ON FIELD PISCIF WITH VALUE : " + policy.Customer.CIFNo);
                }
                else if (string.IsNullOrEmpty(policy.Customer.Name))
                {
                    error.AppendFormat("INVALID CHAR ON FIELD PISNAM WITH VALUE : NULL or EMPTY");
                }
                else if (string.IsNullOrEmpty(policy.Customer.SexId))
                {
                    error.AppendFormat("INVALID CHAR ON FIELD PISSEX WITH VALUE : NULL or EMPTY");
                }
                else if (!string.IsNullOrEmpty(policy.Customer.SexId) && (policy.Customer.SexId != "F" && policy.Customer.SexId != "M"))
                {
                    error.AppendFormat("INVALID CHAR ON FIELD PISSEX WITH VALUE : " + policy.Customer.SexId);
                }
                else if (string.IsNullOrEmpty(policy.Customer.IdentityTypeId))
                {
                    error.AppendFormat("INVALID CHAR ON FIELD PISCFC WITH VALUE : NULL or EMPTY");
                }
                else if (string.IsNullOrEmpty(policy.Customer.IdentityNo))
                {
                    error.AppendFormat("INVALID CHAR ON FIELD PISCFS WITH VALUE : NULL or EMPTY");
                }
                else if (policy.CreditValue < 0)
                {
                    error.AppendFormat("INVALID FLOAT ON FIELD PISAFT WITH VALUE : " + policy.CreditValue);
                }
                else if (policy.Premium < 0)
                {
                    error.AppendFormat("INVALID FLOAT ON FIELD PISAMT WITH VALUE : " + policy.Premium);
                }
                else if (string.IsNullOrEmpty(policy.Customer.BirthDate.ToString()))
                {
                    error.AppendFormat("INVALID DATE ON FIELD PISDB6 or PISDOB");
                }
                else if (string.IsNullOrEmpty(policy.EffectiveDate.ToString()))
                {
                    error.AppendFormat("INVALID DATE ON FIELD PISOP6 or PISOPD");
                }
                else if (string.IsNullOrEmpty(policy.Insureds[0].CoverageUntil.ToString()))
                {
                    error.AppendFormat("INVALID DATE ON FIELD PISMT6 or PISMTD");
                }
                else if (string.IsNullOrEmpty(policy.PaidToDate.ToString()))
                {
                    error.AppendFormat("INVALID DATE ON FIELD PISLP6 or PISLPD");
                }
            }
            else
            {
                if (string.IsNullOrEmpty(policy.Customer.CIFNo))
                {
                    error.AppendFormat("CIFNo is empty, ");
                }
                if (string.IsNullOrEmpty(policy.EffectiveDate.ToString()))
                {
                    error.AppendFormat("Effective date is empty, ");
                }

                if (string.IsNullOrEmpty(policy.Customer.SexId))
                {
                    error.AppendFormat("SexId is empty, ");
                }
                if (string.IsNullOrEmpty(policy.Insureds[0].BirthDate.ToString()))
                {
                    error.AppendFormat("Birth date is empty, ");
                }
            }
            if (string.IsNullOrEmpty(policy.Insureds[0].BirthDate.ToString()))
            {
                error.AppendFormat("Birth date is empty, ");
            }

            if (error.Length > 0)
            {
                //throw new NotValidException(error.ToString()); 
                throw new Exception(error.ToString());
            }
        }
        private DataSet ExecuteSqlQuery(JobFiles jobFile)
        {
            DataSet ds = new DataSet();
            string sql = string.Empty;
            sql = $"SELECT * FROM UPLOADMTR where JobFileId = @jobFileId";

            LogHelper.LogInfo($"Select data UploadMTR - Start", _pluginMetadata);
            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                try
                {
                    #region AddParameter  
                    var jobFileId = new SqlParameter("jobFileId", SqlDbType.VarChar);
                    jobFileId.Value = jobFile.JobFileId;
                    command.Parameters.Add(jobFileId);
                    #endregion

                    #region fill data from JobDetail
                    connection.Open();
                    SqlDataAdapter da = new SqlDataAdapter(command);
                    da.Fill(ds);
                    da.Dispose();
                    connection.Close();
                    LogHelper.LogInfo($"Select data UploadMTR - Success", _pluginMetadata);
                    #endregion
                }
                catch (Exception ex)
                {
                    connection.Close();
                    LogHelper.LogInfo($"Select data UploadMTR - Failed", _pluginMetadata);
                    LogHelper.LogInfo(ex.Message, _pluginMetadata);
                }
            }

            return ds;

        }
        private DataSet ExecuteSqlQuery(JobFiles jobFile, int status, bool withLimit)
        {
            DataSet ds = new DataSet();
            string sql = string.Empty;
            if (withLimit)
            {
                sql = "SELECT Top " + recordLimit + " * from JobFileDetail where StatusProcess = @statusProcess and JobFileId = @jobFileId";

            }
            else
            {
                sql = "SELECT * from JobFileDetail where StatusProcess = @statusProcess and JobFileId = @jobFileId";

            }

            LogHelper.LogInfo($"Select DetailFile - Start", _pluginMetadata);
            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                try
                {
                    #region AddParameter 
                    var statusProcess = new SqlParameter("statusProcess", SqlDbType.Int);
                    statusProcess.Value = status;
                    command.Parameters.Add(statusProcess);

                    var jobFileId = new SqlParameter("jobFileId", SqlDbType.VarChar);
                    jobFileId.Value = jobFile.JobFileId;
                    command.Parameters.Add(jobFileId);
                    #endregion

                    #region fill data from JobDetail
                    connection.Open();
                    SqlDataAdapter da = new SqlDataAdapter(command);
                    da.Fill(ds);
                    da.Dispose();
                    connection.Close();
                    LogHelper.LogInfo($"Select DetailFile - Success", _pluginMetadata);
                    #endregion
                }
                catch (Exception ex)
                {
                    connection.Close();
                    LogHelper.LogInfo($"Select DetailFile - Failed", _pluginMetadata);
                    LogHelper.LogInfo(ex.Message, _pluginMetadata);
                }
            }

            return ds;

        }
        /// <summary>
        /// value of status
        /// 0 = data not proccessed
        /// 1 = data is proccessed and success
        /// 2 = data is proccessed and failed
        /// </summary>
        /// <param name="detailId"></param>
        /// <param name="status"></param>
        private void UpdateJobDetail(string detailId, int status)
        {
            string sql = "Update JobFileDetail set StatusProcess = @statusProcess where DetailId = @jobDetailId";
            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                try
                {
                    #region AddParameter  
                    var statusProcess = new SqlParameter("statusProcess", SqlDbType.Int);
                    statusProcess.Value = status;
                    command.Parameters.Add(statusProcess);

                    var jobDetailId = new SqlParameter("jobDetailId", SqlDbType.VarChar);
                    jobDetailId.Value = detailId;
                    command.Parameters.Add(jobDetailId);
                    #endregion  

                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();

                    LogHelper.LogInfo($"Update Detail : " + detailId + " = " + status + " - Success", _pluginMetadata);
                }
                catch (Exception ex)
                {

                    LogHelper.LogInfo($"Update Detail : " + detailId + " = " + status + " - Failed", _pluginMetadata);
                    LogHelper.LogInfo(ex.Message, _pluginMetadata);
                }
            }
        }
        protected void EndProcessJobFiles(JobFiles jobFile, string filePath)
        {
            DataSet ds = new DataSet();
            ds = ExecuteSqlQuery(jobFile, 1, false);
            int rowSuccess = ds.Tables[0].Rows.Count;

            if (rowSuccess > 0)
            {
                jobFile.Status = "Done";
                jobFile.SuksesRow = rowSuccess;
                jobFile.GagalRow = jobFile.TotalRow - rowSuccess;
                jobFile.DateEndProcess = DateTime.Now;
                _jobFilesService.UpdateJobFiles(jobFile);

                fileservice.MoveFile(filePath, successFolder);
            }
            else
            {
                jobFile.Status = "Failed";
                jobFile.SuksesRow = rowSuccess;
                jobFile.GagalRow = jobFile.TotalRow - rowSuccess;
                jobFile.DateEndProcess = DateTime.Now;
                _jobFilesService.UpdateJobFiles(jobFile);

                fileservice.MoveFile(filePath, failedFolder);
            }

        }
        /// <summary>
        /// insert data failed to table jobFileDetailFailed
        /// </summary>
        /// <param name="jobFileId"></param>
        /// <param name="no"></param>
        /// <param name="data"></param>
        private void InsertJobDetailFailed(string jobFileId, int no, string data)
        {
            string sql = "Insert into JOBFILEDETAILFAILED (JobFileId, Row, Data) values (@JobFileId, @Row, @Data)";
            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                try
                {
                    #region AddParameter  
                    var _jobFileId = new SqlParameter("JobFileId", SqlDbType.VarChar);
                    _jobFileId.Value = jobFileId;
                    command.Parameters.Add(_jobFileId);

                    var _row = new SqlParameter("Row", SqlDbType.Int);
                    _row.Value = no;
                    command.Parameters.Add(_row);

                    var _data = new SqlParameter("Data", SqlDbType.VarChar);
                    _data.Value = data;
                    command.Parameters.Add(_data);
                    #endregion  

                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();

                    LogHelper.LogInfo($"Insert Failed No : " + no + " - Success", _pluginMetadata);
                }
                catch (Exception ex)
                {

                    LogHelper.LogInfo($"Insert Failed No : " + no + " - Failed", _pluginMetadata);
                    LogHelper.LogInfo(ex.Message, _pluginMetadata);
                }
            }
        }
        /// <summary>
        /// process failed date which statusProcess=2
        /// </summary>
        /// <param name="jobFile"></param>
        private void ProcFailedData(JobFiles jobFile)
        {
            string[] rows = content.Split(new string[] { LineSep }, StringSplitOptions.None);

            DataSet dsFailed = new DataSet();
            dsFailed = ExecuteSqlQuery(jobFile, 2, false); //copy failed data to table failed
            if (dsFailed.Tables[0].Rows.Count > 0)
            {
                string data = string.Empty;
                int no = 0;
                string jobFileId = jobFile.JobFileId;
                string[] header = rows[0].Split(new string[] { FieldSep }, StringSplitOptions.None);
                data = Implode(header, FieldSep);

                InsertJobDetailFailed(jobFileId, no, data);

                for (int i = 0; i < dsFailed.Tables[0].Rows.Count; i++)
                {
                    for (int row = 1; row < rows.Length; row++)
                    {
                        no = row;
                        string[] colls = rows[row].Split(new string[] { FieldSep }, StringSplitOptions.None);
                        if (dsFailed.Tables[0].Rows[i][2].ToString() == colls[0].Trim() &&
                           dsFailed.Tables[0].Rows[i][4].ToString() == colls[2].Trim() &&
                           dsFailed.Tables[0].Rows[i][5].ToString() == colls[3].Trim() &&
                           dsFailed.Tables[0].Rows[i][7].ToString() == colls[5].Trim()
                          )
                        {
                            data = Implode(colls, FieldSep);
                            break;
                        }
                    }
                    InsertJobDetailFailed(jobFileId, no, data);
                }
            }
        }
        private void ProcFailedDataExcel(ExcelWorksheet sheet, JobFiles jobFile)
        {
            DataSet dsFailed = new DataSet();
            dsFailed = ExecuteSqlQuery(jobFile, 2, false); //copy failed data to table failed
            if (dsFailed.Tables[0].Rows.Count > 0)
            {
                string data = string.Empty;
                int no = 0;
                string jobFileId = jobFile.JobFileId;
                int rowCount = sheet.Dimension.Rows;
                int columns = sheet.Dimension.Columns;
                for (int col = 1; col <= columns; col++)
                {
                    if (col == columns)
                    {
                        data += sheet.Cells[1, col].Value.ToString();
                    }
                    else
                    {
                        data += sheet.Cells[1, col].Value.ToString() + FieldSep;
                    }
                }

                InsertJobDetailFailed(jobFileId, no, data);

                //data = "";
                //for (int i = 0; i < dsFailed.Tables[0].Rows.Count; i++)
                //{
                //    for (int col = 1; col <= columns; col++)
                //    {
                //        no = i;
                //        if (col == columns)
                //        {
                //            data += sheet.Cells[i, col].Value.ToString();
                //        }
                //        else
                //        {
                //            data += sheet.Cells[i, col].Value.ToString() + FieldSep;
                //        }
                //    }

                //    InsertJobDetailFailed(jobFileId, no, data);
                //}
            }
        }
        protected List<JobFileDetail> GetJobFileDetails(ExcelWorksheet sheet, JobFiles jobFile, int status, Dictionary<string, BatchDetail> batches)
        {
            List<Criteria> criterias = new List<Criteria>();
            List<JobFileDetail> jDetails = new List<JobFileDetail>();
            try
            {
                criterias.Add(new Criteria("p.JobFileId", jobFile.JobFileId, Comparison.Equal));
                criterias.Add(new Criteria("p.StatusProcess", status, Comparison.Equal));

                jDetails = _jobFileDetailsService.FindJobFileDetail(criterias, 0, recordLimit, " p.DetailId asc");
                if (jDetails.Count > 0 && sheet != null)
                {
                    int columns = sheet.Dimension.Columns;
                    int row = 2;
                    foreach (JobFileDetail jobDetail in jDetails)
                    {
                        string field = "";
                        BatchDetail detail = new BatchDetail();
                        for (int col = 1; col <= columns; col++)
                        {
                            if (col == columns)
                            {
                                field += sheet.Cells[row, col].Value.ToString();
                            }
                            else
                            {
                                field += sheet.Cells[row, col].Value.ToString() + FieldSep;
                            }
                        }
                        detail.TypeId = "POL";
                        detail.Remark = field;

                        batches.Add(jobDetail.DetailId, detail);
                        row++;
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogError($"error Get JobFileDetail : " + ex.Message + " " + ex.StackTrace + " " + ex.Source, ex, _pluginMetadata);

            }
            return jDetails;
        }
        protected int JobFileDetailAPI(JobFiles jobFile, List<JobFileDetail> jobFileDetails, Dictionary<string, BatchDetail> batches)
        {
            int succeed = 0;
            string addCustomerCLP = _pluginMetadata.PluginSettings.GetValue<string>("addCustomerCLP") != "" ? _pluginMetadata.PluginSettings.GetValue<string>("addCustomerCLP") : "dmtm";

            Axa.Insurance.Model.Customer cust = new Axa.Insurance.Model.Customer();
            List<Policy> policies = new List<Policy>();
            Dictionary<string, JobFileDetail> details = new Dictionary<string, JobFileDetail>();

            if (!string.IsNullOrEmpty(jobFile.CustomerId))
            {
                cust = _customerService.GetCustomer(jobFile.CustomerId);
            }
            else
            {
                throw new Exception("CustomerId is null");
            }

            foreach (JobFileDetail jd in jobFileDetails)
            {
                BatchDetail detail = null;
                JobFileDetail JobDetailGetAggregate = new JobFileDetail();
                try
                {
                    detail = batches[jd.DetailId];
                    jd.OccupationId = GetOccupationId(jd.OccupationId);

                    //validate can'not backdate 
                    ValidateNB(jd);

                    //LogHelper.LogInfo($"CustomerId : " + cust.CustomerId, _pluginMetadata);
                    //LogHelper.LogInfo($"OccupationId : " + jd.OccupationId, _pluginMetadata);
                    //LogHelper.LogInfo($"NPWP : " + cust.NPWP, _pluginMetadata);


                    #region GetPremium
                    //LogHelper.LogInfo($"Proses GetPremium - Start : " + succeed, _pluginMetadata);

                    JobFileDetail JobDetailGetPremium = _apiHelper.GetPremium(jd);
                    //LogHelper.LogInfo($"Proses GetPremium - Premium : " + JobDetailGetPremium.Premium, _pluginMetadata);

                    //LogHelper.LogInfo($"Proses GetPremium - End : " + succeed, _pluginMetadata);
                    #endregion

                    #region GetAggregate
                    //LogHelper.LogInfo($"Proses GetAggregate - Start : " + succeed, _pluginMetadata);
                    if (JobDetailGetPremium.Premium > 0)
                    {
                        JobDetailGetAggregate = _apiHelper.GetAggregate(JobDetailGetPremium);
                        //LogHelper.LogInfo($"Proses GetAggregate - Premium : " + JobDetailGetAggregate.Premium, _pluginMetadata);
                    }

                    //LogHelper.LogInfo($"Proses GetAggregate - End : " + succeed, _pluginMetadata);
                    #endregion

                    #region AddCustomer
                    //LogHelper.LogInfo($"Proses AddCustomer - Start : " + succeed, _pluginMetadata);
                    if (JobDetailGetAggregate.Premium > 0)
                    {
                        switch (addCustomerCLP)
                        {
                            case "dmtm":
                                LogHelper.LogInfo($"Proses AddCustomer - start with : " + addCustomerCLP, _pluginMetadata);
                                AddCustomerCLP(JobDetailGetAggregate, cust);
                                LogHelper.LogInfo($"Proses AddCustomer - end with : " + addCustomerCLP, _pluginMetadata);
                                break;
                            case "api":
                                LogHelper.LogInfo($"Proses AddCustomer - start with : " + addCustomerCLP, _pluginMetadata);
                                JobFileDetail JobDetailAddCustomer = _apiHelper.AddCustomer(cust, JobDetailGetAggregate);
                                string policyNo = _apiHelper.Resp.Body.Customer.hasPolicyAccount == null ? "" : _apiHelper.Resp.Body.Customer.hasPolicyAccount[0].policyNO;
                                string ePolicy = _apiHelper.Resp.Body.Customer.hasPolicyAccount == null ? "" : _apiHelper.Resp.Body.Customer.hasPolicyAccount[0].ePolicy;
                                string policyStatus = _apiHelper.Resp.Body.Customer.hasPolicyAccount == null ? "" : _apiHelper.Resp.Body.Customer.hasPolicyAccount[0].policyStatusCD;

                                //LogHelper.LogInfo($"PolicyNo : " + policyNo, _pluginMetadata);

                                //v2 
                                PolicyHistory history = new PolicyHistory();
                                if (!string.IsNullOrEmpty(ePolicy))
                                {
                                    history.Remark = "Certificate policy successfully created";
                                }
                                else
                                {
                                    history.Remark = "Certificate policy not successfully created";
                                }
                                history.PolicyId = policyNo;
                                history.StatusId = policyStatus;
                                history.EventId = "PU";
                                history.Description = policyNo;
                                history.EventDate = DateTime.Now;
                                history.CreateBy = "System";

                                _historyService.AddPolicyHistory(history);

                                LogHelper.LogInfo($"Proses AddCustomer - end with : " + addCustomerCLP, _pluginMetadata);
                                break;
                        }

                    }

                    //LogHelper.LogInfo($"Proses AddCustomer - End : " + succeed, _pluginMetadata);
                    #endregion

                    succeed += 1;

                    LogHelper.LogInfo($"Update Detail : " + jd.DetailId + " = 1", _pluginMetadata);
                    UpdateJobDetail(jd.DetailId, (int)EnumHelper.StatusJobfileDetail.Proceed);
                    LogHelper.LogInfo($"succeed : " + succeed, _pluginMetadata);
                }
                catch (Exception ex)
                {
                    LogHelper.LogInfo($"Update Detail : " + jd.DetailId + " = 2", _pluginMetadata);
                    UpdateJobDetail(jd.DetailId, (int)EnumHelper.StatusJobfileDetail.Failed);
                    LogHelper.LogError($"error import : " + ex.Message + " " + ex.StackTrace + " " + ex.Source, ex, _pluginMetadata);
                    if (detail != null)
                    {
                        detail.ModifyBy = "System";
                        detail.Description = string.Format("Err: {0}", ex.Message);

                        if (detail.Description.ToLower().Contains("hibernate")) //add by zaky 2019-01-23
                        {
                            //detail.Status = "Invalid";
                            detail.Status = null;
                            detail.OrderId = null;
                        }

                        foreach (string key in batches.Keys)
                        {
                            if (key.Length > jd.DetailId.Length && key.StartsWith(jd.DetailId) && string.IsNullOrEmpty(batches[key].Description)) batches[key].Description = "Err:";
                        }
                    }
                }
            }

            return succeed;
        }
        protected void ValidateCertifcate(List<Policy> policies)
        {
            Task.Delay(3000);

            foreach (Policy pol in policies)
            {
                if (pol.PolicyReceiveDate.HasValue)
                {
                    _policyService.AddHistory(pol.CreateBy, pol, "PU", pol.PolicyId, "Certificate policy successfully created, PolicyReciveDate :  " + pol.PolicyReceiveDate);
                }
                else
                {
                    _policyService.AddHistory(pol.CreateBy, pol, "PU", pol.PolicyId, "Certificate policy not successfully created");
                }
            }
        }
        protected void AddCustomerCLP(JobFileDetail dataDetail, Axa.Insurance.Model.Customer cust)
        {
            #region bind-data Policy
            Policy _pol2 = new Policy();

            _pol2 = _policyService.CreatePolicy(_pol2, dataDetail.ProductId);
            _pol2.PolicyId = dataDetail.DetailId;
            _pol2.CoveragePeriod = dataDetail.Tenor;
            _pol2.CreditValue = dataDetail.CreditValue;
            _pol2.CurrencyId = dataDetail.Currency;
            _pol2.CreateBy = dataDetail.CreateBy;
            _pol2.CreateDate = dataDetail.CreateDate;
            _pol2.SubmitDate = dataDetail.CreateDate;
            _pol2.PaymentMethodId = dataDetail.PaymentMethodId;
            _pol2.PaymentModeId = dataDetail.PaymentModeId;
            _pol2.StatusId = dataDetail.StatusId;
            _pol2.AgentId = "Partner";
            _pol2.ChannelId = "TM";
            _pol2.EffectiveDate = dataDetail.EffectiveDate;
            _pol2.PaidToDate = dataDetail.MatureDate;
            _pol2.CreateBy = "System";
            _pol2.Premium = dataDetail.Premium;
            _pol2.Discount = 0;
            _pol2.RefferenceNo = dataDetail.Reference;
            _pol2.Outstanding = dataDetail.Outstanding;
            _pol2.BranchId = dataDetail.KodeCabang;
            _pol2.BasicPremium = dataDetail.Premium;
            _pol2.CorrespondenceTypeId = "L";
            _pol2.Tenor = _pol2.CoveragePeriod;
            _pol2.Cabang = dataDetail.NamaCabang;
            _pol2.CustomerId = cust.CustomerId;
            _pol2.ProductId = dataDetail.ProductId;
            #endregion

            #region bind-data Customer 
            _pol2.Customer = cust;

            #region bind-data Customer Address
            //Address address = cust.Address;
            #endregion

            #endregion

            #region bind-data Insured
            _pol2.Insureds = new List<Insured>();
            Insured _insured2 = new Insured();
            _insured2.InsuredId = total;
            _insured2.TypeId = "MI";
            _insured2.FirstName = dataDetail.Name;
            _insured2.BirthPlace = dataDetail.BirthPlace;
            _insured2.BirthDate = dataDetail.BirthDate;
            _insured2.ProductId = _pol2.ProductId;
            _insured2.PlanId = "A";
            _insured2.TitleId = GetTitleId(dataDetail.SexId);
            _insured2.SexId = dataDetail.SexId;
            _insured2.EthnicityId = "001";
            _insured2.CountryId = "ID";
            _insured2.RelationshipId = "MI";
            _insured2.OccupationId = dataDetail.OccupationId;
            _insured2.IdentityTypeId = dataDetail.IdentityTypeId;
            _insured2.IdentityNo = dataDetail.IdentityNo;
            _insured2.Premium = dataDetail.Premium;
            _insured2.BasicPremium = dataDetail.Premium;
            _insured2.CoverageUntil = dataDetail.MatureDate;

            _pol2.Insureds.Add(_insured2);
            #endregion

            LogHelper.LogInfo($"ImportPolicy - Start : " + _pol2.CustomerId, _pluginMetadata);
            Policy _policy = _policyService.ImportPolicyLikeManpro(_pol2);
            LogHelper.LogInfo($"ImportPolicy - End : " + _policy.CustomerId, _pluginMetadata);

            LogHelper.LogInfo($"Validation E-certificate - Start : " + _policy.PolicyId, _pluginMetadata);
            if (_policy.PolicyReceiveDate.HasValue)
            {
                _policyService.AddHistory(_policy.CreateBy, _policy, "PU", _policy.PolicyId, "Certificate policy successfully created, PolicyReciveDate :  " + _policy.PolicyReceiveDate);
            }
            else
            {
                _policyService.AddHistory(_policy.CreateBy, _policy, "PU", _policy.PolicyId, "Certificate policy not successfully created");
            }

            LogHelper.LogInfo($"Validation E-certificate - Start : " + _policy.PolicyId, _pluginMetadata);
        }
        protected void ValidateNB(JobFileDetail jobfileDetail)
        {
            StringBuilder error = new StringBuilder();
            DateTime createdate = Convert.ToDateTime(jobfileDetail.CreateDate.Value.ToString("yyyy-MM-dd"));
            DateTime effdate = Convert.ToDateTime(jobfileDetail.EffectiveDate.Value.ToString("yyyy-MM-dd"));
            TimeSpan td = createdate.Subtract(effdate);
            if (td.TotalDays > 0)
            {
                error.AppendFormat("Can't process with backdate");
            }

            //List<Criteria> citeriaRefference = new List<Criteria>(); 
            //citeriaRefference.Add(new Criteria("c.RefferenceNo", jobfileDetail.Reference, Comparison.Equal));
            //citeriaRefference.Add(new Criteria("Query", " not c.StatusId in ('CA', 'CO', 'SU', 'EX', 'DE')", Comparison.None));
            //int duplicatePolicy = _policyService.CountPolicy(citeriaRefference);
            //if (duplicatePolicy > 0)
            //{
            //    error.AppendFormat("Can't process with duplicate policy");
            //}

            if (error.Length > 0)
            {
                throw new Exception(error.ToString());
            }
        }
    }
}
