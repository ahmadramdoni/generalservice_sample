﻿using GeneralService.Plugin;
using System.Collections.Generic;
using GeneralService.Core.Helper;
using System;
using System.IO;
using Axa.Insurance.Service;
using Axa.Insurance.Model;
using Axa.Insurance.Helper;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Configuration;
using System.Data.SqlTypes;
using System.Globalization;
using Newtonsoft.Json;
using RestSharp;
using System.Net;
using GeneralService.ImportPolicyJobFiles.Models;

namespace GeneralService.ImportPolicyJobFiles.Helper
{
    public class APIHelper
    {
        public GetResponse Resp;
        public WriteJsonHelper WriteJson;
        public string base_url = "";
        string exception = "";
        string status = "";
        public string endpoint_sms = ConfigurationManager.AppSettings["EndpointSMS"];
        public string endpointGetPremium;
        public string endpointGetAggregate;
        public string endpointAddCustomer;
        public string userName;
        public string password;
        public string userNameAdd;
        public string passwordAdd;
        private static PluginMetadata _pluginMetadata;
        public APIHelper(PluginMetadata metadata)
        {
            _pluginMetadata = metadata;
            endpointGetPremium = _pluginMetadata.PluginSettings.GetValue<string>("EndpointGetPremium");
            endpointGetAggregate = _pluginMetadata.PluginSettings.GetValue<string>("EndpointGetAggregate");
            endpointAddCustomer = _pluginMetadata.PluginSettings.GetValue<string>("EndpointAddCustomer");
            userName = _pluginMetadata.PluginSettings.GetValue<string>("UserName");
            password = _pluginMetadata.PluginSettings.GetValue<string>("password");
            userNameAdd = _pluginMetadata.PluginSettings.GetValue<string>("UserNameAdd");
            passwordAdd = _pluginMetadata.PluginSettings.GetValue<string>("passwordAdd");
        }
        public JobFileDetail GetPremium(JobFileDetail dataDetail)
        {
            //LogHelper.LogInfo($"Proses GetPremium - Endpoint : " + endpointGetPremium, _pluginMetadata);
            //LogHelper.LogInfo($"Proses GetAggregate - Endpoint : " + endpointGetAggregate, _pluginMetadata);
            //LogHelper.LogInfo($"Proses AddCustomer - Endpoint : " + endpointAddCustomer, _pluginMetadata);

            JobFileDetail jDetail = new JobFileDetail();
            jDetail = dataDetail;
            //This solved SSL Failed Handshake Issue
            ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            RestClient client = new RestClient(endpointGetPremium);
            var request = new RestRequest();

            request.Method = Method.POST;
            WriteJson = new WriteJsonHelper();
            SendRequest req = new SendRequest();
            Header header = new Header();
            SecurityContext securityContext = new SecurityContext();
            securityContext.username = userName;
            securityContext.password = password;
            header.SecurityContext = securityContext;
            req.Header = header;
            Body body = new Body();
            body.transactionId = dataDetail.ProductId + "-" + DateTime.Now.ToString("yyyyMMdd") + "-" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 14);
            body.appID = "DMTM_REST";
            body.entity = "AMFS";
            body.service = "getUWData";
            body.operation = "getPremium";

            GeneralService.ImportPolicyJobFiles.Models.Customer customer = new GeneralService.ImportPolicyJobFiles.Models.Customer();
            List<HasPolicyAccount> hasPolicyAccounts = new List<HasPolicyAccount>();
            HasPolicyAccount hasPolicyAccount = new HasPolicyAccount();

            List<IsAssociatedWithLifeInsuranceProduct> isAssociatedWithLifeInsuranceProducts = new List<IsAssociatedWithLifeInsuranceProduct>();
            IsAssociatedWithLifeInsuranceProduct isAssociatedWithLifeInsuranceProduct = new IsAssociatedWithLifeInsuranceProduct();
            isAssociatedWithLifeInsuranceProduct.productTypeCD = dataDetail.ProductId;
            isAssociatedWithLifeInsuranceProducts.Add(isAssociatedWithLifeInsuranceProduct);


            List<HasDetailsOfRisksIn> hasDetailsOfRisksIns = new List<HasDetailsOfRisksIn>();
            HasDetailsOfRisksIn hasDetailsOfRisksIn = new HasDetailsOfRisksIn();
            hasDetailsOfRisksIn.coveredSalaryAmt = dataDetail.CreditValue.ToString();
            hasDetailsOfRisksIn.expSalaryGrowthRt = dataDetail.Tenor.ToString();

            List<HasPersonalDetailsIn> hasPersonalDetailsIns = new List<HasPersonalDetailsIn>();
            HasPersonalDetailsIn hasPersonalDetailsIn = new HasPersonalDetailsIn();
            hasPersonalDetailsIn.birthDT = dataDetail.BirthDate.Value.ToString("yyyy-MM-dd");
            hasPersonalDetailsIns.Add(hasPersonalDetailsIn);

            hasDetailsOfRisksIn.hasPersonalDetailsIn = hasPersonalDetailsIns;
            hasDetailsOfRisksIns.Add(hasDetailsOfRisksIn);

            hasPolicyAccount.isAssociatedWithLifeInsuranceProduct = isAssociatedWithLifeInsuranceProducts;

            hasPolicyAccount.hasDetailsOfRisksIn = hasDetailsOfRisksIns;
            hasPolicyAccount.policyEffectiveDTTM = dataDetail.EffectiveDate.Value.ToString("yyyy-MM-dd");

            hasPolicyAccounts.Add(hasPolicyAccount);
            customer.hasPolicyAccount = hasPolicyAccounts;

            body.Customer = customer;
            req.Body = body;

            string requestContent = JsonConvert.SerializeObject(req);
            WriteJson.Write(requestContent, "reqGetPremium");
            request.AddParameter("application/json", requestContent, ParameterType.RequestBody);
            //client.ClearHandlers();
            var response = client.Execute(request) as IRestResponse;
            WriteJson.Write(response.Content, "RespGetPremium");
            try
            {
                Resp = JsonConvert.DeserializeObject<GetResponse>(response.Content);
            }
            catch (Exception ex)
            {
                throw new Exception(response.Content);
            }

            if (Resp.Body.status == "1")
            {
                jDetail.Premium = decimal.Parse(Resp.Body.Customer.hasPolicyAccount[0].premiumAMT);
            }
            if (Resp.Body.exception != null)
            {
                throw new Exception(Resp.Body.exception.message + ", "+ Resp.Body.exception.backEndError);
            }
            return jDetail;

        }
        public JobFileDetail GetAggregate(JobFileDetail dataDetail)
        {
            JobFileDetail jDetail = new JobFileDetail();
            jDetail = dataDetail;
            //This solved SSL Failed Handshake Issue
            ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            RestClient client = new RestClient(endpointGetAggregate);
            var request = new RestRequest();

            request.Method = Method.POST;
            WriteJson = new WriteJsonHelper();
            SendRequest req = new SendRequest();
            Header header = new Header();
            SecurityContext securityContext = new SecurityContext();
            securityContext.username = userName;
            securityContext.password = password;
            header.SecurityContext = securityContext;
            req.Header = header;
            Body body = new Body();
            body.transactionId = dataDetail.ProductId + "-" + DateTime.Now.ToString("yyyyMMdd") + "-" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 14);
            body.appID = "DMTM_REST";
            body.entity = "AMFS";
            body.service = "getUWData";
            body.operation = "getAggregate";

            GeneralService.ImportPolicyJobFiles.Models.Customer customer = new GeneralService.ImportPolicyJobFiles.Models.Customer();
            List<HasPolicyAccount> hasPolicyAccounts = new List<HasPolicyAccount>();
            HasPolicyAccount hasPolicyAccount = new HasPolicyAccount();

            List<IsAssociatedWithLifeInsuranceProduct> isAssociatedWithLifeInsuranceProducts = new List<IsAssociatedWithLifeInsuranceProduct>();
            IsAssociatedWithLifeInsuranceProduct isAssociatedWithLifeInsuranceProduct = new IsAssociatedWithLifeInsuranceProduct();
            isAssociatedWithLifeInsuranceProduct.productTypeCD = dataDetail.ProductId;
            isAssociatedWithLifeInsuranceProducts.Add(isAssociatedWithLifeInsuranceProduct);


            List<HasDetailsOfRisksIn> hasDetailsOfRisksIns = new List<HasDetailsOfRisksIn>();
            HasDetailsOfRisksIn hasDetailsOfRisksIn = new HasDetailsOfRisksIn();
            hasDetailsOfRisksIn.coveredSalaryAmt = dataDetail.CreditValue.ToString();
            hasDetailsOfRisksIn.expSalaryGrowthRt = dataDetail.Tenor.ToString();

            List<HasPersonalDetailsIn> hasPersonalDetailsIns = new List<HasPersonalDetailsIn>();
            HasPersonalDetailsIn hasPersonalDetailsIn = new HasPersonalDetailsIn();
            hasPersonalDetailsIn.birthDT = dataDetail.BirthDate.Value.ToString("yyyy-MM-dd");
            hasPersonalDetailsIn.firstName = dataDetail.Name;
            hasPersonalDetailsIn.socialSecurityNO = dataDetail.IdentityNo;
            hasPersonalDetailsIns.Add(hasPersonalDetailsIn);

            hasDetailsOfRisksIn.hasPersonalDetailsIn = hasPersonalDetailsIns;
            hasDetailsOfRisksIns.Add(hasDetailsOfRisksIn);

            hasPolicyAccount.isAssociatedWithLifeInsuranceProduct = isAssociatedWithLifeInsuranceProducts;

            hasPolicyAccount.hasDetailsOfRisksIn = hasDetailsOfRisksIns;
            hasPolicyAccounts.Add(hasPolicyAccount);
            customer.hasPolicyAccount = hasPolicyAccounts;

            body.Customer = customer;
            req.Body = body;

            string requestContent = JsonConvert.SerializeObject(req);
            WriteJson.Write(requestContent, "reqGetAggregate");
            request.AddParameter("application/json", requestContent, ParameterType.RequestBody);
            //client.ClearHandlers();
            var response = client.Execute(request) as IRestResponse;
            WriteJson.Write(response.Content, "RespGetAggregate");
            try
            {
                Resp = JsonConvert.DeserializeObject<GetResponse>(response.Content);
            }
            catch (Exception ex)
            {
                throw new Exception(response.Content);
            }
            if (Resp.Body.status == "1")
            {
                jDetail.Premium = decimal.Parse(Resp.Body.Customer.hasPolicyAccount[0].premiumAMT);
            }
            if (Resp.Body.exception != null)
            {
                throw new Exception(Resp.Body.exception.message + ", " + Resp.Body.exception.backEndError);
            }
            return jDetail;
        }
        public JobFileDetail AddCustomer(Axa.Insurance.Model.Customer cust, JobFileDetail dataDetail)
        {
            JobFileDetail jDetail = new JobFileDetail();
            jDetail = dataDetail;
            //This solved SSL Failed Handshake Issue
            ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            RestClient client = new RestClient(endpointAddCustomer);
            var request = new RestRequest();

            request.Method = Method.POST;
            WriteJson = new WriteJsonHelper();
            SendRequest req = new SendRequest();
            Header header = new Header();
            SecurityContext securityContext = new SecurityContext();
            securityContext.username = userNameAdd;
            securityContext.password = passwordAdd;
            header.SecurityContext = securityContext;
            req.Header = header;
            Body body = new Body();
            body.transactionId = dataDetail.ProductId + "-" + DateTime.Now.ToString("yyyyMMdd") + "-" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 14);
            body.appID = "DMTM_REST";
            body.entity = "AMFS";
            body.service = "addCustomer";
            body.operation = "AddCustomerDMTM";

            GeneralService.ImportPolicyJobFiles.Models.Customer customer = new GeneralService.ImportPolicyJobFiles.Models.Customer();
            List<HasPolicyAccount> hasPolicyAccounts = new List<HasPolicyAccount>();
            List<HasAddressesIn> hasAddressInCusts = new List<HasAddressesIn>();
            List<HasAddressesIn> hasAddressInInss = new List<HasAddressesIn>();
            List<HasPartyAccountDetailsIn> hasPartyAccountDetailsIns = new List<HasPartyAccountDetailsIn>();
            List<HasDetailsOfRisksIn> hasDetailsOfRisksIns = new List<HasDetailsOfRisksIn>();
            List<HasPersonalDetailsIn> hasPersonalDetailsIns = new List<HasPersonalDetailsIn>();
            List<IsAssociatedWithLifeInsuranceProduct> isAssociatedWithLifeInsuranceProducts = new List<IsAssociatedWithLifeInsuranceProduct>();
            List<HasAXAAsiaPolicyPaymentAccount> hasAXAAsiaPolicyPaymentAccounts = new List<HasAXAAsiaPolicyPaymentAccount>();
            List<HasAXAAsiaPartyAccount> hasAXAAsiaPartyAccounts = new List<HasAXAAsiaPartyAccount>();

            CanBeIndividual canBeIndividual = new CanBeIndividual();
            //canBeIndividual.birthDT = cust.BirthDate.Value.ToString("yyyy-MM-dd");
            //canBeIndividual.firstNM = cust.Name;
            //canBeIndividual.genderCD = cust.SexId;
            //canBeIndividual.socialSecurityNO = cust.IdentityNo;
            canBeIndividual.centralTaxNo = cust.NPWP;

            //HasAddressesIn hasAddressInCust = new HasAddressesIn();
            //hasAddressInCust.addressTypeCD = cust.Addresses[0].TypeId;
            //hasAddressInCust.mobilePhoneNO = cust.MobilePhone;
            //hasAddressInCust.telephoneNO = cust.HomePhone;
            //hasAddressInCust.officeTelNo = cust.OfficePhone;
            //hasAddressInCust.addressLine1 = cust.Address.Line1;
            //hasAddressInCust.countryCD = cust.CountryId;
            //hasAddressInCust.emailAddress = cust.Email;
            //hasAddressInCust.cityNM = cust.Addresses[0].City;
            //hasAddressInCust.postalCD = cust.Addresses[0].ZipCode;
            //hasAddressInCust.provinceNm = cust.Addresses[0].ProvinceId;
            //hasAddressInCusts.Add(hasAddressInCust);

            //canBeIndividual.hasAddressesIn = hasAddressInCusts;

            //HasPartyAccountDetailsIn HasPartyAccountDetailsIn = new HasPartyAccountDetailsIn();
            //HasPartyAccountDetailsIn.payorFirstNM = dataDetail.Name;
            //HasPartyAccountDetailsIn.accountBankCD = "1";
            //hasPartyAccountDetailsIns.Add(HasPartyAccountDetailsIn);
            //canBeIndividual.hasPartyAccountDetailsIns = hasPartyAccountDetailsIns;

            HasPolicyAccount hasPolicyAccount = new HasPolicyAccount();
            hasPolicyAccount.paidToDT = dataDetail.MatureDate.Value.ToString("yyyy-MM-dd");
            hasPolicyAccount.validFromDttm = dataDetail.EffectiveDate.Value.ToString("yyyy-MM-dd");
            hasPolicyAccount.termProcessDt = dataDetail.CreateDate.Value.ToString("yyyy-MM-dd");
            hasPolicyAccount.policyRK = dataDetail.Reference;
            hasPolicyAccount.policyStatusCD = string.IsNullOrEmpty(_pluginMetadata.PluginSettings.GetValue<string>("policyStatusCD")) ? "PAI" : _pluginMetadata.PluginSettings.GetValue<string>("policyStatusCD"); //default
            //hasPolicyAccount.agencyBinderNo = "112233";
            hasPolicyAccount.mailingResponsibiltyCd = "F"; //default
            hasPolicyAccount.paymentMethodCD = dataDetail.PaymentMethodId;
            hasPolicyAccount.paymentModeCD = dataDetail.PaymentModeId;
            hasPolicyAccount.paymentStatusCd = string.IsNullOrEmpty(_pluginMetadata.PluginSettings.GetValue<string>("paymentStatusCd")) ? "PAI" : _pluginMetadata.PluginSettings.GetValue<string>("paymentStatusCd");
            hasPolicyAccount.issueBranchCD = dataDetail.KodeCabang;
            //hasPolicyAccount.coveredSalaryAmt = dataDetail.CreditValue.ToString();
            //hasPolicyAccount.expSalaryGrowthRt = dataDetail.Tenor.ToString();

            IsAssociatedWithLifeInsuranceProduct isAssociatedWithLifeInsuranceProduct = new IsAssociatedWithLifeInsuranceProduct();
            isAssociatedWithLifeInsuranceProduct.productTypeCD = dataDetail.ProductId;
            isAssociatedWithLifeInsuranceProducts.Add(isAssociatedWithLifeInsuranceProduct);


            HasDetailsOfRisksIn hasDetailsOfRisksIn = new HasDetailsOfRisksIn();
            hasDetailsOfRisksIn.occupationClassCd = dataDetail.OccupationId;
            hasDetailsOfRisksIn.memberPlanNo = "A";
            hasDetailsOfRisksIn.existingInsuranceInd = "MI";
            hasDetailsOfRisksIn.insuredRK = dataDetail.ProductId;
            hasDetailsOfRisksIn.coveredSalaryAmt = dataDetail.CreditValue.ToString();
            hasDetailsOfRisksIn.expSalaryGrowthRt = dataDetail.Tenor.ToString();
            //IsAssociatedWithIssuer isAssociatedWithIssuer = new IsAssociatedWithIssuer();
            //isAssociatedWithIssuer.issuerName = "Rasyid Bvt";
            //isAssociatedWithIssuer.issuerAppID = "123456789";
            //isAssociatedWithIssuer.issuedNO = "1234567";

            //hasPolicyAccount.isAssociatedWithIssuer = isAssociatedWithIssuer;

            HasPersonalDetailsIn hasPersonalDetailsIn = new HasPersonalDetailsIn();
            hasPersonalDetailsIn.birthDT = dataDetail.BirthDate.Value.ToString("yyyy-MM-dd");
            hasPersonalDetailsIn.birthPlace = dataDetail.BirthPlace;
            hasPersonalDetailsIn.genderCD = dataDetail.SexId;
            hasPersonalDetailsIn.socialSecurityNO = dataDetail.IdentityNo;
            hasPersonalDetailsIn.documentTypeCD = dataDetail.IdentityTypeId;
            hasPersonalDetailsIn.firstNM = dataDetail.Name;

            HasAddressesIn hasAddressInIns = new HasAddressesIn();
            hasAddressInIns.addressTypeCD = "HA";
            hasAddressInIns.mobilePhoneNO = "-";
            hasAddressInIns.telephoneNO = "-";
            hasAddressInIns.officeTelNo = "-";
            hasAddressInIns.addressLine1 = dataDetail.Line1;
            hasAddressInIns.countryCD = "ID";
            hasAddressInIns.emailAddress = "-";
            hasAddressInIns.cityNM = "-";
            hasAddressInIns.postalCD = "-";
            //hasAddressInIns.provinceNm = "-";
            hasAddressInInss.Add(hasAddressInIns);
            hasPersonalDetailsIn.hasAddressesIn = hasAddressInInss;
            hasPersonalDetailsIns.Add(hasPersonalDetailsIn);

            hasDetailsOfRisksIn.hasPersonalDetailsIn = hasPersonalDetailsIns;
            hasDetailsOfRisksIns.Add(hasDetailsOfRisksIn);

            hasPolicyAccount.isAssociatedWithLifeInsuranceProduct = isAssociatedWithLifeInsuranceProducts;
            hasPolicyAccount.hasDetailsOfRisksIn = hasDetailsOfRisksIns;

            //HasAXAAsiaPolicyPaymentAccount hasAXAAsiaPolicyPaymentAccount = new HasAXAAsiaPolicyPaymentAccount();
            //HasAXAAsiaPartyAccount hasAXAAsiaPartyAccount = new HasAXAAsiaPartyAccount();
            //hasAXAAsiaPartyAccount.accountNO = "1234567890";
            //hasAXAAsiaPartyAccount.partyAccountNM = "AZIZ";
            //hasAXAAsiaPartyAccount.cardExpirationDT = "2025";
            //hasAXAAsiaPartyAccount.creditCardNO = "1002003004000";
            //hasAXAAsiaPartyAccount.cardType = "VS";
            //hasAXAAsiaPartyAccount.bankName = "Bank Mandiri";
            //hasAXAAsiaPartyAccounts.Add(hasAXAAsiaPartyAccount);
            //hasAXAAsiaPolicyPaymentAccount.hasAXAAsiaPartyAccount = hasAXAAsiaPartyAccounts;

            hasPolicyAccounts.Add(hasPolicyAccount);
            customer.sourceCInd = string.IsNullOrEmpty(_pluginMetadata.PluginSettings.GetValue<string>("sourceCInd")) ? "CLP" : _pluginMetadata.PluginSettings.GetValue<string>("sourceCInd");
            customer.canBeIndividual = canBeIndividual;
            customer.hasPolicyAccount = hasPolicyAccounts;

            body.Customer = customer;
            req.Body = body;

            string requestContent = JsonConvert.SerializeObject(req);
            WriteJson.Write(requestContent, "reqAddCustomer");
            request.AddParameter("application/json", requestContent, ParameterType.RequestBody);
            //client.ClearHandlers();
            var response = client.Execute(request) as IRestResponse;
            WriteJson.Write(response.Content, "RespAddCustomer");
            try
            {
                Resp = JsonConvert.DeserializeObject<GetResponse>(response.Content);
            }
            catch (Exception ex)
            {
                throw new Exception(response.Content);
            }
            if (Resp.Body.status == "1" && !string.IsNullOrEmpty(Resp.Body.Customer.hasPolicyAccount[0].policyNO))
            {
                LogHelper.LogInfo($"Sukses", _pluginMetadata);
            }
            if (Resp.Body.exception != null)
            {
                throw new Exception(Resp.Body.exception.message + ", " + Resp.Body.exception.backEndError);
            }
            return jDetail;
        }
    }
}
