﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using GeneralService.Core.Helper;
using GeneralService.Plugin;

namespace GeneralService.ImportPolicyJobFiles.Helper
{
    class FileHelper
    {
        public void MoveFile(string source, string destination)
        {
            string fileDestination = null;
            if (!string.Equals(Regex.Match(destination, @"(.{1})\s*$").ToString(), "\\"))
            {
                fileDestination = destination + "\\" + Path.GetFileName(source);
            }

            string fileNameOnly = Path.GetFileNameWithoutExtension(source);
            string extension = Path.GetExtension(source);

            int count = 1;
            while (File.Exists(fileDestination))
            {
                string tempFileName = string.Format("{0}({1})", fileNameOnly, count++);
                fileDestination = Path.Combine(destination, tempFileName + extension);
            }

            File.Move(source, fileDestination);

            LogHelper.LogInfo(string.Format("File has been moved to folder " + new DirectoryInfo(destination).Name), null);
        }
    }
}
