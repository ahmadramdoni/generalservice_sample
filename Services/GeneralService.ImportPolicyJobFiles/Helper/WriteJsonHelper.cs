﻿using GeneralService.Plugin;
using System.Collections.Generic;
using GeneralService.Core.Helper;
using System;
using System.IO;
using Axa.Insurance.Service;
using Axa.Insurance.Model;
using Axa.Insurance.Helper;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Configuration;
using System.Data.SqlTypes;
using System.Globalization;
using Newtonsoft.Json;
using RestSharp; 
using System.Net;

namespace GeneralService.ImportPolicyJobFiles.Helper
{
    public class WriteJsonHelper
    {
        public void Write(string content, string prefixname)
        {
            string path = ConfigurationManager.AppSettings["JsonPath"].ToString();

            try
            {
                if (!string.IsNullOrEmpty(path))
                {
                    path = Path.Combine(path, prefixname + "-" + DateTime.Now.ToString("yyyy-MM-dd hhmmss", DateTimeFormatInfo.InvariantInfo) + ".json");

                    using (StreamWriter sw = File.AppendText(path))
                    {
                        sw.WriteLine(content);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.ToString());
                // do nothing, this is for testing purpose only
            }

        }
    }
}
