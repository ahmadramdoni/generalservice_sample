﻿using GeneralService.Plugin;
using System.Collections.Generic;
using GeneralService.Core.Helper;
using System;
using System.IO;
using Axa.Insurance.Service;
using Axa.Insurance.Model;
using Axa.Insurance.Helper;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Configuration;
using System.Data.SqlTypes;
using System.Globalization;
using OfficeOpenXml;
using System.Linq;
using System.Threading.Tasks;
using Axa.Insurance.Persistence;
using GeneralService.UploadMTR.Helper;

namespace GeneralService.UploadMTR
{
    public class Service : IPlugin
    {
        private IBatchService _batchService = ContextHelper.GetObject<IBatchService>();
        private IBatchPersistence _batchPersistence = ContextHelper.GetObject<IBatchPersistence>();
        private IBatchDetailPersistence _batchDetailPersistence = ContextHelper.GetObject<IBatchDetailPersistence>();
        private IJobFilesService _jobFilesService = ContextHelper.GetObject<IJobFilesService>();
        private IJobFileDetailService _jobFileDetailsService = ContextHelper.GetObject<IJobFileDetailService>();
        private IPolicyService _policyService = ContextHelper.GetObject<IPolicyService>();
        private IPolicyHistoryService _historyService = ContextHelper.GetObject<IPolicyHistoryService>();
        private ICustomerService _customerService = ContextHelper.GetObject<ICustomerService>();
        private IAddressService _addressService = ContextHelper.GetObject<IAddressService>();
        private IOccupationService _occupationService = ContextHelper.GetObject<IOccupationService>();
        private IProductService _productServices = ContextHelper.GetObject<IProductService>();
        private ISequencePersistence _sequencePersistence = ContextHelper.GetObject<ISequencePersistence>();
        private IProductService _productService = ContextHelper.GetObject<IProductService>();
        private ICustomerProductService _customerProductService = ContextHelper.GetObject<ICustomerProductService>();
        private IPremiumService _premiumService = ContextHelper.GetObject<IPremiumService>();

        const string LineSep = "\r\n";
        const string FieldSep = "\t";
        private int total = 0;
        private string _productId = string.Empty;
        private string _statusId = string.Empty;
        private string _jobfileId = string.Empty;
        private string content = string.Empty;
        private string messageError = string.Empty;
        private string successFolder = "";
        private string failedFolder = "";
        private string targetDirectory = "";
        private string uploadBy = "";
        private string _batchId = "";
        private int recordLimit = 0;
        private int recordFiles = 0;
        private int startRecord = 0;
        private Database _database = DatabaseFactory.CreateDatabase();
        private static PluginMetadata _pluginMetadata;
        private Product _product;
        FileHelper fileservice = new FileHelper();
        ContextHelper helper = new ContextHelper();
        private static Dictionary<string, string> _args;
        private string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        DateTime? _dateValue = null;
        DateTime? _birthDate = null;
        DateTime? _effectiveDate = null;
        DateTime? _matureDate = null;
        DateTime? _paymentDate = null;
        int _age = 0;
        public void Run(PluginMetadata metadata, Dictionary<string, string> args = null)
        {
            try
            {
                _args = args;
                _pluginMetadata = metadata;
                BindArgs();

                uploadBy = _pluginMetadata.PluginSettings.GetValue<string>("UploadBy");
                successFolder = _pluginMetadata.PluginSettings.GetValue<string>("SuccessFolder");
                failedFolder = _pluginMetadata.PluginSettings.GetValue<string>("FailedFolder");
                targetDirectory = _pluginMetadata.PluginSettings.GetValue<string>("DestinationFolder");
                recordLimit = _pluginMetadata.PluginSettings.GetValue<int>("MaxRecordProcessedPerBatch") > 0 ? _pluginMetadata.PluginSettings.GetValue<int>("MaxRecordProcessedPerBatch") : 10000;
                recordFiles = _pluginMetadata.PluginSettings.GetValue<int>("MaxRecordFilesSelected") > 0 ? _pluginMetadata.PluginSettings.GetValue<int>("MaxRecordFilesSelected") : 1;
                startRecord = _pluginMetadata.PluginSettings.GetValue<int>("StartRecord") >= 0 ? _pluginMetadata.PluginSettings.GetValue<int>("StartRecord") : 1;

                bool autoGenerate = _pluginMetadata.PluginSettings.GetValue<bool>("AutoGenerateJobFiles");
                if (autoGenerate)
                {
                    GenerateJobFiles();
                }

                LogHelper.LogInfo($"ProductId : " + _productId + ", Status : " + _statusId + ", JobFileId : " + _jobfileId, _pluginMetadata);
                ProcessDirectory();
            }
            catch (Exception ex)
            {
                LogHelper.LogError($"Error executing {metadata.ServiceId}, {ex.Message} || {ex.StackTrace}", ex, _pluginMetadata);
            }
        }
        private void BindArgs()
        {
            if (_args != null)
            {
                if (_args.ContainsKey("JobFileId"))
                {
                    _args.TryGetValue("JobFileId", out _jobfileId);
                    _jobfileId = _jobfileId.Trim();
                }
                if (_args.ContainsKey("ProductId"))
                {
                    _args.TryGetValue("ProductId", out _productId);
                    _productId = _productId.Trim();
                }
                if (_args.ContainsKey("Status"))
                {
                    _args.TryGetValue("Status", out _statusId);
                    _statusId = _statusId.Trim();
                }
            }

        }

        protected void ProcessDirectory()
        {
            FileHelper fileservice = new FileHelper();

            List<Criteria> criterias = new List<Criteria>();
            List<JobFiles> jobFiles = new List<JobFiles>();
            //criterias.Add(new Criteria("Query", " p.Status IN ('Processing','New')", Comparison.None));
            try
            {
                if (!string.IsNullOrEmpty(_productId) && !string.IsNullOrEmpty(_statusId))
                {
                    criterias.Add(new Criteria("p.ProductId", _productId, Comparison.Equal));
                    criterias.Add(new Criteria("p.Status", _statusId, Comparison.Equal));

                }
                else if (!string.IsNullOrEmpty(_jobfileId) && !string.IsNullOrEmpty(_statusId))
                {
                    criterias.Add(new Criteria("p.JobFileId", _jobfileId, Comparison.Equal));
                    criterias.Add(new Criteria("p.Status", _statusId, Comparison.Equal));
                }
                else if (!string.IsNullOrEmpty(_productId))
                {
                    criterias.Add(new Criteria("p.JobFileId", _jobfileId, Comparison.Equal));
                    criterias.Add(new Criteria("p.Status", "New", Comparison.Equal));
                }
                else
                {
                    throw new Exception($"Error executing ProductId or Status or JobFileId is empty");
                }

                jobFiles = _jobFilesService.FindJobFiles(criterias, startRecord, recordFiles, "p.CreateDate asc");

                LogHelper.LogInfo($"job files found = " + jobFiles.Count + " records", _pluginMetadata);

                if (jobFiles != null && jobFiles.Count > 0)
                {
                    foreach (var jobFile in jobFiles)
                    {
                        LogHelper.LogInfo($"processing job file with id = " + jobFile.JobFileId, _pluginMetadata);

                        string filename = jobFile.FileName;
                        string fullPath = Path.Combine(targetDirectory, filename);

                        //if (Path.GetExtension(filename) == ".txt" && File.Exists(fullPath))
                        if (File.Exists(fullPath))
                        {
                            LogHelper.LogInfo($"Process : " + jobFile.JobFileId + " FileName : " + filename + " Start", _pluginMetadata);

                            try
                            {
                                ProcessMTRRevamp(fullPath, jobFile);
                            }
                            catch (System.Exception ex)
                            {
                                fileservice.MoveFile(fullPath, failedFolder);
                                LogHelper.LogError($"Error executing {_pluginMetadata.ServiceId}, {ex.Message} || {ex.StackTrace}", ex, _pluginMetadata);
                            }
                            LogHelper.LogInfo($"Process : " + jobFile.JobFileId + " FileName : " + filename + " Finish", _pluginMetadata);
                        }
                        else if (!string.IsNullOrEmpty(jobFile.Status) && jobFile.Status.ToLower() == "done")
                        {
                            try
                            {
                                Database database = DatabaseFactory.CreateDatabase();
                                DbCommand cmd = database.GetStoredProcCommand("sp_UPLOAD_GENERALSERVICETODMTM");

                                cmd.CommandTimeout = 0;
                                database.ExecuteNonQuery(cmd);
                            }
                            catch (Exception ex)
                            {
                                LogHelper.LogError($"Invalid ExecuteNonQuery : " + ex.Message + " " + ex.StackTrace + " " + ex.Source, ex, _pluginMetadata);
                            }
                            /*
                            SqlClass ObjSql = new SqlClass();

                            using (SqlConnection conn = ObjSql.OpenSqlConn())
                            {
                                try
                                {
                                    using (var command = new SqlCommand($"EXEC sp_UPLOAD_GENERALSERVICETODMTM", conn))
                                    {
                                        command.CommandType = CommandType.Text;

                                        command.ExecuteNonQuery();
                                    }
                                    ObjSql.CloseSqlConn(conn);
                                }
                                catch (Exception ex)
                                {
                                    ObjSql.CloseSqlConn(conn);
                                    //throw new Exception("Invalid ExecuteNonQuery : " + ex.Message);
                                    LogHelper.LogError($"Invalid ExecuteNonQuery : " + ex.Message + " " + ex.StackTrace + " " + ex.Source, ex, _pluginMetadata);
                                }
                            }
                            */

                        }
                        else
                        {
                            throw new Exception($"format file is not .txt or not exist");
                        }
                    }
                }
                else
                {
                    throw new Exception($"jobFiles is Empty");
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex.Message + ex.StackTrace, ex, _pluginMetadata);
            }

        }
        private void GenerateJobFiles()
        {
            LogHelper.LogInfo("GenerateJobFiles", _pluginMetadata);
            var files = Directory.GetFiles(targetDirectory);

            foreach (var file in files)
            {
                FileInfo fileInfo = new FileInfo(file);
                LogHelper.LogInfo($"Filename {fileInfo.Name}", _pluginMetadata);

                if (CheckJobFiles(fileInfo.Name)) //cek apakah jobs sudah ada apa blm
                {
                    LogHelper.LogInfo($"Filename {fileInfo.Name} already exist in the joblist", _pluginMetadata);
                }
                else
                {
                    JobFiles jobFiles = new JobFiles();
                    jobFiles.JobFileId = NextJobFileId();

                    LogHelper.LogInfo($"Filename : {fileInfo.Name}, JobFileId : {jobFiles.JobFileId} processing...", _pluginMetadata);

                    try
                    {
                        jobFiles.CreateDate = DateTime.Now;
                        jobFiles.ProductId = _productId;
                        jobFiles.UserId = uploadBy; // "AutoImport";
                        jobFiles.FileName = fileInfo.Name;
                        //jobFiles.FileSize = fileInfo.Length;
                        jobFiles.FileCreationDate = fileInfo.CreationTime.ToString("yyyy-MM-dd hh:mm:ss");
                        jobFiles.Status = "New";
                        jobFiles.JobType = "UPL";
                        jobFiles.TotalRow = UploadMTR(jobFiles, file);

                        _jobFilesService.AddJobFiles(jobFiles);

                        var batch = CreateBatch(file);
                    }
                    catch (Exception ex)
                    {

                    }

                    LogHelper.LogInfo($"Filename : {fileInfo.Name}, JobFileId : {jobFiles.JobFileId} done...", _pluginMetadata);
                }

            }
        }
        private int UploadMTR(JobFiles jobfile, string filePath)
        {
            string content = string.Empty;
            content = File.ReadAllText(filePath);
            List<Axa.Insurance.Model.UploadMTR> dataUploads = new List<Axa.Insurance.Model.UploadMTR>();
            string[] rows = content.Split(new string[] { "\n" }, StringSplitOptions.None);
            string msg = "";
            int total = 0;
            string dataRow = string.Empty;

            try
            {
                Product product = new Product();
                product = _productService.GetProduct("TRMIDR");

                List<Premium> _premiums = new List<Premium>();
                string _planId = "A";

                _premiums = _premiumService.GetAllPremiums("TRMIDR", _planId);

                for (int row = 0; row < rows.Length; row++)
                {
                    string PISBR = string.Empty; //Cabang
                    string PISACT = string.Empty; //Nomor polis
                    string PISCUR = string.Empty; //Mata Uang/Valuta
                    string PISSTS = string.Empty; //Status 1.Active 2.Close 4.Active 7.Claim 9.Freeze 
                    string PISPRD = string.Empty; //Product
                    string PISCIF = string.Empty; //CIF
                    string PISNAM = string.Empty; //Nama Peserta
                    string PISSEX = string.Empty; //Gender
                    string PISCFC = string.Empty; //ID Card
                    string PISCFS = string.Empty; //ID #
                    string PISAFT = string.Empty; //Manfaat Bulanan (dalam plain text)
                    string PISAMT = string.Empty; //Premi (dalam plain text)
                    string PISCOD = string.Empty; //Kode Claim
                    string PISDB6 = string.Empty; //Tgl Lahir (ddmmyy)
                    string PISOP6 = string.Empty; //Tgl buka Polis (ddmmyy)
                    string PISMT6 = string.Empty; //Jatuh Tempo/Maturity Date (ddmmyy)
                    string PISLP6 = string.Empty; //Tgl terakhir Bayar(ddmmyy)
                    string PISDOB = string.Empty; //Tgl tLahir (julian date)
                    string PISOPD = string.Empty; //Tgl buka Polis(julian date)
                    string PISMTD = string.Empty; //Jatuh Tempo/Maturity Date (julian date)
                    string PISLPD = string.Empty; // Tgl terakhir Bayar (julian date)
                    /*
                        Julian Date
                        2021044 -> hari ke 044 di tahun 2021 
                        Kode Claim -> 
                     */

                    #region Get Data
                    //string dataRow = data.Replace(Environment.NewLine, "");
                    dataRow = rows[row];//.Replace("\r\n", "").Replace("\n", "").Replace("\r", "");
                    int length = dataRow.Length;

                    if (length <= 0)
                    {
                        continue;
                    }
                    else if (length < 6)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, length);
                    }
                    else if (length < 25)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, length - 5);
                    }
                    else if (length < 29)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, length - 24);
                    }
                    else if (length < 30)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, length - 28);
                    }
                    else if (length < 40)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, 1);
                        PISPRD = dataRow.Substring(29, length - 29);
                    }
                    else if (length < 59)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, 1);
                        PISPRD = dataRow.Substring(29, 10);
                        PISCIF = dataRow.Substring(39, length - 39);
                    }
                    else if (length < 79)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, 1);
                        PISPRD = dataRow.Substring(29, 10);
                        PISCIF = dataRow.Substring(39, 19);
                        PISNAM = dataRow.Substring(58, length - 58);
                    }
                    else if (length < 80)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, 1);
                        PISPRD = dataRow.Substring(29, 10);
                        PISCIF = dataRow.Substring(39, 19);
                        PISNAM = dataRow.Substring(58, 20);
                        PISSEX = dataRow.Substring(78, length - 78);
                    }
                    else if (length < 84)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, 1);
                        PISPRD = dataRow.Substring(29, 10);
                        PISCIF = dataRow.Substring(39, 19);
                        PISNAM = dataRow.Substring(58, 20);
                        PISSEX = dataRow.Substring(78, 1);
                        PISCFC = dataRow.Substring(79, length - 79);
                    }
                    else if (length < 124)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, 1);
                        PISPRD = dataRow.Substring(29, 10);
                        PISCIF = dataRow.Substring(39, 19);
                        PISNAM = dataRow.Substring(58, 20);
                        PISSEX = dataRow.Substring(78, 1);
                        PISCFC = dataRow.Substring(79, 4);
                        PISCFS = dataRow.Substring(83, length - 83);
                    }
                    else if (length < 141)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, 1);
                        PISPRD = dataRow.Substring(29, 10);
                        PISCIF = dataRow.Substring(39, 19);
                        PISNAM = dataRow.Substring(58, 20);
                        PISSEX = dataRow.Substring(78, 1);
                        PISCFC = dataRow.Substring(79, 4);
                        PISCFS = dataRow.Substring(83, 40);
                        PISAFT = dataRow.Substring(123, length - 123);
                    }
                    else if (length < 158)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, 1);
                        PISPRD = dataRow.Substring(29, 10);
                        PISCIF = dataRow.Substring(39, 19);
                        PISNAM = dataRow.Substring(58, 20);
                        PISSEX = dataRow.Substring(78, 1);
                        PISCFC = dataRow.Substring(79, 4);
                        PISCFS = dataRow.Substring(83, 40);
                        PISAFT = dataRow.Substring(123, 17);
                        PISAMT = dataRow.Substring(140, length - 140);
                    }
                    else if (length < 159)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, 1);
                        PISPRD = dataRow.Substring(29, 10);
                        PISCIF = dataRow.Substring(39, 19);
                        PISNAM = dataRow.Substring(58, 20);
                        PISSEX = dataRow.Substring(78, 1);
                        PISCFC = dataRow.Substring(79, 4);
                        PISCFS = dataRow.Substring(83, 40);
                        PISAFT = dataRow.Substring(123, 17);
                        PISAMT = dataRow.Substring(140, 17);
                        PISCOD = dataRow.Substring(157, length - 157);
                    }
                    else if (length < 165)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, 1);
                        PISPRD = dataRow.Substring(29, 10);
                        PISCIF = dataRow.Substring(39, 19);
                        PISNAM = dataRow.Substring(58, 20);
                        PISSEX = dataRow.Substring(78, 1);
                        PISCFC = dataRow.Substring(79, 4);
                        PISCFS = dataRow.Substring(83, 40);
                        PISAFT = dataRow.Substring(123, 17);
                        PISAMT = dataRow.Substring(140, 17);
                        PISCOD = dataRow.Substring(157, 1);
                        PISDB6 = dataRow.Substring(158, length - 158);
                    }
                    else if (length < 171)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, 1);
                        PISPRD = dataRow.Substring(29, 10);
                        PISCIF = dataRow.Substring(39, 19);
                        PISNAM = dataRow.Substring(58, 20);
                        PISSEX = dataRow.Substring(78, 1);
                        PISCFC = dataRow.Substring(79, 4);
                        PISCFS = dataRow.Substring(83, 40);
                        PISAFT = dataRow.Substring(123, 17);
                        PISAMT = dataRow.Substring(140, 17);
                        PISCOD = dataRow.Substring(157, 1);
                        PISDB6 = dataRow.Substring(158, 6);
                        PISOP6 = dataRow.Substring(164, length - 164);
                    }
                    else if (length < 177)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, 1);
                        PISPRD = dataRow.Substring(29, 10);
                        PISCIF = dataRow.Substring(39, 19);
                        PISNAM = dataRow.Substring(58, 20);
                        PISSEX = dataRow.Substring(78, 1);
                        PISCFC = dataRow.Substring(79, 4);
                        PISCFS = dataRow.Substring(83, 40);
                        PISAFT = dataRow.Substring(123, 17);
                        PISAMT = dataRow.Substring(140, 17);
                        PISCOD = dataRow.Substring(157, 1);
                        PISDB6 = dataRow.Substring(158, 6);
                        PISOP6 = dataRow.Substring(164, 6);
                        PISMT6 = dataRow.Substring(170, length - 170);
                    }
                    else if (length < 183)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, 1);
                        PISPRD = dataRow.Substring(29, 10);
                        PISCIF = dataRow.Substring(39, 19);
                        PISNAM = dataRow.Substring(58, 20);
                        PISSEX = dataRow.Substring(78, 1);
                        PISCFC = dataRow.Substring(79, 4);
                        PISCFS = dataRow.Substring(83, 40);
                        PISAFT = dataRow.Substring(123, 17);
                        PISAMT = dataRow.Substring(140, 17);
                        PISCOD = dataRow.Substring(157, 1);
                        PISDB6 = dataRow.Substring(158, 6);
                        PISOP6 = dataRow.Substring(164, 6);
                        PISMT6 = dataRow.Substring(170, 6);
                        PISLP6 = dataRow.Substring(176, length - 176);
                    }
                    else if (length < 190)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, 1);
                        PISPRD = dataRow.Substring(29, 10);
                        PISCIF = dataRow.Substring(39, 19);
                        PISNAM = dataRow.Substring(58, 20);
                        PISSEX = dataRow.Substring(78, 1);
                        PISCFC = dataRow.Substring(79, 4);
                        PISCFS = dataRow.Substring(83, 40);
                        PISAFT = dataRow.Substring(123, 17);
                        PISAMT = dataRow.Substring(140, 17);
                        PISCOD = dataRow.Substring(157, 1);
                        PISDB6 = dataRow.Substring(158, 6);
                        PISOP6 = dataRow.Substring(164, 6);
                        PISMT6 = dataRow.Substring(170, 6);
                        PISLP6 = dataRow.Substring(176, 6);
                        PISDOB = dataRow.Substring(182, length - 182);
                    }
                    else if (length < 197)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, 1);
                        PISPRD = dataRow.Substring(29, 10);
                        PISCIF = dataRow.Substring(39, 19);
                        PISNAM = dataRow.Substring(58, 20);
                        PISSEX = dataRow.Substring(78, 1);
                        PISCFC = dataRow.Substring(79, 4);
                        PISCFS = dataRow.Substring(83, 40);
                        PISAFT = dataRow.Substring(123, 17);
                        PISAMT = dataRow.Substring(140, 17);
                        PISCOD = dataRow.Substring(157, 1);
                        PISDB6 = dataRow.Substring(158, 6);
                        PISOP6 = dataRow.Substring(164, 6);
                        PISMT6 = dataRow.Substring(170, 6);
                        PISLP6 = dataRow.Substring(176, 6);
                        PISDOB = dataRow.Substring(182, 7);
                        PISOPD = dataRow.Substring(189, length - 189);
                    }
                    else if (length < 204)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, 1);
                        PISPRD = dataRow.Substring(29, 10);
                        PISCIF = dataRow.Substring(39, 19);
                        PISNAM = dataRow.Substring(58, 20);
                        PISSEX = dataRow.Substring(78, 1);
                        PISCFC = dataRow.Substring(79, 4);
                        PISCFS = dataRow.Substring(83, 40);
                        PISAFT = dataRow.Substring(123, 17);
                        PISAMT = dataRow.Substring(140, 17);
                        PISCOD = dataRow.Substring(157, 1);
                        PISDB6 = dataRow.Substring(158, 6);
                        PISOP6 = dataRow.Substring(164, 6);
                        PISMT6 = dataRow.Substring(170, 6);
                        PISLP6 = dataRow.Substring(176, 6);
                        PISDOB = dataRow.Substring(182, 7);
                        PISOPD = dataRow.Substring(189, 7);
                        PISMTD = dataRow.Substring(196, length - 196);
                    }
                    else if (length < 213)
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, 1);
                        PISPRD = dataRow.Substring(29, 10);
                        PISCIF = dataRow.Substring(39, 19);
                        PISNAM = dataRow.Substring(58, 20);
                        PISSEX = dataRow.Substring(78, 1);
                        PISCFC = dataRow.Substring(79, 4);
                        PISCFS = dataRow.Substring(83, 40);
                        PISAFT = dataRow.Substring(123, 17);
                        PISAMT = dataRow.Substring(140, 17);
                        PISCOD = dataRow.Substring(157, 1);
                        PISDB6 = dataRow.Substring(158, 6);
                        PISOP6 = dataRow.Substring(164, 6);
                        PISMT6 = dataRow.Substring(170, 6);
                        PISLP6 = dataRow.Substring(176, 6);
                        PISDOB = dataRow.Substring(182, 7);
                        PISOPD = dataRow.Substring(189, 7);
                        PISMTD = dataRow.Substring(196, 7);
                        PISLPD = dataRow.Substring(203, length - 203);
                    }
                    else
                    {
                        total += 1;
                        PISBR = dataRow.Substring(0, 5);
                        PISACT = dataRow.Substring(5, 19);
                        PISCUR = dataRow.Substring(24, 4);
                        PISSTS = dataRow.Substring(28, 1);
                        PISPRD = dataRow.Substring(29, 10);
                        PISCIF = dataRow.Substring(39, 19);
                        PISNAM = dataRow.Substring(58, 20);
                        PISSEX = dataRow.Substring(78, 1);
                        PISCFC = dataRow.Substring(79, 4);
                        PISCFS = dataRow.Substring(83, 40);
                        PISAFT = dataRow.Substring(123, 17);
                        PISAMT = dataRow.Substring(140, 17);
                        PISCOD = dataRow.Substring(157, 1);
                        PISDB6 = dataRow.Substring(158, 6);
                        PISOP6 = dataRow.Substring(164, 6);
                        PISMT6 = dataRow.Substring(170, 6);
                        PISLP6 = dataRow.Substring(176, 6);
                        PISDOB = dataRow.Substring(182, 7);
                        PISOPD = dataRow.Substring(189, 7);
                        PISMTD = dataRow.Substring(196, 7);
                        PISLPD = dataRow.Substring(203, 7);
                    }

                    if (string.IsNullOrEmpty(PISACT) || (!string.IsNullOrEmpty(PISACT) && PISACT.Trim().Length < 19))
                    {
                        continue;
                    }

                    #endregion

                    #region bind data

                    Axa.Insurance.Model.UploadMTR _uploadMtr = new Axa.Insurance.Model.UploadMTR();
                    _uploadMtr.ID = jobfile.JobFileId + "-" + (row + 1);
                    _uploadMtr.JobFileId = jobfile.JobFileId;
                    _uploadMtr.PISBR = string.IsNullOrEmpty(PISBR) ? null : PISBR.Trim();
                    _uploadMtr.PISACT = string.IsNullOrEmpty(PISACT) ? null : PISACT.Trim();
                    _uploadMtr.PISCUR = string.IsNullOrEmpty(PISCUR) ? null : PISCUR.Trim();
                    _uploadMtr.PISSTS = string.IsNullOrEmpty(PISSTS) ? null : PISSTS.Trim();
                    _uploadMtr.PISPRD = string.IsNullOrEmpty(PISPRD) ? null : PISPRD.Trim();
                    _uploadMtr.PISCIF = string.IsNullOrEmpty(PISCIF) ? null : PISCIF.Trim();
                    _uploadMtr.PISNAM = string.IsNullOrEmpty(PISNAM) ? null : PISNAM.Trim();
                    _uploadMtr.PISSEX = string.IsNullOrEmpty(PISSEX) ? null : PISSEX.Trim();
                    _uploadMtr.PISCFC = string.IsNullOrEmpty(PISCFC) ? null : PISCFC.Trim();
                    _uploadMtr.PISCFS = string.IsNullOrEmpty(PISCFS) ? null : PISCFS.Trim();
                    _uploadMtr.PISAFT = string.IsNullOrEmpty(PISAFT) ? null : PISAFT.Trim();
                    _uploadMtr.PISAMT = string.IsNullOrEmpty(PISAMT) ? null : PISAMT.Trim();
                    _uploadMtr.PISCOD = string.IsNullOrEmpty(PISCOD) ? null : PISCOD.Trim();
                    _uploadMtr.PISDB6 = string.IsNullOrEmpty(PISDB6) ? null : PISDB6.Trim();
                    _uploadMtr.PISOP6 = string.IsNullOrEmpty(PISOP6) ? null : PISOP6.Trim();
                    _uploadMtr.PISMT6 = string.IsNullOrEmpty(PISMT6) ? null : PISMT6.Trim();
                    _uploadMtr.PISLP6 = string.IsNullOrEmpty(PISLP6) ? null : PISLP6.Trim();
                    _uploadMtr.PISDOB = string.IsNullOrEmpty(PISDOB) ? null : PISDOB.Trim();
                    _uploadMtr.PISOPD = string.IsNullOrEmpty(PISOPD) ? null : PISOPD.Trim();
                    _uploadMtr.PISMTD = string.IsNullOrEmpty(PISMTD) ? null : PISMTD.Trim();
                    _uploadMtr.PISLPD = string.IsNullOrEmpty(PISLPD) ? null : PISLPD.Trim();
                    #endregion

                    #region Validasi Data

                    //validasi data
                    if (string.IsNullOrEmpty(_uploadMtr.INVALIDREASON))
                    {
                        _uploadMtr.INVALIDREASON = ValidateInvalidData(_uploadMtr);
                    }

                    _birthDate = null;
                    _effectiveDate = null;
                    _matureDate = null;
                    _paymentDate = null;
                    _age = 0;

                    if (!string.IsNullOrEmpty(_uploadMtr.PISDB6) && !string.IsNullOrEmpty(_uploadMtr.PISDOB))
                    {
                        _dateValue = null;
                        GetDateUpload(_uploadMtr.PISDB6.Substring(0, 4) + _uploadMtr.PISDOB.Substring(0, 4), "ddmmyyyy");
                        _birthDate = _dateValue;
                    }

                    if (!string.IsNullOrEmpty(_uploadMtr.PISDB6) && !string.IsNullOrEmpty(_uploadMtr.PISDOB))
                    {
                        _dateValue = null;
                        GetDateUpload(_uploadMtr.PISOP6.Substring(0, 4) + _uploadMtr.PISOPD.Substring(0, 4), "ddmmyyyy");
                        _effectiveDate = _dateValue;
                    }
                    if (!string.IsNullOrEmpty(_uploadMtr.PISDB6) && !string.IsNullOrEmpty(_uploadMtr.PISDOB))
                    {
                        _dateValue = null;
                        GetDateUpload(_uploadMtr.PISMT6.Substring(0, 4) + _uploadMtr.PISMTD.Substring(0, 4), "ddmmyyyy");
                        _matureDate = _dateValue;
                    }
                    if (_birthDate.HasValue && _effectiveDate.HasValue)
                    {
                        _age = DateHelper.GetAge(_birthDate.Value, _effectiveDate.Value, product.AgeLogicId); //product.AgeLogicId);
                    }

                    //validasi usia
                    if (string.IsNullOrEmpty(_uploadMtr.INVALIDREASON))
                    {
                        _uploadMtr.INVALIDREASON = ValidateInvalidAge(_uploadMtr, _premiums);// product);
                    }

                    //validasi coverage
                    if (string.IsNullOrEmpty(_uploadMtr.INVALIDREASON))
                    {
                        _uploadMtr.INVALIDREASON = ValidateInvalidCoverage(_uploadMtr, product);// product);
                    }

                    //validasi status data
                    _uploadMtr.STATUSPOLICY = "S";
                    if (!string.IsNullOrEmpty(_uploadMtr.INVALIDREASON))
                    {
                        _uploadMtr.STATUSPOLICY = "F";
                    }
                    #endregion

                    dataUploads.Add(_uploadMtr);

                }

                DataTable dtTemp = MapperUploadMTR(dataUploads);

                #region Connection
                string consString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (DbConnection conn = _database.CreateConnection())
                {
                    //    using (SqlConnection conn = new SqlConnection(consString))
                    //{
                    //msg += "2";
                    conn.Open();
                    //msg += "3";
                    //using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                    //{
                    using (SqlBulkCopy copy = new SqlBulkCopy((SqlConnection)conn, SqlBulkCopyOptions.Default, null))
                    {
                        try
                        {
                            copy.BatchSize = 1000000;
                            //copy.NotifyAfter = 500;
                            copy.DestinationTableName = "UPLOADMTR";
                            copy.BulkCopyTimeout = 0;
                            copy.WriteToServer(dtTemp);
                            //sqlTransaction.Commit();
                            //msg += "4"; 
                        }
                        catch (Exception ex)
                        {
                            //sqlTransaction.Rollback();
                            msg += jobfile.JobFileId + jobfile.FileName + ex.Message + Environment.NewLine + Environment.NewLine + ex.StackTrace;

                            throw new Exception($"Insert Bulk - Failed \n " + msg);
                        }
                    }
                    //}
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(dataRow + "\n" + ex.Message + "_" + ex.Source + "_" + ex.StackTrace);
                //ShowMessage(ex.Message);
            }

            #endregion

            return total;
        }

        protected string ValidateInvalidData(Axa.Insurance.Model.UploadMTR data)
        {
            StringBuilder error = new StringBuilder();

            if (string.IsNullOrEmpty(data.PISBR))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISBR# WITH VALUE : NULL or EMPTY");
            }
            else if (string.IsNullOrEmpty(data.PISACT))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISACT WITH VALUE : NULL or EMPTY");
            }
            else if (!string.IsNullOrEmpty(data.PISACT) && !data.PISACT.All(char.IsDigit))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISACT WITH VALUE : " + data.PISACT);
            }
            else if (string.IsNullOrEmpty(data.PISCUR))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISCUR WITH VALUE : NULL or EMPTY");
            }
            else if (!string.IsNullOrEmpty(data.PISCUR) && !data.PISCUR.All(char.IsLetter) && (data.PISCUR.Contains("IDR") || !data.PISCUR.Contains("USD")))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISCUR WITH VALUE : " + data.PISCUR);
            }
            else if (string.IsNullOrEmpty(data.PISSTS))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISSTS WITH VALUE : NULL or EMPTY");
            }
            else if (!string.IsNullOrEmpty(data.PISSTS) && !data.PISSTS.All(char.IsDigit))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISSTS WITH VALUE : " + data.PISSTS);
            }
            else if (string.IsNullOrEmpty(data.PISPRD))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISPRD WITH VALUE : NULL or EMPTY");
            }
            else if (!string.IsNullOrEmpty(data.PISPRD) && data.PISPRD.All(char.IsDigit))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISPRD WITH VALUE : " + data.PISPRD);
            }
            else if (string.IsNullOrEmpty(data.PISCIF))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISCIF WITH VALUE : NULL or EMPTY");
            }
            else if (!string.IsNullOrEmpty(data.PISCIF) && !data.PISCIF.All(char.IsDigit))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISCIF WITH VALUE : " + data.PISCIF);
            }
            else if (string.IsNullOrEmpty(data.PISNAM))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISNAM WITH VALUE : NULL or EMPTY");
            }
            /*
            else if (string.IsNullOrEmpty(data.PISSEX))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISSEX WITH VALUE : NULL or EMPTY");
            }
            else if (!string.IsNullOrEmpty(data.PISSEX) && !data.PISSEX.All(char.IsLetter) && GetSexId(data.PISSEX))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISSEX WITH VALUE : " + data.PISSEX);
            }
            */
            else if (string.IsNullOrEmpty(data.PISCFC))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISCFC WITH VALUE : NULL or EMPTY");
            }
            else if (!string.IsNullOrEmpty(data.PISCFC) && !data.PISCFC.All(char.IsLetter))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISCFC WITH VALUE : " + data.PISCFC);
            }
            else if (string.IsNullOrEmpty(data.PISCFS))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISCFS WITH VALUE : NULL or EMPTY");
            }
            else if (string.IsNullOrEmpty(data.PISAFT))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISAFT WITH VALUE : NULL or EMPTY");
            }
            else if (!string.IsNullOrEmpty(data.PISAFT) && (!data.PISAFT.All(char.IsDigit) || !CheckValue(data.PISAFT)))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISAFT WITH VALUE : " + data.PISCIF);
            }
            else if (string.IsNullOrEmpty(data.PISAMT))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISAMT WITH VALUE : NULL or EMPTY");
            }
            else if (!string.IsNullOrEmpty(data.PISAMT) && (!data.PISAMT.All(char.IsDigit) || !CheckValue(data.PISAMT)))
            {
                error.AppendFormat("INVALID CHAR ON FIELD PISAMT WITH VALUE : " + data.PISAMT);
            }
            //else if (string.IsNullOrEmpty(data.PISCOD))
            //{
            //    error.AppendFormat("INVALID CHAR ON FIELD PISCOD WITH VALUE : NULL or EMPTY");
            //}
            else if (string.IsNullOrEmpty(data.PISDB6))
            {
                error.AppendFormat("INVALID DATE ON FIELD PISDB6 WITH VALUE : NULL or EMPTY");
            }
            else if (!string.IsNullOrEmpty(data.PISDB6) && !data.PISDB6.All(char.IsDigit) && data.PISDB6.Length < 6)
            {
                error.AppendFormat("INVALID DATE ON FIELD PISDB6 WITH VALUE : " + data.PISDB6);
            }
            else if (!string.IsNullOrEmpty(data.PISDB6) && !string.IsNullOrEmpty(data.PISDOB) && !GetDateUpload(data.PISDB6.Substring(0, 4) + data.PISDOB.Substring(0, 4), "ddmmyyyy"))
            {
                error.AppendFormat("INVALID DATE ON FIELD PISDB6 WITH VALUE : " + data.PISDB6);
            }
            else if (string.IsNullOrEmpty(data.PISOP6))
            {
                error.AppendFormat("INVALID DATE ON FIELD PISOP6 WITH VALUE : NULL or EMPTY");
            }
            else if (!string.IsNullOrEmpty(data.PISOP6) && !data.PISOP6.All(char.IsDigit) && data.PISOP6.Length < 6)
            {
                error.AppendFormat("INVALID DATE ON FIELD PISOP6 WITH VALUE : " + data.PISOP6);
            }
            else if (!string.IsNullOrEmpty(data.PISOP6) && !string.IsNullOrEmpty(data.PISOPD) && !GetDateUpload(data.PISOP6.Substring(0, 4) + data.PISOPD.Substring(0, 4), "ddmmyyyy"))
            {
                error.AppendFormat("INVALID DATE ON FIELD PISOP6 WITH VALUE : " + data.PISOP6);
            }
            else if (string.IsNullOrEmpty(data.PISMT6))
            {
                error.AppendFormat("INVALID DATE ON FIELD PISMT6 WITH VALUE : NULL or EMPTY");
            }
            else if (!string.IsNullOrEmpty(data.PISMT6) && !data.PISMT6.All(char.IsDigit) && data.PISMT6.Length < 6)
            {
                error.AppendFormat("INVALID DATE ON FIELD PISMT6 WITH VALUE : " + data.PISMT6);
            }
            else if (!string.IsNullOrEmpty(data.PISMT6) && !string.IsNullOrEmpty(data.PISMTD) && !GetDateUpload(data.PISMT6.Substring(0, 4) + data.PISMTD.Substring(0, 4), "ddmmyyyy"))
            {
                error.AppendFormat("INVALID DATE ON FIELD PISMT6 WITH VALUE : " + data.PISMT6);
            }
            else if (string.IsNullOrEmpty(data.PISLP6))
            {
                error.AppendFormat("INVALID DATE ON FIELD PISLP6 WITH VALUE : NULL or EMPTY");
            }
            else if (!string.IsNullOrEmpty(data.PISLP6) && !data.PISLP6.All(char.IsDigit) && data.PISLP6.Length < 6)
            {
                error.AppendFormat("INVALID DATE ON FIELD PISLP6 WITH VALUE : " + data.PISLP6);
            }
            else if (!string.IsNullOrEmpty(data.PISLP6) && !string.IsNullOrEmpty(data.PISLPD) && !GetDateUpload(data.PISLP6.Substring(0, 4) + data.PISLPD.Substring(0, 4), "ddmmyyyy") && data.PISLP6 != "000000" && data.PISLPD != "0000000")
            {
                error.AppendFormat("INVALID DATE ON FIELD PISLP6 WITH VALUE : " + data.PISLP6);
            }
            else if (string.IsNullOrEmpty(data.PISDOB))
            {
                error.AppendFormat("INVALID DATE ON FIELD PISDOB WITH VALUE : NULL or EMPTY");
            }
            else if (!string.IsNullOrEmpty(data.PISDOB) && !data.PISDOB.All(char.IsDigit) && data.PISDOB.Length < 7)
            {
                error.AppendFormat("INVALID DATE ON FIELD PISDOB WITH VALUE : " + data.PISDOB);
            }
            else if (string.IsNullOrEmpty(data.PISOPD))
            {
                error.AppendFormat("INVALID DATE ON FIELD PISOPD WITH VALUE : NULL or EMPTY");
            }
            else if (!string.IsNullOrEmpty(data.PISOPD) && !data.PISOPD.All(char.IsDigit) && data.PISOPD.Length < 7)
            {
                error.AppendFormat("INVALID DATE ON FIELD PISOPD WITH VALUE : " + data.PISOPD);
            }
            else if (string.IsNullOrEmpty(data.PISMTD))
            {
                error.AppendFormat("INVALID DATE ON FIELD PISMTD WITH VALUE : NULL or EMPTY");
            }
            else if (!string.IsNullOrEmpty(data.PISMTD) && !data.PISMTD.All(char.IsDigit) && data.PISMTD.Length < 7)
            {
                error.AppendFormat("INVALID DATE ON FIELD PISMTD WITH VALUE : " + data.PISMTD);
            }
            else if (string.IsNullOrEmpty(data.PISLPD))
            {
                error.AppendFormat("INVALID DATE ON FIELD PISLPD WITH VALUE : NULL or EMPTY");
            }
            else if (!string.IsNullOrEmpty(data.PISLPD) && !data.PISLPD.All(char.IsDigit) && data.PISLPD.Length < 7)
            {
                error.AppendFormat("INVALID DATE ON FIELD PISLPD WITH VALUE : " + data.PISLPD);
            }

            return error.ToString();
        }
        protected string ValidateInvalidAge(Axa.Insurance.Model.UploadMTR data, List<Premium> _premiums)//Product product)
        {
            StringBuilder error = new StringBuilder();
            foreach (Premium premium in _premiums)
            {
                if (_age > int.MinValue && (premium.AgeBegin <= _age && premium.AgeEnd >= _age))
                { }
                else
                {
                    error.AppendFormat("INVALID AGE");
                }

                /*
                if (_age < premium.AgeBegin || _age > premium.AgeEnd)
                {
                    error.AppendFormat("INVALID AGE");
                }
                */
            }
            return error.ToString();
        }
        protected bool CheckValue(string value)
        {
            bool checkValue = false;
            decimal val = 0;
            try
            {
                val = Convert.ToInt64(value);
            }
            catch (Exception ex)
            {
                val = -1;
                //messageError += "Row " + total + " value is empty/wrong, ";
            } 
            if (val != -1 && val.ToString().Length <= 13)
            {
                checkValue = true;
            }

            return checkValue;
        }
        protected string ValidateInvalidCoverage(Axa.Insurance.Model.UploadMTR data, Product product)//Product product)
        {
            StringBuilder error = new StringBuilder();

            int months = 0;
            int years = 0;
            int days = 0;
            DateTime td = _matureDate.Value;
            DateTime effdate = _effectiveDate.Value;

            days = td.Day - effdate.Day;
            if (days < 0)
            {
                td = td.AddMonths(-1);
                days += DateTime.DaysInMonth(td.Year, td.Month);
            }

            months = td.Month - effdate.Month;
            if (months < 0)
            {
                td = td.AddYears(-1);
                months += 12;
            }
            years = td.Year - effdate.Year;

            months = months + (years * 12);

            int yearMod = 0;
            int yearVal = 0;
            yearVal = months / 12;
            yearMod = months % 12;

            if (yearMod > 0)
            {
                yearVal += 1;
            }
            if (yearVal + _age > product.MIRenAge)
            {
                error.AppendFormat("INVALID COVERAGE");
            }

            return error.ToString();
        }
        protected bool GetSexId(string sexId)
        {
            bool checkSexId = false;
            switch (sexId)
            {
                case "L":
                case "M":
                case "P":
                case "F":
                    checkSexId = true;
                    break;
                default:
                    checkSexId = false;
                    break;
            }
            return checkSexId;
        }
        protected bool GetDateUpload(string value, string format)
        {
            int day = 0;
            int month = 0;
            int year = 0;
            DateTime? date = null;
            bool checkDate = false;
            try
            {
                switch (format.ToLower().Trim())
                {
                    case "ddmmyyyy":
                        day = int.Parse(value.Substring(0, 2));
                        month = int.Parse(value.Substring(2, 2));
                        year = int.Parse(value.Substring(4, 4));
                        break;
                    case "yymmdd":
                        year = int.Parse(DateTime.Now.Year.ToString().Substring(0, 2) + value.Substring(0, 2));
                        month = int.Parse(value.Substring(2, 2));
                        day = int.Parse(value.Substring(4, 2));
                        break;
                }
                if (year == 0000 || month == 00 || day == 00)
                {
                    date = null;
                }
                else
                {
                    date = new DateTime(year, month, day);
                }
            }
            catch (Exception ex)
            {
                date = null;
            }

            if (date.HasValue)
            {
                checkDate = true;
                _dateValue = date.Value;
            }

            return checkDate;
        }

        protected DataTable MapperUploadMTR(List<Axa.Insurance.Model.UploadMTR> details)
        {
            DataTable dtTemp = new DataTable(); // PolicyHistory
            DataColumn col = new DataColumn();

            col = new DataColumn("ID", typeof(string)); dtTemp.Columns.Add(col); //0
            col = new DataColumn("JOBFILEID", typeof(string)); dtTemp.Columns.Add(col); //1
            col = new DataColumn("PISBR#", typeof(string)); dtTemp.Columns.Add(col); //2
            col = new DataColumn("PISACT", typeof(string)); dtTemp.Columns.Add(col); //3
            col = new DataColumn("PISCUR", typeof(string)); dtTemp.Columns.Add(col); //4
            col = new DataColumn("PISSTS", typeof(string)); dtTemp.Columns.Add(col); //5
            col = new DataColumn("PISPRD", typeof(string)); dtTemp.Columns.Add(col); //6
            col = new DataColumn("PISCIF", typeof(string)); dtTemp.Columns.Add(col); //7
            col = new DataColumn("PISNAM", typeof(string)); dtTemp.Columns.Add(col); //8
            col = new DataColumn("PISSEX", typeof(string)); dtTemp.Columns.Add(col); //9
            col = new DataColumn("PISCFC", typeof(string)); dtTemp.Columns.Add(col); //10
            col = new DataColumn("PISCFS", typeof(string)); dtTemp.Columns.Add(col); //11
            col = new DataColumn("PISAFT", typeof(string)); dtTemp.Columns.Add(col); //12
            col = new DataColumn("PISAMT", typeof(string)); dtTemp.Columns.Add(col); //13
            col = new DataColumn("PISCOD", typeof(string)); dtTemp.Columns.Add(col); //14
            col = new DataColumn("PISDB6", typeof(string)); dtTemp.Columns.Add(col); //15
            col = new DataColumn("PISOP6", typeof(string)); dtTemp.Columns.Add(col); //16
            col = new DataColumn("PISMT6", typeof(string)); dtTemp.Columns.Add(col); //17
            col = new DataColumn("PISLP6", typeof(string)); dtTemp.Columns.Add(col); //18
            col = new DataColumn("PISDOB", typeof(string)); dtTemp.Columns.Add(col); //19
            col = new DataColumn("PISOPD", typeof(string)); dtTemp.Columns.Add(col); //20
            col = new DataColumn("PISMTD", typeof(string)); dtTemp.Columns.Add(col); //21
            col = new DataColumn("PISLPD", typeof(string)); dtTemp.Columns.Add(col); //22 
            col = new DataColumn("STATUSPOLICY", typeof(string)); dtTemp.Columns.Add(col); //23
            col = new DataColumn("INVALIDREASON", typeof(string)); dtTemp.Columns.Add(col); //24 

            if (details.Count > 0)
            {
                foreach (Axa.Insurance.Model.UploadMTR dt in details)
                {
                    DataRow dr = dtTemp.NewRow();
                    dr["ID"] = dt.ID;
                    dr["JOBFILEID"] = dt.JobFileId;
                    dr["PISBR#"] = dt.PISBR;
                    dr["PISACT"] = dt.PISACT;
                    dr["PISCUR"] = dt.PISCUR;
                    dr["PISSTS"] = dt.PISSTS;
                    dr["PISPRD"] = dt.PISPRD;
                    dr["PISCIF"] = dt.PISCIF;
                    dr["PISNAM"] = dt.PISNAM;
                    dr["PISSEX"] = dt.PISSEX;
                    dr["PISCFC"] = dt.PISCFC;
                    dr["PISCFS"] = dt.PISCFS;
                    dr["PISAFT"] = dt.PISAFT;
                    dr["PISAMT"] = dt.PISAMT;
                    dr["PISCOD"] = dt.PISCOD;
                    dr["PISDB6"] = dt.PISDB6;
                    dr["PISOP6"] = dt.PISOP6;
                    dr["PISMT6"] = dt.PISMT6;
                    dr["PISLP6"] = dt.PISLP6;
                    dr["PISDOB"] = dt.PISDOB;
                    dr["PISOPD"] = dt.PISOPD;
                    dr["PISMTD"] = dt.PISMTD;
                    dr["PISLPD"] = dt.PISLPD;
                    dr["STATUSPOLICY"] = dt.STATUSPOLICY;
                    dr["INVALIDREASON"] = dt.INVALIDREASON;
                    dtTemp.Rows.Add(dr);
                }
            }

            return dtTemp;
        }
        protected DataTable MapperBatchDetail(List<BatchDetail> details)
        {
            DataTable dtTemp = new DataTable(); // PolicyHistory
            DataColumn col = new DataColumn();

            col = new DataColumn("BATCHID", typeof(string)); dtTemp.Columns.Add(col); //0
            col = new DataColumn("DETAILID", typeof(string)); dtTemp.Columns.Add(col); //1
            col = new DataColumn("TYPEID", typeof(string)); dtTemp.Columns.Add(col); //2
            col = new DataColumn("DESCRIPTION", typeof(string)); dtTemp.Columns.Add(col); //3
            col = new DataColumn("REMARK", typeof(string)); dtTemp.Columns.Add(col); //4 
            col = new DataColumn("CREATEDATE", typeof(DateTime)); dtTemp.Columns.Add(col); //5
            col = new DataColumn("CREATEBY", typeof(string)); dtTemp.Columns.Add(col); //6
            col = new DataColumn("MODIFYDATE", typeof(DateTime)); dtTemp.Columns.Add(col); //7
            col = new DataColumn("MODIFYBY", typeof(string)); dtTemp.Columns.Add(col); //8
            col = new DataColumn("ORDERID", typeof(string)); dtTemp.Columns.Add(col); //9   
            col = new DataColumn("STATUS", typeof(string)); dtTemp.Columns.Add(col); //10
            col = new DataColumn("REMARK2", typeof(string)); dtTemp.Columns.Add(col); //11 

            if (details.Count > 0)
            {
                foreach (BatchDetail dt in details)
                {
                    DataRow dr = dtTemp.NewRow();
                    dr["BATCHID"] = dt.BatchId;
                    dr["DETAILID"] = dt.DetailId;
                    dr["TYPEID"] = dt.TypeId;
                    dr["DESCRIPTION"] = dt.Description;
                    dr["REMARK"] = dt.Remark;
                    dr["CREATEDATE"] = dt.CreateDate ?? (object)DBNull.Value;
                    dr["CREATEBY"] = dt.CreateBy;
                    dr["MODIFYDATE"] = dt.ModifyDate ?? (object)DBNull.Value;
                    dr["MODIFYBY"] = dt.ModifyBy;
                    dr["ORDERID"] = dt.OrderId;
                    dr["STATUS"] = dt.Status;
                    dr["REMARK2"] = dt.Remark2;
                    dtTemp.Rows.Add(dr);
                }
            }

            return dtTemp;
        }
        private bool CheckJobFiles(string _fileName)
        {
            List<Criteria> criterias = new List<Criteria>();
            criterias.Add(new Criteria("p.FileName", _fileName, Comparison.Equal));
            List<JobFiles> _jobFiles = new List<JobFiles>();
            _jobFiles = _jobFilesService.FindJobFiles(criterias, 0, int.MaxValue, "p.CreateDate desc");

            if (_jobFiles.Count > 0)
                return true;
            else
                return false;
        }
        private Batch CreateBatch(string filePath)
        {
            List<BatchDetail> batches = new List<BatchDetail>();
            string batchDesc = string.Format("Src: {0}, File: {1} Succeed Upload, file will be processed in the background", _productId, Path.GetFileName(filePath));

            string typeId = "FNM";

            BatchDetail detail = new BatchDetail
            {
                Description = "Upload File",
                Remark = "File " + _productId.ToUpper(),
                TypeId = typeId
            };

            batches.Add(detail);

            Batch batch = new Batch
            {
                CreateBy = uploadBy,//"AutoImport",
                ModifyBy = uploadBy,//"AutoImport",
                Description = batchDesc,
                TypeId = "FNM",
                Details = batches
            };
            return _batchService.AddBatch(batch);
        }
        public string NextJobFileId()
        {
            return SqlHelper.ExecuteScalar("select dbo.NextJobFileId()", System.Data.CommandType.Text).ToString();
        }
        protected void ProcessMTRRevamp(string filePath, JobFiles jobFile)
        {

            Dictionary<string, Policy> policies = new Dictionary<string, Policy>();
            Dictionary<string, BatchDetail> batches = new Dictionary<string, BatchDetail>();

            Batch batch = new Batch();
            BatchDetail detail = new BatchDetail();
            try
            {
                if (!string.IsNullOrEmpty(jobFile.Status) && jobFile.Status.ToLower() == "new")
                {
                    jobFile.Remark = string.Empty;
                    ProcessTablesMTRRevamp(batches, jobFile, filePath);
                    jobFile.Status = "Processing";
                    jobFile.TotalRow = total;
                    jobFile.DateStartProcess = DateTime.Now;

                    DataSet ds_unprocessed = new DataSet();
                    DataSet ds_succeed = new DataSet();
                    DataSet ds_failed = new DataSet();
                    string batchDesc = string.Empty;

                    ds_unprocessed = ExecuteSqlQuery(jobFile, 0, false);

                    if (ds_unprocessed.Tables[0].Rows.Count == 0)
                    {
                        ds_succeed = ExecuteSqlQuery(jobFile, 1, false);

                        ds_failed = ExecuteSqlQuery(jobFile, 2, false);

                        batchDesc = string.Format("File: {0}, Total Data : {1}, Success: {2}, Fail: {3}, unprocessed: {4}", Path.GetFileName(filePath), jobFile.TotalRow, ds_succeed.Tables[0].Rows.Count, ds_failed.Tables[0].Rows.Count, ds_unprocessed.Tables[0].Rows.Count);

                        jobFile.SuksesRow = ds_succeed.Tables[0].Rows.Count;
                        jobFile.GagalRow = ds_failed.Tables[0].Rows.Count;

                        //v2
                        if (jobFile.SuksesRow + jobFile.GagalRow == jobFile.TotalRow && jobFile.GagalRow != jobFile.TotalRow)
                        {
                            jobFile.Status = "Done";
                            jobFile.DateEndProcess = DateTime.Now;
                            fileservice.MoveFile(filePath, successFolder);
                        }
                        else if (jobFile.GagalRow == jobFile.TotalRow)
                        {
                            jobFile.Status = "Failed";
                            jobFile.DateEndProcess = DateTime.Now;
                            fileservice.MoveFile(filePath, failedFolder);
                        }
                    }
                    else
                    {
                        batchDesc = string.Format("File: {0}, Total Data : {1}, unprocessed: {2}", Path.GetFileName(filePath), jobFile.TotalRow, ds_unprocessed.Tables[0].Rows.Count);
                    }

                    LogHelper.LogInfo($" Batch Description : " + batchDesc, _pluginMetadata);

                    if (string.IsNullOrEmpty(jobFile.Remark))
                    {
                        LogHelper.LogInfo($" Batch Creating.....", _pluginMetadata);

                        batch = SaveBatch(batches, batchDesc);
                    }

                    //else
                    //{
                    //    LogHelper.LogInfo($" Batch Updating.....", _pluginMetadata);

                    //    batch = UpdateBatch(batches, batchDesc, jobFile, ds_unprocessed.Tables[0].Rows.Count);
                    //}

                    LogHelper.LogInfo($"BatchId : " + batch.BatchId, _pluginMetadata);

                    if (string.IsNullOrEmpty(jobFile.Remark))
                    {
                        jobFile.Remark = batch.BatchId;
                    }

                    _jobFilesService.UpdateJobFiles(jobFile);

                }
            }
            catch (Exception ex)
            {
                jobFile.Status = "Failed";
                jobFile.Remark = "Remark Error: " + ex.Message + " " + ex.StackTrace + " " + ex.Source;
                jobFile.DateEndProcess = DateTime.Now;
                _jobFilesService.UpdateJobFiles(jobFile);
                LogHelper.LogError($"Remark Error: " + ex.Message + " " + ex.StackTrace + " " + ex.Source, ex, _pluginMetadata);
            }
        }
        protected void ProcessTablesMTRRevamp(Dictionary<string, BatchDetail> batches, JobFiles jobFile, string filePath)
        {
            List<JobFileDetail> jobDetails = new List<JobFileDetail>();
            DataSet ds_uploadMTR = new DataSet();
            total = 0;
            string msg = "";
            int row = 0;
            ds_uploadMTR = ExecuteSqlQuery(jobFile);
            //Product _prodIDR = _productServices.GetProduct("TRMIDR");
            //Product _prodUSD = _productServices.GetProduct("TABINS_USD");


            if (ds_uploadMTR.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds_uploadMTR.Tables[0].Rows)
                {
                    row++;
                    total += 1;
                    //for (int row = 0; row < ds_uploadMTR.Tables[0].Rows.Count; row++)
                    //{ 
                    Axa.Insurance.Model.UploadMTR dataMTR = new Axa.Insurance.Model.UploadMTR();
                    BatchDetail detail = new BatchDetail();
                    /*
                    string PISBR = string.Empty; //Cabang
                    string PISACT = string.Empty; //Nomor polis
                    string PISCUR = string.Empty; //Mata Uang/Valuta
                    string PISSTS = string.Empty; //Status 1.Active 2.Close 4.Active 7.Claim 9.Freeze 
                    string PISPRD = string.Empty; //Product
                    string PISCIF = string.Empty; //CIF
                    string PISNAM = string.Empty; //Nama Peserta
                    string PISSEX = string.Empty; //Gender
                    string PISCFC = string.Empty; //ID Card
                    string PISCFS = string.Empty; //ID #
                    string PISAFT = string.Empty; //Manfaat Bulanan (dalam plain text)
                    string PISAMT = string.Empty; //Premi (dalam plain text)
                    string PISCOD = string.Empty; //Kode Claim
                    string PISDB6 = string.Empty; //Tgl Lahir (ddmmyy)
                    string PISOP6 = string.Empty; //Tgl buka Polis (ddmmyy)
                    string PISMT6 = string.Empty; //Jatuh Tempo/Maturity Date (ddmmyy)
                    string PISLP6 = string.Empty; //Tgl terakhir Bayar(ddmmyy)
                    string PISDOB = string.Empty; //Tgl tLahir (julian date)
                    string PISOPD = string.Empty; //Tgl buka Polis(julian date)
                    string PISMTD = string.Empty; //Jatuh Tempo/Maturity Date (julian date)
                    string PISLPD = string.Empty; // Tgl terakhir Bayar (julian date)
                    */
                    /*
                        Julian Date
                        2021044 -> hari ke 044 di tahun 2021 
                        Kode Claim -> 
                     */

                    dataMTR.ID = dr["ID"] == DBNull.Value ? "" : dr["ID"].ToString();
                    dataMTR.PISBR = dr["PISBR#"] == DBNull.Value ? "" : dr["PISBR#"].ToString();
                    dataMTR.PISACT = dr["PISACT"] == DBNull.Value ? "" : dr["PISACT"].ToString();
                    dataMTR.PISCUR = dr["PISCUR"] == DBNull.Value ? "" : dr["PISCUR"].ToString();
                    dataMTR.PISSTS = dr["PISSTS"] == DBNull.Value ? "" : dr["PISSTS"].ToString();
                    dataMTR.PISPRD = dr["PISPRD"] == DBNull.Value ? "" : dr["PISPRD"].ToString();
                    dataMTR.PISCIF = dr["PISCIF"] == DBNull.Value ? "" : dr["PISCIF"].ToString();
                    dataMTR.PISNAM = dr["PISNAM"] == DBNull.Value ? "" : dr["PISNAM"].ToString();
                    dataMTR.PISSEX = dr["PISSEX"] == DBNull.Value ? "" : dr["PISSEX"].ToString();
                    dataMTR.PISCFC = dr["PISCFC"] == DBNull.Value ? "" : dr["PISCFC"].ToString();
                    dataMTR.PISCFS = dr["PISCFS"] == DBNull.Value ? "" : dr["PISCFS"].ToString();
                    dataMTR.PISAFT = dr["PISAFT"] == DBNull.Value ? "0" : dr["PISAFT"].ToString();
                    dataMTR.PISAMT = dr["PISAMT"] == DBNull.Value ? "0" : dr["PISAMT"].ToString();
                    dataMTR.PISCOD = dr["PISCOD"] == DBNull.Value ? "" : dr["PISCOD"].ToString();
                    dataMTR.PISDB6 = dr["PISDB6"] == DBNull.Value ? "" : dr["PISDB6"].ToString();
                    dataMTR.PISOP6 = dr["PISOP6"] == DBNull.Value ? "" : dr["PISOP6"].ToString();
                    dataMTR.PISMT6 = dr["PISMT6"] == DBNull.Value ? "" : dr["PISMT6"].ToString();
                    dataMTR.PISLP6 = dr["PISLP6"] == DBNull.Value ? "" : dr["PISLP6"].ToString();
                    dataMTR.PISDOB = dr["PISDOB"] == DBNull.Value ? "" : dr["PISDOB"].ToString();
                    dataMTR.PISOPD = dr["PISOPD"] == DBNull.Value ? "" : dr["PISOPD"].ToString();
                    dataMTR.PISMTD = dr["PISMTD"] == DBNull.Value ? "" : dr["PISMTD"].ToString();
                    dataMTR.PISLPD = dr["PISLPD"] == DBNull.Value ? "" : dr["PISLPD"].ToString();
                    dataMTR.STATUSPOLICY = dr["STATUSPOLICY"] == DBNull.Value ? "" : dr["STATUSPOLICY"].ToString();
                    dataMTR.INVALIDREASON = dr["INVALIDREASON"] == DBNull.Value ? "" : dr["INVALIDREASON"].ToString();

                    DateTime? effDate = null;
                    DateTime? birthDate = null;
                    DateTime? matureDate = null;
                    DateTime? payDate = null;
                    if (!string.IsNullOrEmpty(dataMTR.PISOP6) && !string.IsNullOrEmpty(dataMTR.PISOPD) && dataMTR.PISOP6.Length >= 4 && dataMTR.PISOPD.Length >= 4)
                    { effDate = GetDate(dataMTR.PISOP6.Substring(0, 4) + dataMTR.PISOPD.Substring(0, 4), "ddmmyyyy"); }
                    if (!string.IsNullOrEmpty(dataMTR.PISDB6) && !string.IsNullOrEmpty(dataMTR.PISDOB) && dataMTR.PISDB6.Length >= 4 && dataMTR.PISDOB.Length >= 4)
                    { birthDate = GetDate(dataMTR.PISDB6.Substring(0, 4) + dataMTR.PISDOB.Substring(0, 4), "ddmmyyyy"); }
                    if (!string.IsNullOrEmpty(dataMTR.PISMT6) && !string.IsNullOrEmpty(dataMTR.PISMTD) && dataMTR.PISMT6.Length >= 4 && dataMTR.PISMTD.Length >= 4)
                    { matureDate = GetDate(dataMTR.PISMT6.Substring(0, 4) + dataMTR.PISMTD.Substring(0, 4), "ddmmyyyy"); }
                    if (!string.IsNullOrEmpty(dataMTR.PISLP6) && !string.IsNullOrEmpty(dataMTR.PISLPD) && dataMTR.PISLP6.Length >= 4 && dataMTR.PISLPD.Length >= 4)
                    { payDate = GetDate(dataMTR.PISLP6.Substring(0, 4) + dataMTR.PISLPD.Substring(0, 4), "ddmmyyyy"); }

                    #region add data
                    JobFileDetail _jobDetail = new JobFileDetail();
                    _jobDetail.JobFileId = jobFile.JobFileId;
                    _jobDetail.No = row.ToString();

                    _jobDetail.EffectiveDate = effDate;
                    _jobDetail.BirthDate = birthDate;
                    _jobDetail.MatureDate = matureDate;
                    _jobDetail.PaymentDate = payDate;

                    _jobDetail.Cifno = dataMTR.PISCIF;
                    _jobDetail.Name = dataMTR.PISNAM;
                    _jobDetail.SexId = dataMTR.PISSEX;
                    _jobDetail.Premium = GetValue(dataMTR.PISAMT) == -1 ? -1 : GetValue(dataMTR.PISAMT) * (decimal)0.01;
                    _jobDetail.PaymentModeId = "M";
                    _jobDetail.PaymentMethodId = "DBB";
                    _jobDetail.IdentityTypeId = dataMTR.PISCFC;
                    _jobDetail.IdentityNo = dataMTR.PISCFS;

                    _jobDetail.Currency = dataMTR.PISCUR;

                    Product _prod = new Product();
                    if (!string.IsNullOrEmpty(_jobDetail.Currency))
                    {
                        if (_jobDetail.Currency.Contains("IDR"))
                        {
                            _jobDetail.ProductId = "TRMIDR";
                            //_prod = _prodIDR;
                        }
                        else if (_jobDetail.Currency.Contains("USD"))
                        {
                            _jobDetail.ProductId = "TABINS_USD";
                            // _prod = _prodUSD;
                        }
                    }

                    _jobDetail.KodeCabang = dataMTR.PISBR;
                    _jobDetail.Reference = dataMTR.PISACT;
                    _jobDetail.CreateDate = jobFile.CreateDate;
                    _jobDetail.CreateBy = jobFile.UserId;
                    _jobDetail.DetailId = dataMTR.ID; //jobFile.JobFileId + "-" + _jobDetail.No;
                    _jobDetail.ClaimCode = dataMTR.PISCOD;
                    _jobDetail.CreditValue = GetValue(dataMTR.PISAFT) == -1 ? -1 : GetValue(dataMTR.PISAFT) * (decimal)0.01;
                    _jobDetail.StatusId = dataMTR.PISSTS;
                    _jobDetail.BankId = "1";
                    _jobDetail.ProductReference = dataMTR.PISPRD;
                    _jobDetail.StatusProcess = 1;
                    _jobDetail.StatusDMTM = 0;

                    if (dataMTR.STATUSPOLICY == "F")
                    {
                        _jobDetail.StatusProcess = 2;
                        _jobDetail.StatusDMTM = 2;

                        if (dataMTR.INVALIDREASON.Contains("AGE") || dataMTR.INVALIDREASON.Contains("COVERAGE"))
                        {
                            detail.Description = dataMTR.INVALIDREASON;
                            detail.Remark = dataMTR.PISBR + "" + dataMTR.PISACT + "" + dataMTR.PISCUR + "" +
                            dataMTR.PISSTS + "" + dataMTR.PISPRD + "" + dataMTR.PISCIF + "" + dataMTR.PISNAM + "" + dataMTR.PISSEX + "" + dataMTR.PISCFC + "" + dataMTR.PISCFS
                            + "" + dataMTR.PISAFT + "" + dataMTR.PISAFT + "" + dataMTR.PISAMT + "" + dataMTR.PISCOD + "" + dataMTR.PISDB6 + "" + dataMTR.PISOP6 + "" + dataMTR.PISMT6
                             + "" + dataMTR.PISLP6 + "" + dataMTR.PISDOB + "" + dataMTR.PISOPD + "" + dataMTR.PISMTD + "" + dataMTR.PISLPD;

                        }
                        else
                        {
                            detail.Description = string.Format("INVALID DATA");
                            detail.Remark = dataMTR.INVALIDREASON;
                        }

                        detail.OrderId = dataMTR.ID;
                        detail.Remark2 = _jobDetail.Reference;
                        detail.CreateDate = DateTime.Now;
                        detail.ModifyDate = DateTime.Now;
                        detail.ModifyBy = "System";
                        detail.CreateBy = "System";
                        detail.DetailId = Convert.ToInt32(dataMTR.ID.Substring(16));

                        batches.Add(dataMTR.ID, detail);
                    }
                    else
                    {
                        if (_jobDetail.StatusId != "2")
                        {
                            _jobDetail.PaymentId = Guid.NewGuid().ToString().ToUpper();
                        }

                        #region Policy Reserved
                        /*
                        string sqlCust = $"SELECT CUSTOMERID FROM CUSTOMER WHERE CIFNO = '{_jobDetail.Cifno}'";

                        DataSet dsCust = new DataSet();
                        DataSet dsPol = new DataSet();
                        dsCust = helper.ExecuteSqlQuery(sqlCust);
                        if (dsCust.Tables[0].Rows.Count > 0)
                        {
                            _jobDetail.CustomerId = dsCust.Tables[0].Rows[0]["CUSTOMERID"].ToString();
                            string sqlPolicy = $"SELECT TOP 1 POLICYNO, CUSTOMERNO, PRODUCTID, CURRENCYID, PAYMENTMODEID, PAYMENTMETHODID, CREDITVALUE FROM POLICY WHERE PRODUCTID = '{_jobDetail.ProductId}' and STATUSID IN('IN','LA') and REFFERENCENO = '{_jobDetail.Reference}' and CUSTOMERNO='{_jobDetail.CustomerId}'";
                            dsPol = helper.ExecuteSqlQuery(sqlPolicy);

                            if (dsPol.Tables[0].Rows.Count > 0)
                            {
                                _jobDetail.IsNB = 0;
                                _jobDetail.PolicyNo = dsPol.Tables[0].Rows[0]["POLICYNO"].ToString();
                            }
                            else
                            {
                                _jobDetail.IsNB = 1;
                                //_jobDetail.PolicyNo = NextPolicyId(_prod);
                            }
                        }
                        else
                        {
                            _jobDetail.IsNB = 1;
                            //_jobDetail.PolicyNo = NextPolicyId(_prod);
                        }
                        */
                        #endregion
                    }

                    jobDetails.Add(_jobDetail);
                    #endregion

                }

            }


            DataTable dtTemp = MapperJobFileDetail(jobDetails);

            #region Connection

            LogHelper.LogInfo($"Insert Bulk - Start", _pluginMetadata);

            using (DbConnection conn = _database.CreateConnection())
            {
                //msg += "2";
                conn.Open();
                //msg += "3";
                using (SqlBulkCopy copy = new SqlBulkCopy((SqlConnection)conn, SqlBulkCopyOptions.Default, null))
                {
                    try
                    {
                        copy.BatchSize = 1000000;
                        //copy.NotifyAfter = 500;
                        copy.DestinationTableName = "JOBFILEDETAIL";
                        copy.BulkCopyTimeout = 0;
                        copy.WriteToServer(dtTemp);
                        //msg += "4";

                        LogHelper.LogInfo($"Insert Bulk - Success", _pluginMetadata);
                    }
                    catch (Exception ex)
                    {
                        msg += ex.Message + Environment.NewLine + Environment.NewLine + ex.StackTrace;

                        LogHelper.LogInfo($"Insert Bulk - Failed \n " + msg, _pluginMetadata);
                        throw new Exception($"Insert Bulk - Failed \n " + msg);
                    }

                }
                conn.Close();
            }

            LogHelper.LogInfo($"Insert Bulk - Finish", _pluginMetadata);
            #endregion

        }
        protected DateTime? GetDate(string value, string format)
        {
            int day = 0;
            int month = 0;
            int year = 0;
            DateTime? date = null;
            try
            {
                switch (format.ToLower().Trim())
                {
                    case "ddmmyyyy":
                        day = int.Parse(value.Substring(0, 2));
                        month = int.Parse(value.Substring(2, 2));
                        year = int.Parse(value.Substring(4, 4));
                        break;
                    case "yymmdd":
                        year = int.Parse(DateTime.Now.Year.ToString().Substring(0, 2) + value.Substring(0, 2));
                        month = int.Parse(value.Substring(2, 2));
                        day = int.Parse(value.Substring(4, 2));
                        break;
                }
                if (year == 0000 || month == 00 || day == 00)
                {
                    date = null;
                }
                else
                {
                    date = new DateTime(year, month, day);
                }
            }
            catch (Exception ex)
            {

            }

            return date;
        }
        string NextPolicyId(Product p)
        {
            long number = _sequencePersistence.Get("Policy", p.ProductId);

            string policyId = p.PolicyPrefix + "-" + number.ToString().PadLeft(6, '0') + GetVerificationCode(number.ToString().PadLeft(6, '0'));

            return policyId;
        }
        public string GetVerificationCode(string number)
        {
            long code = 0;

            code = 98 - ((long.Parse(number) * 100) % 97) % 97;

            //alter by firyan - 20090221 - begin
            return code.ToString().Substring(0, 1);
            //alter by firyan - 20090221 - end
        }
        protected decimal GetValue(string value)
        {
            decimal val = 0;
            try
            {
                val = Convert.ToInt64(value);
            }
            catch (Exception ex)
            {
                val = -1;
                //messageError += "Row " + total + " value is empty/wrong, ";
            }
            return val;
        }
        protected DataTable MapperJobFileDetail(List<JobFileDetail> details)
        {
            DataTable dtTemp = new DataTable(); // PolicyHistory
            DataColumn col = new DataColumn();

            col = new DataColumn("JOBFILEID", typeof(string)); dtTemp.Columns.Add(col); //0
            col = new DataColumn("NO", typeof(string)); dtTemp.Columns.Add(col); //1
            col = new DataColumn("EFFECTIVEDATE", typeof(DateTime)); dtTemp.Columns.Add(col); //2
            col = new DataColumn("CIFNO", typeof(string)); dtTemp.Columns.Add(col); //3
            col = new DataColumn("NAME", typeof(string)); dtTemp.Columns.Add(col); //4
            col = new DataColumn("SEXID", typeof(string)); dtTemp.Columns.Add(col); //5
            col = new DataColumn("AGE", typeof(int)); dtTemp.Columns.Add(col); //6
            col = new DataColumn("PREMIUM", typeof(float)); dtTemp.Columns.Add(col); //7
            col = new DataColumn("OUTSTANDING", typeof(float)); dtTemp.Columns.Add(col); //8
            col = new DataColumn("STATUSPROCESS", typeof(int)); dtTemp.Columns.Add(col); //9
            col = new DataColumn("PRODUCTID", typeof(string)); dtTemp.Columns.Add(col); //10
            col = new DataColumn("PAYMENTMODEID", typeof(string)); dtTemp.Columns.Add(col); //11
            col = new DataColumn("PAYMENTMETHODID", typeof(string)); dtTemp.Columns.Add(col); //12
            col = new DataColumn("CURRENCY", typeof(string)); dtTemp.Columns.Add(col); //13
            col = new DataColumn("CREATEDATE", typeof(DateTime)); dtTemp.Columns.Add(col); //14
            col = new DataColumn("CREATEBY", typeof(string)); dtTemp.Columns.Add(col); //15
            col = new DataColumn("DETAILID", typeof(string)); dtTemp.Columns.Add(col); //16
            col = new DataColumn("MATUREDATE", typeof(DateTime)); dtTemp.Columns.Add(col); //17
            col = new DataColumn("TENOR", typeof(int)); dtTemp.Columns.Add(col); //18
            col = new DataColumn("CREDITVALUE", typeof(float)); dtTemp.Columns.Add(col); //19
            col = new DataColumn("BANKID", typeof(string)); dtTemp.Columns.Add(col); //20
            col = new DataColumn("BANKBRANCHID", typeof(string)); dtTemp.Columns.Add(col); //21
            col = new DataColumn("IDENTITYNO", typeof(string)); dtTemp.Columns.Add(col); //22
            col = new DataColumn("IDENTITYTYPEID", typeof(string)); dtTemp.Columns.Add(col); //23
            col = new DataColumn("OCCUPATIONID", typeof(string)); dtTemp.Columns.Add(col); //24
            col = new DataColumn("LINE1", typeof(string)); dtTemp.Columns.Add(col); //25
            col = new DataColumn("REFERENCE", typeof(string)); dtTemp.Columns.Add(col); //26  
            col = new DataColumn("BIRTHPLACE", typeof(string)); dtTemp.Columns.Add(col); //27  
            col = new DataColumn("BIRTHDATE", typeof(DateTime)); dtTemp.Columns.Add(col); //28 
            col = new DataColumn("KODECABANG", typeof(string)); dtTemp.Columns.Add(col); //29  
            col = new DataColumn("NAMACABANG", typeof(string)); dtTemp.Columns.Add(col); //30  
            col = new DataColumn("PAYMENTDATE", typeof(DateTime)); dtTemp.Columns.Add(col); //30  
            col = new DataColumn("BENEFITVALUE", typeof(float)); dtTemp.Columns.Add(col); //31  
            col = new DataColumn("CLAIMCODE", typeof(string)); dtTemp.Columns.Add(col); //32  
            col = new DataColumn("STATUSID", typeof(string)); dtTemp.Columns.Add(col); //33 
            col = new DataColumn("PRODUCTREFERENCE", typeof(string)); dtTemp.Columns.Add(col); //34
            col = new DataColumn("STATUSDMTM", typeof(int)); dtTemp.Columns.Add(col); //35
            col = new DataColumn("PAYMENTID", typeof(string)); dtTemp.Columns.Add(col); //36

            if (details.Count > 0)
            {
                foreach (JobFileDetail dt in details)
                {
                    DataRow dr = dtTemp.NewRow();
                    dr["JOBFILEID"] = dt.JobFileId;
                    dr["NO"] = dt.No;
                    dr["EFFECTIVEDATE"] = dt.EffectiveDate ?? (object)DBNull.Value;
                    dr["CIFNO"] = dt.Cifno;
                    dr["NAME"] = dt.Name;
                    dr["SEXID"] = dt.SexId;
                    dr["AGE"] = dt.Age;
                    dr["PREMIUM"] = dt.Premium;
                    dr["OUTSTANDING"] = dt.Outstanding;
                    dr["STATUSPROCESS"] = dt.StatusProcess;
                    dr["PRODUCTID"] = dt.ProductId;
                    dr["PAYMENTMODEID"] = dt.PaymentModeId;
                    dr["PAYMENTMETHODID"] = dt.PaymentMethodId;
                    dr["CURRENCY"] = dt.Currency;
                    dr["CREATEDATE"] = dt.CreateDate;
                    dr["CREATEBY"] = dt.CreateBy;
                    dr["DETAILID"] = dt.DetailId;
                    dr["MATUREDATE"] = dt.MatureDate ?? (object)DBNull.Value;
                    dr["TENOR"] = dt.Tenor;
                    dr["CREDITVALUE"] = dt.CreditValue;
                    dr["BANKID"] = dt.BankId;
                    dr["BANKBRANCHID"] = dt.BankBranchId;
                    dr["IDENTITYNO"] = dt.IdentityNo;
                    dr["IDENTITYTYPEID"] = dt.IdentityTypeId;
                    dr["OCCUPATIONID"] = dt.OccupationId;
                    dr["LINE1"] = dt.Line1;
                    dr["REFERENCE"] = dt.Reference;
                    dr["BIRTHPLACE"] = dt.BirthPlace;
                    dr["BIRTHDATE"] = dt.BirthDate ?? (object)DBNull.Value;
                    dr["KODECABANG"] = dt.KodeCabang;
                    dr["NAMACABANG"] = dt.NamaCabang;
                    dr["PAYMENTDATE"] = dt.PaymentDate ?? (object)DBNull.Value;
                    dr["BENEFITVALUE"] = dt.BenefitValue;
                    dr["CLAIMCODE"] = dt.ClaimCode;
                    dr["STATUSID"] = dt.StatusId;
                    dr["PRODUCTREFERENCE"] = dt.ProductReference;
                    dr["STATUSDMTM"] = dt.StatusDMTM;
                    dr["PAYMENTID"] = dt.PaymentId;
                    dtTemp.Rows.Add(dr);
                }
            }

            return dtTemp;
        }

        protected Batch SaveBatch(Dictionary<string, BatchDetail> batches, string description)
        {
            List<BatchDetail> details = new List<BatchDetail>(batches.Values);
            try
            {
                Batch batch = new Batch();
                //Batch batch = _batchService.GetBatch(batchId);

                batch.ModifyBy = "System";
                batch.Description = description;
                batch.TypeId = "IP";
                batch.Details = null;

                batch = _batchService.AddBatch(batch);

                for (int i = details.Count - 1; i >= 0; i--)
                {
                    if (string.IsNullOrEmpty(details[i].Description) || (string.IsNullOrEmpty(details[i].Description) && string.IsNullOrEmpty(details[i].Remark2)))
                    {
                        details.RemoveAt(i);
                    }
                    details[i].BatchId = batch.BatchId;
                }

                LogHelper.LogInfo($"Insert Bulk BatchDetail - Start :" + batch.BatchId, _pluginMetadata);

                if (details != null)
                {
                    DataTable dtTemp = MapperBatchDetail(details);

                    #region Connection

                    LogHelper.LogInfo($"Insert Bulk BatchDetail - Start", _pluginMetadata);

                    using (DbConnection conn = _database.CreateConnection())
                    {
                        //msg += "2";
                        conn.Open();
                        //msg += "3";
                        using (SqlBulkCopy copy = new SqlBulkCopy((SqlConnection)conn, SqlBulkCopyOptions.Default, null))
                        {
                            try
                            {
                                copy.BatchSize = 1000000;
                                //copy.NotifyAfter = 500;
                                copy.DestinationTableName = "BATCHDETAIL";
                                copy.BulkCopyTimeout = 0;
                                copy.WriteToServer(dtTemp);
                                //msg += "4";

                                LogHelper.LogInfo($"Insert Bulk BatchDetail - Success", _pluginMetadata);
                            }
                            catch (Exception ex)
                            {
                                //msg += ex.Message + Environment.NewLine + Environment.NewLine + ex.StackTrace;

                                LogHelper.LogInfo($"Insert Bulk BatchDetail- Failed", _pluginMetadata);
                                throw new Exception($"Insert Bulk BatchDetail - Failed" + ex.Message + "\n" + ex.Source + "\n" + ex.StackTrace);
                            }

                        }
                        conn.Close();
                    }

                    LogHelper.LogInfo($"Insert Bulk BatchDetail - Finish", _pluginMetadata);
                    #endregion 
                }
                return batch;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "\n" + ex.Source + "\n" + ex.StackTrace);
            }
        }
        protected Batch UpdateBatch(Dictionary<string, BatchDetail> batches, string description, JobFiles jobFiles, int unprocessed)
        {
            List<BatchDetail> details = new List<BatchDetail>(batches.Values);
            try
            {
                for (int i = details.Count - 1; i >= 0; i--)
                {
                    if (string.IsNullOrEmpty(details[i].Description) || string.IsNullOrEmpty(details[i].Remark2))
                    {
                        details.RemoveAt(i);
                    }
                    details[i].BatchId = jobFiles.Remark;
                }

                Batch batch = new Batch();

                if (unprocessed == 0)
                {
                    batch = _batchService.GetBatch(jobFiles.Remark);

                    batch.Description = description;
                    batch.ModifyDate = DateTime.Now;
                    _batchPersistence.Update(batch);
                }

                if (details != null)
                {
                    //int detailId = 0;
                    //List<Criteria> criteriaDetail = new List<Criteria>();
                    //criteriaDetail.Add(new Criteria("p.BatchId", jobFiles.Remark, Comparison.Equal));
                    //detailId = _batchDetailPersistence.Find(criteriaDetail);
                    //detailId = _batchDetailPersistence.Count(criteriaDetail);

                    //foreach (BatchDetail detail in details)
                    //{
                    //    //detailId += 1;
                    //    detail.BatchId = jobFiles.Remark;
                    //    //detail.DetailId = detailId;

                    //    _batchDetailPersistence.Add(detail);
                    //}

                    DataTable dtTemp = MapperBatchDetail(details);

                    #region Connection

                    LogHelper.LogInfo($"Insert Bulk BatchDetail - Start", _pluginMetadata);

                    using (DbConnection conn = _database.CreateConnection())
                    {
                        //msg += "2";
                        conn.Open();
                        //msg += "3";
                        using (SqlBulkCopy copy = new SqlBulkCopy((SqlConnection)conn, SqlBulkCopyOptions.Default, null))
                        {
                            try
                            {
                                copy.BatchSize = 500;
                                copy.NotifyAfter = 500;
                                copy.DestinationTableName = "BATCHDETAIL";
                                copy.BulkCopyTimeout = 0;
                                copy.WriteToServer(dtTemp);
                                //msg += "4";

                                LogHelper.LogInfo($"Insert Bulk BatchDetail - Success", _pluginMetadata);
                            }
                            catch (Exception ex)
                            {
                                //msg += ex.Message + Environment.NewLine + Environment.NewLine + ex.StackTrace;

                                LogHelper.LogInfo($"Insert Bulk BatchDetail- Failed", _pluginMetadata);
                                throw new Exception($"Insert Bulk BatchDetail - Failed");
                            }

                        }
                        conn.Close();
                    }

                    LogHelper.LogInfo($"Insert Bulk BatchDetail - Finish", _pluginMetadata);
                    #endregion 
                }
                return batch;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        protected string Implode(string[] values, string sep)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < values.Length; i++)
            {
                if (i < values.Length - 1)
                {
                    sb.AppendFormat("{0}{1}", values[i], sep);
                }
                else
                {
                    sb.AppendFormat("{0}", values[i]);
                }
            }

            return sb.ToString();
        }
        protected string GetMaritalStatusId(string maritalId)
        {
            switch (maritalId)
            {
                case "L":
                case "M":
                    return "M";
                case "P":
                case "F":
                    return "F";
            }

            return null;
        }
        protected string GetOccupationId(string desc)
        {
            string occupationId = string.Empty;
            List<Occupation> occupations = new List<Occupation>();
            List<Criteria> criterias = new List<Criteria>();
            try
            {
                criterias.Add(new Criteria("s.Name", desc, Comparison.Like));
                occupations = _occupationService.FindOccupations(criterias, 0, int.MaxValue, null);
                if (occupations.Count > 0)
                {
                    occupationId = occupations[0].OccupationId;
                }
                else
                {
                    throw new Exception("Occupation is Not Found");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return occupationId;
        }
        protected string GetTitleId(string maritalId)
        {
            switch (maritalId)
            {
                case "M":
                    return "MR";
                case "F":
                    return "MRS";
            }

            return null;
        }
        private DataSet ExecuteSqlQuery(JobFiles jobFile)
        {
            DataSet ds = new DataSet();
            string sql = string.Empty;
            sql = $"SELECT * FROM UPLOADMTR where JobFileId = @jobFileId";

            LogHelper.LogInfo($"Select data UploadMTR - Start", _pluginMetadata);
            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                try
                {
                    #region AddParameter  
                    var jobFileId = new SqlParameter("jobFileId", SqlDbType.VarChar);
                    jobFileId.Value = jobFile.JobFileId;
                    command.Parameters.Add(jobFileId);
                    #endregion

                    #region fill data from JobDetail
                    connection.Open();
                    SqlDataAdapter da = new SqlDataAdapter(command);
                    da.Fill(ds);
                    da.Dispose();
                    connection.Close();
                    LogHelper.LogInfo($"Select data UploadMTR - Success", _pluginMetadata);
                    #endregion
                }
                catch (Exception ex)
                {
                    connection.Close();
                    LogHelper.LogInfo($"Select data UploadMTR - Failed", _pluginMetadata);
                    LogHelper.LogInfo(ex.Message, _pluginMetadata);
                }
            }

            return ds;

        }
        private DataSet ExecuteSqlQuery(JobFiles jobFile, int status, bool withLimit)
        {
            DataSet ds = new DataSet();
            string sql = string.Empty;
            if (withLimit)
            {
                sql = "SELECT Top " + recordLimit + " * from JobFileDetail where StatusProcess = @statusProcess and JobFileId = @jobFileId";

            }
            else
            {
                sql = "SELECT * from JobFileDetail where StatusProcess = @statusProcess and JobFileId = @jobFileId";

            }

            LogHelper.LogInfo($"Select DetailFile - Start", _pluginMetadata);
            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                try
                {
                    #region AddParameter 
                    var statusProcess = new SqlParameter("statusProcess", SqlDbType.Int);
                    statusProcess.Value = status;
                    command.Parameters.Add(statusProcess);

                    var jobFileId = new SqlParameter("jobFileId", SqlDbType.VarChar);
                    jobFileId.Value = jobFile.JobFileId;
                    command.Parameters.Add(jobFileId);
                    #endregion

                    #region fill data from JobDetail
                    connection.Open();
                    SqlDataAdapter da = new SqlDataAdapter(command);
                    da.Fill(ds);
                    da.Dispose();
                    connection.Close();
                    LogHelper.LogInfo($"Select DetailFile - Success", _pluginMetadata);
                    #endregion
                }
                catch (Exception ex)
                {
                    connection.Close();
                    LogHelper.LogInfo($"Select DetailFile - Failed", _pluginMetadata);
                    LogHelper.LogInfo(ex.Message, _pluginMetadata);
                }
            }

            return ds;

        }
        /// <summary>
        /// value of status
        /// 0 = data not proccessed
        /// 1 = data is proccessed and success
        /// 2 = data is proccessed and failed
        /// </summary>
        /// <param name="detailId"></param>
        /// <param name="status"></param>
        private void UpdateJobDetail(string detailId, int status)
        {
            string sql = "Update JobFileDetail set StatusProcess = @statusProcess where DetailId = @jobDetailId";
            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                try
                {
                    #region AddParameter  
                    var statusProcess = new SqlParameter("statusProcess", SqlDbType.Int);
                    statusProcess.Value = status;
                    command.Parameters.Add(statusProcess);

                    var jobDetailId = new SqlParameter("jobDetailId", SqlDbType.VarChar);
                    jobDetailId.Value = detailId;
                    command.Parameters.Add(jobDetailId);
                    #endregion  

                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();

                    LogHelper.LogInfo($"Update Detail : " + detailId + " = " + status + " - Success", _pluginMetadata);
                }
                catch (Exception ex)
                {

                    LogHelper.LogInfo($"Update Detail : " + detailId + " = " + status + " - Failed", _pluginMetadata);
                    LogHelper.LogInfo(ex.Message, _pluginMetadata);
                }
            }
        }
        /// <summary>
        /// insert data failed to table jobFileDetailFailed
        /// </summary>
        /// <param name="jobFileId"></param>
        /// <param name="no"></param>
        /// <param name="data"></param>
        private void InsertJobDetailFailed(string jobFileId, int no, string data)
        {
            string sql = "Insert into JOBFILEDETAILFAILED (JobFileId, Row, Data) values (@JobFileId, @Row, @Data)";
            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                try
                {
                    #region AddParameter  
                    var _jobFileId = new SqlParameter("JobFileId", SqlDbType.VarChar);
                    _jobFileId.Value = jobFileId;
                    command.Parameters.Add(_jobFileId);

                    var _row = new SqlParameter("Row", SqlDbType.Int);
                    _row.Value = no;
                    command.Parameters.Add(_row);

                    var _data = new SqlParameter("Data", SqlDbType.VarChar);
                    _data.Value = data;
                    command.Parameters.Add(_data);
                    #endregion  

                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();

                    LogHelper.LogInfo($"Insert Failed No : " + no + " - Success", _pluginMetadata);
                }
                catch (Exception ex)
                {

                    LogHelper.LogInfo($"Insert Failed No : " + no + " - Failed", _pluginMetadata);
                    LogHelper.LogInfo(ex.Message, _pluginMetadata);
                }
            }
        }
        /// <summary>
        /// process failed date which statusProcess=2
        /// </summary>
        /// <param name="jobFile"></param>
        private void ProcFailedData(JobFiles jobFile)
        {
            string[] rows = content.Split(new string[] { LineSep }, StringSplitOptions.None);

            DataSet dsFailed = new DataSet();
            dsFailed = ExecuteSqlQuery(jobFile, 2, false); //copy failed data to table failed
            if (dsFailed.Tables[0].Rows.Count > 0)
            {
                string data = string.Empty;
                int no = 0;
                string jobFileId = jobFile.JobFileId;
                string[] header = rows[0].Split(new string[] { FieldSep }, StringSplitOptions.None);
                data = Implode(header, FieldSep);

                InsertJobDetailFailed(jobFileId, no, data);

                for (int i = 0; i < dsFailed.Tables[0].Rows.Count; i++)
                {
                    for (int row = 1; row < rows.Length; row++)
                    {
                        no = row;
                        string[] colls = rows[row].Split(new string[] { FieldSep }, StringSplitOptions.None);
                        if (dsFailed.Tables[0].Rows[i][2].ToString() == colls[0].Trim() &&
                           dsFailed.Tables[0].Rows[i][4].ToString() == colls[2].Trim() &&
                           dsFailed.Tables[0].Rows[i][5].ToString() == colls[3].Trim() &&
                           dsFailed.Tables[0].Rows[i][7].ToString() == colls[5].Trim()
                          )
                        {
                            data = Implode(colls, FieldSep);
                            break;
                        }
                    }
                    InsertJobDetailFailed(jobFileId, no, data);
                }
            }
        }
        private void ProcFailedDataExcel(ExcelWorksheet sheet, JobFiles jobFile)
        {
            DataSet dsFailed = new DataSet();
            dsFailed = ExecuteSqlQuery(jobFile, 2, false); //copy failed data to table failed
            if (dsFailed.Tables[0].Rows.Count > 0)
            {
                string data = string.Empty;
                int no = 0;
                string jobFileId = jobFile.JobFileId;
                int rowCount = sheet.Dimension.Rows;
                int columns = sheet.Dimension.Columns;
                for (int col = 1; col <= columns; col++)
                {
                    if (col == columns)
                    {
                        data += sheet.Cells[1, col].Value.ToString();
                    }
                    else
                    {
                        data += sheet.Cells[1, col].Value.ToString() + FieldSep;
                    }
                }

                InsertJobDetailFailed(jobFileId, no, data);

                //data = "";
                //for (int i = 0; i < dsFailed.Tables[0].Rows.Count; i++)
                //{
                //    for (int col = 1; col <= columns; col++)
                //    {
                //        no = i;
                //        if (col == columns)
                //        {
                //            data += sheet.Cells[i, col].Value.ToString();
                //        }
                //        else
                //        {
                //            data += sheet.Cells[i, col].Value.ToString() + FieldSep;
                //        }
                //    }

                //    InsertJobDetailFailed(jobFileId, no, data);
                //}
            }
        }
        protected List<JobFileDetail> GetJobFileDetails(ExcelWorksheet sheet, JobFiles jobFile, int status, Dictionary<string, BatchDetail> batches)
        {
            List<Criteria> criterias = new List<Criteria>();
            List<JobFileDetail> jDetails = new List<JobFileDetail>();
            try
            {
                criterias.Add(new Criteria("p.JobFileId", jobFile.JobFileId, Comparison.Equal));
                criterias.Add(new Criteria("p.StatusProcess", status, Comparison.Equal));

                jDetails = _jobFileDetailsService.FindJobFileDetail(criterias, 0, recordLimit, " p.DetailId asc");
                if (jDetails.Count > 0 && sheet != null)
                {
                    int columns = sheet.Dimension.Columns;
                    int row = 2;
                    foreach (JobFileDetail jobDetail in jDetails)
                    {
                        string field = "";
                        BatchDetail detail = new BatchDetail();
                        for (int col = 1; col <= columns; col++)
                        {
                            if (col == columns)
                            {
                                field += sheet.Cells[row, col].Value.ToString();
                            }
                            else
                            {
                                field += sheet.Cells[row, col].Value.ToString() + FieldSep;
                            }
                        }
                        detail.TypeId = "POL";
                        detail.Remark = field;

                        batches.Add(jobDetail.DetailId, detail);
                        row++;
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogError($"error Get JobFileDetail : " + ex.Message + " " + ex.StackTrace + " " + ex.Source, ex, _pluginMetadata);

            }
            return jDetails;
        }
    }
}
