﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeneralService.Plugin;
using Axa.Insurance.Helper;
using Axa.Insurance.Service;

namespace GeneralService.Sample
{
    public class Service : IPlugin
    {
        private readonly IProductService _productService = ContextHelper.GetObject<IProductService>();

        public void Run(PluginMetadata metadata, Dictionary<string, string> args = null)
        {
            if (args != null)
            {
                if (args.ContainsKey("product"))
                {
                    if (args["product"] == "all")
                    {
                        var products = _productService.GetAllProducts();

                        Console.WriteLine("Show Product using Axa.Insurance.Service -> ProductService");
                        Console.WriteLine($"ProductId | Name");

                        foreach (var product in products)
                        {
                            Console.WriteLine($"{product.ProductId} | {(!string.IsNullOrWhiteSpace(product.PrintingName) ? product.PrintingName : product.Name)}");
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Get product {args["product"]}");

                        var product = _productService.GetProduct(args["product"]);

                        if (product != null)
                        {
                            Console.WriteLine($"Product : {product.ProductId}, ProductName : {(!string.IsNullOrWhiteSpace(product.PrintingName) ? product.PrintingName : product.Name)}");
                        }
                        else
                        {
                            Console.WriteLine("Not found");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Sample with args");
                    foreach (var arg in args)
                    {
                        Console.WriteLine($"key : {arg.Key}, value : {arg.Value}");
                    }
                }
            }
            else
            {
                Console.WriteLine("Sample without args");
            }

            if (metadata.PluginSettings.Any())
            {
                foreach (var setting in metadata.PluginSettings)
                {
                    Console.WriteLine($"Setting key {setting.Key}, Setting value {setting.Value}");
                }
            }
        }
    }
}
