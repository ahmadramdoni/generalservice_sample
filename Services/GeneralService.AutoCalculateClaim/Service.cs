﻿using GeneralService.Plugin;
using System.Collections.Generic;
using GeneralService.Core.Helper;
using System;
using System.IO;
using Axa.Insurance.Service;
using Axa.Insurance.Model;
using Axa.Insurance.Helper;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Configuration;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Axa.Insurance.Persistence;

namespace GeneralService.AutoCalculateClaim
{
    public class Service : IPlugin
    {
        private IBatchService _batchService = ContextHelper.GetObject<IBatchService>();
        private IBatchPersistence _batchPersistence = ContextHelper.GetObject<IBatchPersistence>();
        private IBatchDetailPersistence _batchDetailPersistence = ContextHelper.GetObject<IBatchDetailPersistence>();
        private IJobFilesService _jobFilesService = ContextHelper.GetObject<IJobFilesService>();
        private IJobFileDetailService _jobFileDetailsService = ContextHelper.GetObject<IJobFileDetailService>();
        private IPolicyService _policyService = ContextHelper.GetObject<IPolicyService>();
        private IPolicyHistoryService _historyService = ContextHelper.GetObject<IPolicyHistoryService>();
        private ICustomerService _customerService = ContextHelper.GetObject<ICustomerService>();
        private IAddressService _addressService = ContextHelper.GetObject<IAddressService>();
        private IOccupationService _occupationService = ContextHelper.GetObject<IOccupationService>();
        private IProductService _productServices = ContextHelper.GetObject<IProductService>();
        private ISequencePersistence _sequencePersistence = ContextHelper.GetObject<ISequencePersistence>();
        private IClaimService _claimService = ContextHelper.GetObject<IClaimService>();
        private IBenefitDetailService _benefitDetailService = ContextHelper.GetObject<IBenefitDetailService>();
        private IInsuredService _insuredService = ContextHelper.GetObject<IInsuredService>();
        private IClaimDetailService _claimDetailService = ContextHelper.GetObject<IClaimDetailService>();
        private IClaimHistoryService _claimHistoryService = ContextHelper.GetObject<IClaimHistoryService>();
        private IPolicyHistoryService _policyHistoryService = ContextHelper.GetObject<IPolicyHistoryService>();
        private IBenefitService _benefitService = ContextHelper.GetObject<IBenefitService>();
        private IBenefitChildService _benefitChildService = ContextHelper.GetObject<IBenefitChildService>();
        private IClaimDetailChildService _claimDetailChildService = ContextHelper.GetObject<IClaimDetailChildService>();
        private IBillingService _billingService = ContextHelper.GetObject<IBillingService>(); //add yd 09-09-2011 validation product TLROP
        private IPaymentService _paymentService = ContextHelper.GetObject<IPaymentService>();
        private IBenefitWaitingPeriodService _benefitWaitingPeriodService = ContextHelper.GetObject<IBenefitWaitingPeriodService>();
        private IProductLoanService _productLoanService = ContextHelper.GetObject<IProductLoanService>();

        private static Dictionary<string, string> _args;
        private static PluginMetadata _pluginMetadata; private int total = 0;
        private string _productId = string.Empty;
        private string content = string.Empty;
        private string messageError = string.Empty;
        private double _amountBeforeSBI = 0;
        private double _sbiRate = 0;
        private decimal _totalClaim = 0;
        private decimal _totalPaid = 0;
        private decimal _totalNotPaid = 0;
        private decimal _totalBenefitClaim = 0;
        private decimal _totalAprovedClaim = 0;
        private int _remainBenefitQty = 0;
        private decimal _remainBenefitAmount = 0;
        private List<Claim> _claimsRemainDay = new List<Claim>();
        private string _planId = "";
        private string _claimId = "";
        private string _claimProductId = "";
        private string _insuredId = "";
        private string _oldPlanId = null;
        private Claim m_claim; //add by yd 15-08-2011

        private string executorName = "";
        private string _batchId = "";
        private string sourceData = "";
        private string rangeCriteriaDate = "";
        private int recordLimit = 0;
        private StringBuilder criteriasWhere = new StringBuilder();
        private string connectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
        //protected List<ClaimDetail> sessClaimDetails
        //{
        //    //get { return ViewState["sessClaimDetails"] == null ? new List<ClaimDetail>() : (List<ClaimDetail>)ViewState["sessClaimDetails"]; }
        //    //set { ViewState["sessClaimDetails"] = value; }

        //    get { return Session["sessClaimDetails"] == null ? new List<ClaimDetail>() : (List<ClaimDetail>)Session["sessClaimDetails"]; }
        //    set { Session["sessClaimDetails"] = value; } 
        //}

        //protected List<BenefitDetail> sessBenefits
        //{
        //    //get { return ViewState["sessBenefits"] == null ? new List<BenefitDetail>() : (List<BenefitDetail>)ViewState["sessBenefits"]; }
        //    //set { ViewState["sessBenefits"] = value; }

        //      get { return Session["sessBenefits"] == null ? new List<BenefitDetail>() : (List<BenefitDetail>)Session["sessBenefits"]; }
        //    set { Session["sessBenefits"] = value; } 
        //}
        public void Run(PluginMetadata metadata, Dictionary<string, string> args = null)
        {
            _args = args;
            _pluginMetadata = metadata;
            BindArgs();

            sourceData = _pluginMetadata.PluginSettings.GetValue<string>("SourceData");
            rangeCriteriaDate = _pluginMetadata.PluginSettings.GetValue<string>("RangeCriteriaDate");
            executorName = _pluginMetadata.PluginSettings.GetValue<string>("ExecutorName");
            recordLimit = _pluginMetadata.PluginSettings.GetValue<int>("MaxRecordProcessedPerBatch") > 0 ? _pluginMetadata.PluginSettings.GetValue<int>("MaxRecordProcessedPerBatch") : 10000;

            //ProcessAutoCalculate();
        }
        private void BindArgs()
        {
            criteriasWhere.Append($" 1=1 ");
            if (_args != null)
            {
                foreach (var arg in _args)
                {
                    if (arg.Key.ToLower() == "startdate")
                    {
                        criteriasWhere.Append($" AND {rangeCriteriaDate} >= {arg.Value}");
                    }
                    if (arg.Key.ToLower() == "enddate")
                    {
                        criteriasWhere.Append($" AND {rangeCriteriaDate} <= {arg.Value}");
                    }
                    if (arg.Key.ToLower() != "startdate" && arg.Key.ToLower() != "enddate")
                    {
                        criteriasWhere.Append($" AND {arg.Key.ToLower()} = {arg.Value}");
                    }
                }

                LogHelper.LogInfo(criteriasWhere.ToString(), _pluginMetadata);
            }
        }
        protected void ProcessAutoCalculate()
        {
            List<Criteria> criterias = new List<Criteria>();
            //criterias.Add(new Criteria("Query", " p.Status IN ('Processing','New')", Comparison.None));
            try
            {
                DataSet ds_source_data = new DataSet();
                ds_source_data = SelectSourceData(false);
                if (ds_source_data.Tables[0].Rows.Count > 0)
                {
                    for (int row = 0; row < ds_source_data.Tables[0].Rows.Count; row++)
                    {
                        /* reset claimno */
                        _claimId = "";
                        string ClaimNo = "";
                        ClaimNo = ds_source_data.Tables[0].Rows[row]["ClaimNo"].ToString();
                        _claimId = ClaimNo;

                        _insuredId = "";
                        string InsuredId = "";
                        InsuredId = ds_source_data.Tables[0].Rows[row]["InsuredId"].ToString();
                        _insuredId = InsuredId;

                        Save();
                        //SaveClaimDetail();
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogError(ex.Message + ex.StackTrace, ex, _pluginMetadata);
            }
        }
        private DataSet SelectSourceData(bool withLimit)
        {
            DataSet ds = new DataSet();
            string sql = string.Empty;
            if (string.IsNullOrEmpty(sourceData)) return null;

            if (withLimit)
            {
                sql = "SELECT Top " + recordLimit + " * from " + sourceData + " where " + criteriasWhere.ToString();
            }
            else
            {
                sql = "SELECT * from " + sourceData + " where " + criteriasWhere.ToString();

            }
            LogHelper.LogInfo($"Select SelectSourceData - Start", _pluginMetadata);
            using (SqlConnection connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                try
                {

                    #region fill data from SelectSourceData
                    connection.Open();
                    SqlDataAdapter da = new SqlDataAdapter(command);
                    da.Fill(ds);
                    da.Dispose();
                    connection.Close();
                    LogHelper.LogInfo($"Select SelectSourceData - Success", _pluginMetadata);
                    #endregion
                }
                catch (Exception ex)
                {
                    connection.Close();
                    LogHelper.LogInfo($"Select SelectSourceData - Failed", _pluginMetadata);
                    LogHelper.LogInfo(ex.Message, _pluginMetadata);
                }
            }
            return ds;
        }
        protected void Save()
        {
            //1.save claimdetail
            LogHelper.LogInfo("SaveClaimDetail - Start", _pluginMetadata);
            SaveClaimDetail();
            LogHelper.LogInfo("SaveClaimDetail - End", _pluginMetadata);

            //2. save totalpaid n notpaid in claim
            LogHelper.LogInfo("UpdateTotalPaidNotPaid - Start", _pluginMetadata);
            UpdateTotalPaidNotPaid();
            LogHelper.LogInfo("UpdateTotalPaidNotPaid - End", _pluginMetadata);

            //3.save claimdetailchild
            LogHelper.LogInfo("SaveClaimDetailChild - Start", _pluginMetadata);
            SaveClaimDetailChild();
            LogHelper.LogInfo("SaveClaimDetailChild - End", _pluginMetadata);

            //4. clear session
            //Session["sessClaimDetails"] = null;
        }
        private List<ClaimDetail> SaveClaimDetailSession(bool isCalculate)
        {
            Claim claim = _claimService.GetClaim(_claimId);
            List<ClaimDetail> _claimDetails = new List<ClaimDetail>();
            _claimDetails = _claimDetailService.GetClaimDetails(_claimId);
            List<BenefitDetail> _benefitDetailsSource = new List<BenefitDetail>();
            _benefitDetailsSource = GetInsuredInformationToGetBenefitDetail();
            decimal _totalAmountPaid = 0;

            for (int x = 0; x < _claimDetails.Count; x++)
            {
                for (int y = 0; y < _benefitDetailsSource.Count; y++)
                {

                    int iBenefitId = _benefitDetailsSource[y].BenefitId; //Convert.ToInt32(rgridClaimDetailList.DataKeys[rgridClaimDetailList.Rows[y].RowIndex]["BenefitId"]);
                    int iBenefitDetailId = _benefitDetailsSource[y].BenefitDetailId; //Convert.ToInt32(rgridClaimDetailList.DataKeys[rgridClaimDetailList.Rows[y].RowIndex]["BenefitDetailId"]);
                    if (_claimDetails[x].BenefitId == iBenefitId && _claimDetails[x].BenefitDetailId == iBenefitDetailId)
                    {
                        decimal _AmountClaim = 0;
                        decimal _AmountFromBank = 0;
                        decimal _totalAmountClaim = 0;
                        decimal _benefitValueChild = 0;

                        #region Region : Get rgridClaimDetailList Object
                        /*
                       TextBox txtRemark = (TextBox)rgridClaimDetailList.Rows[y].FindControl("txtRemark");

                       Label lblBenefit = (Label)rgridClaimDetailList.Rows[y].FindControl("lblBenefit");
                       Label lblItem = (Label)rgridClaimDetailList.Rows[y].FindControl("lblItem");
                       Label lblRemainDays = (Label)rgridClaimDetailList.Rows[y].FindControl("lblRemainDays");
                       Label lblBenefitValueRemain = (Label)rgridClaimDetailList.Rows[y].FindControl("lblBenefitValueRemain");

                       TextBox txtAmountFromBank = (TextBox)rgridClaimDetailList.Rows[y].FindControl("txtAmountFromBank");
                       TextBox txtQtyClaim = (TextBox)rgridClaimDetailList.Rows[y].FindControl("txtQtyClaim");
                       TextBox txtBenefitValueClaim = (TextBox)rgridClaimDetailList.Rows[y].FindControl("txtBenefitValueClaim");
                       TextBox txtTotalClaim = (TextBox)rgridClaimDetailList.Rows[y].FindControl("txtTotalClaim");

                       TextBox txtQtyApproved = (TextBox)rgridClaimDetailList.Rows[y].FindControl("txtQtyApproved");
                       TextBox txtBenefitValueApproved = (TextBox)rgridClaimDetailList.Rows[y].FindControl("txtBenefitValueApproved");

                       TextBox lblPaid = (TextBox)rgridClaimDetailList.Rows[y].FindControl("lblPaid");
                       TextBox lblNotPaid = (TextBox)rgridClaimDetailList.Rows[y].FindControl("lblNotPaid");
                        /*
                       /*Start add by NM Oct 2022*/
                        /*
                        Label lblRemainingBenefit = (Label)rgridClaimDetailList.Rows[y].FindControl("lblRemainingBenefit");
                        TextBox txtAmountIncurred = (TextBox)rgridClaimDetailList.Rows[y].FindControl("txtAmountIncurred");
                        */
                        /*End add by NM Oct 2022*/
                        /*
                        GridView gv = (GridView)rgridClaimDetailList.Rows[y].FindControl("rgridBenefitChildList");
                        */
                        #endregion

                        _claimDetails[x].AmountBenefit = _benefitDetailsSource[y].StdAmount; //string.IsNullOrEmpty(lblBenefit.Text) ? 0 : Convert.ToDecimal(lblBenefit.Text);
                        //_claimDetails[x].QuantityClaim = string.IsNullOrEmpty(txtQtyClaim.Text) ? 0 : Convert.ToInt32(txtQtyClaim.Text);

                        //_claimDetails[x].QuantityPaid = string.IsNullOrEmpty(txtQtyApproved.Text) ? 0 : Convert.ToInt32(txtQtyApproved.Text);
                        _claimDetails[x].AmountApproved = _claimDetails[x].AmountBenefit;//string.IsNullOrEmpty(txtBenefitValueApproved.Text) ? 0 : Convert.ToDecimal(txtBenefitValueApproved.Text);
                        //_claimDetails[x].ReceiptBill = string.IsNullOrEmpty(txtAmountIncurred.Text) ? 0 : Convert.ToInt32(txtAmountIncurred.Text);

                        if (isCalculate) // if calculate is true, Calculate Paid and NotPaid ///////////////////////////////////////////////////////////
                        {
                            #region Region : Validation Product MHS copy benefitquantity 1,2,3
                            if (isCalculate && _productId.Contains("MHS") && x <= 2)
                            {
                                if (_claimDetails.Count > 0)
                                {
                                    _claimDetails[x].QuantityClaim = _claimDetails[0].QuantityClaim;
                                }
                            }
                            #endregion

                            #region Region : rgridBenefitChildList calculate
                            Dictionary<int, List<BenefitDetail>> _benefitChildSession = SaveClaimDetailChildSession();
                            if (_benefitChildSession.Count > 0) // Validation Product MJK
                            {
                                //gvUniqueID = gv.UniqueID; // uniquekey ID gridview to save benefitchilds grid
                                //SaveClaimDetailChildSession(); // save benefitchilds before

                                //List<BenefitDetail> _benefitDetails = (List<BenefitDetail>)Session[string.Format("sessBenefitChild{0}", _claimDetails[x].BenefitId)];
                                List<BenefitDetail> _benefitDetails = _benefitChildSession[_claimDetails[x].BenefitId];
                                for (int iGridChild = 0; iGridChild < _benefitDetails.Count; iGridChild++)
                                {
                                    if (_benefitDetails[iGridChild].IsSelect == 1)
                                    {
                                        _AmountClaim = _AmountClaim + _benefitDetails[iGridChild].BenefitChildAmount;
                                        _totalAmountClaim = _AmountClaim;
                                        _benefitValueChild = _benefitValueChild + _benefitDetails[iGridChild].Amount;

                                    }
                                }
                                if (_totalAmountClaim == 0)
                                {
                                    _totalAmountClaim = claim.Amount; //string.IsNullOrEmpty(txtBenefitValueClaim.Text) ? 0 : Convert.ToDecimal(txtBenefitValueClaim.Text);
                                    _AmountClaim = _totalAmountClaim;
                                }

                            }
                            else
                            {
                                _AmountClaim = claim.Amount; //Convert.ToDecimal(txtBenefitValueClaim.Text) == 0 ? Convert.ToDecimal(lblBenefit.Text) : Convert.ToDecimal(txtBenefitValueClaim.Text); //string.IsNullOrEmpty(txtBenefitValueClaim.Text) ? 0 : Convert.ToDecimal(txtBenefitValueClaim.Text);
                                                             //_AmountClaim = BenefitValueClaim(_productId, _AmountClaim, lumsum, _claimDetails[x].QuantityClaim, Convert.ToDecimal(lblBenefit.Text), Convert.ToDecimal(txtBenefitValueClaim.Text));
                                if (_AmountClaim != 0)
                                {
                                    _totalAmountClaim = _claimDetails[x].QuantityClaim * _AmountClaim;//string.IsNullOrEmpty(txtTotalClaim.Text) ? 0 : (float)Convert.ToDecimal(txtTotalClaim.Text);
                                }
                                else
                                {
                                    _totalAmountClaim = _claimDetails[x].QuantityClaim * claim.Amount; //Convert.ToDecimal(lblBenefit.Text);
                                }
                            }
                            #endregion

                            _claimDetails[x].AmountClaim = _AmountClaim;
                            _claimDetails[x].Amount = _totalAmountClaim;


                            //_claimDetails[x].Remark = txtRemark.Text;

                            if (_claimDetails[x].AmountBenefit == 0)
                            {
                                //TextBox txtItem = (TextBox)rgridClaimDetailList.Rows[y].FindControl("txtItem");
                                _claimDetails[x].Description = _benefitDetailsSource[y].Description; //txtItem.Text;
                            }

                            #region Region : Validation Lum Sump Atas
                            // perhitungan lump sump ada 2, di atas dan di bawah
                            foreach (BenefitDetail _benefitDetail in _benefitDetailsSource)
                            {
                                if (m_claim.RegularDays == null) m_claim.RegularDays = 0;
                                if (m_claim.ICUDays == null) m_claim.ICUDays = 0;

                                if (_claimDetails[x].BenefitId == 4 && _productId.Contains("MHS"))
                                {
                                    if (
                                            (_claimDetails[x].BenefitId == _benefitDetail.BenefitId)
                                            && (_benefitDetail.LumpSum)
                                            && ((m_claim.RegularDays + m_claim.ICUDays) >= _benefitDetail.MinQuantity)
                                        )
                                    {
                                        //_claimDetails[x].QuantityPaid = _benefitDetail.MinQuantity;// _claimDetails[x].QuantityClaim;
                                        _claimDetails[x].QuantityPaid = (m_claim.RegularDays + m_claim.ICUDays);
                                        _claimDetails[x].AmountApproved = _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].AmountPaid = _claimDetails[x].AmountBenefit;

                                        _claimDetails[x].AmountClaim = _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].Amount = _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].QuantityClaim = (m_claim.RegularDays + m_claim.ICUDays);
                                        //_claimDetails[x].QuantityClaim = _claimDetails[x].QuantityPaid;// request by bu vidya 20091202
                                        break;
                                    }
                                }
                                else
                                {
                                    if (
                                            (_claimDetails[x].BenefitId == _benefitDetail.BenefitId)
                                            && (_benefitDetail.LumpSum)
                                            && (_claimDetails[x].QuantityClaim >= _benefitDetail.MinQuantity)
                                        )
                                    {
                                        _claimDetails[x].QuantityPaid = _benefitDetail.MinQuantity;// _claimDetails[x].QuantityClaim;
                                        _claimDetails[x].AmountApproved = _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].AmountPaid = _claimDetails[x].AmountBenefit;

                                        _claimDetails[x].AmountClaim = _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].Amount = _claimDetails[x].AmountBenefit;

                                        _claimDetails[x].QuantityClaim = _claimDetails[x].QuantityPaid;// request by bu vidya 20091202
                                        break;
                                    }
                                    else if (
                                                 (_claimDetails[x].BenefitId == _benefitDetail.BenefitId)
                                                    && (_benefitDetail.LumpSum)
                                                    && (_claimDetails[x].QuantityClaim < _benefitDetail.MinQuantity)
                                            )
                                    {
                                        _claimDetails[x].QuantityPaid = 0;
                                        _claimDetails[x].AmountApproved = 0;
                                        _claimDetails[x].AmountPaid = 0;
                                    }
                                    else if (_productId.Contains("MHP") || _productId.Contains("MPPT") || _productId.Contains("MPT") || _productId.Contains("AMMS"))
                                    {
                                        //_claimDetails[x].QuantityClaim = 1;
                                        //_claimDetails[x].QuantityPaid = _claimDetails[x].QuantityClaim;
                                        _claimDetails[x].AmountApproved = _claimDetails[x].QuantityClaim * _claimDetails[x].AmountBenefit; //_claimDetails[x].AmountBenefit;
                                        _claimDetails[x].AmountPaid = _claimDetails[x].QuantityClaim * _claimDetails[x].AmountBenefit; //_claimDetails[x].AmountBenefit;
                                        _claimDetails[x].AmountClaim = _claimDetails[x].QuantityClaim * _claimDetails[x].AmountBenefit; //_claimDetails[x].AmountBenefit;
                                        _claimDetails[x].Amount = _claimDetails[x].QuantityClaim * _claimDetails[x].AmountBenefit;

                                    }
                                }
                            }
                            #endregion


                            if (_AmountClaim != 0 || _claimDetails[x].QuantityClaim != 0)
                                //_claimDetails[x].AmountApproved = _claimDetails[x].RemainDays  == 0 && _claimDetails[x].RemainBenefitAmount ==0 ? 0 : _claimDetails[x].AmountBenefit;  //modify by yd 10-03-2011 _claimDetails[x].AmountApproved =  _claimDetails[x].AmountBenefit;  if the remaindays is zero
                                _claimDetails[x].AmountApproved = _benefitValueChild != 0 ? _AmountClaim : _claimDetails[x].RemainDays <= 0 && _claimDetails[x].RemainBenefitAmount <= 0 ? 0 : _claimDetails[x].AmountBenefit; //modify by yd 30-12-2011
                            else
                                _claimDetails[x].AmountApproved = 0;

                            if (_claimDetails[x].QuantityClaim == 0)
                            {
                                if (_benefitValueChild != 0)
                                {

                                }
                                else
                                {
                                    _claimDetails[x].AmountClaim = 0;
                                    _claimDetails[x].AmountApproved = 0;
                                    _claimDetails[x].AmountPaid = 0;
                                    _claimDetails[x].AmountNotPaid = 0;
                                }
                            }

                            if (_claimDetails[x].QuantityClaim > _claimDetails[x].RemainDays)
                                _claimDetails[x].QuantityPaid = _claimDetails[x].RemainDays;
                            else
                                _claimDetails[x].QuantityPaid = _claimDetails[x].QuantityClaim;

                            #region Region : Validation Product MJK
                            if (_productId.Contains("MJK") || _productId.Contains("MPKS") || _productId.Contains("MSK"))
                            {
                                if (_claimDetails[x].BenefitId == 4)  //Transportation Allowance
                                {
                                    if (m_claim.TypeId == "HEA" || !m_claim.DateOfDeath.HasValue)  //if (!m_claim.DateOfDeath.HasValue) // In patient only
                                    {
                                        _claimDetails[x].AmountClaim = _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].AmountApproved = _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].AmountPaid = _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].Amount = _claimDetails[x].AmountBenefit;
                                    }
                                    else
                                    {
                                        _claimDetails[x].AmountClaim = 0;
                                    }
                                }
                                else if (_claimDetails[x].BenefitId == 5 && m_claim.DateOfDeath.HasValue) // death benefit
                                {
                                    _claimDetails[x].AmountClaim = _claimDetails[x].AmountBenefit;
                                    _claimDetails[x].AmountApproved = _claimDetails[x].AmountBenefit;
                                    _claimDetails[x].AmountPaid = _claimDetails[x].AmountBenefit;
                                    _claimDetails[x].Amount = _claimDetails[x].AmountBenefit;
                                }

                                if (_claimDetails[x].BenefitId == 2 && _claimDetails[x].RemainDays > 0)  //Daily ICU Hospitalization 
                                {
                                    if (x == 1 || _claimDetails[x].BenefitId == 2)
                                    {
                                        if (_claimDetails[x].QuantityClaim > (_claimDetails[x - 1].RemainDays - _claimDetails[x - 1].QuantityClaim)) //if QuantityClaimDailyICU > (remaindaysDailyHospitalization - QuantityClaimDailyHospitalization) 
                                            _claimDetails[x].QuantityPaid = (_claimDetails[x - 1].RemainDays - _claimDetails[x - 1].QuantityClaim);

                                        if (_claimDetails[x].QuantityClaim > 0)
                                        {
                                            _claimDetails[x].AmountClaim = _claimDetails[x].AmountBenefit;
                                            _claimDetails[x].Amount = _claimDetails[x].AmountBenefit * _claimDetails[x].QuantityPaid;
                                            _claimDetails[x].AmountApproved = _claimDetails[x].AmountBenefit;
                                        }

                                    }
                                }

                                if (_claimDetails[x].BenefitId == 1 && _claimDetails[x].RemainDays > 0)  //Daily ICU Hospitalization 
                                {
                                    if (_claimDetails[x].BenefitId == 1)
                                    {
                                        _claimDetails[x].AmountClaim = _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].Amount = _claimDetails[x].AmountBenefit * _claimDetails[x].QuantityPaid;
                                        _claimDetails[x].AmountApproved = _claimDetails[x].AmountBenefit;
                                    }
                                }

                            }

                            #endregion

                            #region Region : Validation Lum Sump Bawah
                            // perhitungan lump sump ada 2, di atas dan di bawah
                            foreach (BenefitDetail _benefitDetail in _benefitDetailsSource)//sessBenefits)
                            {
                                if (m_claim.RegularDays == null) m_claim.RegularDays = 0;
                                if (m_claim.ICUDays == null) m_claim.ICUDays = 0;

                                if (_claimDetails[x].BenefitId == 4 && _productId.Contains("MHS"))
                                {
                                    if (
                                            (_claimDetails[x].BenefitId == _benefitDetail.BenefitId)
                                            && (_benefitDetail.LumpSum)
                                            && ((m_claim.RegularDays + m_claim.ICUDays) >= _benefitDetail.MinQuantity)
                                        )
                                    {
                                        //_claimDetails[x].QuantityPaid = _benefitDetail.MinQuantity;// _claimDetails[x].QuantityClaim;
                                        _claimDetails[x].QuantityPaid = (m_claim.RegularDays + m_claim.ICUDays);
                                        _claimDetails[x].AmountApproved = _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].AmountPaid = _claimDetails[x].AmountBenefit;

                                        _claimDetails[x].AmountClaim = _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].Amount = _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].QuantityClaim = (m_claim.RegularDays + m_claim.ICUDays);
                                        //_claimDetails[x].QuantityClaim = _claimDetails[x].QuantityPaid;// request by bu vidya 20091202
                                        break;
                                    }
                                }
                                else
                                {
                                    if (
                                            (_claimDetails[x].BenefitId == _benefitDetail.BenefitId)
                                            && (_benefitDetail.LumpSum)
                                            && (_claimDetails[x].QuantityClaim >= _benefitDetail.MinQuantity)
                                        )
                                    {
                                        _claimDetails[x].QuantityPaid = _benefitDetail.MinQuantity;// _claimDetails[x].QuantityClaim;
                                        _claimDetails[x].AmountApproved = _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].AmountPaid = _claimDetails[x].AmountBenefit;

                                        _claimDetails[x].AmountClaim = _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].Amount = _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].QuantityClaim = _claimDetails[x].QuantityPaid;// request by bu vidya 20091202
                                        break;
                                    }
                                    else if (
                                                 (_claimDetails[x].BenefitId == _benefitDetail.BenefitId)
                                                    && (_benefitDetail.LumpSum)
                                                    && (_claimDetails[x].QuantityClaim < _benefitDetail.MinQuantity)
                                            )
                                    {
                                        _claimDetails[x].QuantityPaid = 0;
                                        _claimDetails[x].AmountApproved = 0;
                                        _claimDetails[x].AmountPaid = 0;
                                    }
                                    else if (_productId.Contains("MHP") || _productId.Contains("MPPT") || _productId.Contains("MPT") || _productId.Contains("AMMS"))
                                    {
                                        //_claimDetails[x].QuantityClaim = 1; 
                                        _claimDetails[x].QuantityPaid = _claimDetails[x].QuantityClaim;
                                        _claimDetails[x].AmountApproved = _claimDetails[x].QuantityClaim * _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].AmountPaid = _claimDetails[x].QuantityPaid * _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].AmountClaim = _claimDetails[x].QuantityPaid * _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].Amount = _claimDetails[x].QuantityClaim * _claimDetails[x].AmountBenefit;
                                    }
                                    else
                                    {
                                        _claimDetails[x].AmountPaid = _claimDetails[x].QuantityPaid * _claimDetails[x].AmountApproved;

                                        //_claimDetails[x].QuantityPaid = _claimDetails[x].QuantityClaim;
                                        //_claimDetails[x].AmountApproved = _claimDetails[x].QuantityClaim * _claimDetails[x].AmountBenefit;
                                        //_claimDetails[x].AmountPaid = _claimDetails[x].QuantityPaid * _claimDetails[x].AmountBenefit;
                                        //_claimDetails[x].AmountClaim = _claimDetails[x].QuantityPaid * _claimDetails[x].AmountBenefit;
                                        //_claimDetails[x].Amount = _claimDetails[x].QuantityClaim * _claimDetails[x].AmountBenefit;
                                    }
                                }
                            }
                            #endregion

                            if (_benefitChildSession.Count > 0) // if claim detail is parent benefit, then passing validation/calculation qty and amount claim
                            {

                                if (_benefitValueChild > 100) // if benefitValueChild is greater than 100 %, then AmountApproved is AmountBenefit
                                {
                                    _claimDetails[x].AmountApproved = _claimDetails[x].AmountBenefit;
                                    _claimDetails[x].AmountPaid = _claimDetails[x].AmountBenefit;
                                }
                                else
                                {
                                    decimal _resultPersentase = _benefitValueChild * _claimDetails[x].AmountBenefit == 0 ? 0 : _benefitValueChild * _claimDetails[x].AmountBenefit / 100;

                                    if (_resultPersentase > _claimDetails[x].RemainBenefitAmount)
                                    {
                                        _claimDetails[x].AmountApproved = _claimDetails[x].RemainBenefitAmount;
                                        _claimDetails[x].AmountPaid = _claimDetails[x].RemainBenefitAmount;

                                        _claimDetails[x].AmountClaim = _resultPersentase;
                                        _claimDetails[x].Amount = _resultPersentase;
                                        _claimDetails[x].AmountNotPaid = _resultPersentase - _claimDetails[x].RemainBenefitAmount;
                                    }
                                    else
                                    {
                                        _claimDetails[x].AmountApproved = _resultPersentase;
                                        _claimDetails[x].AmountPaid = _resultPersentase;

                                        _claimDetails[x].AmountClaim = _resultPersentase;
                                        _claimDetails[x].Amount = _resultPersentase;
                                        _claimDetails[x].AmountNotPaid = 0;
                                    }
                                }

                            }
                            else
                            {
                                #region Region : Validation Product MJK & MHS
                                if (_productId.Contains("MJK") || _productId.Contains("MPKS") || _productId.Contains("MSK"))
                                {
                                    if (_claimDetails[x].BenefitId != 4)  //Transportation Allowance
                                    {
                                        _claimDetails[x].AmountPaid = _claimDetails[x].QuantityPaid * _claimDetails[x].AmountApproved;
                                    }
                                }
                                #endregion
                            }

                            if (_productId.Contains("MJK")
                                    || _productId.Contains("MPKS")
                                    || _productId.Contains("MSK")
                                    || _productId.Contains("MKE")
                                )
                            {
                                if (_claimDetails[x].BenefitId == 4)  //Transportation Allowance
                                {
                                    if (!m_claim.DateOfDeath.HasValue) // Inpatient only
                                    {
                                        _claimDetails[x].AmountClaim = _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].AmountApproved = _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].AmountPaid = _claimDetails[x].AmountBenefit;
                                        _claimDetails[x].Amount = _claimDetails[x].AmountBenefit;
                                    }
                                    else
                                    {
                                        _claimDetails[x].AmountClaim = 0;
                                    }
                                }
                                else if (_claimDetails[x].BenefitId == 5 && m_claim.DateOfDeath.HasValue) // death benefit
                                {
                                    _claimDetails[x].AmountClaim = _claimDetails[x].AmountBenefit;
                                    _claimDetails[x].AmountApproved = _claimDetails[x].AmountBenefit;
                                    _claimDetails[x].AmountPaid = _claimDetails[x].AmountBenefit;
                                    _claimDetails[x].Amount = _claimDetails[x].AmountBenefit;
                                }

                                if ((_claimDetails[x].AmountPaid - _claimDetails[x].Amount) <= 0)
                                {
                                    if (_claimDetails[x].BenefitId == 3)
                                    {
                                        GetRemainBenefit((long)Convert.ToDouble(_insuredId), _claimId, _claimDetails[x].BenefitDetailId);


                                        //Response.Write(_claimDetails[x].AmountPaid.ToString() + "<BR>");
                                        //Response.Write(_claimDetails[x].Amount.ToString());

                                        if (_remainBenefitAmount <= _claimDetails[x].Amount)
                                        {
                                            _claimDetails[x].AmountApproved = _claimDetails[x].Amount; //_claimDetails[x].AmountBenefit;
                                            _claimDetails[x].AmountPaid = _claimDetails[x].Amount; //_claimDetails[x].AmountPaid;
                                            _claimDetails[x].AmountNotPaid = _claimDetails[x].AmountApproved - _claimDetails[x].AmountPaid;

                                            _claimDetails[x].AmountBenefit = _claimDetails[x].AmountBenefit;
                                            _claimDetails[x].RemainDays = _claimDetails[x].QuantityPaid - _remainBenefitQty;
                                            _claimDetails[x].RemainBenefitAmount = _claimDetails[x].AmountBenefit - _remainBenefitAmount;
                                        }
                                        else
                                        {
                                            _claimDetails[x].AmountApproved = _claimDetails[x].Amount;
                                            _claimDetails[x].AmountPaid = _claimDetails[x].Amount;
                                            _claimDetails[x].AmountNotPaid = 0;

                                            _claimDetails[x].AmountBenefit = _claimDetails[x].AmountBenefit;
                                            _claimDetails[x].RemainDays = _claimDetails[x].QuantityPaid - _remainBenefitQty;
                                            _claimDetails[x].RemainBenefitAmount = _claimDetails[x].AmountBenefit - _remainBenefitAmount;
                                        }
                                    }
                                }
                            }

                            if (_productId.Contains("MKE"))
                            {
                                if (_claimDetails[x].RemainBenefitAmount > 0 &&
                                    _claimDetails[x].RemainBenefitAmount >= _claimDetails[x].AmountClaim)
                                {
                                    if ((_claimDetails[x].AmountPaid - _claimDetails[x].Amount) <= 0)
                                    {
                                        if (_claimDetails[x].BenefitId == 3) // Medical Expense
                                        {
                                            _claimDetails[x].AmountClaim = claim.Amount; //string.IsNullOrEmpty(txtBenefitValueClaim.Text) ? 0 : Convert.ToDecimal(txtBenefitValueClaim.Text);

                                            _claimDetails[x].AmountApproved = _claimDetails[x].AmountClaim;
                                            _claimDetails[x].AmountPaid = _claimDetails[x].AmountClaim;
                                            _claimDetails[x].AmountNotPaid = 0;
                                            _claimDetails[x].Amount = _claimDetails[x].AmountClaim;
                                            _claimDetails[x].AmountBenefit = _claimDetails[x].AmountBenefit;
                                        }
                                    }
                                }
                                else
                                {
                                    //ShowMessage(string.Format("Claim amount can not be greater than actual remain amount: {0}", _claimDetails[x].RemainBenefitAmount.ToString("N")));

                                    if (_claimDetails[x].AmountBenefit - _claimDetails[x].RemainBenefitAmount <= 0)
                                    {
                                        _claimDetails[x].AmountClaim = 0;
                                        _claimDetails[x].AmountApproved = 0;
                                        _claimDetails[x].AmountPaid = 0;
                                        _claimDetails[x].AmountNotPaid = 0;
                                        _claimDetails[x].Amount = 0;
                                    }
                                    else
                                    {
                                        _claimDetails[x].AmountClaim = _claimDetails[x].RemainBenefitAmount;
                                        _claimDetails[x].AmountApproved = _claimDetails[x].RemainBenefitAmount;
                                        _claimDetails[x].AmountPaid = _claimDetails[x].RemainBenefitAmount;
                                        _claimDetails[x].AmountNotPaid = 0;
                                        _claimDetails[x].Amount = _claimDetails[x].RemainBenefitAmount;
                                    }
                                }
                            }
                        }

                        #region Region : Validation Product MHL
                        if (_productId.Contains("MHL"))
                        {
                            if (_claimDetails[x].BenefitId == 3)  //Daily ICU Hospitalization 
                            {
                                if (_claimDetails[x].RemainDays > 0)
                                {
                                    if (_claimDetails[x - 1].QuantityClaim > 0)
                                    {
                                        if (_claimDetails[x - 1].RemainDays > (_claimDetails[x - 1].QuantityClaim + _claimDetails[x].QuantityClaim))
                                            _claimDetails[x - 1].QuantityPaid = _claimDetails[x - 1].QuantityClaim + _claimDetails[x].QuantityClaim;
                                        else
                                        {
                                            _claimDetails[x - 1].QuantityPaid = _claimDetails[x - 1].RemainDays;
                                            if (_claimDetails[x - 1].QuantityClaim == _claimDetails[x - 1].RemainDays)
                                                _claimDetails[x - 1].AmountPaid = _claimDetails[x - 1].AmountBenefit * (_claimDetails[x - 1].QuantityClaim - _claimDetails[x].QuantityClaim);
                                            else
                                                _claimDetails[x - 1].AmountPaid = _claimDetails[x - 1].AmountBenefit * (_claimDetails[x - 1].RemainDays - _claimDetails[x].QuantityClaim);
                                        }
                                    }
                                    if (_claimDetails[x - 2].QuantityClaim > 0)
                                    {
                                        if (_claimDetails[x - 2].RemainDays > (_claimDetails[x - 2].QuantityClaim + _claimDetails[x].QuantityClaim))
                                            _claimDetails[x - 2].QuantityPaid = _claimDetails[x - 2].QuantityClaim + _claimDetails[x].QuantityClaim;
                                        else
                                        {
                                            _claimDetails[x - 2].QuantityPaid = _claimDetails[x - 2].RemainDays;
                                            if (_claimDetails[x - 2].QuantityClaim == _claimDetails[x - 2].RemainDays)
                                                _claimDetails[x - 2].AmountPaid = _claimDetails[x - 2].AmountBenefit * (_claimDetails[x - 2].QuantityClaim - _claimDetails[x].QuantityClaim);
                                            else
                                                _claimDetails[x - 2].AmountPaid = _claimDetails[x - 2].AmountBenefit * (_claimDetails[x - 2].RemainDays - _claimDetails[x].QuantityClaim);
                                        }
                                    }
                                }
                                if (_claimDetails[x - 1].QuantityPaid > _claimDetails[x - 2].QuantityPaid)//if paid bene2 > bene1 then paid bene1=bene2
                                {
                                    if (_claimDetails[x - 2].RemainDays > _claimDetails[x - 1].QuantityPaid)
                                        _claimDetails[x - 2].QuantityPaid = _claimDetails[x - 1].QuantityPaid;
                                    else
                                        _claimDetails[x - 2].QuantityPaid = _claimDetails[x - 2].RemainDays;

                                    //if bene2 remain <=30 then bene3-bene2
                                    if ((_claimDetails[x - 1].RemainDays - _claimDetails[x - 1].QuantityClaim) <= 30 && _claimDetails[x - 1].QuantityClaim > 0)
                                    {
                                        int _remain3 = (_claimDetails[x - 1].RemainDays - _claimDetails[x - 1].QuantityClaim);

                                        if ((_claimDetails[x].RemainDays - _claimDetails[x].QuantityClaim) >= _remain3)
                                            _claimDetails[x].QuantityPaid = _claimDetails[x].RemainDays - (_claimDetails[x - 1].RemainDays - _claimDetails[x - 1].QuantityPaid);//_claimDetails[x].RemainDays - _remain3;
                                        else
                                        {
                                            if ((_claimDetails[x].RemainDays - _claimDetails[x].QuantityClaim) >= _claimDetails[x].RemainDays - (_claimDetails[x - 1].RemainDays - _claimDetails[x - 1].QuantityPaid))
                                                _claimDetails[x].QuantityPaid = _claimDetails[x].RemainDays - (_claimDetails[x - 1].RemainDays - _claimDetails[x - 1].QuantityPaid);
                                            else
                                            {
                                                _claimDetails[x].QuantityPaid = _claimDetails[x].QuantityClaim;
                                                _claimDetails[x].AmountPaid = _claimDetails[x].AmountBenefit * _claimDetails[x].QuantityClaim;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (_claimDetails[x - 1].RemainDays > _claimDetails[x - 2].QuantityPaid)
                                        _claimDetails[x - 1].QuantityPaid = _claimDetails[x - 2].QuantityPaid;
                                    else
                                        _claimDetails[x - 1].QuantityPaid = _claimDetails[x - 1].RemainDays;

                                    //Response.Write(30-(_claimDetails[x - 2].RemainDays - _claimDetails[x - 2].QuantityClaim));
                                    // if bene1 remain <=30 then bene3-bene1
                                    if ((_claimDetails[x - 2].RemainDays - _claimDetails[x - 2].QuantityClaim) <= 30 && _claimDetails[x - 2].QuantityClaim > 0)
                                    {
                                        int _remain3 = (_claimDetails[x - 2].RemainDays - _claimDetails[x - 2].QuantityClaim);

                                        if ((_claimDetails[x].RemainDays - _claimDetails[x].QuantityClaim) >= _remain3)
                                            _claimDetails[x].QuantityPaid = _claimDetails[x].RemainDays - (_claimDetails[x - 2].RemainDays - _claimDetails[x - 2].QuantityPaid);
                                        else
                                        {
                                            if ((_claimDetails[x].RemainDays - _claimDetails[x].QuantityClaim) >= _claimDetails[x].RemainDays - (_claimDetails[x - 2].RemainDays - _claimDetails[x - 2].QuantityPaid))
                                                _claimDetails[x].QuantityPaid = _claimDetails[x].RemainDays - (_claimDetails[x - 2].RemainDays - _claimDetails[x - 2].QuantityPaid);
                                            else
                                            {
                                                _claimDetails[x].QuantityPaid = _claimDetails[x].QuantityClaim;
                                                _claimDetails[x].AmountPaid = _claimDetails[x].AmountBenefit * _claimDetails[x].QuantityClaim;
                                            }
                                        }
                                    }

                                }
                            }
                            if (_claimDetails[x].RemainDays <= 0)
                            {
                                _claimDetails[x].AmountPaid = 0;
                                _claimDetails[x].QuantityPaid = 0;
                                _claimDetails[x].AmountApproved = 0;
                            }
                        }

                        #endregion

                        #region Calculate New Product
                        if (isCalculate)
                        {
                            /*
                            Claim claim = _claimService.GetClaim(_claimId);

                            _AmountFromBank = string.IsNullOrEmpty(txtAmountFromBank.Text) ? 0 : Convert.ToDecimal(txtAmountFromBank.Text);
                            _AmountClaim = Convert.ToDecimal(txtBenefitValueClaim.Text) == 0 ? _AmountClaim : Convert.ToDecimal(txtBenefitValueClaim.Text); //string.IsNullOrEmpty(txtBenefitValueClaim.Text) ? 0 : Convert.ToDecimal(txtBenefitValueClaim.Text);//string.IsNullOrEmpty(txtBenefitValueClaim.Text) ? 0 : Convert.ToDecimal(txtBenefitValueClaim.Text);
                            if (claim.TypeId == "TD")
                            {
                                decimal lastBalance = 0;
                                decimal calBalance = 0;

                                if (!string.IsNullOrEmpty(txtLastBalance.Text))
                                {
                                    lastBalance = decimal.Parse(txtLastBalance.Text, CultureInfo.InvariantCulture);
                                    calBalance = lastBalance * (decimal)0.1;
                                    if (calBalance > 100000)
                                    {
                                        _AmountClaim = calBalance;
                                    }
                                    else
                                    {
                                        _AmountClaim = 100000;
                                    }

                                    _claimDetails[x].AmountClaim = _AmountClaim;
                                    _claimDetails[x].Amount = _AmountClaim;
                                    _claimDetails[x].AmountApproved = _AmountClaim;
                                    _claimDetails[x].AmountPaid = _AmountClaim;
                                }
                            }
                            if (_productId.ToUpper().Contains("MANPRO") && claim.TypeId == "TPD")
                            {
                                _claimDetails[x].AmountFromBank = _AmountFromBank;
                                _claimDetails[x].AmountClaim = _AmountClaim;
                                _claimDetails[x].Amount = _AmountClaim;
                                _claimDetails[x].AmountApproved = _AmountClaim;
                                _claimDetails[x].AmountPaid = _AmountClaim;
                            }
                            if (_productId.ToUpper().Contains("MANPRO") && claim.TypeId == "MAJ")
                            {
                                if (_claimDetails[x].BenefitId == 1) //Amount to BM
                                {
                                    _claimDetails[x].AmountClaim = _AmountClaim;
                                    _claimDetails[x].AmountFromBank = _AmountFromBank;
                                    _claimDetails[x].AmountApproved = _AmountClaim;
                                    _claimDetails[x].AmountPaid = _AmountClaim;
                                    _claimDetails[x].Amount = _AmountClaim;
                                }
                                if (_claimDetails[x].BenefitId == 2) //Amount to Beneficiary
                                {
                                    _claimDetails[x].AmountClaim = _claimDetails[x - 1].AmountClaim * 2;
                                    _claimDetails[x].AmountFromBank = _claimDetails[x - 1].AmountFromBank;
                                    _claimDetails[x].AmountApproved = _claimDetails[x - 1].AmountClaim * 2;
                                    _claimDetails[x].AmountPaid = _claimDetails[x - 1].AmountClaim * 2;
                                    _claimDetails[x].Amount = _claimDetails[x - 1].AmountClaim * 2;
                                }
                            } */
                            /*
                            if (claim.Policy.Product.CategoryId == "CP")
                            {
                                decimal _principalDebt = string.IsNullOrEmpty(txtPrincipalDebt.Text) ? 0 : Convert.ToDecimal(txtPrincipalDebt.Text);
                                decimal _interestCredit = string.IsNullOrEmpty(txtInterestCredit.Text) ? 0 : Convert.ToDecimal(txtInterestCredit.Text);

                                decimal _total = _principalDebt + _interestCredit;

                                if (_claimDetails[x].AmountBenefit > 0 &&
                                   _claimDetails[x].AmountBenefit >= _total)
                                {
                                    _claimDetails[x].AmountClaim = _total;
                                    _claimDetails[x].Amount = _total;
                                    _claimDetails[x].AmountApproved = _total;
                                    _claimDetails[x].AmountPaid = _total;
                                    _claimDetails[x].PrincipalDebt = _principalDebt;
                                    _claimDetails[x].InterestOfCredit = _interestCredit;
                                }
                                else
                                {
                                    _claimDetails[x].AmountClaim = 0;
                                    _claimDetails[x].Amount = 0;
                                    _claimDetails[x].AmountApproved = 0;
                                    _claimDetails[x].AmountPaid = 0;
                                    _claimDetails[x].PrincipalDebt = 0;
                                    _claimDetails[x].InterestOfCredit = 0;
                                    //txtPrincipalDebt.Text = "0";
                                    //txtInterestCredit.Text = "0";
                                    //ShowMessage(string.Format("Claim amount can not be greater than amount benefit: {0}", _claimDetails[x].AmountBenefit.ToString("N")));

                                }

                            } */
                            /*
                            if (claim.Policy.Product.IsPaymentMethodClaim)
                            {
                                if (!string.IsNullOrEmpty(claim.PaymentOfClaim) && claim.PaymentOfClaim.ToUpper() == "L")
                                {
                                    //function lumpsum
                                    decimal amountLumpsum = (decimal)LumpsumAmount(claim);
                                    _claimDetails[x].AmountClaim = amountLumpsum;
                                    _claimDetails[x].Amount = amountLumpsum;
                                    _claimDetails[x].AmountApproved = amountLumpsum;
                                    _claimDetails[x].AmountPaid = amountLumpsum;
                                }
                                else if (!string.IsNullOrEmpty(claim.PaymentOfClaim) && claim.PaymentOfClaim.ToUpper() == "M")
                                {
                                    //function monthly coverage
                                    decimal amountMonthlyCoverage = MonthlyCoverageAmount(claim);
                                    _claimDetails[x].AmountClaim = amountMonthlyCoverage;
                                    _claimDetails[x].Amount = amountMonthlyCoverage;
                                    _claimDetails[x].AmountApproved = amountMonthlyCoverage;
                                    _claimDetails[x].AmountPaid = amountMonthlyCoverage;
                                }
                                else
                                {
                                    ShowMessage("Choose Payment of Claim Lumpsum or Monthly Coverage");
                                }

                                if (m_claim.Policy.Product.IsPaymentMethodClaim && m_claim.PaymentOfClaim == "L")
                                {
                                    txtAmountBeforeSBI.Text = string.Format("{0:n2}", _amountBeforeSBI);
                                    txtSBIRate.Text = string.Format("{0:n2}", _sbiRate);
                                }
                            } */
                        }
                        /*
                        else // add by doni 2021-12-08 claim amount and amount from BM could be edited and total them after click save button
                        {
                            _AmountFromBank = string.IsNullOrEmpty(txtAmountFromBank.Text) ? 0 : Convert.ToDecimal(txtAmountFromBank.Text);
                            _AmountClaim = string.IsNullOrEmpty(txtBenefitValueClaim.Text) ? 0 : Convert.ToDecimal(txtBenefitValueClaim.Text);
                            Claim claim = _claimService.GetClaim(_claimId);
                            if (_productId.ToUpper().Contains("MANPRO") && claim.TypeId == "MAJ")
                            {
                                _claimDetails[x].AmountClaim = _AmountClaim;
                                _claimDetails[x].AmountFromBank = _AmountFromBank;
                                _claimDetails[x].AmountApproved = _AmountClaim;
                                _claimDetails[x].AmountPaid = _AmountClaim;
                                _claimDetails[x].Amount = _AmountClaim;
                            }
                        }
                        */
                        #endregion

                        _totalAmountPaid = _totalAmountPaid + _claimDetails[x].AmountPaid;

                    }

                }
            }
            return _claimDetails;
        }
        protected double LumpsumAmount(Claim claim)
        {
            //Suku SBI
            //double sukuSBI = 0.07;
            List<Criteria> _criteriasLoan = new List<Criteria>();
            _criteriasLoan.Add(new Criteria("c.ProductId", claim.Policy.ProductId, Comparison.Equal));
            List<ProductLoan> _loans = _productLoanService.FindProductLoan(_criteriasLoan, 0, 1, "c.ProductId Desc");
            _criteriasLoan = null;

            _sbiRate = _loans.Count <= 0 ? 0 : _loans[0].LoanPercentage;

            //Coverage Period
            int coveragePeriod = claim.Policy.CoveragePeriod;
            int policyAge = CalculatePolicyAge(claim);
            double MB = 0;
            decimal averageMonthlyCoverage = getMonthlyCoverage(claim.Policy);

            MB = (double)averageMonthlyCoverage;

            //formula v1
            double i = Math.Pow(1.0 + 0, 1.0 / 12) - 1.0;
            double v = 1.0 / (1.0 + i);
            double D = i * v;
            double v12nt = Math.Pow(v, coveragePeriod - policyAge);
            double result = (1.0 - v12nt) / D * MB;

            //formula v1
            double i_2 = Math.Pow(1.0 + _sbiRate, 1.0 / 12) - 1.0;
            double v_2 = 1.0 / (1.0 + i_2);
            double D_2 = i_2 * v_2;
            double v12nt_2 = Math.Pow(v_2, coveragePeriod - policyAge);
            double result_2 = (1.0 - v12nt_2) / D_2 * MB;

            _amountBeforeSBI = result;
            return result_2;
        }
        private int CalculatePolicyAge(Claim c)
        {
            if (string.IsNullOrEmpty(c.AdmissionDate.ToString())
               && string.IsNullOrEmpty(c.DateOfDeath.ToString())
               )
                return 0;

            DateTime td;
            int months = 0;
            int years = 0;
            int days = 0;

            if (string.IsNullOrEmpty(c.AdmissionDate.ToString()))
            {
                td = c.DateOfDeath.Value;
            }
            else
            {
                td = c.AdmissionDate.Value;
            }

            if (c.Insured.CoverageUntil.HasValue && td > c.Insured.CoverageUntil.Value)
            {
                td = c.Insured.CoverageUntil.Value;
            }

            DateTime effdate = c.Policy.EffectiveDate.Value;

            days = td.Day - effdate.Day;
            if (days < 0)
            {
                td = td.AddMonths(-1);
                days += DateTime.DaysInMonth(td.Year, td.Month);
            }

            months = td.Month - effdate.Month;
            if (months < 0)
            {
                td = td.AddYears(-1);
                months += 12;
            }
            years = td.Year - effdate.Year;

            months = months + (years * 12);

            return months;
        }
        protected decimal MonthlyCoverageAmount(Claim claim)
        {
            decimal result = 0;
            decimal averageMonthlyCoverage = getMonthlyCoverage(claim.Policy);
            DateTime dateBefore = DateTime.Now;
            DateTime dateNow = DateTime.Now;

            if (string.IsNullOrEmpty(claim.AdmissionDate.ToString()) && string.IsNullOrEmpty(claim.DateOfDeath.ToString()))
                return 0;

            int months = 0;
            if (string.IsNullOrEmpty(claim.AdmissionDate.ToString()))
            {
                dateBefore = claim.DateOfDeath.Value;
            }
            else
            {
                dateBefore = claim.AdmissionDate.Value;
            }

            if (claim.Insured.CoverageUntil.HasValue && dateNow > claim.Insured.CoverageUntil.Value)
            {
                dateNow = claim.Insured.CoverageUntil.Value;
            }

            months = ((dateNow.Year - dateBefore.Year) * 12) + dateNow.Month - dateBefore.Month;

            if (dateBefore.Day <= claim.Policy.EffectiveDate.Value.Day)
            {
                months += 1;
            }

            result = months * averageMonthlyCoverage;

            return result;
        }
        protected decimal getMonthlyCoverage(Policy _policy)
        {
            decimal sumAssured = 0;
            decimal totalMonthlyBenefit = 0;
            int count = 6;
            List<Premium> premiums = _policy.Insureds[0].Plan.Premiums;

            if (_policy != null)
            {
                List<Criteria> _criterias = new List<Criteria>();

                _criterias.Add(new Criteria("c.PolicyId", _policy.PolicyId, Comparison.Equal));
                _criterias.Add(new Criteria("c.StatusId", "PAI", Comparison.Equal));
                _criterias.Add(new Criteria("c.TypeId", "PRM", Comparison.Equal));
                List<Payment> _pays = _paymentService.FindPayments(_criterias, 0, count, " c.PaymentDate Desc");

                if (_pays.Count > 0)
                {
                    foreach (Payment pay in _pays)
                    {
                        totalMonthlyBenefit += pay.MonthlyBenefit;
                    }

                    if (totalMonthlyBenefit > 0)
                    {
                        sumAssured = totalMonthlyBenefit / _pays.Count;
                    }

                    if (sumAssured > 0 && sumAssured > premiums[0].Amount)
                    {
                        sumAssured = premiums[0].Amount;
                    }
                }

            }

            return sumAssured;
        }
        private void SaveClaimDetail()
        {
            List<ClaimDetail> _claimDetailDB = new List<ClaimDetail>();
            List<ClaimDetail> _claimDetailSessions = new List<ClaimDetail>();
            _claimDetailSessions = SaveClaimDetailSession(true); //modify yd 20090831, pass calculation
            //_claimDetailSessions = (List<ClaimDetail>)sessClaimDetails;// CleansingBeforeSave(); // clean data before save
            _claimDetailDB = _claimDetailService.GetClaimDetails(_claimId);

            for (int db = 0; db < _claimDetailDB.Count; db++)
            {
                bool bIsFound = false;
                for (int ses = 0; ses < _claimDetailSessions.Count; ses++)
                {
                    if (_claimDetailDB[db].BenefitDetailId == _claimDetailSessions[ses].BenefitDetailId)
                    {
                        if (CheckDifferent(_claimDetailDB[db], _claimDetailSessions[ses])) // if record in table is different, then update record from session
                            _claimDetailService.UpdateClaimDetail(_claimDetailSessions[ses]);
                        bIsFound = true;
                        break;
                    }
                }
                if (!bIsFound) // if record not found in session, then delete record in table
                    _claimDetailService.DeleteClaimDetail(_claimDetailDB[db]);
            }

            for (int ses = 0; ses < _claimDetailSessions.Count; ses++)
            {
                bool bIsFound = false;
                for (int db = 0; db < _claimDetailDB.Count; db++)
                {
                    if (_claimDetailDB[db].BenefitDetailId == _claimDetailSessions[ses].BenefitDetailId)
                    {
                        bIsFound = true;
                        break;
                    }
                }
                if (string.IsNullOrEmpty(_claimDetailSessions[ses].Description)) //if record is empty, then not save in table
                    bIsFound = true;
                if (!bIsFound)
                    _claimDetailService.AddClaimDetail(_claimDetailSessions[ses]);
            }

            #region History
            Claim _claim = new Claim();
            _claim = _claimService.GetClaim(_claimId);
            ClaimHistory _claimHistory = new ClaimHistory();
            PolicyHistory _policyHistory = new PolicyHistory();
            if (_claimDetailDB.Count > 0)
            {
                _claimHistory.Description = string.Format("Modify Claim Detail");
                _claimHistory.Remark = string.Format("Claim Detail Update# ClaimNo : {0}", _claimId);
                _policyHistory.Description = string.Format("Claim Detail Update# ClaimNo : {0}", _claimId);
            }
            else
            {
                _claimHistory.Description = string.Format("Add Claim Detail");
                _claimHistory.Remark = string.Format("Claim Detail Added# ClaimNo : {0}", _claimId);
                _policyHistory.Description = string.Format("Claim Detail Added# ClaimNo : {0}", _claimId);
            }

            _claimHistory.ClaimNo = _claimId;
            _claimHistory.CreateBy = executorName;
            _claimHistory.EventDate = DateTime.Now;
            _claimHistory.EventId = "HC";
            _claimHistory.StatusId = _claim.StatusId;
            //_claimHistory.Description = string.Format("Total amount {0}", hidTotalPaid.Value);

            _claimHistoryService.AddClaimHistory(_claimHistory);

            _policyHistory.PolicyId = _claim.PolicyId;
            _policyHistory.CreateBy = executorName;
            _policyHistory.EventDate = DateTime.Now;
            _policyHistory.EventId = "HC";
            _policyHistory.StatusId = _claim.Policy.StatusId;
            //_policyHistory.Remark = string.Format("Total amount {0}", hidTotalPaid.Value);
            _policyHistoryService.AddPolicyHistory(_policyHistory);

            #endregion

            //sdf 16102015
            double selisihHari;

            if (_claim.Policy.NCBDueDate.HasValue && _claim.AdmissionDate.HasValue)
            {
                if (_claim.SubmitDate.Value.Date > _claim.Policy.NCBDueDate.Value.Date)
                {
                    if (_claim.AdmissionDate.Value.Date < _claim.Policy.NCBDueDate.Value.Date)
                    {

                        LogHelper.LogInfo("NCB already Due in " + ((DateTime)_claim.Policy.NCBDueDate).ToString("dd/MM/yyyy") + " Save Success", _pluginMetadata);
                        //ShowMessage("NCB already Due in " + ((DateTime)_claim.Policy.NCBDueDate).ToString("dd/MM/yyyy") + "<br> Save Success");
                    }
                    else
                    {
                        LogHelper.LogInfo("Success", _pluginMetadata);
                        //ShowMessage("Success");
                    }
                }
            }
            else if (_claim.Policy.NCBDueDate.HasValue && _claim.DateOfDeath.HasValue)
            {
                if (_claim.SubmitDate.Value.Date > _claim.Policy.NCBDueDate.Value.Date)
                {
                    if (_claim.DateOfDeath.Value.Date < _claim.Policy.NCBDueDate.Value.Date)
                    {
                        LogHelper.LogInfo("NCB already Due in " + ((DateTime)_claim.Policy.NCBDueDate).ToString("dd/MM/yyyy") + " Save Success", _pluginMetadata);
                        // ShowMessage("NCB already Due in " + ((DateTime)_claim.Policy.NCBDueDate).ToString("dd/MM/yyyy") + "<br> Save Success");
                    }
                    else
                    {
                        LogHelper.LogInfo("Success", _pluginMetadata);
                        // ShowMessage("Success");
                    }
                }
            }
            else
            {
                LogHelper.LogInfo("Success", _pluginMetadata);
                // ShowMessage("Success");
            }
        }
        private void UpdateTotalPaidNotPaid()
        {
            Claim _claim = new Claim();
            _claim = _claimService.GetClaim(_claimId);

            if (_claim == null) return;

            List<ClaimDetail> _claimDetails = _claimDetailService.GetClaimDetails(_claimId); // find claimdetail in database ;
            _totalPaid = 0;
            _totalNotPaid = 0;
            _totalClaim = 0;
            for (int i = 0; i < _claimDetails.Count; i++)
            {
                _totalClaim = _totalClaim + _claimDetails[i].Amount;
                _totalPaid = _totalPaid + _claimDetails[i].AmountPaid;
                _totalNotPaid = _totalNotPaid + _claimDetails[i].AmountNotPaid;
            }

            _claim.Amount = _totalClaim;
            _claim.AmountPaid = _totalPaid;
            _claim.AmountNotPaid = _totalNotPaid;
            _claim.ModifyBy = executorName;
            _claim.ModifyDate = DateTime.Now;
            // Page.ClientScript.RegisterStartupScript(GetType(), "PaidTotal", string.Format("<script>HeaderTotalPaid({0},{1},{2})</script>", _totalPaid == 0 ? "0" : _totalPaid.ToString("#"), _totalNotPaid == 0 ? "0" : _totalNotPaid.ToString("#"), _totalClaim == 0 ? "0" : _totalClaim.ToString("#")));//{0},{1},{2}=>paid, notpaid, claimAmount
            _claimService.UpdateClaim(_claim);
            _claim = null;
            _totalPaid = 0;
            _totalNotPaid = 0;
            _totalClaim = 0;

        }
        private Dictionary<int, List<BenefitDetail>> SaveClaimDetailChildSession()
        {
            List<ClaimDetail> _claimDetailSessions = SaveClaimDetailSession(false);
            List<BenefitDetail> _benefitDetails = new List<BenefitDetail>();
            Dictionary<int, List<BenefitDetail>> _allBenefitDetails = new Dictionary<int, List<BenefitDetail>>();
            for (int i = 0; i < _claimDetailSessions.Count; i++)
            {
                int _benefitDetailId = _claimDetailSessions[i].BenefitDetailId;
                int _benefitId = _claimDetailSessions[i].BenefitId;
                //GridView gv = (GridView)rgridClaimDetailList.Rows[i].FindControl("rgridBenefitChildList");
                //if (gv.UniqueID == gvUniqueID)
                //{
                //    if (gv.Rows.Count == 0) return;
                //    int _iBenefitParentId = Convert.ToInt32(rgridClaimDetailList.DataKeys[i]["BenefitId"]);
                //List<BenefitDetail> _benefitDetails = (List<BenefitDetail>)Session[string.Format("sessBenefitChild{0}", _iBenefitParentId)];
                _benefitDetails = GetBenefitChilds(_benefitId);// (List<BenefitDetail>)ViewState[string.Format("sessBenefitChild{0}", _iBenefitParentId)];
                                                               //for (int x = 0; x < _benefitDetails.Count; x++)
                                                               //{
                                                               //    //for (int y = 0; y < gv.Rows.Count; y++)
                                                               //    //{
                                                               //    if (_benefitDetails[x].BenefitId == _benefitId)//Convert.ToInt32(gv.DataKeys[y]["BenefitId"]))
                                                               //    {
                                                               //        //TextBox txtBenefitChildAmount = (TextBox)gv.Rows[y].FindControl("txtBenefitChildAmount");
                                                               //        //CheckBox chkBenefitChildSelect = (CheckBox)gv.Rows[y].FindControl("chkBenefitChildSelect");

                //        _benefitDetails[x].BenefitChildAmount = string.IsNullOrEmpty(_claimDetailSessions[i]) ? 0 : Convert.ToDecimal(txtBenefitChildAmount.Text); // temporary store 
                //        _benefitDetails[x].IsSelect = chkBenefitChildSelect.Checked ? 1 : 0;

                //        break;
                //    }
                //    //}
                //}
                //ViewState[string.Format("sessBenefitChild{0}", _iBenefitParentId)] = null;
                //ViewState[string.Format("sessBenefitChild{0}", _iBenefitParentId)] = _benefitDetails;

                /*

                Session[string.Format("sessBenefitChild{0}", _iBenefitParentId)] = null;
                Session[string.Format("sessBenefitChild{0}", _iBenefitParentId)] = _benefitDetails;
                 */

                //}
                _allBenefitDetails.Add(_benefitId, _benefitDetails);
            }
            return _allBenefitDetails;
        }
        private List<BenefitDetail> GetBenefitChilds(int benefitId)
        {
            List<BenefitDetail> _benefitDetails = new List<BenefitDetail>();
            List<Criteria> _criterias;
            List<ClaimDetailChild> _claimDetailChilds = _claimDetailChildService.GetAllClaimDetailChilds(_claimId);
            bool bIsFound = false;
            foreach (BenefitChild _benefitChild in _benefitChildService.GetAllBenefitChilds(_productId, benefitId))
            {
                _criterias = new List<Criteria>();
                _criterias.Add(new Criteria("ProductId", _productId, Comparison.Equal));
                _criterias.Add(new Criteria("PlanId", _planId, Comparison.Equal));
                _criterias.Add(new Criteria("BenefitId", _benefitChild.ChildId, Comparison.Equal));

                foreach (BenefitDetail oBenefitDetail in _benefitDetailService.FindBenefitDetails(_criterias, 0, 100, ""))
                {
                    bIsFound = false;
                    foreach (ClaimDetailChild oClaimDetailChild in _claimDetailChilds)
                    {
                        if (oBenefitDetail.BenefitId == oClaimDetailChild.ChildId)
                        {
                            oBenefitDetail.BenefitChildAmount = oClaimDetailChild.Amount;
                            oBenefitDetail.IsSelect = 1;
                            _benefitDetails.Add(oBenefitDetail);
                            bIsFound = true;
                            break;
                        }
                    }
                    if (!bIsFound)
                        _benefitDetails.Add(oBenefitDetail);
                }
            }
            return _benefitDetails;
        }
        private void SaveClaimDetailChild() // add by yd 20090901
        {
            //try
            //{
            List<BenefitDetail> _claimDetailSessions = new List<BenefitDetail>();
            List<ClaimDetailChild> _claimDetailDB = new List<ClaimDetailChild>();
            ClaimDetailChild _claimDetailChildSess;
            List<ClaimDetail> _claimDetails = new List<ClaimDetail>();
            _claimDetails = _claimDetailService.GetClaimDetails(_claimId);

            for (int i = 0; i < _claimDetails.Count; i++)
            {
                Dictionary<int, List<BenefitDetail>> _benefitChildSession = SaveClaimDetailChildSession();
                //GridView gv = (GridView)rgridClaimDetailList.Rows[i].FindControl("rgridBenefitChildList");
                int _benefitId = _claimDetails[i].BenefitId; //Convert.ToInt32(rgridClaimDetailList.DataKeys[i]["BenefitId"]);

                //if (Session[string.Format("sessBenefitChild{0}", _benefitId)] != null)
                if (_benefitChildSession.Count > 0)
                    if (_benefitChildSession.ContainsKey(_benefitId))
                    {
                        _claimDetailSessions = new List<BenefitDetail>();
                        //_claimDetailSessions = (List<BenefitDetail>)Session[string.Format("sessBenefitChild{0}", _benefitId)];
                        _claimDetailSessions = _benefitChildSession[_benefitId];

                        _claimDetailDB = new List<ClaimDetailChild>();
                        _claimDetailDB = _claimDetailChildService.GetAllClaimDetailChilds(_claimId);

                        #region Region : Proccess Synchronizing
                        for (int db = 0; db < _claimDetailDB.Count; db++)
                        {
                            bool bIsFound = false;
                            for (int ses = 0; ses < _claimDetailSessions.Count; ses++)
                            {
                                if (_claimDetailDB[db].ChildId == _claimDetailSessions[ses].BenefitId)
                                {
                                    if (_claimDetailSessions[ses].IsSelect == 1)
                                    {
                                        if (CheckDifferentClaimDetailChild(_claimDetailDB[db], _claimDetailSessions[ses])) // if record in table is different, then update record from session
                                        {
                                            _claimDetailChildSess = new ClaimDetailChild();
                                            _claimDetailChildSess.ClaimNo = _claimId;
                                            _claimDetailChildSess.ChildId = _claimDetailSessions[ses].BenefitId;
                                            _claimDetailChildSess.Amount = _claimDetailSessions[ses].BenefitChildAmount;
                                            _claimDetailChildService.UpdateClaimDetailChild(_claimDetailChildSess);
                                            _claimDetailChildSess = null;
                                        }
                                        bIsFound = true;
                                        break;
                                    }
                                }
                            }
                            if (!bIsFound) // if record not found in session, then delete record in table
                                _claimDetailChildService.DeleteClaimDetailChild(_claimDetailDB[db].ClaimNo, _claimDetailDB[db].ChildId);
                        }

                        for (int ses = 0; ses < _claimDetailSessions.Count; ses++)
                        {
                            bool bIsFound = false;
                            for (int db = 0; db < _claimDetailDB.Count; db++)
                            {
                                if (_claimDetailDB[db].ChildId == _claimDetailSessions[ses].BenefitId)
                                {
                                    bIsFound = true;
                                    break;
                                }
                            }
                            if (_claimDetailSessions[ses].IsSelect == 0) //if record is empty, then not save in table
                                bIsFound = true;
                            if (!bIsFound)
                            {
                                _claimDetailChildSess = new ClaimDetailChild();
                                _claimDetailChildSess.ClaimNo = _claimId;
                                _claimDetailChildSess.ChildId = _claimDetailSessions[ses].BenefitId;
                                _claimDetailChildSess.Amount = _claimDetailSessions[ses].BenefitChildAmount;
                                _claimDetailChildService.AddClaimDetailChild(_claimDetailChildSess);
                                _claimDetailChildSess = null;
                            }
                        }
                        #endregion
                    }
            }
        }
        private bool CheckDifferent(ClaimDetail claimDetail1, ClaimDetail claimDetail2)
        {
            bool bIsFound = false;


            if (
                claimDetail1.Description != claimDetail2.Description ||
                claimDetail1.Quantity != claimDetail2.Quantity ||
                claimDetail1.QuantityPaid == claimDetail2.QuantityPaid ||
                claimDetail1.Amount != claimDetail2.Amount ||
                claimDetail1.AmountPaid != claimDetail2.AmountPaid ||
                claimDetail1.AmountNotPaid != claimDetail2.AmountNotPaid ||
                claimDetail1.Remark != claimDetail2.Remark ||
                claimDetail1.QuantityClaim != claimDetail2.QuantityClaim ||
                claimDetail1.AmountClaim != claimDetail2.AmountClaim ||
                claimDetail1.AmountApproved != claimDetail2.AmountApproved ||
                claimDetail1.AmountFromBank != claimDetail2.AmountFromBank ||
                claimDetail1.PrincipalDebt != claimDetail2.PrincipalDebt ||
                claimDetail1.InterestOfCredit != claimDetail2.InterestOfCredit
                )
                bIsFound = true;

            return bIsFound;
        }
        private bool CheckDifferentClaimDetailChild(ClaimDetailChild claimDetail1, BenefitDetail claimDetail2)
        {
            bool bIsFound = false;

            if (
                claimDetail1.Amount != claimDetail2.BenefitChildAmount

                )
                bIsFound = true;

            return bIsFound;
        }
        private List<BenefitDetail> GetInsuredInformationToGetBenefitDetail()
        {
            List<BenefitDetail> _benefitDetails = new List<BenefitDetail>();
            Insured _insured = _insuredService.GetInsured((long)Convert.ToDouble(_insuredId));
            if (_insured == null) return _benefitDetails;
            _planId = _insured.PlanId;

            //add by doni 2021-04-21 - begin

            List<Benefit> _benefits = new List<Benefit>();
            List<Criteria> _criteriasBenefits = new List<Criteria>();
            _criteriasBenefits.Add(new Criteria("ProductId", _insured.ProductId, Comparison.Equal));
            if (_insured.ProductId.Contains("MPA"))
            {
                _criteriasBenefits.Add(new Criteria("Query", " (BenefitGroup = '" + m_claim.TypeId + "' or BenefitGroup = NULL)", Comparison.None));
            }
            else
            {
                _criteriasBenefits.Add(new Criteria("Query", " (IsChild IS NULL or IsChild <> 1) and (BenefitGroup = '" + m_claim.TypeId + "' or BenefitGroup = NULL)", Comparison.None));
            }

            _benefits = _benefitService.FindBenefits(_criteriasBenefits, 0, int.MaxValue, " Sequence");

            //add by doni 2021-04-21 - end


            //List<Benefit> _benefits = _benefitService.GetAllBenefits(_insured.ProductId); //remark by doni 2021-04-21

            if (_benefits.Count == 0) return _benefitDetails;
            List<Criteria> _criterias;

            if (m_claim.TypeId == "CA") // type cancer 18-08-2011
                _planId = _oldPlanId == null ? _insured.PlanId : _oldPlanId; // validation for MPKC, check policy is change plan period, old plan or current plan

            for (int iBenefit = 0; iBenefit < _benefits.Count; iBenefit++)
            {

                //Response.Write("BenefitID: " + _benefits[iBenefit].BenefitId.ToString());

                _criterias = new List<Criteria>();
                _criterias.Add(new Criteria("ProductId", _insured.ProductId, Comparison.Equal));
                //_criterias.Add(new Criteria("PlanId", _insured.PlanId, Comparison.Equal));
                _criterias.Add(new Criteria("PlanId", _planId, Comparison.Equal));
                _criterias.Add(new Criteria("BenefitId", _benefits[iBenefit].BenefitId, Comparison.Equal));

                if (_benefits[iBenefit].LevelId == "I")
                    _criterias.Add(new Criteria("InsuredTypeId", _insured.TypeId, Comparison.Equal));

                if (_benefits[iBenefit].CriteriaDescription != null)
                {
                    if (_benefits[iBenefit].CriteriaDescription.Contains("Sex"))
                        _criterias.Add(new Criteria("SexId", _insured.SexId, Comparison.Equal));

                    if (_benefits[iBenefit].CriteriaDescription.Contains("Age"))
                    {
                        //v1
                        //_criterias.Add(new Criteria("Query", string.Format("{0} between AGEBEGIN AND AGEEND", _insured.Age), Comparison.None));

                        //v2 - Claim: Insured age based on Effective Date
                        int insuredAgeBasedEffective = DateHelper.GetAge(_insured.BirthDate.Value, _insured.Policy.EffectiveDate.Value, _insured.Product.AgeLogicId);

                        _criterias.Add(new Criteria("Query", string.Format("{0} between AGEBEGIN AND AGEEND", insuredAgeBasedEffective), Comparison.None));
                    }

                    if (_benefits[iBenefit].CriteriaDescription.Contains("Risk"))
                    {
                        //_criterias.Add(new Criteria("SexId", _insured.SexId, Comparison.Equal));
                    }
                }

                foreach (BenefitDetail oBenefitDetail in _benefitDetailService.FindBenefitDetails(_criterias, 0, int.MaxValue, ""))
                {

                    #region validation product MSP
                    if (m_claim.DateOfDeath != null && (_productId.Contains("MSP") || _productId.Contains("MSL")))
                    {
                        //Response.Write("Hello 1");

                        if (m_claim.IsAccident) //Death due to Accident:
                        {
                            //Response.Write("Hello 2");

                            if (_benefits[iBenefit].BenefitId == 3)
                            {
                                //Response.Write("Hello 3");

                                oBenefitDetail.StdAmount = 2 * oBenefitDetail.StdAmount;
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                            }
                            else
                            {
                                if (_productId.Contains("MSP"))
                                {
                                    oBenefitDetail.StdAmount = 2 * oBenefitDetail.StdAmount;
                                    oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                }
                                else
                                {
                                    oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                                    oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                }

                            }
                        }
                        else //Death due to non-accidental causes:
                        {
                            if (
                                    (DiffFechaMonth() > 12) ||
                                    (DiffFechaMonth() == 12 && (m_claim.DateOfDeath.Value.Date > m_claim.Policy.EffectiveDate.Value.Date))
                                )
                            {
                                oBenefitDetail.StdAmount = oBenefitDetail.StdAmount; //•	Year 2 onwards: 100% SA
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                            }
                            else if (
                                        ((DiffFechaMonth() == 12 && (m_claim.DateOfDeath.Value.Date <= m_claim.Policy.EffectiveDate.Value.Date)) ||
                                        (DiffFechaMonth() < 12) && _benefits[iBenefit].BenefitId == 1 && _productId.Contains("MSP"))
                                    ||
                                        (
                                            ((DiffFechaMonth() == 12 && (m_claim.DateOfDeath.Value.Date <= m_claim.Policy.EffectiveDate.Value.Date)) ||
                                            (DiffFechaMonth() < 12) && _benefits[iBenefit].BenefitId < 3 && _productId.Contains("MSL"))
                                    )
                                    )
                            {
                                oBenefitDetail.StdAmount = GetReturnOfCollectionPremium(); //•	1st year: 100% return of collected premium (at the time of claim).
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                            }
                            else
                            {
                                oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                            }
                        }
                    }
                    else if (m_claim.DateOfDeath == null && (_productId.Contains("MSP") || _productId.Contains("MSL")))// == "TLROP" )
                        oBenefitDetail.StdAmount = 0;
                    #endregion

                    #region validation product MPK Mandiri proteksi Kanker
                    if (_productId.Contains("MPK"))
                    {
                        oBenefitDetail.StdAmount = oBenefitDetail.StdAmount - CheckPaymentNCB(_insured.Policy);
                    }
                    #endregion

                    #region validation product MHL*
                    if (m_claim.DateOfDeath != null && _productId.Contains("MHL"))
                    {
                        if (
                                (DiffFechaMonth() < 48) ||
                                (DiffFechaMonth() == 48 && (m_claim.DateOfDeath.Value.Date <= m_claim.Policy.EffectiveDate.Value.Date))
                            )    //•	Year 1-4 mendapatkan collection premium (benefit 4)
                        {
                            if (oBenefitDetail.BenefitId == 4)
                            {
                                oBenefitDetail.StdAmount = GetReturnOfCollectionPremium(); //•	1st-4th year: 100% return of collected premium (at the time of claim).
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                oBenefitDetail.StdQuantity = 1;
                            }
                            else if (oBenefitDetail.BenefitId == 5)
                            {
                                oBenefitDetail.StdAmount = 0;
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                oBenefitDetail.StdQuantity = 0;
                            }
                            else
                            {
                                oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                            }
                        }
                        else if (DiffFechaMonth() == 48 && (m_claim.DateOfDeath.Value.Date > m_claim.Policy.EffectiveDate.Value.Date))
                        {
                            if (oBenefitDetail.BenefitId == 4)
                            {
                                oBenefitDetail.StdAmount = 0;  // > 4 tahun
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                oBenefitDetail.StdQuantity = 0;
                            }
                            else
                            {
                                oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                            }
                        }
                        else
                        {
                            if (oBenefitDetail.BenefitId == 4)
                            {
                                oBenefitDetail.StdAmount = 0;  // > 4 tahun
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                oBenefitDetail.StdQuantity = 0;
                            }
                            else
                            {
                                oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                            }
                        }
                    }
                    else if (m_claim.DateOfDeath == null && _productId.Contains("MHL"))
                    {
                        oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                        oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                    }

                    #endregion

                    #region validation product AMMS*

                    if (m_claim.DateOfDeath != null &&
                            (_productId.Equals("AMMS1") ||
                                _productId.Equals("AMMS1N") ||
                                _productId.Equals("AMMS2") ||
                                _productId.Equals("AMMS3") ||
                                _productId.Equals("AMMS4") ||
                                _productId.Equals("AMMS5")
                            )
                        )
                    {
                        if (m_claim.IsAccident)
                        {
                            if (oBenefitDetail.BenefitId == 1) //accident: 1
                            {
                                oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                oBenefitDetail.StdQuantity = (oBenefitDetail.BenefitId == 1 ? oBenefitDetail.StdQuantity : 1);
                            }
                            else
                            {
                                oBenefitDetail.StdAmount = 0;
                                oBenefitDetail.Amount = 0;
                                oBenefitDetail.StdQuantity = 0;
                            }
                        }
                        else
                        {
                            if (oBenefitDetail.BenefitId == 1)
                            {
                                oBenefitDetail.StdAmount = 0;
                                oBenefitDetail.Amount = 0;
                                oBenefitDetail.StdQuantity = 0;
                            }
                            else
                            {
                                oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                oBenefitDetail.StdQuantity = 1;
                            }
                        }
                    }
                    #region "AMMS_AMMSA"
                    else if (m_claim.DateOfDeath != null && (_productId.Equals("AMMS") || _productId.Equals("AMMSA")))
                    {
                        if (m_claim.IsAccident)
                        {
                            if (oBenefitDetail.BenefitId == 3 || oBenefitDetail.BenefitId == 4 || oBenefitDetail.BenefitId == 5) //accident: 3 | 4 | 5
                            {
                                oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                oBenefitDetail.StdQuantity = (oBenefitDetail.BenefitId == 1 ? oBenefitDetail.StdQuantity : 1);
                            }
                            else if (oBenefitDetail.BenefitId == 1 || oBenefitDetail.BenefitId == 2 || oBenefitDetail.BenefitId == 6)
                            {
                                oBenefitDetail.StdAmount = 0;
                                oBenefitDetail.Amount = 0;
                                oBenefitDetail.StdQuantity = 0;
                            }
                        }
                        else
                        {
                            if (oBenefitDetail.BenefitId != 6) //accident: 3 | 4 | 5
                            {
                                oBenefitDetail.StdAmount = 0;
                                oBenefitDetail.Amount = 0;
                                oBenefitDetail.StdQuantity = 0;
                            }
                            else if (oBenefitDetail.BenefitId == 6)
                            {
                                oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                oBenefitDetail.StdQuantity = 1;
                            }
                        }
                    }
                    else if (m_claim.DateOfDeath == null && (_productId.Equals("AMMS") || _productId.Equals("AMMSA")))
                    {
                        if (m_claim.BenefitTypeId == "S")
                        {
                            if (oBenefitDetail.BenefitId == 1 || oBenefitDetail.BenefitId == 2) //sickness: 1 | 2
                            {
                                oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                oBenefitDetail.StdQuantity = (oBenefitDetail.BenefitId == 1 ? oBenefitDetail.StdQuantity : 1);
                            }
                            else
                            {
                                oBenefitDetail.StdAmount = 0;
                                oBenefitDetail.Amount = 0;
                                oBenefitDetail.StdQuantity = 0;
                            }
                        }
                    }
                    #endregion
                    #region "AMMS30"
                    else if (m_claim.DateOfDeath != null && (_productId.Equals("AMMS30")))
                    {
                        if (m_claim.IsAccident)
                        {
                            if (oBenefitDetail.BenefitId == 2 || oBenefitDetail.BenefitId == 3 || oBenefitDetail.BenefitId == 4) //death accident: 2 | 3; tpd: 4
                            {
                                oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                oBenefitDetail.StdQuantity = (oBenefitDetail.BenefitId == 1 ? oBenefitDetail.StdQuantity : 1);
                            }
                            else
                            {
                                oBenefitDetail.StdAmount = 0;
                                oBenefitDetail.Amount = 0;
                                oBenefitDetail.StdQuantity = 0;
                            }
                        }
                        else
                        {
                            if (oBenefitDetail.BenefitId != 5) //accident: 2 | 3 | 4
                            {
                                oBenefitDetail.StdAmount = 0;
                                oBenefitDetail.Amount = 0;
                                oBenefitDetail.StdQuantity = 0;
                            }
                            else if (oBenefitDetail.BenefitId == 5)
                            {
                                oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                oBenefitDetail.StdQuantity = 1;
                            }
                        }
                    }
                    else if (m_claim.DateOfDeath == null && (_productId.Equals("AMMS30")))
                    {
                        if (m_claim.BenefitTypeId == "S")
                        {
                            if (oBenefitDetail.BenefitId == 1) //sickness: 1
                            {
                                oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                oBenefitDetail.StdQuantity = (oBenefitDetail.BenefitId == 1 ? oBenefitDetail.StdQuantity : 1);
                            }
                            else
                            {
                                oBenefitDetail.StdAmount = 0;
                                oBenefitDetail.Amount = 0;
                                oBenefitDetail.StdQuantity = 0;
                            }
                        }
                        else if (m_claim.BenefitTypeId == "TPD")
                        {
                            if (m_claim.IsAccident)
                            {
                                if (oBenefitDetail.BenefitId == 4) // TPD
                                {
                                    oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                                    oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                    oBenefitDetail.StdQuantity = 1;
                                }
                                else
                                {
                                    oBenefitDetail.StdAmount = 0;
                                    oBenefitDetail.Amount = 0;
                                    oBenefitDetail.StdQuantity = 0;
                                }
                            }
                        }
                    }
                    #endregion
                    else if (m_claim.DateOfDeath == null &&
                            (_productId.Equals("AMMS1") ||
                                _productId.Equals("AMMS1N") ||
                                _productId.Equals("AMMS2") ||
                                _productId.Equals("AMMS3") ||
                                _productId.Equals("AMMS4") ||
                                _productId.Equals("AMMS5")
                            ))
                    {
                        oBenefitDetail.StdAmount = 0;
                        oBenefitDetail.Amount = 0;
                        oBenefitDetail.StdQuantity = 0;
                    }
                    #endregion

                    #region validation product MKE*

                    if (m_claim.DateOfDeath != null &&
                        (_productId.Contains("MKE"))
                        )
                    {
                        if (m_claim.IsAccident)
                        {
                            if (oBenefitDetail.BenefitId == 1 && m_claim.TypeId != "TPD") // death benefit
                            {
                                oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                oBenefitDetail.StdQuantity = 1;
                            }
                            else if (oBenefitDetail.BenefitId == 2 && m_claim.TypeId == "TPD") // TPD
                            {
                                oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                oBenefitDetail.StdQuantity = 1;
                            }
                            else
                            {
                                oBenefitDetail.StdAmount = 0;
                                oBenefitDetail.Amount = 0;
                                oBenefitDetail.StdQuantity = 0;
                            }
                        }
                        else
                        {
                            if (oBenefitDetail.BenefitId == 1)
                            {
                                oBenefitDetail.StdAmount = 0;
                                oBenefitDetail.Amount = 0;
                                oBenefitDetail.StdQuantity = 0;
                            }
                            else
                            {
                                oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                oBenefitDetail.StdQuantity = 1;
                            }
                        }
                    }
                    else if (m_claim.DateOfDeath == null &&
                        (_productId.Contains("MKE")) &&
                        m_claim.TypeId == "TPD")
                    {
                        if (m_claim.IsAccident)
                        {
                            if (oBenefitDetail.BenefitId == 2) // TPD
                            {
                                oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                                oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                                oBenefitDetail.StdQuantity = 1;
                            }
                            else
                            {
                                oBenefitDetail.StdAmount = 0;
                                oBenefitDetail.Amount = 0;
                                oBenefitDetail.StdQuantity = 0;
                            }
                        }
                    }
                    else if (m_claim.DateOfDeath == null &&
                        (_productId.Contains("MKE")) &&
                        m_claim.TypeId == "HEA")
                    {
                        if (oBenefitDetail.BenefitId == 3) // Medical Expense
                        {
                            oBenefitDetail.StdAmount = oBenefitDetail.StdAmount;
                            oBenefitDetail.Amount = oBenefitDetail.StdAmount;
                            oBenefitDetail.StdQuantity = 1;
                        }
                        else
                        {
                            oBenefitDetail.StdAmount = 0;
                            oBenefitDetail.Amount = 0;
                            oBenefitDetail.StdQuantity = 0;
                        }
                    }

                    #endregion

                    _benefitDetails.Add(oBenefitDetail);
                }
            }


            return _benefitDetails;

        }
        private decimal GetReturnOfCollectionPremium() //1 yr 100% return of collected premium. 2th 100% SA. 
        {
            List<Criteria> _criterias = new List<Criteria>();
            _criterias.Add(new Criteria("PolicyNo", m_claim.PolicyId, Comparison.Equal));
            _criterias.Add(new Criteria("StatusId", "P", Comparison.Equal));

            List<Billing> _billings = _billingService.FindBillings(_criterias, 0, int.MaxValue, string.Empty);
            decimal _payment = 0;
            for (int i = 0; i < _billings.Count; i++)
            {
                _payment += _billings[i].Amount;
            }
            _billings = null;
            return _payment;
        }
        private int DiffFechaMonth()
        {
            DateTime dt1 = m_claim.Policy.EffectiveDate.Value;
            DateTime dt2 = m_claim.DateOfDeath.Value;

            DateTime pseudoTimeSpan = new DateTime(Math.Abs(dt1.Ticks - dt2.Ticks));
            return ((pseudoTimeSpan.Year - 1) * 12) + (pseudoTimeSpan.Month - 1);

        }
        private decimal CheckPaymentNCB(Policy policy)
        {
            List<Criteria> _criterias = new List<Criteria>();
            _criterias.Add(new Criteria("TypeId", "NCB", Comparison.Equal));
            _criterias.Add(new Criteria("c.StatusId", "PAI", Comparison.Equal));
            _criterias.Add(new Criteria("c.PolicyId", policy.PolicyId, Comparison.Equal));

            List<Payment> _payments = _paymentService.FindPayments(_criterias, 0, int.MaxValue, "StatusDate desc");
            decimal _total = 0;
            for (int i = 0; i < _payments.Count; i++)
            {
                _total += _payments[i].Amount;
            }

            return _total;
        }
        private int GetRemainBenefit(long insuredId, string claimId, int benefirDetailId)
        {
            #region Region : add by yd 10-03-2011 tambahan validasi remaindays pertahun dari effectivedate

            Claim _claim = _claimService.GetClaim(claimId);
            DateTime _theDateStart = new DateTime();
            DateTime _theDateEnd = new DateTime();
            int i = 0;
            string _query = string.Empty;
            DateTime _treatment = _claim.AdmissionDate != null ? _claim.AdmissionDate.Value : _claim.CreateDate.Value; // tanggal masuk
            DateTime _discharge = _claim.DischargeDate != null ? _claim.DischargeDate.Value : _claim.CreateDate.Value; // tanggal keluar

            TimeSpan hospitalDays = _discharge - _treatment;

            for (i = 0; i <= int.MaxValue; i++)
            {
                if (_claim.Policy.EffectiveDate.Value.AddYears(i) >= _treatment)  //> _claim.CreateDate.Value) //comment by yd 30-12-2011
                {
                    _theDateStart = _claim.Policy.EffectiveDate.Value.AddYears(i - 1);
                    _theDateEnd = _claim.Policy.EffectiveDate.Value.AddYears(i);
                    break;
                }
            }

            if (_claim.DateOfDeath.HasValue && _claim.TypeId == "TPD")
                _query = string.Format(" c.DateOfDeath between '{0}' and '{1}' ", _theDateStart.ToString("yyyy-MM-dd 0:00:00"), _theDateEnd.ToString("yyyy-MM-dd 23:59:59"));
            else
                _query = string.Format(" c.AdmissionDate between '{0}' and '{1}' ", _theDateStart.ToString("yyyy-MM-dd 0:00:00"), _theDateEnd.ToString("yyyy-MM-dd 23:59:59"));  // modify c.CreateDate -> c.AdmissionDate by yd 30-12-2011

            #endregion Region : end add by yd 10-03-2011 tambahan validasi remaindays pertahun dari effectivedate

            List<Criteria> _criterias = new List<Criteria>();
            _criterias.Add(new Criteria("c.StatusId", "APP", Comparison.Equal));
            _criterias.Add(new Criteria("c.InsuredId", insuredId, Comparison.Equal));
            _criterias.Add(new Criteria("Query", _query, Comparison.None)); // add by yd 10-03-2011 tambahan validasi remaindays pertahun
            _claimsRemainDay = _claimService.FindClaims(_criterias, 0, 100000, "");
            _criterias = null;

            if (_claimsRemainDay.Count == 0) return 0;

            TimeSpan intervalDays = TimeSpan.Zero;
            int intervalDay = 0;

            int _totalRemainDays = 0;
            decimal _totalRemainBenefitAmount = 0;
            for (i = 0; i < _claimsRemainDay.Count; i++)
            {
                long _insuredId = (long)Convert.ToDouble(claimId);
                long _insuredIdDB = (long)Convert.ToDouble(_claimsRemainDay[i].ClaimNo);

                if (_insuredId >= _insuredIdDB)
                {
                    foreach (ClaimDetail _claimDetail in _claimsRemainDay[i].Details)
                    {
                        if (benefirDetailId == _claimDetail.BenefitDetailId)
                        {
                            if (_claimDetail.BenefitId == 1 && (_productId.Contains("MJK") || _productId.Contains("MSK")))
                            {
                                //cek hari yang terpakai dari tahun polis yg baru akibat klaim sebelumnya. 
                                //Hitung dari ulang tahun polis s/d tanggal keluar
                                if (_claimsRemainDay[i].StatusId != "APP")
                                {
                                    intervalDays = _claimsRemainDay[i].DischargeDate.Value - _theDateStart;
                                    intervalDay = intervalDays.Days - 1;
                                }
                                else
                                {
                                    intervalDay = 0;
                                }
                                _totalRemainDays = (_totalRemainDays + _claimDetail.QuantityPaid) - intervalDay;
                                _totalRemainBenefitAmount = _totalRemainBenefitAmount + _claimDetail.AmountPaid;
                            }
                            else
                            {
                                _totalRemainDays = (_totalRemainDays + _claimDetail.QuantityPaid);
                                _totalRemainBenefitAmount = _totalRemainBenefitAmount + _claimDetail.AmountPaid;

                                //Response.Write(_totalRemainBenefitAmount.ToString() + "<BR>");
                            }
                            break;
                        }

                    }
                }
            }

            _remainBenefitQty = _totalRemainDays;
            _remainBenefitAmount = _totalRemainBenefitAmount;

            return _totalRemainDays;
        }

    }


}
