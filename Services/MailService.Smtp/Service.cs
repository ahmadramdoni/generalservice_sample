﻿using GeneralService.Plugin;
using System;
using System.Collections.Generic;
using GeneralService.Core.Helper;
using System.Net.Mail;
using System.Net;

namespace MailService.Smtp
{
    public class Service : IEmailPlugin
    {
        private static PluginMetadata _metadata;
        private MailArgs _mailArgs;

        public void Send(PluginMetadata metadata, MailArgs mailArgs)
        {
            try
            {
                _metadata = metadata;

                _mailArgs = mailArgs;
                _mailArgs.MailDisplayName = !string.IsNullOrWhiteSpace(mailArgs.MailDisplayName) ? mailArgs.MailDisplayName : metadata.PluginSettings.GetValue<string>("MailDisplayName");

                Validate();

                SendMail();               
            }
            catch (Exception ex)
            {
                LogHelper.LogError($"Failed sending email : {ex.Message}, {ex.StackTrace}", ex ,metadata);
            }
        }

        private void Validate()
        {
            List<string> errorMessage = new List<string>();

            if (string.IsNullOrWhiteSpace(_mailArgs.Recipients))
            {
                errorMessage.Add("Destination mail is empty");
            }

            if (errorMessage.Count > 0)
            {
                throw new Exception(string.Join(", ", errorMessage));
            }
        }

        private void SendMail()
        {
            using (var smtp = new SmtpClient())
            {
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(_metadata.PluginSettings.GetValue<string>("EmailFrom"), _metadata.PluginSettings.GetValue<string>("MailPassword"));
                smtp.Port = _metadata.PluginSettings.GetValue<int>("SMTPPort");
                smtp.EnableSsl = _metadata.PluginSettings.GetValue<bool>("EnableSsl");
                smtp.Host = _metadata.PluginSettings.GetValue<string>("SMTPServer");

                var destinations = _mailArgs.Recipients.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                using (var mail = new MailMessage())
                {
                    mail.From = new MailAddress(_metadata.PluginSettings.GetValue<string>("EmailFrom"), _mailArgs.MailDisplayName);

                    foreach (var address in destinations)
                    {
                        mail.To.Add(address);
                    }

                    mail.Subject = _mailArgs.Subject;
                    mail.IsBodyHtml = _metadata.PluginSettings.GetValue<bool>("IsBodyHtml");
                    mail.Body = _mailArgs.Body;

                    if (_mailArgs.Attachments != null)
                    {
                        foreach (var att in _mailArgs.Attachments)
                        {
                            var attachment = new Attachment(att);
                            mail.Attachments.Add(attachment);
                        }
                    }
                    smtp.Send(mail);
                }
            }
        }
    }
}
